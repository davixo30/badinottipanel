<?php

use App\Models\Induction;
use App\Models\InductionWorker;
use App\Models\Worker;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class InductionsTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class InductionsWorkersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            $workers = json_decode(Storage::disk('local')->get('database/induction_workers.json'));

            foreach ($workers as $worker) {
                $dbWorker = Worker::select('id')->where('rut', $worker->rut)->first();

                foreach (get_object_vars($worker) as $item => $value) {


                    if ($item !== 'rut' && $value !== '') {
                        try {
                            // obtiene el nombre, si viene con '|algo' lo omite, ya que se usa para diferenciar
                            // inducciones repetidas en clientes distintos
                            $name = strpos($item, '|') === false ? $item : explode('|', $item)[0];

                            $dbInduction = Induction::select('id')->where('name', $name)->first();
                            $date = Carbon::createFromFormat('d-m-Y', $value)->toDateString();

                            InductionWorker::create([
                                'worker_id' => $dbWorker->id,
                                'induction_id' => $dbInduction->id,
                                'induction_date' => $date
                            ]);
                        } catch (\Exception $exception) {
                            throw $exception;
                        }
                    }
                }
            }
        });
    }
}
