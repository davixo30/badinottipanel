<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class InductionsTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class InductionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $inductions = json_decode(Storage::disk('local')->get('database/inductions.json'));

        foreach ($inductions as $induction) {
            DB::table('inductions')->where('name', $induction)->delete();
            DB::table('inductions')->insert(['name' => $induction]);
        }
    }
}
