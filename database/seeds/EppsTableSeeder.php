<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class EppsTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class EppsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $epps = json_decode(Storage::disk('local')->get('database/epps.json'));

        foreach ($epps as $epp) {
            DB::table('epps')->where('name', $epp)->delete();
            DB::table('epps')->insert([
                'name' => $epp
            ]);
        }
    }
}
