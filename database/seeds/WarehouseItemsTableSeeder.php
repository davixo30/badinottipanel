<?php

use App\Models\WarehouseItem;
use App\Models\WarehouseItemPrice;
use App\Models\WarehouseItemSize;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class WarehouseItemsTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class WarehouseItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = json_decode(Storage::disk('local')->get('database/warehouse_items.json'));

        DB::transaction(function () use ($items) {
            $classificationsCache = [];
            $measurementsCache = [];

            foreach ($items as $item) {
                // verifica si existe la clasificacion en el caché
                if (!array_key_exists($item->classification, $classificationsCache)) {
                    $classificationsCache[$item->classification] = DB::table('warehouse_item_classifications')
                        ->where('name', $item->classification)->select('id')->first()->id;
                }

                // verifica si existe la unidad de medida en el caché
                if (!array_key_exists($item->measurement_unit, $measurementsCache)) {
                    $measurementsCache[$item->measurement_unit] = DB::table('measurement_units')
                        ->where('name', $item->measurement_unit)->select('id')->first()->id;
                }

                // primero verifica si existe el item
                $dbItem = WarehouseItem::where('name', $item->name)->first();

                // si no existe debe crearlo
                if ($dbItem === null) {
                    $dbItem = new WarehouseItem([
                        'name' => $item->name,
                        'price' => $item->price,
                        'classification_id' => $classificationsCache[$item->classification],
                        'measurement_unit_id' => $measurementsCache[$item->measurement_unit]
                    ]);
                    $dbItem->save();
                    WarehouseItemPrice::create(['warehouse_item_id' => $dbItem->id, 'price' => $item->price]);
                }

                // guarda la talla
                $size = new WarehouseItemSize([
                    'quantity' => $item->quantity,
                    'warehouse_item_id' => $dbItem->id,
                    'name' => $item->size
                ]);

                $size->save();
            }
        });
    }
}
