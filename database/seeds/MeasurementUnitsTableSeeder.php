<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class MeasurementUnitsTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class MeasurementUnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = json_decode(Storage::disk('local')->get('database/measurement_units.json'));

        foreach ($units as $unit) {
            DB::table('measurement_units')->where('name', $unit)->delete();
            DB::table('measurement_units')->insert([
                'name' => $unit
            ]);
        }
    }
}
