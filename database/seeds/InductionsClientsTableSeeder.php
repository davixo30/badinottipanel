<?php

use App\Models\Induction;
use App\Models\InductionWorker;
use App\Models\Worker;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class InductionsClientsTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class InductionsClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            $inductions = json_decode(Storage::disk('local')->get('database/induction_clients.json'));

            foreach ($inductions as $induction => $clients) {
                $dbInduction = DB::table('inductions')->select('id')
                    ->where('name', $induction)->first();
                $inductionId = $dbInduction->id;

                foreach ($clients as $client) {
                    DB::table('client_inductions')->insert([
                        'induction_id' => $inductionId,
                        'client_id' => DB::table('clients')
                            ->select('id')
                            ->where('name', $client)
                            ->first()
                            ->id
                    ]);
                }
            }
        });
    }
}
