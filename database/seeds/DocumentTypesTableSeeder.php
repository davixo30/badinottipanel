<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class DocumentTypesTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class DocumentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            'Carnet de identidad',
            'Matrícula de embarque',
            'Reglamento interno',
            'Seguro de vida'
        ];

        foreach ($types as $type) {
            DB::table('document_types')->where('name', $type)->delete();
            DB::table('document_types')->insert([
                'name' => $type
            ]);
        }
    }
}
