<?php

use App\Models\Supplier;
use Illuminate\Database\Seeder;

/**
 * Class SuppliersTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Supplier::class, 10)->create();
    }
}
