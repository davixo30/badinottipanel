<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class UsersTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = json_decode(Storage::disk('local')->get('database/users.json'));

        DB::transaction(function () use ($users) {
            foreach ($users as $i => $user) {
                // obtiene la comuna a la que pertenece
                $commune = DB::table('communes')
                    ->where('name', 'like', $user->commune)->first();

                // obtiene estado civil
                $maritalStatus = DB::table('marital_statuses')
                    ->where('name', 'like', $user->marital_status)->first();

                $userId = DB::table('users')
                    ->insertGetId([
                        'name' => $user->name,
                        'last_name' => $user->father_last_name . ' ' . $user->mother_last_name,
                        'code' => $i,
                        'rut' => $user->rut,
                        'email' => $user->email,
                        'birthdate' => (new Carbon($user->birthdate))->format('Y-m-d'),
                        'phone_number' => $user->phone_number,
                        'address' => $user->address,
                        'commune_id' => $commune->id,
                        'marital_status_id' => $maritalStatus->id,
                        'created_at' => Carbon::now()->format('Y-m-d'),
                        'password' => bcrypt('123456'),
                        // 'type' => 'admin',
                        'position_id' => DB::table('positions')
                            ->where('name', $user->position)
                            ->select('id')
                            ->first()
                            ->id
                    ]);

                // inserta el contrato del usuario
                DB::table('user_contracts')
                    ->insert([
                        'date' => (new Carbon($user->contract_date))->format('Y-m-d'),
                        'user_id' => $userId,
                        'afp_id' => DB::table('afps')
                            ->where('name', 'like', $user->afp)->first()->id,
                        'health_forecast_id' => DB::table('health_forecasts')
                            ->where('name', $user->health_forecast)->first()->id,
                        'contract_type_id' => DB::table('contract_types')
                            ->where('name', strtolower($user->contract_type))->first()->id
                    ]);
            }
        });
    }
}
