<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class CriteriaTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CriteriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $criteria = json_decode(Storage::disk('local')->get('database/qualification_criteria.json'));

        foreach ($criteria as $criterion) {
            DB::table('criteria')
                ->where('name', $criterion->name)
                ->delete();

            DB::table('criteria')
                ->insert((array) $criterion);
        }
    }
}
