<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class RolesTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = json_decode(Storage::disk('local')->get('database/roles.json'));

        foreach ($roles as $role) {
            DB::table('roles')->where('name', $role)->delete();
            DB::table('roles')->insert([
                'name' => $role
            ]);
        }
    }
}
