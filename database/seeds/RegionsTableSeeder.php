<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class RegionsTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = json_decode(Storage::disk('local')->get('database/regions.json'));

        foreach ($regions as $region) {
            DB::table('regions')->where('name', $region->name)->delete();
            $regionId = DB::table('regions')->insertGetId([
                'name' => $region->name,
                'ordinal' => $region->ordinal
            ]);

            foreach ($region->communes as $commune) {
                DB::table('communes')->where('name', $commune->name)->delete();
                DB::table('communes')->insert([
                    'name' => $commune->name,
                    'region_id' => $regionId
                ]);
            }
        }
    }
}
