<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class WarehouseItemClassificationsTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class WarehouseItemClassificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $classifications = json_decode(Storage::disk('local')->get('database/item_classifications.json'));

        foreach ($classifications as $classification) {
            DB::table('warehouse_item_classifications')->where('name', $classification)->delete();
            DB::table('warehouse_item_classifications')->insert([
                'name' => $classification
            ]);
        }
    }
}
