<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class ShipTypesTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class ShipTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            'embarcación menor',
            'embarcación mayor'
        ];

        foreach ($types as $type) {
            DB::table('ship_types')->where('name', $type)->delete();
            DB::table('ship_types')->insert([
                'name' => $type
            ]);
        }
    }
}
