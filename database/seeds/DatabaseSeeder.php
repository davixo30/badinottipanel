<?php

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AttributesTableSeeder::class);
        $this->call(MaritalStatusesTableSeeder::class);
        $this->call(RegionsTableSeeder::class);
        $this->call(AfpsTableSeeder::class);
        $this->call(HealthForecastsTableSeeder::class);
        $this->call(PositionsTableSeeder::class);
        $this->call(ContractTypesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ShipTypesTableSeeder::class);
        $this->call(CertificatesTableSeeder::class);
        $this->call(ShipsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(CriteriaTableSeeder::class);
        $this->call(QualifiersTableSeeder::class);
        $this->call(ClientsTableSeeder::class);
        $this->call(WorkersTableSeeder::class);
        $this->call(InductionsTableSeeder::class);
        $this->call(InductionsClientsTableSeeder::class);
        // $this->call(InductionsWorkersTableSeeder::class);
        $this->call(CrewStatusesTableSeeder::class);
        // $this->call(EppsTableSeeder::class);
        // $this->call(EppDeliveriesTableSeeder::class);
        $this->call(HeadlinesTableSeeder::class);
        $this->call(BankAccountsTableSeeder::class);
        $this->call(FlightStretchesTableSeeder::class);
        $this->call(AirlinesTableSeeder::class);
        $this->call(AirportsTableSeeder::class);
        $this->call(LodgingProvidersTableSeeder::class);
        $this->call(RelocationProvidersTableSeeder::class);
        $this->call(DocumentTypesTableSeeder::class);

        // bodega
        $this->call(MeasurementUnitsTableSeeder::class);
        $this->call(SuppliersTableSeeder::class);
        $this->call(WarehouseItemClassificationsTableSeeder::class);
        $this->call(WarehouseItemsTableSeeder::class);

        //
        $this->call(TicketStatusesTableSeeder::class);
    }
}
