<?php

use App\Models\BankAccount;
use Illuminate\Database\Seeder;

/**
 * Class BankAccountsTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class BankAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accounts = [
            [
                'account_number' => '6103011',
                'account_name' => 'Pasajes personal embarcado'
            ]
        ];

        foreach ($accounts as $account) {
            BankAccount::create($account);
        }
    }
}
