<?php

use App\Models\RelocationProvider;
use Illuminate\Database\Seeder;

/**
 * Class RelocationProvidersTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class RelocationProvidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(RelocationProvider::class, 10)->create();
    }
}
