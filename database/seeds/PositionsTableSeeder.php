<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class PositionsTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $positions = json_decode(Storage::disk('local')->get('database/positions.json'));

        foreach ($positions as $position) {
            DB::table('positions')->where('name', $position)->delete();
            DB::table('positions')->insert([
                'name' => $position
            ]);
        }
    }
}
