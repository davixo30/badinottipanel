<?php

use App\Models\LodgingProvider;
use Illuminate\Database\Seeder;

/**
 * Class LodgingProvidersTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class LodgingProvidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(LodgingProvider::class, 10)->create();
    }
}
