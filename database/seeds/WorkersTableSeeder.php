<?php

use App\Models\Worker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

/**
 * Class WorkersTableSeeder
 *
 * @author Juan Gamonal H <juangamonalħ@gmail.com>
 */
class WorkersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('populate:workers');
    }
}
