<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class TicketStatusesTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class TicketStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = ['No comprado', 'Volado', 'Por volar', 'En stock'];

        foreach ($statuses as $status) {
            DB::table('ticket_statuses')->where('name', $status)->delete();
            DB::table('ticket_statuses')->insert([
                'name' => $status
            ]);
        }
    }
}
