<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class CrewStatusesTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CrewStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            'Borrador',
            'Filtro tripulación',
            'En calendario'
        ];

        foreach ($statuses as $status) {
            DB::table('crew_statuses')->where('name', $status)->delete();
            DB::table('crew_statuses')->insert([
                'name' => $status
            ]);
        }
    }
}
