<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class MaritalStatusesTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class MaritalStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $afps = [
            'Soltero',
            'Casado',
            'Viudo',
            'Divorciado',
            'Conviviente civil'
        ];

        foreach ($afps as $afp) {
            DB::table('marital_statuses')->where('name', $afp)->delete();
            DB::table('marital_statuses')->insert([
                'name' => $afp
            ]);
        }
    }
}
