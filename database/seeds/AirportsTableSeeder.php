<?php

use App\Models\BankAccount;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class AirportsTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class AirportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $airports = json_decode(Storage::disk('local')->get('database/airports.json'));

        foreach ($airports as $airport) {
            DB::table('airports')->where('city', $airport->city)->delete();
            DB::table('airports')->insert([
                'city' => $airport->city,
                'iata' => $airport->iata
            ]);
        }
    }
}
