<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class CertificatesTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CertificatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::create()->toDateTimeString();
        $certifications = json_decode(Storage::disk('local')->get('database/certificates.json'));
        $lessId = DB::table('ship_types')->where('name', 'embarcación menor')->first()->id;
        $higherId = DB::table('ship_types')->where('name', 'embarcación mayor')->first()->id;
        $insert = function (string $name, int $id) use ($now) {
            DB::table('certificates')->where('name', $name)->where('ship_type_id', $id)->delete();
            DB::table('certificates')->insert([
                'name' => $name,
                'ship_type_id' => $id,
                'created_at' => $now
            ]);
        };

        foreach ($certifications->menores as $certificacion) {
            $insert($certificacion, $lessId);
        }

        foreach ($certifications->mayores as $certificacion) {
            $insert($certificacion, $higherId);
        }
    }
}
