<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class AttributesTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class AttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attributes = json_decode(Storage::disk('local')->get('database/attributes.json'));

        foreach ($attributes as $attribute) {
            DB::table('attributes')->where('name', $attribute)->delete();
            DB::table('attributes')->insert([
                'name' => $attribute
            ]);
        }
    }
}
