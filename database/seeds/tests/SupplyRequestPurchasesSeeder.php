<?php

use App\Models\Client;
use App\Models\Crew;
use App\Models\CrewStatus;
use App\Models\Headline;
use App\Models\Ship;
use App\Models\SupplyRequest;
use App\Models\WarehouseItemSize;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Prepara el ambiente para generar compras a partir de solicitudes de abastecimiento
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class SupplyRequestPurchasesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // datos de tripulaciones
        $clients = Client::select('id')->get()->pluck('id');
        $ships = Ship::select('id')->get()->pluck('id');

        DB::transaction(function () use ($clients, $ships) {
            $sizes = WarehouseItemSize::all()->pluck('id');
            $faker = Faker\Factory::create();

            foreach ($clients as $client) {
                $shipId = $ships->random();

                // crea la tripulación
                $uploadDate = Carbon::now();
                $downloadDate = $uploadDate->addDays(25)->toDateTimeString();
                $crew = Crew::create([
                    'client_id' => $client,
                    'ship_id' => $shipId,
                    'status_id' => CrewStatus::getInCalendarStatusId(),
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'upload_date' => $uploadDate->toDateTimeString(),
                    'download_date' => $downloadDate,
                    'real_download_date' => $downloadDate
                ]);

                // añade trabajadores a la tripulación
                Headline::where('ship_id', $shipId)->select('worker_id')->get()->pluck('worker_id');

                // crea la solicitud de abastecimiento para dicha tripulación
                SupplyRequest::create([
                    'crew_id' => $crew->id,
                    'items' => [
                        [
                            'id' => $sizes->random(),
                            'quantity' => $faker->numberBetween(1, 15),
                            'observations' => $faker->words(3, true)
                        ], [
                            'id' => $sizes->random(),
                            'quantity' => $faker->numberBetween(1, 15),
                            'observations' => $faker->words(3, true)
                        ], [
                            'id' => $sizes->random(),
                            'quantity' => $faker->numberBetween(1, 15),
                            'observations' => $faker->words(3, true)
                        ]
                    ],
                    'epp' => []
                ]);
            }
        });
    }
}
