<?php

use App\Models\Purchase;
use App\Models\Supplier;
use App\Models\WarehouseItemSize;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class OperationsPurchasesSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class OperationsPurchasesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            $now = Carbon::create()->toDateString();
            $sizes = WarehouseItemSize::all()->pluck('id');
            $suppliers = Supplier::all();

            foreach ($suppliers as $supplier) {
                $items = [];

                foreach ([1, 2, 3, 4, 5] as $i) {
                    $items[] = [
                        'size_id' => $sizes->random(),
                        'quantity' => $i
                    ];
                }

                Purchase::create([
                    'purchase' => $now,
                    'supplier_id' => $supplier->id,
                    'items' => $items
                ]);
            }
        });
    }
}
