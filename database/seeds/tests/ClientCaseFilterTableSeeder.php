<?php

use App\Models\Airline;
use App\Models\Airport;
use App\Models\Client;
use App\Models\Crew;
use App\Models\CrewStatus;
use App\Models\CrewWorker;
use App\Models\FlightStretch;
use App\Models\Headline;
use App\Models\Quotation;
use App\Models\Ship;
use App\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * TODO
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class ClientCaseFilterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // datos de tripulaciones
        $clients = Client::select('id')->get()->pluck('id');
        $ships = Ship::select('id')->get()->pluck('id');

        DB::transaction(function () use ($clients, $ships) {
            foreach ($clients as $client) {
                $shipId = $ships->random();

                // crea la tripulación
                $uploadDate = Carbon::now();
                $downloadDate = $uploadDate->addDays(25)->toDateTimeString();
                $crew = Crew::create([
                    'client_id' => $client,
                    'ship_id' => $shipId,
                    'status_id' => CrewStatus::getCrewFilterStatusId(),
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'upload_date' => $uploadDate->toDateTimeString(),
                    'download_date' => $downloadDate,
                    'real_download_date' => $downloadDate
                ]);

                // añade trabajadores a la tripulación
                $workers = Headline::where('ship_id', $shipId)->select('worker_id')->get()->pluck('worker_id');

                foreach ($workers as $worker) {
                    CrewWorker::create(['crew_id' => $crew->id, 'worker_id' => $worker]);
                }
            }
        });
    }
}
