<?php

use App\Models\Airline;
use App\Models\Airport;
use App\Models\Client;
use App\Models\Crew;
use App\Models\CrewStatus;
use App\Models\CrewWorker;
use App\Models\FlightStretch;
use App\Models\Headline;
use App\Models\Quotation;
use App\Models\Ship;
use App\Models\Ticket;
use App\Models\TicketGroup;
use App\Types\FlightStretchesTypes;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Este seeder genera la estructura para realizar pruebas con las cotizaciones, no debería correr en el seed inicial
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class QuotationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // datos de tripulaciones
        $clients = Client::select('id')->get()->pluck('id');
        $ships = Ship::select('id')->get()->pluck('id');

        // datos de pasajes
        $airlines = Airline::select('id')->get()->pluck('id');
        $airports = Airport::select('id')->get()->pluck('id');

        DB::transaction(function () use ($clients, $ships, $airports, $airlines) {
            $goingId = FlightStretch::select('id')->where('name', FlightStretchesTypes::$GOING)->first()->id;
            $returnId = FlightStretch::select('id')->where('name', FlightStretchesTypes::$RETURN)->first()->id;
            $faker = Faker\Factory::create();

            foreach ($clients as $client) {
                $shipId = $ships->random();

                // crea la tripulación
                $uploadDate = Carbon::now();
                $downloadDate = $uploadDate->addDays(25)->toDateTimeString();
                $crew = Crew::create([
                    'client_id' => $client,
                    'ship_id' => $shipId,
                    'status_id' => CrewStatus::getInCalendarStatusId(),
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'upload_date' => $uploadDate->toDateTimeString(),
                    'download_date' => $downloadDate,
                    'real_download_date' => $downloadDate
                ]);

                // crea la cotización para esta tripulación
                $quotation = Quotation::create(['crew_id' => $crew->id]);

                // añade trabajadores a la tripulación y sus respectivos pasajes
                $workers = Headline::where('ship_id', $shipId)->select('worker_id')->get()->pluck('worker_id');

                foreach ($workers as $worker) {
                    $crewWorker = CrewWorker::create(['crew_id' => $crew->id, 'worker_id' => $worker]);

                    // crea pasaje de ida
                    $ticketGroup = TicketGroup::create([
                        'price' => $faker->numberBetween(30000, 100000),
                        'flight_date' => Carbon::now(),
                        'departure_time' => $faker->time(),
                        'arrival_time' => $faker->time(),
                        'flight_stretch_id' => $goingId,
                        'departure_airport_id' => $airports->random(),
                        'arrival_airport_id' => $airports->random(),
                        'airline_id' => $airlines->random(),
                        'quotation_id' => $quotation->id
                    ]);

                    Ticket::create([
                        'crew_worker_id' => $crewWorker->id,
                        'ticket_group_id' => $ticketGroup->id
                    ]);

                    // crea pasaje de vuelta
                    $ticketGroup = TicketGroup::create([
                        'price' => $faker->numberBetween(30000, 100000),
                        'flight_date' => Carbon::now(),
                        'departure_time' => $faker->time(),
                        'arrival_time' => $faker->time(),
                        'flight_stretch_id' => $returnId,
                        'departure_airport_id' => $airports->random(),
                        'arrival_airport_id' => $airports->random(),
                        'airline_id' => $airlines->random(),
                        'quotation_id' => $quotation->id
                    ]);

                    Ticket::create([
                        'crew_worker_id' => $crewWorker->id,
                        'ticket_group_id' => $ticketGroup->id
                    ]);
                }

                // guarda estado final de la cotización
                $quotation->update(['entered' => 1, 'entered_at' => Carbon::now()]);
            }
        });
    }
}
