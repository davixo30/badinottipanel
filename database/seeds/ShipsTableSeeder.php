<?php

use App\Models\Ship;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class ShipsTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class ShipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $date = Carbon::now()->toDateTimeString();
        $ships = json_decode(Storage::disk('local')->get('database/ships.json'));
        $lessId = DB::table('ship_types')->where('name', 'embarcación menor')->first()->id;
        $higherId = DB::table('ship_types')->where('name', 'embarcación mayor')->first()->id;

        foreach ($ships as $i => $ship) {
            $supervisor = $ship->supervisor;
            $lastReview = $ship->last_review;
            unset($ship->supervisor);
            unset($ship->last_review);

            DB::table('ships')->where('call_identifier', $ship->call_identifier)->delete();
            Ship::create(
                array_merge([
                    // TODO: cambiar code
                    'code' => $i,
                    'created_at' => $date,
                    'supervisor_id' => DB::table('users')
                        ->where('rut', $supervisor)
                        ->select('id')
                        ->first()
                        ->id,
                    'type_id' => $ship->name === 'TTE Reyes' ? $higherId : $lessId,
                    'last_review' => (new Carbon($lastReview))->toDateTimeString()
                ], (array) $ship)
            );
        }
    }
}
