<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class HealthForecastsTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class HealthForecastsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $forecasts = [
            'Banmédica',
            'Colmena',
            'Consalud',
            'Cruz Blanca S.A.',
            'Fonasa',
            'Nueva Mas Vida',
            'Vida Tres',
            'Sin Previsión'
        ];

        foreach ($forecasts as $forecast) {
            DB::table('health_forecasts')->where('name', $forecast)->delete();
            DB::table('health_forecasts')->insert([
                'name' => $forecast
            ]);
        }
    }
}
