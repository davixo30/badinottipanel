<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class HeadlinesTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class HeadlinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ships = json_decode(Storage::disk('local')->get('database/headlines.json'));

        DB::transaction(function ($item) use ($ships) {
            foreach ($ships as $shipName => $headlines) {
                $shipId = DB::table('ships')->select('id')->where('name', $shipName)->first()->id;

                foreach ($headlines as $headline) {
                    $workerId = DB::table('workers')->select('id')->where('rut', $headline)->first()->id;

                    DB::table('headlines')->where('ship_id', $shipId)
                        ->where('worker_id', $workerId)
                        ->delete();
                    DB::table('headlines')->insert(['ship_id' => $shipId, 'worker_id' => $workerId]);
                }
            }
        });
    }
}
