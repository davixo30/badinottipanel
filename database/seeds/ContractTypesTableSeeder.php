<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class ContractTypesTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class ContractTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //TODO: cambiar a capitalize y ajustar en front
        $types = [
            'a plazo',
            'boleta',
            'faena',
            'indefinido',
            'plazo fijo',
            'por obra'
        ];

        foreach ($types as $type) {
            DB::table('contract_types')->where('name', $type)->delete();
            DB::table('contract_types')->insert([
                'name' => $type
            ]);
        }
    }
}
