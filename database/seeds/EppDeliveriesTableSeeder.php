<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class EppDeliveriesTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class EppDeliveriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $epps = json_decode(Storage::disk('local')->get('database/epp_worker.json'));

        $dates = [];
        $sizes = [];
        $deliveryCounter = 1;

        foreach (get_object_vars($epps) as $i => $epp) {
            if (strpos($i, 'Fecha Entrega') !== false) {
                $dates[$i] = $epp;
                $deliveryCounter++;
            } else {
                $sizes[$i] = ['counter' => $deliveryCounter, 'items' => $epp];
            }
        }

        DB::transaction(function () use ($dates, $sizes) {
            foreach ($sizes as $eppName => $deliveries) {
                foreach ($deliveries['items'] as $worker => $size) {
                    $deliveryDate = $dates['Fecha Entrega ' . $deliveries['counter']]->$worker;

                    // solo si tiene talla y fecha de entrega inserta la entrega
                    if ($deliveryDate !== '' && $size !== '') {
                        $deliveryGroupId = DB::table('epp_delivery_groups')->insertGetId([
                            'delivery' => \Carbon\Carbon::create($deliveryDate),
                            'worker_id' => DB::table('workers')->where('rut', $worker)->first()->id
                        ]);

                        DB::table('epp_deliveries')->insertGetId([
                            'size' => $size,
                            'epp_id' => DB::table('epps')->where('name', 'like', $eppName)->first()->id,
                            'epp_delivery_group_id' => $deliveryGroupId,
                            'status' => 1
                        ]);
                    }
                }
            }
        });
    }
}
