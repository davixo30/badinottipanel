<?php

use App\Models\BankAccount;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class AirlinesTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class AirlinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $airlines = [
            'LATAM',
            'Sky Airline',
            'JetSmart'
        ];

        foreach ($airlines as $airline) {
            DB::table('airlines')->where('name', $airline)->delete();
            DB::table('airlines')->insert([
                'name' => $airline
            ]);
        }
    }
}
