<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class AfpsTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class AfpsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $afps = [
            'Capital',
            'Capremer - Régimen 6',
            'Cuprum',
            'Habitat',
            'Modelo',
            'Planvital',
            'Provida',
            'Sin Cotización'
        ];

        foreach ($afps as $afp) {
            DB::table('afps')->where('name', $afp)->delete();
            DB::table('afps')->insert([
                'name' => $afp
            ]);
        }
    }
}
