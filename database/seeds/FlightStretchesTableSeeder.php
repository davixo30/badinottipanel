<?php

use App\Models\BankAccount;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class FlightStretchesTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class FlightStretchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stretches = ['Ida', 'Vuelta'];

        foreach ($stretches as $stretch) {
            DB::table('flight_stretches')->where('name', $stretch)->delete();
            DB::table('flight_stretches')->insert([
                'name' => $stretch
            ]);
        }
    }
}
