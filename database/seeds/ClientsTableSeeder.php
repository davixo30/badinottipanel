<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class ClientsTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clients = json_decode(Storage::disk('local')->get('database/clients.json'));

        foreach ($clients as $client) {
            DB::table('clients')->where('name', $client)->delete();
            DB::table('clients')->insert([
                'name' => $client
            ]);
        }
    }
}
