<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class QualifiersTableSeeder
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class QualifiersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            'jmayorga@badinotti.com',
            'pguerra@badinotti.com',
            'gmansilla@badinotti.com',
            'omunoz@badinotti.com'
        ];

        foreach ($users as $user) {
            $id = DB::table('users')
                ->where('email', $user)
                ->first()
                ->id;

            DB::table('qualifiers')->where('id', $id)->delete();
            DB::table('qualifiers')->insert([
                'id' => $id,
                'created_at' => Carbon::now()
            ]);
        }
    }
}
