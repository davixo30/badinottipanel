<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateInvoicedQuotationsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateInvoicedQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // TODO definir largo de orden de compra y factura
        Schema::create('invoiced_quotations', function (Blueprint $table) {
            $table->unsignedBigInteger('quotation_id')->primary();
            $table->string('purchase_order', 50)->unique();
            $table->string('bill_number', 50)->unique();
            $table->unsignedTinyInteger('bank_account_id');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('quotation_id')->references('id')->on('quotations');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoiced_quotations');
    }
}
