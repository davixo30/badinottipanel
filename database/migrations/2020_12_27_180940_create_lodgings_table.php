<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateLodgingsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com
 */
class CreateLodgingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lodgings', function (Blueprint $table) {
            $table->id();
            $table->date('arrival_date');
            $table->date('departure_date');
            $table->boolean('food');
            $table->unsignedBigInteger('lodging_provider_id');
            $table->unsignedInteger('price');
            $table->unsignedBigInteger('crew_worker_id');
            $table->timestamps();

            $table->foreign('lodging_provider_id')->references('id')->on('suppliers');
            $table->foreign('crew_worker_id')->references('id')->on('crew_worker');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lodgings');
    }
}
