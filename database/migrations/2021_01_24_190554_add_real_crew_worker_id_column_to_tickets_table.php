<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class AddRealCrewWorkerIdColumnToTicketsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class AddRealCrewWorkerIdColumnToTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->unsignedBigInteger('real_crew_worker_id')->nullable()->unique();
            $table->foreign('real_crew_worker_id')->references('id')->on('crew_worker');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            //
        });
    }
}
