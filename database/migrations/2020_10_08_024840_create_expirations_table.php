<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateExpirationsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateExpirationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expirations', function (Blueprint $table) {
            // TODO: renombrar a id solamente
            $table->unsignedBigInteger('worker_id');
            $table->date('ci')->nullable();
            $table->date('enrollment')->nullable();
            $table->date('ri')->nullable();
            $table->date('exam')->nullable();
            $table->date('life_insurance')->nullable();
            // TODO: usar solo updated_at
            $table->timestamps();

            $table->foreign('worker_id')
                ->references('id')
                ->on('workers')
                ->onDelete('cascade');

            $table->primary('worker_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expirations');
    }
}
