<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class AddPriceColumnToPurchaseWarehouseItemSizeTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class AddPriceColumnToPurchaseWarehouseItemSizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_warehouse_item_size', function (Blueprint $table) {
            $table->unsignedInteger('price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_warehouse_item_size', function (Blueprint $table) {
            //
        });
    }
}
