<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreatePurchaseWarehouseItemSizeTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreatePurchaseWarehouseItemSizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_warehouse_item_size', function (Blueprint $table) {
            $table->unsignedInteger('quantity');
            $table->unsignedBigInteger('purchase_id');
            $table->unsignedBigInteger('warehouse_item_size_id');

            $table->foreign('purchase_id')->references('id')->on('purchases');
            $table->foreign('warehouse_item_size_id')->references('id')->on('warehouse_item_sizes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_warehouse_item_size');
    }
}
