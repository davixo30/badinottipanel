<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateUserContractsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateUserContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_contracts', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->primary();
            $table->date('date');
            // TODO: solo es nullable por la carga masiva
            $table->string('identity_card_copy', 50)->nullable();
            $table->unsignedTinyInteger('afp_id');
            $table->unsignedTinyInteger('health_forecast_id');
            $table->unsignedTinyInteger('contract_type_id');

            $table->foreign('afp_id')
                ->references('id')
                ->on('afps');

            $table->foreign('health_forecast_id')
                ->references('id')
                ->on('health_forecasts');

            $table->foreign('contract_type_id')
                ->references('id')
                ->on('contract_types');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_contracts');
    }
}
