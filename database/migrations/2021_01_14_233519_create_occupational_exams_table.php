<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateOccupationalExamsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateOccupationalExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('occupational_exams', function (Blueprint $table) {
            $table->id();
            $table->date('exam_date');
            $table->boolean('condition');
            // estos 3 campos pueden ser nulos, ya que depende de condición
            // expiration_date depende de condition = 1
            $table->date('expiration_date')->nullable();
            // el certificado depende de la condition = 1
            $table->string('certificate')->nullable();
            // observations depende de condition = 0
            $table->text('observations')->nullable();

            $table->unsignedBigInteger('worker_id');
            $table->unsignedBigInteger('uploader_id');
            $table->timestamp('created_at');

            $table->foreign('worker_id')->references('id')->on('workers');
            $table->foreign('uploader_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('occupational_exams');
    }
}
