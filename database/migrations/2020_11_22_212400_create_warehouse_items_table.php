<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateWarehouseItemsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateWarehouseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // TODO: definir largos
        Schema::create('warehouse_items', function (Blueprint $table) {
            $table->id();
            $table->string('name', 150)->unique();
            $table->unsignedInteger('price')->default(0);
            $table->unsignedTinyInteger('classification_id');
            $table->unsignedTinyInteger('measurement_unit_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('classification_id')->references('id')
                ->on('warehouse_item_classifications');
            $table->foreign('measurement_unit_id')->references('id')
                ->on('measurement_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_items');
    }
}
