<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateCertificateShipTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateCertificateShipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificate_ship', function (Blueprint $table) {
            $table->id();
            $table->date('emission')->nullable();
            $table->date('expiration')->nullable();
            $table->string('certificate', 50);
            $table->unsignedBigInteger('ship_id');
            $table->unsignedTinyInteger('certificate_id');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('ship_id')
                ->references('id')
                ->on('ships')
                ->cascadeOnDelete();

            $table->foreign('certificate_id')
                ->references('id')
                ->on('certificates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificate_ship');
    }
}
