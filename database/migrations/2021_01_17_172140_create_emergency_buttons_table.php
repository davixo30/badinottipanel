<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateEmergencyButtonsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateEmergencyButtonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emergency_buttons', function (Blueprint $table) {
            $table->id();
            $table->string('reason');
            $table->unsignedBigInteger('crew_id');
            $table->unsignedBigInteger('emergency_crew_worker_id');
            $table->unsignedBigInteger('replacement_crew_worker_id')->nullable();
            $table->unsignedBigInteger('uploader_id');
            $table->timestamps();

            $table->foreign('crew_id')->references('id')->on('crews');
            $table->foreign('emergency_crew_worker_id')
                ->references('id')
                ->on('crew_worker');
            $table->foreign('replacement_crew_worker_id')
                ->references('id')
                ->on('crew_worker');
            $table->foreign('uploader_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emergency_buttons');
    }
}
