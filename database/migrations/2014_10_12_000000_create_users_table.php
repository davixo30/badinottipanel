<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateUsersTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // TODO: definir largos
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('last_name');
            $table->string('avatar', 50)->default('profile.png');
            $table->string('code', 10)->unique();
            $table->string('rut')->unique();
            $table->string('email')->unique();
            $table->date('birthdate');
            $table->string('phone_number');
            $table->string('address', 255);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->unsignedInteger('commune_id');
            $table->unsignedTinyInteger('marital_status_id');
            $table->unsignedTinyInteger('position_id');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('commune_id')
                ->references('id')
                ->on('communes');

            $table->foreign('marital_status_id')
                ->references('id')
                ->on('marital_statuses');

            $table->foreign('position_id')
                ->references('id')
                ->on('positions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
