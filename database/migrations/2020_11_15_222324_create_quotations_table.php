<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateQuotationsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->id();
            // indica si la cotización fue validada o rechazada por operaciones, permite nulo ya que se crean
            // cotizaciones 'fantasma', las cuales aún no han sido confirmadas
            $table->boolean('status')->nullable();
            // indica si la cotización es 'fantasma' o no, entered = true significa que logística ya la creo y dejó
            // de ser una cotización 'fantasma'
            $table->boolean('entered')->default(false);
            $table->unsignedBigInteger('crew_id');
            $table->text('rejection_reason')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('entered_at')->nullable();
            $table->timestamp('confirmed_at')->nullable();

            $table->foreign('crew_id')->references('id')->on('crews');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotations');
    }
}
