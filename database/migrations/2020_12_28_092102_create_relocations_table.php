<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateRelocationsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateRelocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relocations', function (Blueprint $table) {
            $table->id();
            $table->timestamp('pickup');
            $table->string('arrival', 255);
            $table->string('departure', 255);
            $table->unsignedBigInteger('relocation_provider_id');
            $table->unsignedInteger('price');
            $table->unsignedBigInteger('crew_worker_id');
            $table->timestamps();

            $table->foreign('relocation_provider_id')->references('id')->on('relocation_providers');
            $table->foreign('crew_worker_id')->references('id')->on('crew_worker');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relocations');
    }
}
