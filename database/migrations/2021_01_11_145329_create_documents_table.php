<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateDocumentsTable
 *
 * @author Juan Gamonal H <jgamonal@ucsc.cl>
 */
class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->date('expiration_date');
            $table->unsignedBigInteger('uploader_id');
            $table->unsignedTinyInteger('document_type_id');
            $table->unsignedBigInteger('worker_id');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('uploader_id')->references('id')->on('users');
            $table->foreign('document_type_id')->references('id')->on('document_types');
            $table->foreign('worker_id')->references('id')->on('workers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
