<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateTicketPurchasesTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateTicketPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_purchases', function (Blueprint $table) {
            $table->id();
            $table->string('flight_code', 100);
            $table->unsignedInteger('dearness')->default(0);
            $table->unsignedInteger('penalty')->default(0);
            $table->unsignedBigInteger('uploader_id')->nullable();
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('id')->references('id')->on('tickets');
            $table->foreign('uploader_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_purchases');
    }
}
