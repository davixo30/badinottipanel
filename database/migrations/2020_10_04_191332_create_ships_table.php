<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateShipsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateShipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // TODO: definir largos
        // TODO: codigo no debe ser nullable, arreglar en seeds
        Schema::create('ships', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50)->unique();
            $table->string('code', 10)->unique()->unique();
            $table->string('plate', 50)->unique();
            $table->string('call_identifier', 15)->unique();
            $table->unsignedTinyInteger('capacity');
            $table->unsignedTinyInteger('engines');
            $table->unsignedTinyInteger('generators');
            $table->date('last_careen')->nullable();
            $table->date('last_review')->nullable();
            $table->unsignedBigInteger('supervisor_id');
            $table->unsignedTinyInteger('type_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('supervisor_id')
                ->references('id')
                ->on('users');

            $table->foreign('type_id')
                ->references('id')
                ->on('ship_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ships');
    }
}
