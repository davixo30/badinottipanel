<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class AddCriticalStockColumnsToWarehouseItemsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class AddCriticalStockColumnsToWarehouseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('warehouse_items', function (Blueprint $table) {
            // stock crítico
            $table->unsignedTinyInteger('critical_stock')->nullable();
            // tiempo de respuesta (días)
            $table->unsignedTinyInteger('response_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warehouse_items', function (Blueprint $table) {
            //
        });
    }
}
