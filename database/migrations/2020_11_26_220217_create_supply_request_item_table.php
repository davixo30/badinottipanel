<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateSupplyRequestItemTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateSupplyRequestItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supply_request_item', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('quantity');
            $table->string('observations', 255)->nullable();
            $table->enum('action', ['purchase', 'warehouse'])->nullable();
            $table->unsignedInteger('price')->nullable();
            $table->unsignedBigInteger('supply_request_id');
            $table->unsignedBigInteger('warehouse_item_size_id');

            $table->foreign('supply_request_id')->references('id')->on('supply_requests');
            $table->foreign('warehouse_item_size_id')->references('id')->on('warehouse_item_sizes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supply_request_item');
    }
}
