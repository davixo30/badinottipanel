<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateTicketGroupsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateTicketGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_groups', function (Blueprint $table) {
            $table->id();
            $table->date('flight_date');
            $table->time('departure_time');
            $table->time('arrival_time');
            $table->unsignedInteger('price');
            $table->unsignedTinyInteger('airline_id');
            $table->unsignedTinyInteger('flight_stretch_id');
            $table->unsignedTinyInteger('departure_airport_id');
            $table->unsignedTinyInteger('arrival_airport_id');
            $table->unsignedBigInteger('quotation_id');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('airline_id')->references('id')->on('airlines');
            $table->foreign('flight_stretch_id')->references('id')->on('flight_stretches');
            $table->foreign('departure_airport_id')->references('id')->on('airports');
            $table->foreign('quotation_id')->references('id')->on('quotations');
            $table->foreign('arrival_airport_id')->references('id')->on('airports');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_groups');
    }
}
