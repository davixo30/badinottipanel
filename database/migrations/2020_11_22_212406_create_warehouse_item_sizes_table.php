<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateWarehouseItemSizesTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateWarehouseItemSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_item_sizes', function (Blueprint $table) {
            $table->id();
            $table->string('name')->default('');
            $table->unsignedInteger('quantity')->default(0);
            $table->unsignedBigInteger('warehouse_item_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('warehouse_item_id')->references('id')->on('warehouse_items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_item_sizes');
    }
}
