<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateEppDeliveriesTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateEppDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('epp_deliveries', function (Blueprint $table) {
            $table->id();
            $table->string('observations', 255);
            $table->unsignedBigInteger('epp_delivery_group_id');
            $table->unsignedBigInteger('warehouse_item_size_id');

            $table->foreign('warehouse_item_size_id')
                ->references('id')
                ->on('warehouse_item_sizes');

            $table->foreign('epp_delivery_group_id')
                ->references('id')
                ->on('epp_delivery_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('epp_deliveries');
    }
}
