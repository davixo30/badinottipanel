<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateImcsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateImcsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imcs', function (Blueprint $table) {
            $table->id();
            $table->unsignedTinyInteger('month');
            $table->unsignedInteger('year');
            // estatura
            $table->unsignedFloat('height');
            // peso
            $table->unsignedFloat('weight');
            // imc total
            $table->unsignedFloat('imc');
            $table->unsignedBigInteger('worker_id');
            $table->unsignedBigInteger('uploader_id');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('worker_id')->references('id')->on('workers');
            $table->foreign('uploader_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imcs');
    }
}
