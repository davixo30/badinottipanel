<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateCrewWorkerTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateCrewWorkerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crew_worker', function (Blueprint $table) {
            $table->id();
            // indica si se encuentra filtrado este tripulante
            $table->boolean('filtered')->default(false);
            // solicita alojamiento?
            $table->boolean('lodging')->default(false);
            // solicita alimentación?
            $table->boolean('lodging_food')->default(false);
            // solicita traslado?
            $table->boolean('relocation')->default(false);
            // observaciones de vuelo en logística, se deja acá para no redundar en cada pasaje
            $table->string('observations', 255)->nullable();
            // razón por la cual el tripulante fue filtrado por el supervisor
            $table->string('filter_reason')->nullable();
            $table->unsignedBigInteger('crew_id');
            $table->unsignedBigInteger('worker_id');

            $table->foreign('crew_id')
                ->references('id')
                ->on('crews')
                ->cascadeOnDelete();

            $table->foreign('worker_id')
                ->references('id')
                ->on('workers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crew_worker');
    }
}
