<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class AddEmergencySupportToTicketGroupsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class AddEmergencySupportToTicketGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_groups', function (Blueprint $table) {
            $table->unsignedBigInteger('quotation_id')->nullable()->change();
            $table->unsignedBigInteger('emergency_quotation_id')->nullable();
            $table->foreign('emergency_quotation_id')->references('id')->on('emergency_quotations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_groups', function (Blueprint $table) {
            //
        });
    }
}
