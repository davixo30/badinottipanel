<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateInductionWorkersTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateInductionWorkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('induction_workers', function (Blueprint $table) {
            $table->id();
            $table->date('induction_date');
            $table->unsignedBigInteger('induction_id');
            $table->string('certificate')->unique();
            $table->unsignedBigInteger('worker_id');
            $table->unsignedBigInteger('uploader_id');

            $table->foreign('induction_id')
                ->references('id')
                ->on('inductions');

            $table->foreign('worker_id')
                ->references('id')
                ->on('workers');

            $table->foreign('uploader_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('induction_workers');
    }
}
