<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreatePurchasesTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->id();
            $table->boolean('status')->nullable();
            $table->text('rejection_reason')->nullable();
            $table->unsignedBigInteger('supplier_id');
            $table->unsignedBigInteger('supply_request_id')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->date('purchase');
            $table->date('confirmed_at')->nullable();
            $table->unsignedTinyInteger('only_warehouse');

            $table->foreign('supplier_id')->references('id')->on('suppliers');
            $table->foreign('supply_request_id')->references('id')->on('supply_requests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
