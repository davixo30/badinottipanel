<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateContractsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->unsignedBigInteger('worker_id')->primary();
            $table->date('date')->nullable();
            $table->integer('salary')->nullable();
            $table->string('identity_card_copy', 50)->nullable();
            $table->string('boarding_license', 50)->nullable();
            $table->unsignedTinyInteger('afp_id')->nullable();
            $table->unsignedTinyInteger('health_forecast_id')->nullable();
            $table->unsignedTinyInteger('contract_type_id')->nullable();
            $table->unsignedBigInteger('uploader_id');

            $table->foreign('afp_id')
                ->references('id')
                ->on('afps');

            $table->foreign('health_forecast_id')
                ->references('id')
                ->on('health_forecasts');

            $table->foreign('contract_type_id')
                ->references('id')
                ->on('contract_types');

            $table->foreign('worker_id')
                ->references('id')
                ->on('workers');

            $table->foreign('uploader_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
