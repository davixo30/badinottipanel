<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateHealthForecastsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateHealthForecastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('health_forecasts', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name', 25)->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('health_forecasts');
    }
}
