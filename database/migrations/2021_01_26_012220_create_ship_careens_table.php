<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateShipCareensTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateShipCareensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ship_careens', function (Blueprint $table) {
            $table->id();
            $table->date('careen_date');
            $table->string('observations', 255)->nullable();
            $table->string('certificate')->unique();
            $table->unsignedBigInteger('ship_id');
            $table->unsignedBigInteger('uploader_id');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('ship_id')->references('id')->on('ships');
            $table->foreign('uploader_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ship_careens');
    }
}
