<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateCertificatesTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificates', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name', 150);
            $table->unsignedTinyInteger('ship_type_id');

            $table->foreign('ship_type_id')
                ->references('id')
                ->on('ship_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificates');
    }
}
