<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateTechnicalSpecificationsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateTechnicalSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technical_specifications', function (Blueprint $table) {
            $table->unsignedBigInteger('ship_id')->primary();

            /** Características generales */
            // armador
            $table->string('shipowner')->nullable();
            // puerto de matrícula
            $table->string('plate_port')->nullable();
            // tonelaje de registro grueso (T.R.G.)
            $table->decimal('trg')->unsigned()->nullable();
            // bollard pull (ton)
            $table->decimal('bollard_pull')->unsigned()->nullable();
            // desplazamiento liviano (ton)
            $table->decimal('light_displacement')->unsigned()->nullable();

            /** Características de construcción */
            // año de construcción
            $table->unsignedInteger('year_of_construction')->nullable();
            // país
            $table->string('country')->nullable();
            // astilleros
            $table->string('shipyards')->nullable();
            // eslora total (mts)
            $table->decimal('total_length')->unsigned()->nullable();
            // manga (mts)
            $table->decimal('sleeve')->unsigned()->nullable();
            // puntal a cubierta principal (mts)
            $table->decimal('strut_to_main_deck')->unsigned()->nullable();
            // capacidad estanque de combustible (lts)
            $table->decimal('fuel_tank_capacity')->unsigned()->nullable();
            // capacidad estanque de agua dulce (lts)
            $table->decimal('fresh_water_pond_capacity')->unsigned()->nullable();
            // observaciones
            $table->string('observations')->nullable();

            /** Propulsión */
            // motor principal
            $table->string('main_engine')->nullable();
            // potencia
            $table->string('power')->nullable();
            // serie motor
            $table->string('motor_series')->nullable();
            // caja de transmisión
            $table->string('transmission_box')->nullable();
            // serie caja de transmisión
            $table->string('transmission_box_series')->nullable();
            // relación
            $table->string('relation')->nullable();
            // velocidad de crucero (nudos)
            $table->decimal('cruising_speed')->unsigned()->nullable();
            // consumo de combustible (litros/hora)
            $table->decimal('fuel_consumption')->unsigned()->nullable();
            // tipo hélice
            $table->string('propeller_type')->nullable();

            $table->timestamps();
            $table->foreign('ship_id')->references('id')->on('ships');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technical_specifications');
    }
}
