<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateQualificationsTable
 *
 * @author Juan Gamonal H <juangamonal@gmail.com>
 */
class CreateQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qualifications', function (Blueprint $table) {
            $table->id();
            $table->decimal('average', 2, 1, true)->default(0);
            $table->date('qualification_date');
            $table->unsignedBigInteger('qualifier_id');
            $table->unsignedBigInteger('worker_id');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('qualifier_id')
                ->references('id')
                ->on('qualifiers');

            $table->foreign('worker_id')
                ->references('id')
                ->on('workers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qualifications');
    }
}
