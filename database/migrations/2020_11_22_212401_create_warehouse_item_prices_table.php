<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateWarehouseItemPricesTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateWarehouseItemPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_item_prices', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('price')->default(0);
            $table->unsignedBigInteger('warehouse_item_id');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('warehouse_item_id')->references('id')->on('warehouse_items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_item_prices');
    }
}
