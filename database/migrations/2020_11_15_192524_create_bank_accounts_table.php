<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateBankAccountsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // TODO: verificar largos de campos
        Schema::create('bank_accounts', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('account_number', 50)->unique();
            $table->string('account_name', 100)->unique();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_accounts');
    }
}
