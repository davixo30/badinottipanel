<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateEppDeliveryGroupsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateEppDeliveryGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('epp_delivery_groups', function (Blueprint $table) {
            $table->id();
            $table->date('delivery');
            $table->unsignedBigInteger('worker_id');
            $table->unsignedBigInteger('uploader_id');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('worker_id')->references('id')->on('workers');
            $table->foreign('uploader_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('epp_delivery_groups');
    }
}
