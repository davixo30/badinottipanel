<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class AddNullableToPriceColumnInTicketGroups
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class AddNullableToPriceColumnInTicketGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_groups', function (Blueprint $table) {
            $table->unsignedInteger('price')->nullable()->change();
            $table->unsignedBigInteger('stock_ticket_id')->nullable();

            $table->foreign('stock_ticket_id')->references('id')->on('tickets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_groups', function (Blueprint $table) {
            //
        });
    }
}
