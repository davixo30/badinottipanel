<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateRolesTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateRoleWorkerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_worker', function (Blueprint $table) {
            $table->unsignedBigInteger('worker_id');
            $table->unsignedTinyInteger('role_id');

            $table->foreign('worker_id')
                ->references('id')
                ->on('workers');

            $table->foreign('role_id')
                ->references('id')
                ->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_worker');
    }
}
