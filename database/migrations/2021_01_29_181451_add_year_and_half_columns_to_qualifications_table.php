<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class AddYearAndHalfColumnsToQualificationsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class AddYearAndHalfColumnsToQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qualifications', function (Blueprint $table) {
            $table->unsignedInteger('year');
            $table->unsignedTinyInteger('half');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qualifications', function (Blueprint $table) {
            //
        });
    }
}
