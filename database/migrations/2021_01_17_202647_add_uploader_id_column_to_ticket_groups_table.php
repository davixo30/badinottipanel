<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class AddUploaderIdColumnToTicketGroupsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class AddUploaderIdColumnToTicketGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_groups', function (Blueprint $table) {
            $table->unsignedBigInteger('uploader_id');

            $table->foreign('uploader_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_groups', function (Blueprint $table) {
            //
        });
    }
}
