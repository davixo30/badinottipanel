<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateEppDeliverySupplyRequestTable
 *
 * @author Juan Gamonal H <jgamonal@ucsc.cl>
 */
class CreateEppDeliverySupplyRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('epp_delivery_supply_request', function (Blueprint $table) {
            $table->id();
            $table->enum('action', ['purchase', 'warehouse'])->nullable();
            $table->unsignedInteger('price')->nullable();
            $table->unsignedBigInteger('supply_request_id');
            $table->unsignedBigInteger('warehouse_item_size_id');
            $table->unsignedBigInteger('worker_id');

            $table->foreign('supply_request_id')->references('id')->on('supply_requests');
            $table->foreign('warehouse_item_size_id')->references('id')->on('warehouse_item_sizes');
            $table->foreign('worker_id')->references('id')->on('workers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('epp_delivery_supply_request');
    }
}
