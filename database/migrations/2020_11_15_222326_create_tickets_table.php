<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateTicketsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            // $table->boolean('bought')->default(0);
            $table->unsignedBigInteger('crew_worker_id');
            $table->unsignedBigInteger('ticket_group_id');
            // $table->unsignedBigInteger('real_crew_worker_id')->nullable()->unique();

            $table->foreign('crew_worker_id')->references('id')->on('crew_worker');
            $table->foreign('ticket_group_id')->references('id')
                ->on('ticket_groups')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
