<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmergencyQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emergency_quotations', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            // indica si la cotización es 'fantasma' o no, entered = true significa que logística ya la creo y dejó
            // de ser una cotización 'fantasma'
            $table->boolean('entered')->default(false);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('entered_at')->nullable();

            $table->foreign('id')->references('id')->on('emergency_buttons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emergency_quotations');
    }
}
