<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateCrewsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateCrewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crews', function (Blueprint $table) {
            $table->id();
            // fecha subida
            $table->date('upload_date');
            // fecha bajada protocolar
            $table->date('download_date');
            // fecha bajada real
            $table->date('real_download_date');
            $table->string('observations',80)->nullable();
            $table->unsignedTinyInteger('client_id');
            $table->unsignedBigInteger('ship_id');
            $table->unsignedTinyInteger('status_id');
            $table->unsignedTinyInteger('update_eskuad')->default(0);
            $table->timestamps();

            $table->foreign('client_id')
                ->references('id')
                ->on('clients');

            $table->foreign('ship_id')
                ->references('id')
                ->on('ships')
                ->cascadeOnDelete();

            $table->foreign('status_id')
                ->references('id')
                ->on('crew_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crews');
    }
}
