<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateWarehouseItemClassificationsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateWarehouseItemClassificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_item_classifications', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name', 100)->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_item_classifications');
    }
}
