<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class AddExtraColumnsToCrewWorkerTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class AddExtraColumnsToCrewWorkerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crew_worker', function (Blueprint $table) {
            $table->boolean('is_replacement')->default(0);
            $table->boolean('with_emergency')->default(0);
            $table->boolean('late_entry')->default(0);
            $table->date('entry_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crew_worker', function (Blueprint $table) {
            //
        });
    }
}
