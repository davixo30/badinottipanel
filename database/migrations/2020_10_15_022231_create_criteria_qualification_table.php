<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateCriteriaQualificationTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateCriteriaQualificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('criteria_qualification', function (Blueprint $table) {
            $table->id();
            $table->enum('grade', [0, 1, 2, 3, 4, 5, 6, 7])->default(0);
            $table->unsignedTinyInteger('criteria_id');
            $table->unsignedBigInteger('qualification_id');

            $table->foreign('criteria_id')
                ->references('id')
                ->on('criteria');

            $table->foreign('qualification_id')
                ->references('id')
                ->on('qualifications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criteria_qualification');
    }
}
