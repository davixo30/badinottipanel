<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateClientInductionsTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateClientInductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_inductions', function (Blueprint $table) {
            $table->unsignedTinyInteger('client_id');
            $table->unsignedBigInteger('induction_id');

            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('induction_id')->references('id')->on('inductions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_inductions');
    }
}
