<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateWorkersTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateWorkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // TODO: definir largos
        Schema::create('workers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('last_name');
            // TODO: largo
            $table->string('avatar', 50)->default('profile.png');
            $table->string('code', 10)->nullable();
            $table->string('rut')->unique();
            $table->date('birthdate');
            $table->string('phone_number', 15);
            $table->string('address', 255);
            $table->unsignedInteger('commune_id');
            $table->unsignedTinyInteger('marital_status_id');
            $table->unsignedTinyInteger('attribute_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('attribute_id')
                ->references('id')
                ->on('attributes');

            $table->foreign('commune_id')
                ->references('id')
                ->on('communes');

            $table->foreign('marital_status_id')
                ->references('id')
                ->on('marital_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workers');
    }
}
