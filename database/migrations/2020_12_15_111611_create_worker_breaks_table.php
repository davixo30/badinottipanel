<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateWorkerBreaksTable
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateWorkerBreaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worker_breaks', function (Blueprint $table) {
            $table->unsignedBigInteger('crew_worker_id')->primary();
            $table->date('start');
            $table->date('end');
            $table->boolean('status')->default(0);

            $table->foreign('crew_worker_id')->references('id')->on('crew_worker')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worker_breaks');
    }
}
