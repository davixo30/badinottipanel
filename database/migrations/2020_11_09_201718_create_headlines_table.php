<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateHeadlinesTable
 *
 * Utiliza softDeletes con el fin de poder quitar titulares y que posteriormente quede un registro de los trabajadores
 * que fueron titulares en ciertas embarcaciones
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 */
class CreateHeadlinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('headlines', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ship_id');
            $table->unsignedBigInteger('worker_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('ship_id')->references('id')->on('ships');
            $table->foreign('worker_id')->references('id')->on('workers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('headlines');
    }
}
