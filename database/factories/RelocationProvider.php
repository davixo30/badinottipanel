<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RelocationProvider;
use Faker\Generator as Faker;

$factory->define(RelocationProvider::class, function (Faker $faker) {
    return [
        'name' => $faker->words(2, true)
    ];
});
