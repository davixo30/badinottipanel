<?php

namespace App\Types;

/**
 * Class TicketStatusesTypes
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Types
 */
class TicketStatusesTypes
{
    /** @var string */
    public static $NO_COMPRADO = 'No comprado';

    /** @var string */
    public static $VOLADO = 'Volado';

    /** @var string */
    public static $POR_VOLAR = 'Por volar';

    /** @var string */
    public static $STOCK = 'En stock';
}
