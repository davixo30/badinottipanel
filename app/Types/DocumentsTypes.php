<?php

namespace App\Types;

/**
 * Class DocumentsTypes
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Types
 */
class DocumentsTypes
{
    /** @var string */
    public static $CARNET_IDENTIDAD = 'Carnet de identidad';

    /** @var string */
    public static $MATRICULA_EMBARQUE = 'Matrícula de embarque';

    /** @var string */
    public static $REGLAMENTO_INTERNO = 'Reglamento interno';

    /** @var string */
    public static $SEGURO_VIDA = 'Seguro de vida';
}
