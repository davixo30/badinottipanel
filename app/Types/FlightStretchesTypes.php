<?php

namespace App\Types;

/**
 * Class FlightStretchesTypes
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Types
 */
class FlightStretchesTypes
{
    /** @var string */
    public static $GOING = 'Ida';

    /** @var string */
    public static $RETURN = 'Vuelta';
}
