<?php

namespace App\Types;

/**
 * Class CrewStatusesTypes
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Types
 */
class CrewStatusesTypes
{
    /** @var string */
    public static $BORRADOR = 'Borrador';

    /** @var string */
    public static $FILTRO_CASO_CLIENTE = 'Filtro tripulación';

    /** @var string */
    public static $EN_CALENDARIO = 'En calendario';
}
