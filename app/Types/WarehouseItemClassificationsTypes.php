<?php

namespace App\Types;

/**
 * Class WarehouseItemClassificationsTypes
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Types
 */
class WarehouseItemClassificationsTypes
{
    /** @var string */
    public static $EPP_BOTA = 'EPP Bota';

    /** @var string  */
    public static $EPP_BOTIN = 'EPP Botín';

    /** @var string  */
    public static $EPP_OTROS = 'EPP Otros';

    /** @var string  */
    public static $EPP_ROPA = 'EPP Ropa';

    /** @var string  */
    public static $OTROS = 'Otros';
}
