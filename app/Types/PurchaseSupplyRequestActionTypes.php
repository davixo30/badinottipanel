<?php

namespace App\Types;

/**
 * Class PurchaseSupplyRequestActionTypes
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Types
 */
class PurchaseSupplyRequestActionTypes
{
    /** @var string */
    public static $PURCHASE = 'purchase';

    /** @var string */
    public static $WAREHOUSE = 'warehouse';
}
