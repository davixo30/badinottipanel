<?php

namespace App\Http\Requests\Eskuad;

/**
 * Class NewCrewRequest
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Requests\Eskuad
 */
class NewCrewRequest
{
    /** @var string */
    public $ship_code;

    /** @var object[] */
    public $workers;

    /**
     * NewCrewRequest constructor.
     *
     * @param $crew
     */
    public function __construct($crew)
    {
        $this->ship_code = $crew->ship->code;
        $this->workers = array_map(function ($w) {
            $worker = $w['worker'];

            return (object) [
                'name' => $worker['name'],
                'last_name' => $worker['last_name'],
                'rut' => $worker['rut'],
                'roles' => array_map(function ($role) {
                    return $role['name'];
                }, $worker['roles'])
            ];
        }, $crew->workers->toArray());
    }
}
