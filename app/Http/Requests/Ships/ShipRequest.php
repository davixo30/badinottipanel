<?php

namespace App\Http\Requests\Ships;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ShipRequest
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Requests\Ships
 */
class ShipRequest extends FormRequest
{
    // TODO: definir largos
    /** @var array */
    private $rules = [
        'name' => 'required|string|max:50',
        'code' => 'required|string|max:10|unique:ships',
        'plate' => 'required|string|max:50|unique:ships',
        'call_identifier' => 'required|string|max:15|unique:ships',
        'capacity' => 'required|int|min:1',
        'engines' => 'required|int|min:1',
        'generators' => 'required|int|min:1',
        'supervisor_id' => 'required|exists:users,id',
        'type_id' => 'required|exists:ship_types,id'
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('post')) {
            return $this->createRules();
        } elseif ($this->isMethod('put')) {
            return $this->updateRules();
        }
    }

    /**
     * Define validation rules to store method for resource creation
     *
     * @return array
     */
    public function createRules(): array
    {
        return $this->rules;
    }

    /**
     * Define validation rules to update method for resource update
     *
     * @return array
     */
    public function updateRules(): array
    {
        return array_merge($this->rules, [
            'code' => $this->rules['code'] . ',code,' . $this->get('id'),
            'plate' => $this->rules['plate'] . ',plate,' . $this->get('id'),
            'call_identifier' => $this->rules['call_identifier'] . ',call_identifier,' . $this->get('id')
        ]);
    }
}
