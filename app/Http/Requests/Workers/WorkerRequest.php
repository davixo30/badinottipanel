<?php

namespace App\Http\Requests\Workers;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class WorkerRequest
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Requests\Workers
 */
class WorkerRequest extends FormRequest
{
    // TODO: definir largos
    /** @var array */
    private $rules = [
        'name' => 'required|string',
        'last_name' => 'required|string',
        'code' => 'string|max:10|nullable',
        // TODO: validador de rut
        'rut' => 'required|string|unique:workers,id',
        'birthdate' => 'required|date',
        'phone_number' => 'required|string|max:9',
        'address' => 'required|string|max:255',
        'attribute_id' => 'required|exists:attributes,id',
        'region_id' => 'required|exists:regions,id',
        'commune_id' => 'required|exists:communes,id',
        'marital_status_id' => 'required|exists:marital_statuses,id'
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return $this->rules;
        if ($this->isMethod('post')) {
            return $this->createRules();
        } elseif ($this->isMethod('put')) {
            return $this->updateRules();
        }
    }

    /**
     * Define validation rules to store method for resource creation
     *
     * @return array
     */
    public function createRules(): array
    {
       // return $this->rules;
    }

    /**
     * Define validation rules to update method for resource update
     *
     * @return array
     */
    public function updateRules(): array
    {
        /*return array_merge($this->rules, [
            'rut' => $this->rules['rut'] . ',rut,' . $this->get('id')
        ]);*/

        /*return array_merge($this->rules, [
            'rut' => $this->rules['rut'] . ',rut,' . $this->get('id')
        ]);*/
    }
}
