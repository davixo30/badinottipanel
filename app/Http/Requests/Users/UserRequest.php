<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UserRequest
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Requests\Users
 */
class UserRequest extends FormRequest
{
    // TODO: definir largos
    /** @var array */
    private $rules = [
        'name' => 'required|string',
        'last_name' => 'required|string',
        // TODO: validador de rut
        'code' => 'required|string|max:10|unique:users',
        'address' => 'required|string',
        'birthdate' => 'required|date',
        'rut' => 'required|string|unique:users',
        'email' => 'required|email|unique:users',
        'phone_number' => 'required|string|unique:users',
        'position_id' => 'required|exists:positions,id',
        'marital_status_id' => 'required|exists:marital_statuses,id',
        'commune_id' => 'required|exists:communes,id'
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    /* TODO: verificar si borrar
    public function authorize()
    {
        return true;
    }
    */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('post')) {
            return $this->createRules();
        } elseif ($this->isMethod('put')) {
            return $this->updateRules();
        }
    }

    /**
     * Define validation rules to store method for resource creation
     *
     * @return array
     */
    public function createRules(): array
    {
        return $this->rules;
    }

    /**
     * Define validation rules to update method for resource update
     *
     * @return array
     */
    public function updateRules(): array
    {
        return array_merge($this->rules, [
            'code' => $this->rules['code'] . ',code,' . $this->get('id'),
            'rut' => $this->rules['rut'] . ',rut,' . $this->get('id'),
            'email' => $this->rules['email'] . ',email,' . $this->get('id'),
            'phone_number' => $this->rules['phone_number'] . ',phone_number,' . $this->get('id')
        ]);
    }
}
