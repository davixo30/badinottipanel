<?php

namespace App\Http\Controllers;

use App\Http\Controllers\API\V1\BaseController;
use GuzzleHttp\Client;
use App\Http\Requests\Eskuad\NewCrewRequest;
use App\Models\Crew;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Models\Worker;
use App\Models\WorkerAttribute;
use App\Models\Contract;

/**
 * Class ClientController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class EskuadController extends BaseController
{
    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //    $this->middleware('auth:api');
    // }

    public static function uploadWorkers() {
        $filePath = public_path('badi2.xlsx');
        // Check if the file exists
        if (file_exists($filePath)) {
            // Load the spreadsheet
            $spreadsheet = IOFactory::load($filePath);

            // Get the first sheet in the spreadsheet
            $sheet = $spreadsheet->getActiveSheet();

            // Get the highest row and column numbers
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            $records = Worker::all();
            // Delete each record
            foreach ($records as $record) {
                $record->delete();
            }
      
            // Iterate through rows and columns to read data
            $data = [];
            for ($row = 3; $row <= $highestRow; $row++) {
                // $rowData = [];
                // for ($col = 'A'; $col <= $highestColumn; $col++) {
                //     $cellValue = $sheet->getCell($col . $row)->getValue();
                //     $rowData[] = $cellValue;
                // }
                // $data[] = $rowData;
                DB::transaction(function () use ($sheet, $highestRow, $highestColumn,$row) {
                $rut = str_replace('.', '', $sheet->getCell('C'. $row)->getValue());
                $wk = DB::table('workers')->where('rut',$rut)->first();
                // dd($worker);
                $worker = null;
                if (!$wk) {
                    $worker = new Worker;
                    // no existe atributo por defecto disponible
                    $worker->attribute_id = 10;
                    // no existe estado civil por defecto soltero
                    $worker->marital_status_id = 1;
                } else {
                    DB::table('workers')->where('id',$wk->id)->update(['deleted_at' => null]);
                    $worker = Worker::find($wk->id);
                }
                $worker->name = $sheet->getCell('A'. $row)->getValue();
                $worker->last_name = $sheet->getCell('B'. $row)->getValue();
                $worker->avatar = '';
                //$worker->code = $row;
                $worker->rut = $rut;
                // hacer split de fecha
                $fecha = explode("/", $sheet->getCell('G'. $row)->getValue());
                $dia = $fecha[0];
                $mes = $fecha[1];
                $anio = $fecha[2];
                $worker->birthdate = $anio."-".$mes."-".$dia;
                //fin fecha
                $worker->phone_number = $sheet->getCell('M'. $row)->getValue();
                $worker->address = $sheet->getCell('N'. $row)->getValue();
                // buscar comuna por nombre
                $comuna = DB::table("communes")->where('name', $sheet->getCell('O'. $row)->getValue())->first();
                $worker->commune_id = $comuna->id;
                // fin comuna
                $worker->send_email = 1;
                $worker->red_day = 0;
                $worker->deleted_at = null;
                $worker->save();
                $worker->code = $worker->id;
                $worker->save();

                if (!$wk) {
                    $w = new WorkerAttribute;
                    $w->worker_id = $worker->id;
                    $w->attribute_id = 10;
                    $w->save(); 
                } 
                DB::table('role_worker')
                    ->where('worker_id', $worker->id)
                    ->delete();
                    
                $roles = explode(",", $sheet->getCell('F'. $row)->getValue());
                for ($i = 0; $i < sizeOf($roles); $i++) {
                        $rol = DB::table('roles')->where('name',$roles[$i])->first();
                        DB::table('role_worker')->insert(['role_id' => $rol->id,'worker_id' => $worker->id]);
                }

                $contract = Contract::find($worker->id);
                $fecha_contrato = explode("/", $sheet->getCell('I'. $row)->getValue());
                $dia_contrato = $fecha_contrato[0];
                $mes_contrato = $fecha_contrato[1];
                $anio_contrato = $fecha_contrato[2];
                $fecha_contrato = $anio_contrato."-".$mes_contrato."-".$dia_contrato;
                $prevision = DB::table('afps')->where('name', $sheet->getCell('Z'. $row)->getValue())->first();
                $salud = DB::table('health_forecasts')->where('name', $sheet->getCell('AA'. $row)->getValue())->first();
                $tipo_contrato = DB::table('contract_types')->where('name', $sheet->getCell('J'. $row)->getValue())->first();
                if (!$contract) {
                    DB::table('contracts')->insert(['worker_id'=> $worker->id,'uploader_id'=>25,'date'=> $fecha_contrato,'salary'=>1,'afp_id'=>$prevision->id,'health_forecast_id'=>$salud->id,'contract_type_id'=>$tipo_contrato->id]);
                } else {
                    DB::table('contracts')->where('worker_id', $worker->id)->update(['date'=> $fecha_contrato,'salary'=>1,'afp_id'=>$prevision->id,'health_forecast_id'=>$salud->id,'contract_type_id'=>$tipo_contrato->id]);
                }
                //dd($sheet->getCell('AC'. $row)->getValue());
                //REVISAR EXPLODE FORMATO
                $v_cedula = $sheet->getCell('S'. $row)->getValue();//1    
                if ($v_cedula !== '') {
                    if (strpos($v_cedula, "/") !== false) {
                        $v_cedula = explode("/", $sheet->getCell('S'. $row)->getValue());//1  
                        $dia_cedula = $v_cedula[0];
                        $mes_cedula = $v_cedula[1];
                        $anio_cedula = $v_cedula[2];
                        $fecha_cedula = $anio_cedula."-".$mes_cedula."-".$dia_cedula;
                        DB::table('documents')->where('worker_id', $worker->id)->where('document_type_id',1)->delete();
                        DB::table('documents')->insert(['worker_id'=> $worker->id,'uploader_id'=>25,'expiration_date'=> $fecha_cedula,'document_type_id'=>1]);
                    }
                }

                $v_matricula = $sheet->getCell('AC'. $row)->getValue();//2
                if ($v_matricula !== '') {
                    if (strpos($v_matricula, "/") !== false) {
                        $v_matricula = explode("/", $sheet->getCell('AC'. $row)->getValue());//2
                        $dia_matricula = $v_matricula[0];
                        $mes_matricula = $v_matricula[1];
                        $anio_matricula = $v_matricula[2];
                        $fecha_matricula = $anio_matricula."-".$mes_matricula."-".$dia_matricula;
                        DB::table('documents')->where('worker_id', $worker->id)->where('document_type_id',2)->delete();
                        DB::table('documents')->insert(['worker_id'=> $worker->id,'uploader_id'=>25,'expiration_date'=> $fecha_matricula,'document_type_id'=>2]);
                    }
                }

                $v_interno = $sheet->getCell('Y'. $row)->getValue();//3
                if ($v_interno !== '') {
                    if (strpos($v_interno, "/") !== false) {
                        $v_interno = explode("/", $sheet->getCell('Y'. $row)->getValue());//3
                        $dia_interno = $v_interno[0];
                        $mes_interno = $v_interno[1];
                        $anio_interno = $v_interno[2];
                        $fecha_interno = $anio_interno."-".$mes_interno."-".$dia_interno;
                        DB::table('documents')->where('worker_id', $worker->id)->where('document_type_id',3)->delete();
                        DB::table('documents')->insert(['worker_id'=> $worker->id,'uploader_id'=>25,'expiration_date'=> $fecha_interno,'document_type_id'=>3]);
                    }
                }
               });
               if ($row === 104) {
                break;
            }
            }

            return response()->json();
            // $data now contains the data from the spreadsheet
           // dd($data); // Display the data (for demonstration purposes)
        } else {
            // File doesn't exist
            dd("File not found");
            return response()->json(['message'=>'File not found']);
        }
    }

    public static function newShip($shipId)
    {

        $ship = DB::table("ships")->where("id",$shipId)->select("external_code","name","id")->first();
        if ($ship) {
            $crewData = [];
            $person = (object) [
                'nombres' => 'Undefined',
                'apellidos' => 'Undefined',
                'rut' => 'Undefined',
                'rol' => 'Undefined',
                'tripulantes' => 'Undefined',
                'informacion' => 'Undefined',
            ];
            array_push($crewData, $person);
            $datasource = (object) [
                'name' => 'Tripulacion '.$ship->name,
                'rows' => $crewData,
            ];
            $url = env('ESKUAD_API_URL')."/api/v1/data-sources";
            $headers = [
                'headers' => [
                    'api-key' => env('ESKUAD_API_KEY'),
                ],
                'json' => $datasource,
            ];
            $client = new Client(['verify' => false]);
            $res = $client->request('POST', $url, $headers);
            $result = $res->getBody()->getContents();
            if ($res->getStatusCode() === 200) {
                $data = json_decode($result, true);
                DB::table('ships')->where('id',$ship->id)->update(['external_code' => $data['id']]);
            }
        }
        return $ship;
    }

    public static function updateDatasource($crewId, $updateEskuad)
    {
        $dia = DB::table("crews")->where("id",$crewId)->get();
        if($dia){
            foreach($dia as $d){
                try {
                    $ship = DB::table("ships")->where("id",$d->ship_id)->select("external_code","name","id")->first();
                    $d->external_code = $ship->external_code;
                    $crew = Crew::with('ship:id,code', 'workers')->findOrFail($d->id);
                    foreach($crew->workers as $key => $worker){
                        if($worker->worker==null){
                            $crew->workers->forget($key);
                        }
                    }
                    $payload = new NewCrewRequest($crew);
                    $request = json_decode(json_encode($payload));
    
                    $crewData = [];
                    foreach($request->workers as $w){
                        $rut = str_replace(".","",$w->rut);
                        $roles = '';
                        if (sizeOf($w->roles) == 0) {
                            $roles = 'Sin rol definido';
                        } else {
                            foreach($w->roles as $key => $value){
                                $roles = $roles.$value.',';    
                                break;          
                            }
                        }
    
                        if (substr($roles, -1) === ',') {
                            $roles = rtrim($roles, ',');
                        }
    
                        $person = (object) [
                            'nombres' => $w->name,
                            'apellidos' => $w->last_name,
                            'rut' => $rut,
                            'rol' => $roles,
                            'tripulantes' => $w->name.' '.$w->last_name,
                            'tripulantes_' => $w->name.' '.$w->last_name,
                            'informacion' => $w->name.' '.$w->last_name.' - Rut:'.$rut.' - Rol:'.$roles,
                        ];
                        array_push($crewData, $person);
                    }
    
                    $datasource = (object) [
                        'name' => 'Tripulacion '.$ship->name,
                        'rows' => $crewData,
                    ];
                    
                    if ($ship->external_code === null) {
                        $url = env('ESKUAD_API_URL')."/api/v1/data-sources";
                        $headers = [
                            'headers' => [
                                'api-key' => env('ESKUAD_API_KEY'),
                            ],
                            'json' => $datasource,
                        ];
                        $client = new Client(['verify' => false]);
                        $res = $client->request('POST', $url, $headers);
                        $result = $res->getBody()->getContents();
                        if ($res->getStatusCode() === 200) {
                            $data = json_decode($result, true);
                            if ($updateEskuad === true) {
                                DB::table('crews')->where('id',$d->id)->update(['update_eskuad' => 1]);
                            }
                            DB::table('ships')->where('id',$ship->id)->update(['external_code' => $data['id']]);
                        }
                    } else {
                        $url = env('ESKUAD_API_URL')."/api/v1/data-sources/".$ship->external_code;
                        $headers = [
                            'headers' => [
                                'api-key' => env('ESKUAD_API_KEY'),
                            ],
                            'json' => $datasource,
                        ];
                        $client = new Client(['verify' => false]);
                        $res = $client->request('PATCH', $url, $headers);
                        $result = $res->getBody()->getContents();
                        if ($res->getStatusCode() === 200) {
                            if ($updateEskuad === true) {
                                DB::table('crews')->where('id',$d->id)->update(['update_eskuad' => 1]);
                            }
                        }
                    }
                } catch (Exception $e) {
                    \Log::info("Error Updating");
                    \Log::info("Error Update Eskuad Datasource :". $e);
                }
            }
    }
    return $dia;
    }
}