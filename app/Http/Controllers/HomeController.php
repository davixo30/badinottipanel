<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\WorkerAttribute;
use App\Models\PendingAttribute;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use DateTime;




class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

       return redirect('dashboard');
    }

    public function ajustaembarcados(int $id){
        return "watafac";
    }


    public function exportSupplyrequest(int $id){
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $dt1 = new DateTime();
        $today = $dt1->format("Y-m-d");


        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $inicio = 1;
        $provisiones = DB::table("provisions")->where("crew_id",$id)->first();
        if($provisiones){
            $sheet->setCellValue('A'.$inicio, "Viveres para ".$provisiones->workers_quantity." persona(s)");
            $sheet->mergeCells('A'.$inicio.':E'.$inicio);
            $sheet->getStyle('A'.$inicio)->getFont()->setBold(true);
            $sheet->getStyle('A'.$inicio)->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A'.$inicio.":E".$inicio)->applyFromArray($styleArray); 
            $sheet->getStyle("A".$inicio)->getFont()->setSize(18);
            $inicio = $inicio +2;
        }
      
      

        

        $supply = DB::table("supply_request_item as e")
        ->where("e.supply_request_id",$id)
        ->get();
        foreach($supply as $e){
               $latalla = "";
                $elitem = "";
                $launidad = "";
                
                $talla = DB::table("warehouse_item_sizes")->where("id",$e->warehouse_item_size_id)->first();
                if($talla){
                    $latalla = $talla->name;
                    $itemepp = DB::table("warehouse_items")->where("id",$talla->warehouse_item_id)->first();
                    if($itemepp){
                        $elitem = $itemepp->name;
                        $unidad = DB::table("measurement_units")->where("id",$itemepp->measurement_unit_id)->first();
                        if($unidad){
                            $launidad = $unidad->name;
                        }
                    }
                }
                $e->item = $elitem;
                $e->size = $latalla;
                $e->measurement = $launidad;

                
                
        }

        $inicioarticulos = $inicio;

        $sheet->setCellValue('A'.$inicio, "Artículos de bodega");
        $sheet->mergeCells('A'.$inicio.':E'.$inicio);
        $sheet->getStyle('A'.$inicio)->getFont()->setBold(true);
        $sheet->getStyle('A'.$inicio)->getAlignment()->setHorizontal('center');
        $inicio = $inicio +1;
        $sheet->setCellValue('A'.$inicio, "Articulo");
        $sheet->setCellValue('B'.$inicio, "Talla");
        $sheet->setCellValue('C'.$inicio, "Cantidad");
        $sheet->setCellValue('D'.$inicio, "Unidad Medida");
        $sheet->setCellValue('E'.$inicio, "Observacion");
        $sheet->getStyle('A'.$inicio.':E'.$inicio)->getFont()->setBold(true);
        $sheet->getStyle('A'.$inicio.':E'.$inicio)->getAlignment()->setHorizontal('center');
        $inicio = $inicio +1;
        foreach($supply as $e){
            $sheet->setCellValue('A'.$inicio, $e->item);
            $sheet->setCellValue('B'.$inicio, $e->size);
            $sheet->setCellValue('C'.$inicio, $e->quantity);
            $sheet->setCellValue('D'.$inicio, $e->measurement);
            $sheet->setCellValue('E'.$inicio, $e->observations);
            $inicio = $inicio +1;
        }

        $sheet->getStyle('A'.$inicioarticulos.":E".($inicio-1))->applyFromArray($styleArray); 

        $inicio = $inicio +2;
        $inicioepp = $inicio;

       $epp = DB::table("epp_delivery_supply_request as e")
               ->join("workers as w","w.id","=","e.worker_id")
               ->where("e.supply_request_id",$id)
               ->select("w.name","w.last_name","w.rut","w.id","e.warehouse_item_size_id")
               ->get();
        
               foreach($epp as $e){
                $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$e->id)->get();
                $rol = "Sin Rol";
                foreach($roles as $role){
                    if($rol == "Sin Rol"){
                        $rol = "";
                    }
                    if($rol==""){
                        $rol = $role->name;
                    }else{
                        $rol = $rol.",".$role->name;
                    }
                }
                $e->role = $rol;

                $latalla = "";
                $elitem = "";
                
                $talla = DB::table("warehouse_item_sizes")->where("id",$e->warehouse_item_size_id)->first();
                if($talla){
                    $latalla = $talla->name;
                    $itemepp = DB::table("warehouse_items")->where("id",$talla->warehouse_item_id)->first();
                    if($itemepp){
                        $elitem = $itemepp->name;
                    }
                }
                $e->item = $elitem;
                $e->size = $latalla;
               }

            
            $sheet->setCellValue('A'.$inicio, "Entrega de EPP");
            $sheet->mergeCells('A'.$inicio.":E".$inicio);
            $sheet->getStyle('A'.$inicio)->getFont()->setBold(true);
            $sheet->getStyle('A'.$inicio)->getAlignment()->setHorizontal('center');
            $inicio = $inicio +1;
            $sheet->setCellValue('A'.$inicio, "Nombre");
            $sheet->setCellValue('B'.$inicio, "Rut");
            $sheet->setCellValue('C'.$inicio, "Rol");
            $sheet->setCellValue('D'.$inicio, "EPP");
            $sheet->setCellValue('E'.$inicio, "Talla");
            $sheet->getStyle('A'.$inicio.':E'.$inicio)->getFont()->setBold(true);
            $sheet->getStyle('A'.$inicio.':E'.$inicio)->getAlignment()->setHorizontal('center');
            $inicio = $inicio +1;
            foreach($epp as $e){
                $sheet->setCellValue('A'.$inicio, $e->name." ".$e->last_name);
                $sheet->setCellValue('B'.$inicio, $e->rut);
                $sheet->setCellValue('C'.$inicio, $e->role);
                $sheet->setCellValue('D'.$inicio, $e->item);
                $sheet->setCellValue('E'.$inicio, $e->size);
                $inicio = $inicio +1;
            }

            $sheet->getStyle('A'.$inicioepp.":E".($inicio-1))->applyFromArray($styleArray); 
      
            foreach(range('A','E') as $columnID) {
                $sheet->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

    

        try {
            $writer = new Xlsx($spreadsheet);
            $file = 'Solicitud Abastecimiento '.$today.'.xlsx';
            header("Content-Description: File Transfer");
            header('Content-Disposition: attachment; filename="' . $file . '"');
            header('Content-type: application/vnd.ms-excel');
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Expires: 0');     
            $writer->save("php://output");
        } catch (\PHPExcel_Reader_Exception $e) {
            return response("Error al generar documento solicitado", 500);
        }
    }


    public function ajustaembarcados222(int $id){

        $workers = DB::table("crews")
                    ->join("crew_worker","crew_worker.crew_id","=","crews.id")
                    ->join("workers","workers.id","=","crew_worker.worker_id")
                    ->where("crews.id",$id)
                    ->where("workers.deleted_at",null)
                    ->select("crew_worker.late_entry","crew_worker.with_emergency","crew_worker.worker_id as worker_id","crews.real_download_date","crews.id","crews.upload_date","crew_worker.entry_date")
                    ->get();



                    foreach($workers as $worker){
                        if($worker->with_emergency==0){
                            DB::table("workers")->where("id",$worker->worker_id)->update(["attribute_id"=>1]);
                            DB::table("worker_attributes")->where("worker_id",$worker->worker_id)->where("crew_id",$worker->id)->where("attribute_id",1)->delete();
                            DB::table("pending_attributes")->where("worker_id",$worker->worker_id)->where("crew_id",$worker->id)->where("attribute_id",2)->delete();
    
                            if($worker->entry_date!=null){
                                $w = new WorkerAttribute;
                                $w->worker_id = $worker->worker_id;
                                $w->attribute_id = 1;
                                $w->date_start = $worker->entry_date;
                                $w->date_end = date('Y-m-d', strtotime($worker->real_download_date));
                                $w->crew_id = $worker->id;
                                $w->save();
    
                                $np = new PendingAttribute;
                                $np->attribute_id = 2;
                                $np->worker_id = $worker->worker_id;
                                $np->date = date('Y-m-d', strtotime($worker->real_download_date. ' + 1 days'));
                                $np->status = 0;
                                $np->crew_id = $worker->id;
                                $np->save();
                            }else{
                                $w = new WorkerAttribute;
                                $w->worker_id = $worker->worker_id;
                                $w->attribute_id = 1;
                                $w->date_start = $worker->upload_date;
                                $w->date_end = date('Y-m-d', strtotime($worker->real_download_date));
                                $w->crew_id = $worker->id;
                                $w->save();
    
                                $np = new PendingAttribute;
                                $np->attribute_id = 2;
                                $np->worker_id = $worker->worker_id;
                                $np->date = date('Y-m-d', strtotime($worker->real_download_date. ' + 1 days'));
                                $np->status = 0;
                                $np->crew_id = $worker->id;
                                $np->save();
                            }
                        }else{
                            DB::table("workers")->where("id",$worker->worker_id)->update(["attribute_id"=>2]);
                            DB::table("worker_attributes")->where("worker_id",$worker->worker_id)->where("crew_id",$worker->id)->where("attribute_id",2)->delete();
                            DB::table("pending_attributes")->where("worker_id",$worker->worker_id)->where("crew_id",$worker->id)->where("attribute_id",10)->delete();
    
                            $inicio = date('Y-m-d', strtotime($worker->real_download_date. ' + 1 days'));
                            $fin = date('Y-m-d', strtotime($worker->real_download_date.' + 21 days'));
                            $disponible = date('Y-m-d', strtotime($worker->real_download_date. ' + 22 days'));
                            if($worker->late_entry==1){
                                $inicio = date('Y-m-d', strtotime($worker->entry_date. ' + 1 days'));
                                $fin = date('Y-m-d', strtotime($worker->entry_date.' + 21 days'));
                                $disponible = date('Y-m-d', strtotime($worker->entry_date. ' + 22 days'));
                            }

                            $w = new WorkerAttribute;
                            $w->worker_id = $worker->worker_id;
                            $w->attribute_id = 2;
                            $w->date_start = $inicio;
                            $w->date_end = $fin;
                            $w->crew_id = $worker->id;
                            $w->save();
                        // Log::info("Pending attribute 2");
                            $np = new PendingAttribute;
                            $np->attribute_id = 10;
                            $np->worker_id = $worker->worker_id;
                            $np->date = $disponible;
                            $np->crew_id = $worker->id;
                            $np->status = 0;
                            $np->save();
                        }
                        
                    }

            return $workers;
    }

}
