<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\AFP;
use Illuminate\Http\Response;

/**
 * Class AFPController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class AFPController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el listado de afps
     *
     * @return Response
     */
    public function __invoke()
    {
        return $this->sendResponse(AFP::all(), 'AFP list');
    }
}
