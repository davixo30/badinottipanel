<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Http\Requests\Ships\ShipRequest;
use App\Models\Certificate;
use App\Models\Headline;
use App\Models\Ship;
use App\Models\ShipCareen;
use App\Models\ShipReview;
use App\Models\TechnicalSpecifications;
use App\Models\Worker;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\EskuadController;

/**
 * Class ShipController
 *
 * @author Juan Gamonal H <juangamonal@gmail.com>
 * @package App\Http\Controllers\API
 */
class ShipController extends BaseController
{
    /** @var string[] */
    private $requestOnly = [
        'name', 'code', 'plate', 'call_identifier', 'capacity',
        'engines', 'generators', 'supervisor_id', 'type_id'
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el listado de embarcaciones
     *
     * @return Response
     */
    public function index()
    {
        return $this->sendResponse(
            Ship::orderBy('name','asc')->with('supervisor:id,name,last_name')->get(),
            'Ships list'
        );
    }

    /**
     * Obtiene la ficha de una embarcación
     * TODO: refactorizar, no necesita tanto hueveo, ademas usar eloquent
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(int $id)
    {
        // TODO: scar estos DB y pasar a modelos
        $ship = Ship::with('headlines', 'supervisor:id,name,last_name')->findOrFail($id);
        $certificates = DB::table('certificates')
            ->select('certificates.id', 'certificates.name')
            ->leftJoin(
                'certificate_ship',
                'certificates.id',
                '=',
                'certificate_ship.certificate_id'
            )
            ->where('certificates.ship_type_id', $ship->type_id)
            ->groupBy('certificates.id')
            ->get();

        foreach ($certificates as $certificate) {
            $certificate->certificate_ship = DB::table('certificate_ship')
                ->select('emission', 'expiration', 'certificate', 'created_at')
                ->where('ship_id', $id)
                ->where('certificate_id', $certificate->id)
                ->latest('created_at')
                ->first();
        }

        $ship->certifications = $certificates;

        return $this->sendResponse(
            $ship,
            'Embarcación encontrada'
        );
    }

    /**
     * Almacena una nueva embarcación
     *
     * @param ShipRequest $request
     *
     * @return Response
     */
    public function store(ShipRequest $request)
    {   
        $ship = Ship::create($request->only($this->requestOnly));
        $data = $this->sendResponse(
            $ship,
            'Embarcación creada correctamente'
        );

        EskuadController::newShip($ship->id);
        return $data;
    }

    /**
     * Modifica una embarcación existente
     *
     * @param ShipRequest $request
     * @param int $id
     *
     * @return Response
     */
    public function update(ShipRequest $request, int $id)
    {
        return $this->sendResponse(
            Ship::findOrFail($id)->update($request->only($this->requestOnly)),
            'Embarcación editada correctamente'
        );
    }

    /**
     * Elimina una embarcación existente
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy(int $id)
    {
        return $this->sendResponse(
            Ship::findOrFail($id)->delete(),
            'Embarcación eliminada correctamente'
        );
    }

    /**
     * Obtiene estado de certificados de embarcación
     *
     * @param int $id
     *
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getCertificates(int $id)
    {
        $this->validate(new Request(['id' => $id]), ['id' => 'required|exists:ships']);

        return $this->sendResponse(Ship::getCertificates($id), 'Certificados de la embarcación');
    }

    public function getPlans(int $id)
    {
        $this->validate(new Request(['id' => $id]), ['id' => 'required|exists:ships']);

        return $this->sendResponse(Ship::getPlans($id), 'Planos de la embarcación');
    }

    /**
     * Guarda estado de certificados de embarcación
     *
     * @param int $shipId
     * @param Request $request
     *
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function createCertificate(int $shipId, Request $request)
    {
        // valida los datos
        // TODO: como verificar esta validacion?
        // $this->validate(new Request(['ship_id' => $shipId]), ['ship_id' => 'required|exists:ships,id']);
        $this->validate($request, [
            'certificate_id' => 'required|exists:certificates,id',
            'emission' => 'required|date',
            'expiration' => 'required|date',
            'certificate' => 'required|file|mimes:pdf'
        ]);

        // almacena el archivo
        $certificate = $request->file('certificate');
        $fileName = time() . '.' . $certificate->extension();
        $spaceEnv = env('DO_SPACE_ENV');
        Storage::putFileAs("$spaceEnv/ship-certificates", $certificate, $fileName);

        return $this->sendResponse(
            Ship::createCertificate(array_merge($request->only(
                'certificate_id',
                'emission',
                'expiration'
            ), ['ship_id' => $shipId, 'certificate' => $fileName])),
            'Certificación creada correctamente'
        );
    }

    public function createPlans(int $shipId, Request $request)
    {
        // valida los datos
        // TODO: como verificar esta validacion?
        // $this->validate(new Request(['ship_id' => $shipId]), ['ship_id' => 'required|exists:ships,id']);
        $this->validate($request, [
            'plans_id' => 'required|exists:ship_plans_types,id',
            'certificate' => 'required|file|mimes:pdf'
        ]);

        // almacena el archivo
        $certificate = $request->file('certificate');
        $fileName = time() . '.' . $certificate->extension();
        $spaceEnv = env('DO_SPACE_ENV');
        Storage::putFileAs("$spaceEnv/ship-plans", $certificate, $fileName);

        return $this->sendResponse(
            Ship::createPlans(['ship_id' => $shipId,'description'=>'','date'=>date('Y-m-d H:i:s'),'filename' => $fileName,'ship_plans_type_id'=>$request->plans_id]),
            'Certificación creada correctamente'
        );
    }

    /**
     * Registra una nueva revista
     *
     * @param int $shipId
     * @param Request $request
     *
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function createReview(int $shipId, Request $request)
    {
        // parsea la peticion
        if ($request->get('observations') === 'null') {
            $request->merge(['observations' => null]);
        }

        $this->validate($request, [
            'review_date' => 'required|date',
            'observations' => 'present|nullable|string|max:255',
            'certificate' => 'required|file|mimes:pdf'
        ]);

        // almacena el archivo
        $certificate = $request->file('certificate');
        $fileName = time() . '.' . $certificate->extension();
        $spaceEnv = env('DO_SPACE_ENV');
        Storage::putFileAs("$spaceEnv/ship-reviews", $certificate, $fileName);

        return $this->sendResponse(
            ShipReview::create(array_merge($request->only(
                'observations',
                'review_date'
            ), ['ship_id' => $shipId, 'certificate' => $fileName])),
            'Revista creada'
        );
    }

    /**
     * Obtiene el listado de revistas registradas en la embarcación
     *
     * @param int $shipId
     *
     * @return Response
     */
    public function getReviews(int $shipId)
    {
        return $this->sendResponse(
            ShipReview::where('ship_id', $shipId)->with('uploader:id,name,last_name')->paginate(10),
            'Revistas obtenidas'
        );
    }

    /**
     * Registra una nueva carena
     *
     * @param int $shipId
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return Response
     */
    public function createCareen(int $shipId, Request $request)
    {
        // parsea la peticion
        if ($request->get('observations') === 'null') {
            $request->merge(['observations' => null]);
        }

        $this->validate($request, [
            'careen_date' => 'required|date',
            'observations' => 'present|nullable|string|max:255',
            'certificate' => 'required|file|mimes:pdf'
        ]);

        // almacena el archivo
        $certificate = $request->file('certificate');
        $fileName = time() . '.' . $certificate->extension();
        $spaceEnv = env('DO_SPACE_ENV');
        Storage::putFileAs("$spaceEnv/ship-careens", $certificate, $fileName);

        return $this->sendResponse(
            ShipCareen::create(array_merge($request->only(
                'observations',
                'careen_date'
            ), ['ship_id' => $shipId, 'certificate' => $fileName])),
            'Carena creada'
        );
    }

    /**
     * Obtiene el listado de carenas registradas en la embarcación
     *
     * @param int $shipId
     *
     * @return Response
     */
    public function getCareens(int $shipId)
    {
        return $this->sendResponse(
            ShipCareen::where('ship_id', $shipId)->with('uploader:id,name,last_name')->paginate(10),
            'Carenas obtenidas'
        );
    }

    /**
     * Obtiene el listado de trabajadores disponibles para asignar titulares
     *
     * @param int $shipId
     *
     * @return Response
     */
    public function getAvailableWorkersForHeadline(int $shipId)
    {
        return $this->sendResponse(
            Worker::getAvailableForShipHeadline($shipId)->orderBy("name","asc")->with('roles')->paginate(5),
            'Listado de trabajadores'
        );
    }

    /**
     * Obtiene el listado de titulares de una embarcación
     *
     * @param int $shipId
     *
     * @return Response
     */
    public function getHeadlines(int $shipId)
    {
        return $this->sendResponse(
            Headline::with('worker')->where('ship_id', $shipId)->get(),
            'Listado de titulares'
        );
    }

    /**
     * Crea nuevos titulares en la embarcación
     *
     * @param int $shipId
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return Response
     */
    public function setHeadlines(int $shipId, Request $request)
    {
        $this->validate($request, [
            'workers' => 'present|array',
            'workers.*' => 'exists:workers,id'
        ]);

        return $this->sendResponse(
            Ship::setHeadlines($shipId, $request->get('workers')),
            'Titulares creados'
        );
    }

    /**
     * Crea nuevos titulares en la embarcación
     *
     * @param int $shipId
     * @param int $workerId
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return Response
     */
    public function deleteHeadline(int $shipId, int $workerId)
    {
        // TODO: validar así?
        $this->validate(new Request(['ship' => $shipId, 'worker' => $workerId]), [
            'ship' => 'exists:ships,id',
            'worker' => 'exists:workers,id'
        ]);

        $headline = Headline::where('ship_id', $shipId)->where('worker_id', $workerId)->first();
        Headline::destroy($headline->id);

        return $this->sendResponse('Ok', 'Titular eliminado');
    }

    /**
     * Obtiene las especificaciones técnicas de una embarcación
     *
     * @param int $shipId
     *
     * @return Response
     */
    public function getTechnicalSpecifications(int $shipId)
    {
        return $this->sendResponse(TechnicalSpecifications::find($shipId), 'Ship found');
    }

    /**
     * Guarda las especificaciones técnicas de una embarcación
     *
     * @param int $shipId
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return Response
     */
    public function setTechnicalSpecifications(int $shipId, Request $request)
    {
        $this->validate($request, [
            'shipowner' => 'nullable|string|max:255',
            'plate_port' => 'nullable|string|max:255',
            'trg' => 'nullable|numeric|min:0',
            'bollard_pull' => 'nullable|numeric|min:0',
            'light_displacement' => 'nullable|numeric|min:0',
            'year_of_construction' => 'nullable|int|min:0',
            'country' => 'nullable|string|max:255',
            'shipyards' => 'nullable|string|max:255',
            'total_length' => 'nullable|numeric|min:0',
            'sleeve' => 'nullable|numeric|min:0',
            'strut_to_main_deck' => 'nullable|numeric|min:0',
            'fuel_tank_capacity' => 'nullable|numeric|min:0',
            'fresh_water_pond_capacity' => 'nullable|numeric|min:0',
            'observations' => 'nullable|string|max:255',
            'main_engine' => 'nullable|string|max:255',
            'power' => 'nullable|string|max:255',
            'motor_series' => 'nullable|string|max:255',
            'transmission_box' => 'nullable|string|max:255',
            'transmission_box_series' => 'nullable|string|max:255',
            'relation' => 'nullable|string|max:255',
            'cruising_speed' => 'nullable|numeric|min:0',
            'fuel_consumption' => 'nullable|numeric|min:0',
            'propeller_type' => 'nullable|string|max:255'
        ]);

        $specifications = TechnicalSpecifications::find($shipId);

        if ($specifications) {
            $specifications->fill($request->all());
        } else {
            $specifications = new TechnicalSpecifications(array_merge($request->all(), ['ship_id' => $shipId]));
        }

        return $this->sendResponse($specifications->save(), 'Technical specifications saved');
    }
}
