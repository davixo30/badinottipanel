<?php

namespace App\Http\Controllers\API\Warehouse;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\WarehouseItem;
use Illuminate\Http\Request;

/**
 * Class WarehouseItemController
 * TODO max de atributos en validaciones de crear y editar
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Warehouse
 */
class WarehouseItemController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el listado de artículos de bodega
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->sendResponse(
            WarehouseItem::orderBy('name','asc')->with('classification', 'measurement_unit', 'sizes')->get(),
            'Warehouse items list'
        );
    }

    /**
     * Obtiene el listado de artículos de bodega con sus tallas
     *
     * @return \Illuminate\Http\Response
     */
    public function withSizes()
    {
        return $this->sendResponse(
            WarehouseItem::with('classification', 'measurement_unit', 'sizes')->get(),
            'Warehouse items list'
        );
    }

    /**
     * Obtiene un artículo y toda su información asociada
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return $this->sendResponse(WarehouseItem::findOrFail($id), 'Artículo encontrado');
    }

    /**
     * Almacena un nuevo artículo de bodega
     *
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:warehouse_items,name',
            'price' => 'required|int|min:0',
            'critical_stock' => 'present|nullable|int|min:0',
            'response_time' => 'present|nullable|int|min:0',
            'classification_id' => 'required|exists:warehouse_item_classifications,id',
            'measurement_unit_id' => 'required|exists:measurement_units,id'
        ]);

        return $this->sendResponse(
            WarehouseItem::create($request->only([
                'name',
                'price',
                'critical_stock',
                'response_time',
                'classification_id',
                'measurement_unit_id'
            ])),
            'Artículo creado correctamente'
        );
    }

    /**
     * Modifica un artículo existente
     *
     * @param int $id
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function update(int $id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:warehouse_items,name,' . $id,
            'price' => 'required|int|min:0',
            'critical_stock' => 'present|nullable|int|min:0',
            'response_time' => 'present|nullable|int|min:0',
            'classification_id' => 'required|exists:warehouse_item_classifications,id',
            'measurement_unit_id' => 'required|exists:measurement_units,id'
        ]);

        return $this->sendResponse(
            WarehouseItem::updateItem($id, $request->only(
                ['name', 'price', 'classification_id', 'measurement_unit_id', 'critical_stock', 'response_time']
            )),
            'Artículo editado correctamente'
        );
    }

    /**
     * Elimina un artículo de la bodega
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        return $this->sendResponse(WarehouseItem::findOrFail($id)->delete(), 'Artículo eliminado correctamente');
    }
}
