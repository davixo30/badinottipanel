<?php

namespace App\Http\Controllers\API\Warehouse;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\BankAccount;

/**
 * Class BankAccountController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Warehouse
 */
class BankAccountController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de cuentas bancarias disponibles
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->sendResponse(BankAccount::all(), 'Bank accounts list');
    }
}
