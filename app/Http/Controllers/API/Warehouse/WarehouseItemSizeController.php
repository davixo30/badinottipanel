<?php

namespace App\Http\Controllers\API\Warehouse;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\MeasurementUnit;
use App\Models\WarehouseItemSize;
use Illuminate\Http\Request;

/**
 * Class WarehouseItemSizeController
 *
 * // TODO: max env alidaciones de crear y editar
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Warehouse
 */
class WarehouseItemSizeController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el listado de tallas de un artículo de bodega
     *
     * @param int $itemId
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $itemId)
    {
        return $this->sendResponse(WarehouseItemSize::where('warehouse_item_id', $itemId)->get(), 'Sizes list');
    }

    /**
     * Crea una nueva talla
     *
     * @param int $itemId
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function store(int $itemId, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'quantity' => 'required|int|min:0'
        ]);

        return $this->sendResponse(
            WarehouseItemSize::create(array_merge(
                $request->only(['name', 'quantity']),
                ['warehouse_item_id' => $itemId]
            )),
            'Size created'
        );
    }

    public function update(int $itemId, int $sizeId, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'quantity' => 'required|int|min:0'
        ]);

        return $this->sendResponse(
            WarehouseItemSize::findOrFail($sizeId)->update(array_merge(
                $request->only(['name', 'quantity']),
                ['warehouse_item_id' => $itemId, 'id' => $sizeId]
            )),
            'Size deleted'
        );
    }

    /**
     * Elimina una talla de un artículo de bodega
     *
     * @param int $itemId
     * @param int $sizeId
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $itemId, int $sizeId)
    {
        return $this->sendResponse(WarehouseItemSize::findOrFail($sizeId)->delete(), 'Size deleted');
    }

    public function confirmQuantities(Request $request)
    {
        $this->validate($request, [
            'quantities' => 'required|array',
            'quantities.*.id' => 'required|exists:warehouse_item_sizes,id',
            'quantities.*.quantity' => 'required|int|min:0'
        ]);

        WarehouseItemSize::updateQuantities($request->get('quantities'));

        return $this->sendResponse(true, 'Ok');
    }
}
