<?php

namespace App\Http\Controllers\API\Warehouse;

use App\Http\Controllers\API\V1\BaseController;
use App\Jobs\NewPurchase;
use App\Models\InvoicedPurchase;
use App\Models\Purchase;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
/**
 * Class PurchaseController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Warehouse
 */
class PurchaseController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de compras pendientes de facturar
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $compras =   Purchase::select('id', 'purchase', 'supplier_id')->invoicePending()->where("closed",0)
       // ->with('supplier', 'supply_request:id')->where("only_warehouse",0)->whereNull("supply_request_id")->orderBy("purchases.id","desc")->get();

       $compras =   Purchase::select('id', 'purchase', 'supplier_id')->invoicePending()->where("closed",0)
        ->with('supplier', 'supply_request:id')->where("only_warehouse",0)->whereNull("supply_request_id")->orderBy("purchases.id","desc")->get();
        return $this->sendResponse(
            $compras,
            'Purchases list'
        );
    }


    public function getPurchaseCrew()
    {

        $requests = DB::table("supply_requests")
        ->join("crews","crews.id","=","supply_requests.id")
        ->join("ships","ships.id","=","crews.ship_id")
        ->join("purchases","purchases.supply_request_id","=","supply_requests.id")
        ->join("clients","clients.id","=","crews.client_id")
        ->select("ships.name as ship","clients.name as client","crews.upload_date","crews.real_download_date","crews.download_date","purchases.id")
        ->where("purchases.status",1)
        ->where("purchases.closed",0)
        ->whereNotNull("purchases.confirmed_at")
        ->orderBy("purchases.id","desc")
        ->get();


        return $this->sendResponse(
            $requests,
            'Purchases list'
        );
    }

    /**
     * Obtiene el detalle de una compra para ser facturada
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {   
        $purchase = Purchase::with('supplier', 'warehouse_item_sizes')->findOrFail($id);
        if($purchase){
            $purchase->invoice = DB::table("invoices")
            ->join("invoice_purchase","invoice_purchase.invoice_id","=","invoices.id")
            ->join("bank_accounts","bank_accounts.id","=","invoices.account_id")
            ->where("invoice_purchase.purchase_id","=",$id)
            ->select("bank_accounts.account_name","bank_accounts.account_number","invoices.invoice_number as bill_number","invoices.purchase_order","invoices.purchase_order_date","invoice_purchase.created_at")
            ->first();
        }
        return $this->sendResponse($purchase
            ,
            'Purchase found'
        );
    }

    public function showCrew(int $id)
    {        
        $crew = Purchase::with('invoice', 'supplier', 'warehouse_item_sizes')->findOrFail($id);
        if($crew){
            $thecrew = DB::table("supply_requests")
            ->join("crews","crews.id","=","supply_requests.id")
            ->join("ships","ships.id","=","crews.ship_id")
            ->join("purchases","purchases.supply_request_id","=","supply_requests.id")
            ->join("clients","clients.id","=","crews.client_id")
            ->select("crews.id as crew_id","purchases.status","purchases.confirmed_at","ships.name as ship","clients.name as client","crews.upload_date","crews.real_download_date","crews.download_date","supply_requests.id")
            ->where("purchases.id",$crew->id)
            ->first();
            $crew->crew = $thecrew;
            $provisions = DB::table("provisions")->where("provisions.crew_id",$thecrew->crew_id)->first();            
            if($provisions){
                $provisions->supplier = "Sin Proveedor";
                if($provisions->supplier_id<>null){
                    $supplier = DB::table("suppliers")->where("id",$provisions->supplier_id)->first();
                    if($supplier){
                        $provisions->supplier = $supplier->name;
                    }

                }
            }
            $crew->provisions = $provisions;


            foreach($crew->warehouse_item_sizes as $w){
                $dato = DB::table("suppliers")->where("id",$w->supplier_id)->first();
                if($dato){
                    $w->supplier_name = $dato->name;
                }else{
                    $w->supplier_name = "Sin proveedor";
                }
            }
       
        }
        return $this->sendResponse(
            $crew,
            'Purchase found'
        );
    }

    /**
     * Guarda una nueva compra "manual"
     *
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //return $request;
        $this->validate($request, [
            'items' => 'required|array',
            'items.*.size_id' => 'required|exists:warehouse_item_sizes,id',
            'items.*.quantity' => 'required|int|min:1',
            'items.*.price' => 'present|nullable|int|min:0',
            'purchase' => 'required|date',
            'supplier_id' => 'required|exists:suppliers,id'
        ]);

        // guarda la compra
        $purchase = Purchase::create($request->only('purchase', 'items', 'supplier_id'));

        // emite el evento para notificar a Operaciones
        NewPurchase::dispatch($purchase->id);

        return $this->sendResponse($purchase, 'Purchase created');
    }

    /**
     * Solicita facturar una comra
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function invoice(int $id, Request $r)
    {



       

        $this->validate($r, [
            'bank_account_id' => 'required|exists:bank_accounts,id',
            'bill_number' => 'required|string|max:50|unique:invoiced_purchases,bill_number',
            'purchase_order' => 'required|string|max:50|unique:invoiced_purchases,purchase_order',
            'purchase_order_date' => 'required'
        ]);

            $purchase = DB::table("purchases")->where("id",$id)->first();
            $existe = DB::table("invoices")->where("supplier_id",$purchase->supplier_id)->where("invoice_number",$r->bill_number)->first();
            //return response()->json($existe);
            if($existe){
                $existe2 =  DB::table("invoice_purchase")->where("purchase_id",$id)->where("invoice_id",$existe->id)->first();
                if($existe2){

                }else{
                    DB::table("invoice_purchase")->insert(['purchase_id'=>$id,'invoice_id'=>$existe->id,'created_at'=>date('Y-m-d H:i:s') ]);
                }
                
            }else{
                $last = DB::table("invoices")->insertGetId(['airline_id'=>null,'supplier_id'=>$purchase->supplier_id,'account_id'=>$r->bank_account_id,'purchase_order'=>$r->purchase_order,'purchase_order_date'=>$r->purchase_order_date,'invoice_number'=>$r->bill_number,'uploader_id'=>Auth::id()]);
                DB::table("invoice_purchase")->insert(['purchase_id'=>$id,'invoice_id'=>$last,'created_at'=>date('Y-m-d H:i:s') ]);
                
                DB::table("purchase_warehouse_item_size")->where("purchase_id",$id)->update(['invoice_id'=>$last,'invoice_user_id'=>Auth::id(),'invoice_date'=>date('Y-m-d H:i:s')]);

            }
            DB::table("purchases")->where("id",$id)->update(['quoted'=>1,'closed'=>1]);
           // $dato = DB::table("invoices")->where("id",$last)->first();
            //return response()->json($dato);


       /* return $this->sendResponse(InvoicedPurchase::create([
            'purchase_id' => $id,
            'bank_account_id' => $request->get('bank_account_id'),
            'bill_number' => $request->get('bill_number'),
            'purchase_order' => $request->get('purchase_order'),
        ]), 'Purchase invoiced');*/
    }
}
