<?php

namespace App\Http\Controllers\API\Warehouse;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\EPPDelivery;
use App\Models\EPPDeliveryGroup;
use App\Models\Worker;
use Illuminate\Http\Request;

/**
 * Class WorkerController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Warehouse
 */
class WorkerController extends BaseController
{
    /**
     * Create a new controller instance.
     * TODO: verificar si esto se debería sacar o cambiar
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene los datos del trabajador para entrega de EPP
     *
     * @param int $workerId
     *
     * @return \Illuminate\Http\Response
     */
    public function getForEppDelivery(int $workerId)
    {
        return $this->sendResponse(
            Worker::select('id', 'name', 'last_name', 'rut')->find($workerId),
            'Trabajador encontrado'
        );
    }

    /**
     * Genera una nueva entrega de EPP
     *
     * @param int $workerId
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function saveEPPDelivery(int $workerId, Request $request)
    {
        $this->validate($request, [
            'delivery' => 'required|date',
            'deliveries' => 'required|array',
            'deliveries.*.id' => 'required|int|exists:warehouse_item_sizes,id',
            'deliveries.*.observations' => 'required|string|max:255',
        ]);

        return $this->sendResponse(
            EPPDeliveryGroup::create(array_merge(['worker_id' => $workerId], $request->only('delivery', 'deliveries'))),
            'EPP entregado'
        );
    }

    /**
     * Obtiene la última entrega de un epp a un trabajador
     *
     * @param int $workerId
     * @param int $eppId
     *
     * @return \Illuminate\Http\Response
     */
    public function getLastEPPDelivery(int $workerId, int $eppId)
    {
        return $this->sendResponse(EPPDelivery::getLastDelivery($eppId, $workerId), 'Última entrega encontrada');
    }
}
