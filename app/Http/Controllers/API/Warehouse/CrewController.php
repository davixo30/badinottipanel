<?php

namespace App\Http\Controllers\API\Warehouse;

use App\Http\Controllers\API\V1\BaseController;
use App\Jobs\NewPurchase;
use App\Models\Crew;
use App\Models\Purchase;
use App\Models\SupplyRequestWarehouseItem;
use App\Types\PurchaseSupplyRequestActionTypes;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\Models\EPPDeliverySupplyRequest;
use App\Models\WarehouseItemSize;

/**
 * Class CrewController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Warehouse
 */
class CrewController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de tripulaciones con solicitudes de abastecimiento pendientes
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       /* $requests = DB::table("supply_requests")
        ->join("crews","crews.id","=","supply_requests.id")
        ->join("ships","ships.id","=","crews.ship_id")
        ->join("purchases","purchases.supply_request_id","=","supply_requests.id")
        ->join("clients","clients.id","=","crews.client_id")
        ->select("ships.name as ship","clients.name as client","crews.upload_date","crews.real_download_date","crews.download_date","supply_requests.id","crews.supply_request")
        ->where("purchases.status",0)
        ->whereNull("purchases.confirmed_at")
        ->get();*/

       // return response()->json($requests);

       $requests = Crew::calendar()->pendingSupplyRequestPurchase()->latest()->with(
            'client:id,name',
            'purchase',
            'ship:id,name'
        )->where("supply_request",1)->orderBy("crews.id","desc")->get();
        return $this->sendResponse($requests, 'Crew lis2t');
    }

    /**
     * Obtiene la tripulación para poder traer la solicitud de abastecimiento
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $find = Crew::with('client', 'ship', 'supply_request')->findOrFail($id);
        if($find){
            //$find->provisions = DB::table("provisions")->join("suppliers","suppliers.id","=","provisions.supplier_id")->select("provisions.*",'suppliers.name as supplier')->where("provisions.crew_id",$find->id)->first();
            $provisions = DB::table("provisions")->where("provisions.crew_id",$find->id)->first();
            
            if($provisions){
                $provisions->supplier = "Sin Proveedor";
                if($provisions->supplier_id<>null){
                    $supplier = DB::table("suppliers")->where("id",$provisions->supplier_id)->first();
                    if($supplier){
                        $provisions->supplier = $supplier->name;
                    }

                }
            }
            $find->provisions = $provisions;
      
        }
        return $this->sendResponse($find
            ,
            'Crew found'
        );
    }

    /**
     * Valida una solicitud de abastecimiento y genera la compra
     *
     * @param int $id
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function purchaseSupplyRequest(int $id, Request $request)
    {
        $this->validate($request, [
            'provisions_id' => 'required',
            //'date' => 'required|date',
            //'total_price' => 'required',
            //'observation' => 'required',
            //'supplier_id' => 'required|exists:suppliers,id',
            'epp_deliveries' => 'present|array',
            'epp_deliveries.*.id' => 'required|exists:epp_delivery_supply_request,id',
            'epp_deliveries.*.action' => 'required|string|in:purchase,warehouse',
            'epp_deliveries.*.price' => 'required|int|min:0',
            //'epp_deliveries.*.date' => 'required',
            //'epp_deliveries.*.supplier_id' => 'required',
            'warehouse_items' => 'present|array',
            'warehouse_items.*.id' => 'required|exists:supply_request_item,id',
            'warehouse_items.*.action' => 'required|string|in:purchase,warehouse',
            'warehouse_items.*.price' => 'required|int|min:0',
           // 'warehouse_items.*.date' => 'required',
           // 'warehouse_items.*.supplier_id' => 'required',
        ]);

        // guarda la compra
        $purchase = Purchase::createThroughSupplyRequest(
            $request->get('date'),
            $request->get('supplier_id'),
            $id,
            $request->get('epp_deliveries'),
            $request->get('warehouse_items')
        );

        DB::table("provisions")->where("id",$request->provisions_id)->update(['observation'=>$request->observation,'price'=>$request->total_price,'supplier_id'=>$request->supplier_id,'date'=>$request->date]);

        $many = Purchase::with('supplier', 'warehouse_item_sizes')->find($purchase->id);
        if($many){
            if(count($many->warehouse_item_sizes)==0){
                Purchase::validatePurchaseOnlyWarehouse($purchase->id);
            }
        }

        //Supply request purchased

        // emite el evento para notificar a Operaciones
        $savedpurchase = Purchase::find($purchase->id);
        if($savedpurchase->only_warehouse==0){
            NewPurchase::dispatch($purchase->id);
        }


        return $this->sendResponse($purchase, 'Supply request purchased');
    }
}
