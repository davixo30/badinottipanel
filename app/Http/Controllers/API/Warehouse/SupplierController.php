<?php

namespace App\Http\Controllers\API\Warehouse;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Supplier;
use DB;

/**
 * Class SupplierController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Warehouse
 */
class SupplierController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de proveedores disponibles
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->sendResponse(Supplier::orderBy("name","asc")->get(), 'Suppliers list');
    }


    public function getSuppliersByType($type){

           $tipo = DB::table("supplier_types")->where("name",'like','%'.$type.'%')->first();
           return $this->sendResponse(Supplier::where("supplier_type_id",$tipo->id)->orderBy("name","asc")->get(), 'Suppliers list');
    }
}
