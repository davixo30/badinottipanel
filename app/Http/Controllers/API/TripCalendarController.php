<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Crew;
use App\Models\CrewStatus;
use App\Models\Position;
use App\Types\CrewStatusesTypes;
use DB;
/**
 * Class TripCalendarController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class TripCalendarController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de tripulaciones en calendario
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
            $db = Crew::calendar()->with('client', 'ship:id,name')->withCount('workers')->get();
            $calendarized = [];
            foreach($db as $d){
               if ($d->ship !== null) {
                array_push($calendarized, $d);
               }
            }
            return $this->sendResponse($calendarized,'Crew list');

    }

    public function lastcrews()
    {

        $db = Crew::calendar()->with('ship:id,name')->withCount('workers')->groupBy("ship_id")->get();
        $calendarized = [];
        foreach($db as $d){
            if ($d->ship !== null) {
                $last = DB::table("crews")->where("ship_id",$d->ship_id)->where("status_id","3")->orderBy("id","desc")->first();
                if($last){
                    $client = DB::table("clients")->where("id",$last->client_id)->first();
                    $d->client = $client;
                    $d->upload_date = $last->upload_date;
                    $d->real_download_date = $last->real_download_date;
                }
                array_push($calendarized, $d);
               }
        }

        return $this->sendResponse(
           $calendarized,
            'Crew last list'
        );
    }
}
