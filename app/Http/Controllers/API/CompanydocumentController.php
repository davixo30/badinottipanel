<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use Illuminate\Http\Request;
use DB;
use Storage;
use Illuminate\Support\Facades\Auth;
use App\Models\Document;

class CompanydocumentController extends BaseController
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }


    public function createDocument(Request $request){
        $this->validate($request, [
            'document_id' => 'required|exists:company_document_types,id',
            'certificate' => 'required|file|mimes:pdf'
        ]);

        // almacena el archivo
        $certificate = $request->file('certificate');
        $fileName = time() . '.' . $certificate->extension();
        $spaceEnv = env('DO_SPACE_ENV');
        Storage::putFileAs("$spaceEnv/company-documents", $certificate, $fileName);
        
        $data = ['uploader_id'=> Auth::id(),'filename' => $fileName,'observation'=>$request->observation,'expiration_date'=>$request->expiration_date,'document_type_id'=>$request->document_id,'created_at'=>date("Y-m-d H:i:s")];
     

        if($request->document_id==1){
            DB::table("documents")->where("document_type_id","4")->delete();
            $tripulantes = DB::table("workers")->get();
            foreach($tripulantes as $t){
                Document::create([
                    'document_type_id' => 4,
                    'expiration_date' => $request->expiration_date,
                    'worker_id' => $t->id
                ]);
            }
        }

        return $this->sendResponse(
            DB::table('company_documents')->insert($data),
            'Certificación creada correctamente'
        );
    }


    public function getDocuments(){
        $documentos = DB::table("company_documents")
                    ->join("users","users.id","=","company_documents.uploader_id")
                    ->join("company_document_types","company_document_types.id","=","company_documents.document_type_id")
                    ->select("users.name as uploader_name","users.last_name as uploader_last_name","observation",'expiration_date',"filename","company_document_types.name as type","company_documents.created_at")
                    ->orderBy("created_at","desc")
                    ->get();
        return $this->sendResponse(
            $documentos
            ,
            'Listado documentos de empresa'
        );
    }
}
