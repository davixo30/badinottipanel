<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Contract;
use App\Models\Worker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;

/**
 * Class ContractController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class ContractController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el contrato de un trabajador según su ID
     *
     * @param int $workerId
     *
     * @return \Illuminate\Http\Response
     */
    public function get(int $workerId)
    {
        $contrato = Contract::find($workerId);
        $worker = Worker::find($workerId);

        if($contrato){
            $contrato->send_email = $worker->send_email;
            if($worker->red_day==null){
                $contrato->red_day = 0;
            }else{
                $contrato->red_day = $worker->red_day;
            }
        }
       
       
        return $this->sendResponse($contrato, 'Contrato de trabajador encontrado');
    }

    /**
     * Guarda el contrato de un trabajador
     * TODO: es complicado por que utilizar una suerte de 'upsert', una vez bien modelado el contrato, corregir
     *
     * @param int $workerId
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function set(int $workerId, Request $request)
    {

      //  return response()->json($request->red_day);
        $this->validate(new Request(['id' => $workerId]), ['id' => 'exists:workers,id']);
        // TODO: definir min y max de salary
        // TODO: validar size de pdf
        $this->validate($request, [
            'date' => 'date',
            'salary' => 'int|min:1',
            'identity_card_copy' => 'file|mimes:pdf|max:10240',
            'boarding_license' => 'file|mimes:pdf|max:10240',
            'afp_id' => 'exists:afps,id',
            'health_forecast_id' => 'exists:health_forecasts,id',
            'contract_type_id' => 'exists:contract_types,id'
        ]);

        // crea estructura a almacenar
        $data = ['worker_id' => $workerId];

        if ($request->has('date')) $data['date'] = $request->get('date');
        if ($request->has('salary')) $data['salary'] = $request->get('salary');
        if ($request->has('afp_id')) $data['afp_id'] = $request->get('afp_id');
        if ($request->has('health_forecast_id')) $data['health_forecast_id'] =
            $request->get('health_forecast_id');
        if ($request->has('contract_type_id')) $data['contract_type_id'] = $request->get('contract_type_id');

        // verifica archivos
        $hasIdentityCard = $request->has('identity_card_copy');
        $hasBoardingLicense = $request->has('boarding_license');

        if ($hasBoardingLicense || $hasIdentityCard) {
            // almacena los archivos
            $fileName = time() . '.pdf';
            $spaceEnv = env('DO_SPACE_ENV');

            if ($hasBoardingLicense) {
                Storage::putFileAs("$spaceEnv/boarding-licenses", $request->file('boarding_license'), $fileName);
                $data['boarding_license'] = $fileName;
            }

            if ($hasIdentityCard) {
                Storage::putFileAs("$spaceEnv/identity-card-copies", $request->file('identity_card_copy'), $fileName);
                $data['identity_card_copy'] = $fileName;
            }
        }

        // guarda el contrato del trabajador
        $contract = Contract::find($workerId);



        if ($contract) {
            $contract->fill($data);
        } else {
            $contract = new Contract($data);
        }

        $contract->save();
        DB::UPDATE("update workers set red_day=? where id=?",array($request->red_day,$workerId));

        return $this->sendResponse($contract, 'Contrato de trabajador guardado');
    }
}
