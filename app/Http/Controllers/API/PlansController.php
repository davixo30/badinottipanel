<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use Illuminate\Http\Request;
use DB;

class PlansController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de certificados disponibles para una embarcación
     *
     * @param int $id ID embarcación
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function __invoke(int $id)
    {
        $types = DB::table("ship_plans_types")->get();
        return $this->sendResponse($types
            ,
            'plans list'
        );
    }
}
