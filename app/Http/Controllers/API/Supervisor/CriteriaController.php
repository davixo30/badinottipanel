<?php

namespace App\Http\Controllers\API\Supervisor;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Criteria;

/**
 * Class CriteriaController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Supervisor
 */
class CriteriaController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de criterios de calificación disponibles
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->sendResponse(Criteria::all(), 'Qualifiers list');
    }
}
