<?php

namespace App\Http\Controllers\API\Supervisor;

use App\Http\Controllers\API\V1\BaseController;
use App\Jobs\NewSupplyRequest;
use App\Models\SupplyRequest;
use App\Models\Provision;
use Illuminate\Http\Request;
use DB;


/**
 * Class SupplyRequestController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Supervisor
 */
class SupplyRequestController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene una solicitud de abastecimiento
     *
     * @param int $crewId
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $crewId)
    {
        return $this->sendResponse(
            SupplyRequest::with('epp_deliveries', 'warehouse_items')->find($crewId),
            'Supply request found'
        );
    }

    /**
     * Crea una nueva solicitud de abastecimiento
     *
     * @param int $crewId
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(int $crewId, Request $request)
    {

        /*$this->validate($request, [
            'items' => 'present|array',
            'items.*.id' => 'required|exists:warehouse_item_sizes,id',
            'items.*.quantity' => 'required|int|min:1',
            'items.*.description' => 'nullable|string|max:255',
            'epp' => 'present|array',
            'epp.*.id' => 'required|exists:warehouse_item_sizes,id',
            'epp.*.worker_id' => 'required|exists:workers,id',
            'quantity' => 'required|int|min:1'
        ]);*/
       

        $this->validate($request, [
            'items' => 'present|array',
            'items.*.id' => 'exists:warehouse_item_sizes,id',
            'items.*.quantity' => 'int|min:1',
            'items.*.description' => 'nullable|string|max:255',
            'epp' => 'present|array',
            'epp.*.id' => 'exists:warehouse_item_sizes,id',
            'epp.*.worker_id' => 'exists:workers,id',
            'quantity' => 'required|int|min:1'
        ]);

        $supplyRequest = SupplyRequest::create([
            'crew_id' => $crewId,
            'code' => $request->get('code'),
            'items' => $request->get('items'),
            'epp' => $request->get('epp')
        ]);

        Provision::create(['workers_quantity'=>$request->quantity,'crew_id'=>$crewId]);

        // debe notificar a bodega para generar una compra sobre esta solicitud de abastecimiento
        NewSupplyRequest::dispatch($supplyRequest->id);
       

        DB::update("UPDATE crews set supply_request=? where id=?",array(1,$crewId));

        $this->sendResponse(true, 'Ok');
    }
}
