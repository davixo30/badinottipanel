<?php

namespace App\Http\Controllers\API\Supervisor;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Crew;
use App\Models\EPP;
use App\Models\SupplyRequest;

/**
 * Class EPPController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Supervisor
 */
class EPPController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de epp
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->sendResponse(EPP::orderBy('name')->with('sizes')->get(), 'EPP list');
    }
}
