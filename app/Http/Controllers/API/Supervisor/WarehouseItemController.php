<?php

namespace App\Http\Controllers\API\Supervisor;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Position;
use App\Models\WarehouseItem;

/**
 * Class WarehouseItemController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Supervisor
 */
class WarehouseItemController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de artículos de bodega disponibles
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->sendResponse(
            WarehouseItem::others()->with('measurement_unit', 'sizes')->orderBy("name","asc")->get(),
            'Warehouse items list'
        );
    }
}
