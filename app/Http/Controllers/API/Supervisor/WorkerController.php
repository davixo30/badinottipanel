<?php

namespace App\Http\Controllers\API\Supervisor;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Crew;
use App\Models\Qualification;
use Illuminate\Http\Request;

/**
 * Class WorkerController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Supervisor
 */
class WorkerController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de trabajadores de la tripulación
     *
     * @param int $crewId
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(int $crewId)
    {

        $workers = Crew::with('workers')->findOrFail($crewId)->workers->pluck('worker')->toArray();
       
        return $this->sendResponse(array_map(function ($worker) {
            $worker['last_crew'] = Crew::getWorkerLastCrew($worker['id']);
            return $worker;
        }, $workers), 'Workers list');
    }

    /**
     * Califica a un trabajador
     *
     * @param int $id
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function qualify(int $id, Request $request)
    {
        $this->validate($request, [
            'qualification' => 'required|date',
            'year' => 'required|int|min:2000|max:3000',
            'half' => 'required|int|min:1|max:2',
            'qualifications' => 'required|array',
            'qualifications.*.criteria_id' => 'required|exists:criteria,id',
            'qualifications.*.grade' => 'required|min:0|max:7'
        ]);

        return $this->sendResponse(
            Qualification::createWithCriteria(
                $id,
                $request->get('qualification'),
                $request->get('year'),
                $request->get('half'),
                $request->get('qualifications')
            ),
            'Worker qualified'
        );
    }
}
