<?php

namespace App\Http\Controllers\API\Supervisor;

use App\Http\Controllers\API\V1\BaseController;
use App\Http\Controllers\EskuadController;
use App\Jobs\CrewWorkersFiltered;
use App\Jobs\ScheduledCrew;
use App\Models\Crew;
use App\Models\CrewStatus;
use App\Models\CrewWorker;
use App\Models\Worker;
use App\Models\Ship;
use App\Models\WorkerAttribute;
use App\Models\PendingAttribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Auth;
use DB;

/**
 * Class CrewController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Supervisor
 */
class CrewController extends BaseController
{
    /**
     * Create a new controller instance.
     * TODO: verificar si esto se debería sacar o cambiar
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el listado de tripulaciones
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->sendResponse(Crew::supervisor()->latest()->with(
            'client:id,name',
            'ship:id,name,supervisor_id',
            'status'
        )->orderBy("crews.id","desc")->get(), 'Crews list 4');
    }

    /**
     * Obtiene los datos de una tripulación para filtro caso cliente
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $crew = Crew::with(
            'client:id,name',
            'ship:id,name,capacity',
            'status',
            'full_workers'
        )->findOrFail($id);


        /*foreach ($crew->full_workers as $key => $worker) {
            if($worker->worker==null){
                $crew->full_workers->forget($key);
            }
        } */

        // renombra el atributo para evitar conflictos en front
        $crew->workers = $crew->full_workers;
        unset($crew->full_workers);

       

        return $this->sendResponse($crew, 'Tripulación encontrada');
    }

    /**
     * Guarda la confirmación de supervisor de filtros caso cliente
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function setCrewFilter(int $id, Request $request)
    {
        $this->validate($request, [
            'workers' => 'present|array',
            'workers.*.id' => 'required|int|exists:workers,id'
            // 'workers.*.reason' => 'required|string|max:255'
        ]);
 
        $crew = Crew::find($id);

        // debe revisar el estado, de ser true solamente despacha las notificaciones
        if (Crew::setCrewFilter($id, $request->get('workers'))) {
            // despacha evento de tripulación calendarizada
            ScheduledCrew::dispatch($crew);
        } else {
            // notifica a jefe de flota que la tripulación
            CrewWorkersFiltered::dispatch($crew);
        }

        $filtro = count($request->get('workers')) === 0;
        if($filtro){
            $crewworker = DB::table("crew_worker")
                            ->join("workers","crew_worker.worker_id","=","workers.id")
                            ->where("filtered","0")
                            ->where("workers.deleted_at",null)
                            ->where("crew_id",$id)
                            ->get();

                $ship = Ship::findOrFail($crew->ship_id);

                foreach($crewworker as $c){
                    $atributo = $c->attribute_id;
                    if($c->attribute_id == 14){
                        $atributo = 11;
                    }
                    if($c->attribute_id == 15){
                        $atributo = 12;
                    }
                    if($c->attribute_id<>14 && $c->attribute_id<>15){
                        $atributo = 16;
                    }

                    $wk = Worker::withTrashed()->find($c->worker_id);
                   // $wk = DB::table("workers")->where("id",$c->worker_id)->first();


                    $w = new WorkerAttribute;
                    $w->worker_id = $c->worker_id;
                    $w->crew_id = $id;


                    if($crew->upload_date==date("Y-m-d")){

                            $wk->attribute_id = 21;
                            $w->attribute_id = 21;
                            $w->date_start = date("Y-m-d");
                            $w->date_end = date("Y-m-d",strtotime($crew->upload_date." - 1 days"));
                            $np = new PendingAttribute;
                            $np->attribute_id = 1;
                            $np->worker_id = $c->worker_id;
                            $np->date = date("Y-m-d",strtotime($crew->upload_date." + 1 days"));
                            $np->crew_id = $id;
                            $np->status = 0;
                            $np->save();
                    }else{
                        if($crew->upload_date>date("Y-m-d")){
                            $wk->attribute_id = $atributo;
                            $w->attribute_id = $atributo;
                            $w->date_start = date("Y-m-d");
                            $w->date_end = date("Y-m-d",strtotime($crew->upload_date." - 1 days"));
                            $np = new PendingAttribute;
                            $np->attribute_id = 21;
                            $np->worker_id = $c->worker_id;
                            $np->date = date("Y-m-d",strtotime($crew->upload_date));
                            $np->crew_id = $id;
                            $np->status = 0;
                            $np->save();
                            $np = new PendingAttribute;
                            $np->attribute_id = 1;
                            $np->worker_id = $c->worker_id;
                            $np->date = date("Y-m-d",strtotime($crew->upload_date." + 1 days"));
                            $np->crew_id = $id;
                            $np->status = 0;
                            $np->save();
                        }

                        if(($crew->upload_date<date("Y-m-d")) && ($crew->real_download_date>=date("Y-m-d"))){
                            $wk->attribute_id = 1;
                            $w->date_start = $crew->upload_date;
                            $w->date_end = $crew->real_download_date;
                            $w->attribute_id = 1;

                            $np = new PendingAttribute;
                            $np->attribute_id = 2;
                            $np->worker_id = $c->worker_id;
                            $np->date = date('Y-m-d', strtotime($crew->real_download_date. ' + 1 days'));
                            $np->status = 0;
                            $np->crew_id = $crew->id;
                            $np->save();
                        }

                    }
                    $wk->save();
                    $w->save();

                }
                // aqui actualizar datasource
                EskuadController::updateDatasource($id, false);

        }else{
            //si vuelve a borrador
            $crewworker = DB::table("crew_worker")
                            ->join("workers","crew_worker.worker_id","=","workers.id")
                            ->where("crew_id",$id)
                            ->get();

                foreach($crewworker as $c){
                    $ultimo_atributo = 2;
                    $last = WorkerAttribute::where("worker_id",$c->worker_id)->whereIn("attribute_id",array(2,10,1))->orderBy("id","desc")->first();
                    if($last){
                        $ultimo_atributo = $last->attribute_id;
                    }
                    $w = Worker::find($c->worker_id);
                    $w->attribute_id = $ultimo_atributo;
                    $w->save();

                    $wa = new WorkerAttribute;
                    $wa->worker_id = $c->worker_id;
                    $wa->crew_id = $id;
                    $wa->attribute_id = 19;
                    $wa->save();

                    $wa = new WorkerAttribute;
                    $wa->worker_id = $c->worker_id;
                    $wa->crew_id = $id;
                    $wa->attribute_id = $ultimo_atributo;
                    $wa->save();

                }
        }
        return $this->sendResponse('Ok', 'Guardado el filtro tripulación');
    }
}
