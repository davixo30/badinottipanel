<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\API\V1\BaseController;
use Illuminate\Http\Request;
use App\Models\Person;
use App\Models\Worker;
use DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Models\Purchase;
use App\Jobs\NotifyWorker;
use Twilio\Rest\Client;


class PersonController extends BaseController
{
    /**
     * Create a new controller instance.
     * TODO: verificar si esto se debería sacar o cambiar
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el listado de tripulaciones
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->query('search');
        $personas = Person::where("status",1)->orderBy('name','desc')->get();
      
        return $this->sendResponse($personas
            ,
            'person list'
        );
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'last_name' => 'required',
            'rut' => 'required|min:8|unique:persons,rut',
            'email' => 'required|email'
        ]);
        $persona = Person::create(['name'=>$request->name,'last_name'=>$request->last_name,'rut'=>$request->rut,'email'=>$request->email,'phone'=>$request->phone,'role'=>$request->role]);
        $this->sendResponse($persona,"persona agregada");
    }

    public function update(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'last_name' => 'required',
            'rut' => 'required|min:8|unique:persons,rut,'.$request->id,
            'email' => 'required|email'
        ]);
        $persona = Person::findOrFail($request->id);
        $persona->name = $request->name;
        $persona->last_name = $request->last_name;
        $persona->rut = $request->rut;
        $persona->email = $request->email;
        $persona->role = $request->role;
        $persona->phone = $request->phone;
        $persona->save();
        //$persona = Person::update(['name'=>$request->name,'last_name'=>$request->last_name,'rut'=>$request->rut,'email'=>$request->email]);
        $this->sendResponse($persona,"persona actualizada");
    }

    public function destroy(int $id)
    {
        $person = Person::findOrFail($id);
        $person->status = 0;
        $person->save();
        return $this->sendResponse(
            $person,
            'persona eliminada correctamente'
        );
    }

    public function getPerson(int $id)
    {
        $person = Person::findOrFail($id);
        return $this->sendResponse(
            $person,
            'persona encontrada'
        );
    }

    public function getAccomodations(int $id)
    {

        $accomodations = DB::table("accommodation_person")
                        ->join("accommodations","accommodations.id","=","accommodation_person.accommodation_id")
                        ->join("suppliers","suppliers.id","=","accommodations.supplier_id")
                        ->select("suppliers.id as supplier_id","accommodations.id","suppliers.name as supplier","accommodations.price","accommodations.observation","accommodations.entry_date","accommodations.exit_date","accommodation_person.created_at")
                        ->where("accommodation_person.person_id",$id)
                        ->orderBy("accommodations.id","desc")
                        ->get();
        
      
        return $this->sendResponse(
            $accomodations,
            'persona encontrada'
        );
    }

    public function getAccommodation(int $id)
    {

        $accomodations = DB::table("accommodations")
                        ->join("suppliers","suppliers.id","=","accommodations.supplier_id")
                        ->select("suppliers.id as supplier","accommodations.price","accommodations.observation","accommodations.entry_date","accommodations.exit_date")
                        ->where("accommodations.id",$id)
                        ->first();

        $person = DB::table("accommodation_person")->join("persons","persons.id","=","accommodation_person.person_id")->select("persons.id","persons.name","persons.last_name","persons.rut")->where("accommodation_id",$id)->where("accommodation_person.status",1)->get();
        $workers = DB::table("accommodation_worker")->join("workers","workers.id","=","accommodation_worker.worker_id")->select("workers.id","workers.name","workers.last_name","workers.rut")->where("accommodation_id",$id)->where("accommodation_worker.status",1)->get();
       
        $accomodations->persons = $person;
        $accomodations->workers = $workers;
      
        return $this->sendResponse(
            $accomodations,
            'persona encontrada'
        );
    }


    public function getAllAccommodations(Request $request)
    {
        $search = $request->query('search');
        $accomodations = DB::table("accommodations")
                        ->join("suppliers","suppliers.id","=","accommodations.supplier_id")
                        ->select("suppliers.id as supplier_id","accommodations.id","suppliers.name as supplier","accommodations.price","accommodations.observation","accommodations.entry_date","accommodations.exit_date","accommodations.created_at")
                        /*->when($search, function ($query, $search) {
                            return $query->where('suppliers.name', 'like', "%$search%")
                                ->orWhere('accommodations.entry_date', 'like', "%$search%")
                                ->orWhere('accommodations.exit_date', 'like', "%$search%")
                                ->orWhere('accommodations.observation', 'like', "%$search%");
                        })*/
                        ->orderBy("accommodations.id","desc")
                        ->get();

        foreach($accomodations as $a){
            $cantidad1 = DB::table("accommodation_person")->where("status",1)->where("accommodation_id",$a->id)->count();
            $cantidad2 = DB::table("accommodation_worker")->where("status",1)->where("accommodation_id",$a->id)->count();

            $a->quantity = $cantidad1 + $cantidad2;
        }
       

        
      
        return $this->sendResponse(
            $accomodations,
            'persona encontrada'
        );
    }

    public function getAllAccommodationsQuotable(Request $request)
    {

       /* $epp = DB::table("epp_deliveries")->join("epp_delivery_groups","epp_deliveries.epp_delivery_group_id","=","epp_delivery_groups.id")
                                          ->join("workers","workers.id","=","epp_delivery_groups.worker_id")
                                          ->join("warehouse_item_sizes","warehouse_item_sizes.id","=","epp_deliveries.warehouse_item_size_id")
                                          ->join("warehouse_items","warehouse_items.id","=","warehouse_item_sizes.warehouse_item_id")
                                            ->select("workers.name","workers.last_name","workers.rut","warehouse_items.name as item")
                                            //->whereRaw("DATE_ADD(epp_delivery_groups.delivery,INTERVAL 1 YEAR) = '".$date."'")
                                            ->get();
                                            return $epp;*/
        $search = $request->query('search');
        $accomodations = DB::table("accommodations")
                        ->join("suppliers","suppliers.id","=","accommodations.supplier_id")
                        ->select("suppliers.id as supplier_id","accommodations.id","suppliers.name as supplier","accommodations.price","accommodations.observation","accommodations.entry_date","accommodations.exit_date","accommodations.created_at")
                        ->where("accommodations.invoice_id",null)
                       /* ->when($search, function ($query, $search) {
                            return $query->where('suppliers.name', 'like', "%$search%")
                                ->orWhere('accommodations.entry_date', 'like', "%$search%")
                                ->orWhere('accommodations.exit_date', 'like', "%$search%")
                                ->orWhere('accommodations.observation', 'like', "%$search%");
                        })*/
                        ->orderBy("accommodations.id","desc")
                        ->get();

        foreach($accomodations as $a){
            $cantidad1 = DB::table("accommodation_person")->where("status",1)->where("accommodation_id",$a->id)->count();
            $cantidad2 = DB::table("accommodation_worker")->where("status",1)->where("accommodation_id",$a->id)->count();

            $a->quantity = $cantidad1 + $cantidad2;
        }
       

        
      
        return $this->sendResponse(
            $accomodations,
            'persona encontrada'
        );
    }

    public function getTransfers(int $id)
    {

        $transfers = DB::table("person_transfer")
                        ->join("transfers","transfers.id","=","person_transfer.transfer_id")
                        ->join("suppliers","suppliers.id","=","transfers.supplier_id")
                        ->select("suppliers.id as supplier_id","suppliers.name as supplier","transfers.price","transfers.observation","transfers.upload_date","transfers.entry_text","transfers.exit_text","transfers.quotation","person_transfer.created_at")
                        ->where("person_transfer.person_id",$id)
                        ->where("person_transfer.status",1)
                        ->orderBy("transfers.id","desc")
                        ->get();
      
        return $this->sendResponse(
            $transfers,
            'traslados encontrados'
        );
    }

    public function getTransfer(int $id)
    {

        $transfers = DB::table("transfers")
                        ->join("suppliers","suppliers.id","=","transfers.supplier_id")
                        ->select("suppliers.id as supplier","transfers.price","transfers.observation","transfers.upload_date","transfers.entry_text","transfers.exit_text","transfers.quotation")
                        ->where("transfers.id",$id)
                        ->orderBy("transfers.id","desc")
                        ->first();
        
        $person = DB::table("person_transfer")->join("persons","persons.id","=","person_transfer.person_id")->select("persons.id","persons.name","persons.last_name","persons.rut")->where("transfer_id",$id)->where("person_transfer.status",1)->get();
        $workers = DB::table("worker_transfer")->join("workers","workers.id","=","worker_transfer.worker_id")->select("workers.id","workers.name","workers.last_name","workers.rut")->where("transfer_id",$id)->where("worker_transfer.status",1)->get();
       

        $transfers->persons = $person;
        $transfers->workers = $workers;
      
        return $this->sendResponse(
            $transfers,
            'traslado encontrado'
        );
    }

    public function getAllTransfers(Request $request)
    {
        $search = $request->query('search');
        $transfers = DB::table("transfers")
                        ->join("suppliers","suppliers.id","=","transfers.supplier_id")
                        ->select("suppliers.id as supplier_id","transfers.id","suppliers.name as supplier","transfers.price","transfers.observation","transfers.upload_date","transfers.entry_text","transfers.exit_text","transfers.quotation","transfers.created_at")
                       /* ->when($search, function ($query, $search) {
                            return $query->where('name', 'like', "%$search%")
                                ->orWhere('transfers.upload_date', 'like', "%$search%")
                                ->orWhere('transfers.entry_text', 'like', "%$search%")
                                ->orWhere('transfers.exit_text', 'like', "%$search%")
                                ->orWhere('transfers.observation', 'like', "%$search%");
                        })*/
                        ->orderBy("transfers.id","desc")
                        ->get();
      
                        foreach($transfers as $a){
                            $cantidad1 = DB::table("person_transfer")->where("status",1)->where("transfer_id",$a->id)->count();
                            $cantidad2 = DB::table("worker_transfer")->where("status",1)->where("transfer_id",$a->id)->count();
                
                            $a->quantity = $cantidad1 + $cantidad2;
                        }

        return $this->sendResponse(
            $transfers,
            'traslados encontrados'
        );
    }

    public function getAllTransfersQuotable(Request $request)
    {
        $search = $request->query('search');
        $transfers = DB::table("transfers")
                        ->join("suppliers","suppliers.id","=","transfers.supplier_id")
                        ->select("suppliers.id as supplier_id","transfers.id","suppliers.name as supplier","transfers.price","transfers.observation","transfers.upload_date","transfers.entry_text","transfers.exit_text","transfers.quotation","transfers.created_at")
                        ->where("transfers.quotation",1)
                        ->where("transfers.invoice_id",null)
                        /*->when($search, function ($query, $search) {
                            return $query->where('name', 'like', "%$search%")
                                ->orWhere('transfers.upload_date', 'like', "%$search%")
                                ->orWhere('transfers.entry_text', 'like', "%$search%")
                                ->orWhere('transfers.exit_text', 'like', "%$search%")
                                ->orWhere('transfers.observation', 'like', "%$search%");
                        })*/
                        ->orderBy("transfers.id","desc")
                        ->get();
      
                        foreach($transfers as $a){
                            $cantidad1 = DB::table("person_transfer")->where("status",1)->where("transfer_id",$a->id)->count();
                            $cantidad2 = DB::table("worker_transfer")->where("status",1)->where("transfer_id",$a->id)->count();
                
                            $a->quantity = $cantidad1 + $cantidad2;
                        }

        return $this->sendResponse(
            $transfers,
            'traslados encontrados'
        );
    }

    public function setAccomodations(Request $request)
    {

        $last = DB::table("accommodations")->insertGetId(['price'=>$request->price,'supplier_id'=>$request->supplier_id,'entry_date'=>$request->entry_date,'exit_date'=>$request->exit_date,'observation'=>$request->observation,'created_at'=>date('Y-m-d H:i:s')]);
        $inserted = DB::table("accommodation_person")->insert(['person_id'=>$request->id,'accommodation_id'=>$last,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
        return $this->sendResponse(
            $request,
            'alojamiento guardado'
        );
    }

    public function setAccommodationsMultiple(Request $request)
    {
        if($request->isedit==1){
            DB::table("accommodations")->where("id",$request->accommodation_id)->update(['price'=>$request->price,'supplier_id'=>$request->supplier_id,'entry_date'=>$request->entry_date,'exit_date'=>$request->exit_date,'observation'=>$request->observation,'updated_at'=>date('Y-m-d H:i:s')]);
            DB::update("UPDATE accommodation_person set status = ? where accommodation_id=?",array(0,$request->accommodation_id));
            foreach($request->selected as $d){
                $persona = DB::table("accommodation_person")->where("person_id",$d['id'])->where("accommodation_id",$request->accommodation_id)->first();
                if($persona){
                    DB::update("UPDATE accommodation_person set status = ? where person_id=? and accommodation_id=?",array(1,$d['id'],$request->accommodation_id));
                }else{
                    $inserted = DB::table("accommodation_person")->insert(['person_id'=>$d['id'],'accommodation_id'=>$request->accommodation_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
                }
             }

             DB::update("UPDATE accommodation_worker set status = ? where accommodation_id=?",array(0,$request->accommodation_id));
            foreach($request->selectedWorkers as $d){
                $persona = DB::table("accommodation_worker")->where("worker_id",$d['id'])->where("accommodation_id",$request->accommodation_id)->first();
                if($persona){
                    DB::update("UPDATE accommodation_worker set status = ? where worker_id=? and accommodation_id=?",array(1,$d['id'],$request->accommodation_id));
                }else{
                    $inserted = DB::table("accommodation_worker")->insert(['worker_id'=>$d['id'],'accommodation_id'=>$request->accommodation_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
                }
             }
        }else{
            $last = DB::table("accommodations")->insertGetId(['price'=>$request->price,'supplier_id'=>$request->supplier_id,'entry_date'=>$request->entry_date,'exit_date'=>$request->exit_date,'observation'=>$request->observation,'created_at'=>date('Y-m-d H:i:s')]);
            foreach($request->selected as $d){
                $inserted = DB::table("accommodation_person")->insert(['person_id'=>$d['id'],'accommodation_id'=>$last,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
            }
            foreach($request->selectedWorkers as $d){
                $inserted = DB::table("accommodation_worker")->insert(['worker_id'=>$d['id'],'accommodation_id'=>$last,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
         
            }
        }
        return $this->sendResponse(
            $request,
            'alojamiento guardado'
        );
    }

    public function setTransfers(Request $request)
    {

        $last = DB::table("transfers")->insertGetId(['quotation'=>$request->no_facturar,'price'=>$request->price,'upload_date'=>$request->upload_date,'supplier_id'=>$request->supplier_id,'entry_text'=>$request->entry_text,'exit_text'=>$request->exit_text,'observation'=>$request->observation,'created_at'=>date('Y-m-d H:i:s')]);
        $inserted = DB::table("person_transfer")->insert(['person_id'=>$request->id,'transfer_id'=>$last,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
        return $this->sendResponse(
            $request,
            'traslado guardado'
        );
    }

    public function setTransfersMultiple(Request $request)
    {
        if($request->isedit==1){
            DB::table("transfers")->where("id",$request->transfer_id)->update(['quotation'=>$request->no_facturar,'price'=>$request->price,'upload_date'=>$request->upload_date,'supplier_id'=>$request->supplier_id,'entry_text'=>$request->entry_text,'exit_text'=>$request->exit_text,'observation'=>$request->observation,'updated_at'=>date('Y-m-d H:i:s')]);
            DB::update("UPDATE person_transfer set status = ? where transfer_id=?",array(0,$request->transfer_id));
            foreach($request->selected as $d){
                $persona = DB::table("person_transfer")->where("person_id",$d['id'])->where("transfer_id",$request->transfer_id)->first();
                if($persona){
                    DB::update("UPDATE person_transfer set status = ? where person_id=? and transfer_id=?",array(1,$d['id'],$request->transfer_id));
                }else{
                    DB::table("person_transfer")->insert(['person_id'=>$d['id'],'transfer_id'=>$request->transfer_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
                }
             }

             DB::update("UPDATE worker_transfer set status = ? where transfer_id=?",array(0,$request->transfer_id));
            foreach($request->selectedWorkers as $d){
                $persona = DB::table("worker_transfer")->where("worker_id",$d['id'])->where("transfer_id",$request->transfer_id)->first();
                if($persona){
                    DB::update("UPDATE worker_transfer set status = ? where worker_id=? and transfer_id=?",array(1,$d['id'],$request->transfer_id));
                }else{
                    DB::table("worker_transfer")->insert(['worker_id'=>$d['id'],'transfer_id'=>$request->transfer_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
                }
             }
        }else{
            $last = DB::table("transfers")->insertGetId(['quotation'=>$request->no_facturar,'price'=>$request->price,'upload_date'=>$request->upload_date,'supplier_id'=>$request->supplier_id,'entry_text'=>$request->entry_text,'exit_text'=>$request->exit_text,'observation'=>$request->observation,'created_at'=>date('Y-m-d H:i:s')]);
            foreach($request->selected as $d){
                $inserted = DB::table("person_transfer")->insert(['person_id'=>$d['id'],'transfer_id'=>$last,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
            }
            foreach($request->selectedWorkers as $d){
                $inserted = DB::table("worker_transfer")->insert(['worker_id'=>$d['id'],'transfer_id'=>$last,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
            }
        }
           
        return $this->sendResponse(
            $request,
            'traslado guardado'
        );
    }

    public function setNewStockTicket(Request $r)
    {
        $last = DB::table("ticket_groups")->insertGetId(['oc'=>$r->purchase_order,'oc_date'=>$r->purchase_order_date,'have_stock'=>1,'is_stock'=>1,'code'=>$r->code,'penalty'=>$r->penalty,'dearness'=>$r->dearness,'ticket_type'=>'new_stock','flight_date'=>$r->flight_date,'departure_time'=>$r->departure_time,'arrival_time'=>$r->arrival_time,'price'=>$r->price,'airline_id'=>$r->airline_id,'flight_stretch_id'=>$r->flight_stretch_id,'departure_airport_id'=>$r->departure_airport_id,'arrival_airport_id'=>$r->arrival_airport_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id(),'extra_price'=>$r->extra_price,'plus'=>$r->tarifa_plus]);  
        DB::table("stock_tickets")->insert(['oc'=>$r->purchase_order,'oc_date'=>$r->purchase_order_date,'original_ticket_id'=>$last,'ticket_user_id'=>null,'ticket_type'=>'new_stock','code'=>$r->code,'penalty'=>$r->penalty,'dearness'=>$r->dearness,'flight_date'=>$r->flight_date,'departure_time'=>$r->departure_time,'arrival_time'=>$r->arrival_time,'price'=>$r->price,'airline_id'=>$r->airline_id,'flight_stretch_id'=>$r->flight_stretch_id,'departure_airport_id'=>$r->departure_airport_id,'arrival_airport_id'=>$r->arrival_airport_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id(),'extra_price'=>$r->extra_price,'plus'=>$r->tarifa_plus]);
              
        return $this->sendResponse(
            $r,
            'pasaje guardado'
        );
    }

    public function setTickets(Request $r)
    {
        $last = DB::table("ticket_groups")->insertGetId(['flight_date'=>$r->flight_date,'departure_time'=>$r->departure_time,'arrival_time'=>$r->arrival_time,'price'=>$r->price,'airline_id'=>$r->airline_id,'flight_stretch_id'=>$r->flight_stretch_id,'departure_airport_id'=>$r->departure_airport_id,'arrival_airport_id'=>$r->arrival_airport_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id(),'extra_price'=>$r->extra_price,'plus'=>$r->tarifa_plus]);  
        $inserted = DB::table("person_ticket")->insert(['person_id'=>$r->id,'ticket_group_id'=>$last,'created_at'=>date('Y-m-d H:i:s'),'status'=>1,'uploader_id'=>Auth::id(),"carry_price"=>$r->carry_price]);
        
        return $this->sendResponse(
            $r,
            'pasaje guardado'
        );
    }

    public function setTicketsMultiple(Request $r)
    {

        // response()->json($r->all());
        $nostockcount = 0;
        $manycount = 0;
        $isstock = 0;
        $havestock = 0;
        if($r->isedit==1){

            $actual = DB::table("ticket_groups")->where("id",$r->ticket_id)->first();

            DB::table("ticket_groups")->where("id",$r->ticket_id)->update(['voucher_id'=>null,'stock_ticket_id'=>null,'payment_type'=>null,'payment_use_price'=>0]);

            $cambiopago = $actual->payment_type;
            $cambiostockid =$actual->stock_ticket_id;
            $cambiovoucher = $actual->voucher_id;

            if($r->stock_ticket_id<>$actual->payment_type){
                $cambiopago = $r->stock_ticket_id;
                if($actual->payment_type==1){
                    DB::table("stock_tickets")->where("id",$actual->stock_ticket_id)->update(['used'=>0]);
                    DB::table("ticket_groups")->where("id",$r->ticket_id)->update(['stock_ticket_id'=>null]);
                }
                if($actual->payment_type==2){
                    $last_voucher = DB::table("log_vouchers")->where('airline_voucher_id',$actual->voucher_id)->where('ticket_group_id',$r->ticket_id)->first();
                    $airlinevoucher = DB::table("airline_vouchers")->where('id',$actual->voucher_id)->first();
                    DB::table("log_vouchers")->where('airline_voucher_id',$actual->voucher_id)->where('ticket_group_id',$r->ticket_id)->update(['status' => 0]);
                    DB::table("airline_vouchers")->where("id",$actual->voucher_id)->update(['status'=>0,'price'=>($airlinevoucher->price + $last_voucher->required_price),'price_used'=>($airlinevoucher->price_used - $last_voucher->required_price)]);
                    DB::table("ticket_groups")->where("id",$r->ticket_id)->update(['voucher_id'=>null]);
                    $estado =  DB::table("airline_vouchers")->where("id",$actual->voucher_id)->first();
                    if($estado){
                        if($estado->price>0){
                            DB::table("airline_vouchers")->where("id",$actual->voucher_id)->update(['status'=>1]);
                        }
                    }
                }
                
            }


            if($r->stock_ticket_id==1){
                if($r->stock_id<>$actual->stock_ticket_id){
                  DB::table("stock_tickets")->where("id",$r->stock_id)->update(['used'=>1,'stock_used_date'=>date('Y-m-d H:i:s')]);
                }
            }
            if($r->stock_ticket_id==2){
                if($r->voucher_id<>$actual->voucher_id){                
                    $voucher = DB::table("airline_vouchers")->where("id",$r->voucher_id)->first();
                    if($voucher){
                        $value = $r->total_price;
                        if($r->total_price>$voucher->price){
                            $value = $voucher->price;
                        }
                        $diff = $voucher->price - $value;
                        $disabled = 1;
                        if($diff == 0){
                            $disabled = 0;
                        }           
                        DB::table("log_vouchers")->insert(['date'=>date('Y-m-d H:i:s'),'required_price'=>$value,'voucher_price'=>$voucher->price,'balance'=>$diff,'ticket_group_id'=>$r->ticket_id,'airline_voucher_id'=>$r->voucher_id,'status'=>1]);
                        DB::table("airline_vouchers")->where("id",$r->voucher_id)->update(['status'=>$disabled,'price_used'=>($voucher->price_used + $value),'price'=>$diff]);
                    }
             }
            }

            if($havestock==0){
                DB::table("ticket_groups")->where("id",$r->ticket_id)->update(['payment_use_price'=>$r->payment_price,'passengers'=>$r->passengers,'total_price'=>$r->real_total_price,'unitary_extra_price'=>$r->total_extra_price,'stock_ticket_id'=>$r->stock_id,'voucher_id'=>$r->voucher_id,'expiration_date'=>$r->expiration_date,'payment_type'=>$r->stock_ticket_id,'code'=>$r->code,'penalty'=>$r->penalty,'dearness'=>$r->dearness,'flight_date'=>$r->flight_date,'departure_time'=>$r->departure_time,'arrival_time'=>$r->arrival_time,'price'=>$r->price,'airline_id'=>$r->airline_id,'flight_stretch_id'=>$r->flight_stretch_id,'departure_airport_id'=>$r->departure_airport_id,'arrival_airport_id'=>$r->arrival_airport_id,'updated_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id(),'extra_price'=>$r->extra_price,'plus'=>$r->tarifa_plus]);
            }

            $find = DB::table("ticket_groups")->where("id",$r->ticket_id)->first();


            foreach($r->tostock as $d){
                DB::table("person_ticket")->where("id",$d['person_ticket_id'])->update(['stock'=>1,'ticket_status_id'=>4]);
                $original = DB::table("person_ticket")->where("id",$d['person_ticket_id'])->first();
                //DB::table("stock_tickets")->insert(['original_ticket_id'=>$r->ticket_id,'ticket_user_id'=>$original->person_id,'ticket_type'=>$find->ticket_type,'code'=>$find->code,'penalty'=>$find->penalty,'dearness'=>$find->dearness,'flight_date'=>$find->flight_date,'departure_time'=>$find->departure_time,'arrival_time'=>$find->arrival_time,'price'=>$find->price,'airline_id'=>$find->airline_id,'flight_stretch_id'=>$find->flight_stretch_id,'departure_airport_id'=>$find->departure_airport_id,'arrival_airport_id'=>$find->arrival_airport_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id(),'extra_price'=>$find->extra_price,'plus'=>$find->plus,'user_type'=>'person']);
                $havestock = 1;
                if(intval($d['carry_price'])>0){
                    DB::table("stock_tickets")->insert(['original_ticket_id'=>$r->ticket_id,'ticket_user_id'=>$original->person_id,'ticket_type'=>$find->ticket_type,'code'=>$find->code,'penalty'=>0,'dearness'=>0,'flight_date'=>$find->flight_date,'departure_time'=>$find->departure_time,'arrival_time'=>$find->arrival_time,'price'=>$d['carry_price'],'airline_id'=>$find->airline_id,'flight_stretch_id'=>$find->flight_stretch_id,'departure_airport_id'=>$find->departure_airport_id,'arrival_airport_id'=>$find->arrival_airport_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id(),'extra_price'=>0,'plus'=>$find->plus,'user_type'=>'person','is_carry'=>1]);
                }
            }

            foreach($r->tostockWorkers as $d){
                DB::table("worker_ticket")->where("id",$d['worker_ticket_id'])->update(['stock'=>1,'ticket_status_id'=>4]);
                $original = DB::table("worker_ticket")->where("id",$d['worker_ticket_id'])->first();
                //DB::table("stock_tickets")->insert(['original_ticket_id'=>$r->ticket_id,'ticket_user_id'=>$original->worker_id,'ticket_type'=>$find->ticket_type,'code'=>$find->code,'penalty'=>$find->penalty,'dearness'=>$find->dearness,'flight_date'=>$find->flight_date,'departure_time'=>$find->departure_time,'arrival_time'=>$find->arrival_time,'price'=>$find->price,'airline_id'=>$find->airline_id,'flight_stretch_id'=>$find->flight_stretch_id,'departure_airport_id'=>$find->departure_airport_id,'arrival_airport_id'=>$find->arrival_airport_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id(),'extra_price'=>$find->extra_price,'plus'=>$find->plus,'user_type'=>'worker']);
                $havestock = 1;

                if(intval($d['carry_price'])>0){
                    DB::table("stock_tickets")->insert(['original_ticket_id'=>$r->ticket_id,'ticket_user_id'=>$original->worker_id,'ticket_type'=>$find->ticket_type,'code'=>$find->code,'penalty'=>0,'dearness'=>0,'flight_date'=>$find->flight_date,'departure_time'=>$find->departure_time,'arrival_time'=>$find->arrival_time,'price'=>$d['carry_price'],'airline_id'=>$find->airline_id,'flight_stretch_id'=>$find->flight_stretch_id,'departure_airport_id'=>$find->departure_airport_id,'arrival_airport_id'=>$find->arrival_airport_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id(),'extra_price'=>0,'plus'=>$find->plus,'user_type'=>'worker','is_carry'=>1]);
                }

            }

            foreach($r->tolost as $d){
                DB::table("person_ticket")->where("id",$d['person_ticket_id'])->update(['ticket_status_id'=>5,'lost_datetime'=>date('Y-m-d H:i:s'),'lost_user_id'=>Auth::id()]);
            }
            foreach($r->tolostWorkers as $d){
                DB::table("worker_ticket")->where("id",$d['worker_ticket_id'])->update(['ticket_status_id'=>5,'lost_datetime'=>date('Y-m-d H:i:s'),'lost_user_id'=>Auth::id()]);
            }

                
            DB::update("UPDATE person_ticket set status = ? where ticket_group_id=?",array(0,$r->ticket_id));
            foreach($r->selected as $d){
                $persona = DB::table("person_ticket")->where("person_id",$d['id'])->where("ticket_group_id",$r->ticket_id)->first();
                if($persona){
                    DB::update("UPDATE person_ticket set status = ?, carry_price=?,extra_price=?,observation=?,penalty=?,quotation=?,penalty_quotation=? where person_id=? and ticket_group_id=?",array(1,$d['carry_price'],$d['extra_price'],$d['observation'],$d['penalty'],$d['quotation'],$d["penalty_quotation"],$d['id'],$r->ticket_id));
                }else{
                    DB::table("person_ticket")->insert(['person_id'=>$d['id'],'ticket_group_id'=>$r->ticket_id,'created_at'=>date('Y-m-d H:i:s'),'status'=>1,'uploader_id'=>Auth::id(),"carry_price"=>$d['carry_price'],"penalty"=>$d['penalty'],"extra_price"=>$d['extra_price'],"observation"=>$d['observation'],"quotation"=>$d['quotation'],"penalty_quotation"=>$d["penalty_quotation"]]);
                }
             }

             


            DB::update("UPDATE worker_ticket set status = ? where ticket_group_id=?",array(0,$r->ticket_id));
            foreach($r->selectedWorkers as $d){
                $persona = DB::table("worker_ticket")->where("worker_id",$d['id'])->where("ticket_group_id",$r->ticket_id)->first();
                if($persona){
                    DB::update("UPDATE worker_ticket set status = ?, carry_price=?,extra_price=?,observation=?,penalty=?,quotation=?,penalty_quotation=? where worker_id=? and ticket_group_id=?",array(1,$d['carry_price'],$d['extra_price'],$d['observation'],$d['penalty'],$d['quotation'],$d["penalty_quotation"],$d['id'],$r->ticket_id));
                }else{
                    DB::table("worker_ticket")->insert(['ticket_price'=>$find->price,'worker_id'=>$d['id'],'ticket_group_id'=>$r->ticket_id,'created_at'=>date('Y-m-d H:i:s'),'status'=>1,'uploader_id'=>Auth::id(),"carry_price"=>$d['carry_price'],"penalty"=>$d['penalty'],"extra_price"=>$d['extra_price'],"observation"=>$d['observation'],"quotation"=>$d['quotation'],"penalty_quotation"=>$d["penalty_quotation"]]);
                }
             }


             $many = DB::table("person_ticket")->where("ticket_group_id",$r->ticket_id)->where("status",1)->get();
             $manyWorkers = DB::table("worker_ticket")->where("ticket_group_id",$r->ticket_id)->where("status",1)->get();
             $many = sizeof($many) + sizeof($manyWorkers);

             $nostock = DB::table("person_ticket")->where("ticket_group_id",$r->ticket_id)->where("status",1)->where("stock",1)->get();
             $nostockWorkers = DB::table("worker_ticket")->where("ticket_group_id",$r->ticket_id)->where("status",1)->where("stock",1)->get();
             $nostock = sizeof($nostock) + sizeof($nostockWorkers);
   
             if($many===$nostock){
                 $isstock = 1;
             }


             foreach($r->tostock as $d){      
                $original = DB::table("person_ticket")->where("id",$d['person_ticket_id'])->first();
                $newprice = round(($r->price + $original->penalty + ($r->penalty / $many) + ($r->dearness / $many)),0);
                DB::table("stock_tickets")->insert(['original_ticket_id'=>$r->ticket_id,'ticket_user_id'=>$original->person_id,'ticket_type'=>$find->ticket_type,'code'=>$find->code,'penalty'=>0,'dearness'=>0,'flight_date'=>$find->flight_date,'departure_time'=>$find->departure_time,'arrival_time'=>$find->arrival_time,'price'=>$newprice,'airline_id'=>$find->airline_id,'flight_stretch_id'=>$find->flight_stretch_id,'departure_airport_id'=>$find->departure_airport_id,'arrival_airport_id'=>$find->arrival_airport_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id(),'extra_price'=>0,'plus'=>$find->plus,'user_type'=>'person']);
            }

            foreach($r->tostockWorkers as $d){
                $original = DB::table("worker_ticket")->where("id",$d['worker_ticket_id'])->first();
                $newprice = round(($r->price + $original->penalty + ($r->penalty / $many) + ($r->dearness / $many)),0);
                DB::table("stock_tickets")->insert(['original_ticket_id'=>$r->ticket_id,'ticket_user_id'=>$original->worker_id,'ticket_type'=>$find->ticket_type,'code'=>$find->code,'penalty'=>0,'dearness'=>0,'flight_date'=>$find->flight_date,'departure_time'=>$find->departure_time,'arrival_time'=>$find->arrival_time,'price'=>$newprice,'airline_id'=>$find->airline_id,'flight_stretch_id'=>$find->flight_stretch_id,'departure_airport_id'=>$find->departure_airport_id,'arrival_airport_id'=>$find->arrival_airport_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id(),'extra_price'=>0,'plus'=>$find->plus,'user_type'=>'worker']);

            }
             
             
             $nostockcount = $nostock;
             $manycount = $many;

             DB::table("ticket_groups")->where("id",$r->ticket_id)->update(['oc'=>$r->purchase_order,'oc_date'=>$r->purchase_order_date,'have_stock'=>$havestock,'is_stock'=>$isstock]);

             return $this->sendResponse(
                ['is_stock'=>$isstock,'have_stock'=>$havestock,'nostock'=>$nostockcount,'many'=>$manycount],
                'pasaje guardado'
            );
        }else{
            
            $last = DB::table("ticket_groups")->insertGetId(['oc'=>$r->purchase_order,'oc_date'=>$r->purchase_order_date,'payment_use_price'=>$r->payment_price,'payment_type'=>$r->stock_ticket_id,'passengers'=>$r->passengers,'total_price'=>$r->real_total_price,'unitary_extra_price'=>$r->total_extra_price,'stock_ticket_id'=>$r->stock_id,'voucher_id'=>$r->voucher_id,'expiration_date'=>$r->expiration_date,'ticket_type'=>'normal','code'=>$r->code,'penalty'=>$r->penalty,'dearness'=>$r->dearness,'flight_date'=>$r->flight_date,'departure_time'=>$r->departure_time,'arrival_time'=>$r->arrival_time,'price'=>$r->price,'airline_id'=>$r->airline_id,'flight_stretch_id'=>$r->flight_stretch_id,'departure_airport_id'=>$r->departure_airport_id,'arrival_airport_id'=>$r->arrival_airport_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id(),'extra_price'=>$r->extra_price,'plus'=>$r->tarifa_plus]);  
            foreach($r->selected as $d){
                $inserted = DB::table("person_ticket")->insert(['person_id'=>$d['id'],'ticket_group_id'=>$last,'created_at'=>date('Y-m-d H:i:s'),'status'=>1,'uploader_id'=>Auth::id(),"carry_price"=>$d['carry_price'],"penalty"=>$d['penalty'],"extra_price"=>$d['extra_amount'],"observation"=>$d['observation'],"quotation"=>$d['quotation'],"penalty_quotation"=>$d["penalty_quotation"]]);
            }
            foreach($r->selectedWorkers as $d){
                $inserted = DB::table("worker_ticket")->insert(['ticket_price'=>$r->price,'worker_id'=>$d['id'],'ticket_group_id'=>$last,'created_at'=>date('Y-m-d H:i:s'),'status'=>1,'uploader_id'=>Auth::id(),"carry_price"=>$d['carry_price'],"penalty"=>$d['penalty'],"extra_price"=>$d['extra_amount'],"observation"=>$d['observation'],"quotation"=>$d['quotation'],"penalty_quotation"=>$d["penalty_quotation"]]);
            }
            if($r->stock_ticket_id==1){
                DB::table("stock_tickets")->where("id",$r->stock_id)->update(['used'=>1,'stock_used_date'=>date('Y-m-d H:i:s')]);
            }
            if($r->stock_ticket_id==2){
                $voucher = DB::table("airline_vouchers")->where("id",$r->voucher_id)->first();
                if($voucher){
                    $value = $r->total_price;
                    if($r->total_price>$voucher->price){
                        $value = $voucher->price;
                    }
                    $diff = $voucher->price - $value;
                    $disabled = 1;
                    if($diff == 0){
                        $disabled = 0;
                    }
                    DB::table("log_vouchers")->insert(['date'=>date('Y-m-d H:i:s'),'required_price'=>$value,'voucher_price'=>$voucher->price,'balance'=>$diff,'ticket_group_id'=>$last,'airline_voucher_id'=>$r->voucher_id]);
                    DB::table("airline_vouchers")->where("id",$r->voucher_id)->update(['status'=>$disabled,'price_used'=>($voucher->price_used + $value),'price'=>$diff]);
                }
            }
            return $this->sendResponse(
                ['is_stock'=>$isstock,'have_stock'=>$havestock,'nostock'=>$nostockcount,'many'=>$manycount],
                'pasaje guardado'
            );
        }
        
       
    }


    public function getAllTicketsQuotable(Request $request)
    {
        $search = $request->query('search');
        $tickets = DB::table("ticket_groups")
                        ->join("airlines","airlines.id","=","ticket_groups.airline_id")
                        ->join("flight_stretches","flight_stretches.id","=","ticket_groups.flight_stretch_id")
                        ->select("ticket_groups.oc","ticket_groups.oc_date","ticket_groups.airline_id","ticket_groups.code","ticket_groups.ticket_type","ticket_groups.passengers","ticket_groups.id","ticket_groups.plus","ticket_groups.id","ticket_groups.arrival_airport_id","ticket_groups.departure_airport_id","ticket_groups.flight_date","ticket_groups.departure_time","ticket_groups.arrival_time","airlines.name as airline","flight_stretches.name as stretch","ticket_groups.extra_price","ticket_groups.price")
                       // ->where("ticket_groups.invoice_id",null)
                       /* ->when($search, function ($query, $search) {
                            return $query->where('airlines.name', 'like', "%$search%");
                        })*/
                        ->where("ticket_groups.crew_id",null)
                        ->where("ticket_groups.is_stock","0")
                        ->orderBy("ticket_groups.id","desc")
                        ->get();

                        foreach($tickets as $t){
                            $supplier = DB::table("suppliers")->where("code",$t->airline_id)->where("supplier_type_id",5)->first();
                            $t->supplier_id = $supplier->id;

                            $airport1 = DB::table("airports")->where("id",$t->departure_airport_id)->first();
                            $airport2 = DB::table("airports")->where("id",$t->arrival_airport_id)->first();
                            if($airport1){
                                $t->airport1 = $airport1->city;
                            }
                            if($airport2){
                                $t->airport2 = $airport2->city;
                            }
                            $carry = 0;
                            $passengers = $t->passengers;
                            $t->type = $t->ticket_type;

                            $count = DB::table("person_ticket")->where("status","1")->where("ticket_group_id",$t->id)->get();
                            if(sizeof($count)>0){
                                foreach($count as $c){
                                    $carry = $carry + intval($c->carry_price);
                                }
                            }
                            $count = DB::table("worker_ticket")->where("status","1")->where("ticket_group_id",$t->id)->get();
                            if(sizeof($count)>0){
                                foreach($count as $c){
                                    $carry = $carry + intval($c->carry_price);
                                }
                            }
                            if($t->ticket_type=='crew'){
                                $cantidad = DB::table("tickets")->where("ticket_group_id",$t->id)->where("ticket_status_id","1")->get();
                                foreach($cantidad as $c){
                                    $carry = $carry + intval($c->carry_price);
                                }
                            }
                           
                            $trip = DB::table("crew_tickets")->where("ticket_group_id",$t->id)->first();
                            if($trip){
                                $crew = DB::table("crews")
                                ->join("ships","ships.id","=","crews.ship_id")
                                ->join("clients","clients.id","=","crews.client_id")
                                ->select("clients.name as client","ships.name as ship","crews.upload_date")
                                ->where("crews.id",$trip->crew_id)
                                ->first();
                                if($crew){
                                    $t->upload_date = $crew->upload_date;
                                    $t->crew = $crew->ship." / ".$crew->client.'('.$crew->upload_date.')';
                                }else{
                                    $t->crew = '-';
                                    $t->upload_date = '';
                                }
                                
                            }else{
                                $t->crew = '-';
                                $t->upload_date = '';
                            }
                            if($t->code==null || $t->code==''){
                                $t->code = '-';
                            }

                            $t->total_price = intval($passengers) * intval($t->price);
                            $t->carry = $carry;
                        }
      
        return $this->sendResponse(
            $tickets,
            'pasajes encontrados'
        );
    }


    public function getAllTickets(Request $request)
    {
        $search = $request->query('search');
        $tickets = DB::table("ticket_groups")
                        ->join("airlines","airlines.id","=","ticket_groups.airline_id")
                        ->join("flight_stretches","flight_stretches.id","=","ticket_groups.flight_stretch_id")
                        ->select("ticket_groups.oc","ticket_groups.oc_date","ticket_groups.code","ticket_groups.ticket_type","ticket_groups.passengers","ticket_groups.id","ticket_groups.plus","ticket_groups.id","ticket_groups.arrival_airport_id","ticket_groups.departure_airport_id","ticket_groups.flight_date","ticket_groups.departure_time","ticket_groups.arrival_time","airlines.name as airline","flight_stretches.name as stretch","ticket_groups.extra_price","ticket_groups.price")
                        /*->when($search, function ($query, $search) {
                            return $query->where('airlines.name', 'like', "%$search%");
                        })*/
                        ->where("ticket_groups.is_stock","0")
                        ->orderBy("ticket_groups.id","desc")
                        ->get();

                        foreach($tickets as $t){
                            $airport1 = DB::table("airports")->where("id",$t->departure_airport_id)->first();
                            $airport2 = DB::table("airports")->where("id",$t->arrival_airport_id)->first();
                            if($airport1){
                                $t->airport1 = $airport1->city;
                            }
                            if($airport2){
                                $t->airport2 = $airport2->city;
                            }
                            $carry = 0;
                            $passengers = $t->passengers;
                            $t->type = $t->ticket_type;

                            $count = DB::table("person_ticket")->where("status","1")->where("ticket_group_id",$t->id)->get();
                            if(sizeof($count)>0){
                                foreach($count as $c){
                                    $carry = $carry + intval($c->carry_price);
                                }
                            }
                            $count = DB::table("worker_ticket")->where("status","1")->where("ticket_group_id",$t->id)->get();
                            if(sizeof($count)>0){
                                foreach($count as $c){
                                    $carry = $carry + intval($c->carry_price);
                                }
                            }
                            if($t->ticket_type=='crew'){
                                $cantidad = DB::table("tickets")->where("ticket_group_id",$t->id)->where("ticket_status_id","1")->get();
                                foreach($cantidad as $c){
                                    $carry = $carry + intval($c->carry_price);
                                }
                            }
                           
                            $trip = DB::table("crew_tickets")->where("ticket_group_id",$t->id)->first();
                            if($trip){
                                $crew = DB::table("crews")
                                ->join("ships","ships.id","=","crews.ship_id")
                                ->join("clients","clients.id","=","crews.client_id")
                                ->select("clients.name as client","ships.name as ship","crews.upload_date")
                                ->where("crews.id",$trip->crew_id)
                                ->first();
                                if($crew){
                                    $t->upload_date = $crew->upload_date;
                                    $t->crew = $crew->ship." / ".$crew->client.'('.$crew->upload_date.')';
                                }else{
                                    $t->crew = ' - ';
                                    $t->upload_date = '';
                                }
                                
                            }else{
                                $t->crew = ' - ';
                                $t->upload_date = '';
                            }
                            if($t->code==null || $t->code==''){
                                $t->code = ' - ';
                            }

                            $t->total_price = intval($passengers) * intval($t->price);
                            $t->carry = $carry;
                        }
      
        return $this->sendResponse(
            $tickets,
            'pasajes encontrados'
        );
    }




    public function getTicket(int $id)
    {


    
        $tickets = DB::table("ticket_groups")
                        ->join("airlines","airlines.id","=","ticket_groups.airline_id")
                        ->join("flight_stretches","flight_stretches.id","=","ticket_groups.flight_stretch_id")
                        ->select("ticket_groups.invoice_id","ticket_groups.oc_date","ticket_groups.oc","ticket_groups.ticket_status_id","ticket_groups.passengers","ticket_groups.stock_ticket_id","ticket_groups.voucher_id","ticket_groups.payment_type","ticket_groups.expiration_date","ticket_groups.penalty","ticket_groups.code","ticket_groups.dearness","ticket_groups.is_stock","ticket_groups.have_stock","ticket_groups.plus","ticket_groups.id","ticket_groups.arrival_airport_id","ticket_groups.departure_airport_id","ticket_groups.flight_date","ticket_groups.departure_time","ticket_groups.arrival_time","airlines.id as airline","flight_stretches.id as stretch","ticket_groups.extra_price","ticket_groups.price")
                        ->where("ticket_groups.id",$id)
                        ->first();

        $person = DB::table("person_ticket")->join("persons","persons.id","=","person_ticket.person_id")->select("person_ticket.invoice_id","person_ticket.penalty_invoice_id","person_ticket.penalty_quotation","persons.id","persons.name","persons.last_name","persons.rut","person_ticket.carry_price","person_ticket.stock","person_ticket.id as person_ticket_id","person_ticket.extra_price","person_ticket.quotation","person_ticket.penalty","person_ticket.observation","person_ticket.ticket_status_id")->where("ticket_group_id",$id)->where("person_ticket.status",1)->get();
        $workers = DB::table("worker_ticket")->join("workers","workers.id","=","worker_ticket.worker_id")->select("worker_ticket.invoice_id","worker_ticket.penalty_invoice_id","worker_ticket.penalty_quotation","workers.id","workers.name","workers.last_name","workers.rut","worker_ticket.carry_price","worker_ticket.stock","worker_ticket.id as worker_ticket_id","worker_ticket.extra_price","worker_ticket.quotation","worker_ticket.penalty","worker_ticket.observation","worker_ticket.ticket_status_id")->where("ticket_group_id",$id)->where("worker_ticket.status",1)->get();
       
        $supplier = DB::table("suppliers")->where("code",$tickets->airline)->first();

        $tickets->supplier_id = $supplier->id;

        $tickets->persons = $person;
        $tickets->workers = $workers;
                                   
      
        return $this->sendResponse(
            $tickets,
            'pasajes encontrados'
        );
    }


    public function getTicketExternal(int $id)
    {
        $tickets = DB::table("ticket_groups")
                        ->join("airlines","airlines.id","=","ticket_groups.airline_id")
                        ->join("flight_stretches","flight_stretches.id","=","ticket_groups.flight_stretch_id")
                        ->select("ticket_groups.invoice_id","airlines.id as airline","ticket_groups.oc_date","ticket_groups.oc","ticket_groups.ticket_status_id","ticket_groups.passengers","ticket_groups.stock_ticket_id","ticket_groups.voucher_id","ticket_groups.payment_type","ticket_groups.expiration_date","ticket_groups.penalty","ticket_groups.code","ticket_groups.dearness","ticket_groups.is_stock","ticket_groups.have_stock","ticket_groups.plus","ticket_groups.id","ticket_groups.arrival_airport_id","ticket_groups.departure_airport_id","ticket_groups.flight_date","ticket_groups.departure_time","ticket_groups.arrival_time","airlines.name as airline_name","flight_stretches.id as stretch","ticket_groups.extra_price","ticket_groups.price","ticket_groups.total_price")
                        ->where("ticket_groups.id",$id)
                        ->first();

        $person = DB::table("person_ticket")->join("persons","persons.id","=","person_ticket.person_id")->select("person_ticket.invoice_id","person_ticket.penalty_invoice_id","person_ticket.penalty_quotation","persons.id","persons.name","persons.last_name","persons.rut","person_ticket.carry_price","person_ticket.stock","person_ticket.id as person_ticket_id","person_ticket.extra_price","person_ticket.quotation","person_ticket.penalty","person_ticket.observation","person_ticket.ticket_status_id")->where("ticket_group_id",$id)->where("person_ticket.status",1)->get();
        $workers = DB::table("worker_ticket")->join("workers","workers.id","=","worker_ticket.worker_id")->select("worker_ticket.invoice_id","worker_ticket.penalty_invoice_id","worker_ticket.penalty_quotation","workers.id","workers.name","workers.last_name","workers.rut","worker_ticket.carry_price","worker_ticket.stock","worker_ticket.id as worker_ticket_id","worker_ticket.extra_price","worker_ticket.quotation","worker_ticket.penalty","worker_ticket.observation","worker_ticket.ticket_status_id")->where("ticket_group_id",$id)->where("worker_ticket.status",1)->get();
       
        $supplier = DB::table("suppliers")->where("code",$tickets->airline)->first();

        $air1 = DB::table("airports")->where("id",$tickets->departure_airport_id)->first();
        $air2 = DB::table("airports")->where("id",$tickets->arrival_airport_id)->first();

        $tickets->airport1 = $air1->iata." - ".$air1->city;
        $tickets->airport2 = $air2->iata." - ".$air2->city;

        $tickets->supplier_id = $supplier->id;

        $tickets->persons = $person;
        $tickets->workers = $workers;

        $tickets->passengers = sizeof($person) + sizeof($workers);
                                   
      
        return $this->sendResponse(
            $tickets,
            'pasajes encontrados'
        );
    }

    public function setStockTicket(Request $r){

        DB::table("person_ticket")->where("id",$r->person_ticket_id)->update(['stock'=>1]);

        $many = DB::table("person_ticket")->where("ticket_group_id",$r->ticket_id)->where("status",1)->get();
        $nostock = DB::table("person_ticket")->where("ticket_group_id",$r->ticket_id)->where("status",1)->where("stock",1)->get();
        $isstock = 0;
        if(sizeof($many)==sizeof($nostock)){
            $isstock = 1;
        }


        DB::table("ticket_groups")->where("id",$r->ticket_id)->update(['have_stock'=>1,'is_stock'=>$isstock]);

        return $this->sendResponse(
           $isstock,
            'pasajes pasado a stock'
        );
    }


    public function getAllTicketsLost(Request $request)
    {

        $search = $request->query('search');
        $tickets = DB::table("ticket_groups")
                        ->join("airlines","airlines.id","=","ticket_groups.airline_id")
                        ->join("flight_stretches","flight_stretches.id","=","ticket_groups.flight_stretch_id")
                        ->join("worker_ticket","worker_ticket.ticket_group_id","=","ticket_groups.id")
                        ->join("workers","workers.id","=","worker_ticket.worker_id")
                        ->select("ticket_groups.oc","ticket_groups.oc_date","worker_ticket.lost_user_id","workers.name as name","workers.last_name as last_name","workers.rut as rut","worker_ticket.lost_datetime as lost_datetime","worker_ticket.worker_id as worker_id","ticket_groups.code","ticket_groups.ticket_type","ticket_groups.id","ticket_groups.plus","ticket_groups.arrival_airport_id","ticket_groups.departure_airport_id","ticket_groups.flight_date","ticket_groups.departure_time","ticket_groups.arrival_time","airlines.name as airline","flight_stretches.name as stretch","ticket_groups.extra_price","ticket_groups.price")
                      /*  ->when($search, function ($query, $search) {
                            return $query->where('workers.name', 'like', "%$search%")
                                ->orWhere('workers.last_name', 'like', "%$search%")
                                ->orWhere('ticket_groups.code', 'like', "%$search%")
                                ->orWhere('airlines.name', 'like', "%$search%");
                        })*/
                        ->where("worker_ticket.ticket_status_id",5)
                        ->orderBy("worker_ticket.lost_datetime","desc")
                        ->get();

                        foreach($tickets as $t){
                            $airport1 = DB::table("airports")->where("id",$t->departure_airport_id)->first();
                            $airport2 = DB::table("airports")->where("id",$t->arrival_airport_id)->first();
                            if($airport1){
                                $t->airport1 = $airport1->city;
                            }
                            if($airport2){
                                $t->airport2 = $airport2->city;
                            }

                            $role = "";
                                            
                                $person = DB::table("workers")->where("id",$t->worker_id)->first();
                                $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$person->id)->get();
                                $rol = "";
                                foreach($roles as $r){
                                    $role = $role." ".$r->name;
                                }
                                 
                            $t->role = $role; 

                            $persona = DB::table("users")->where("id",$t->lost_user_id)->first();
                            $t->lost_name = $persona->name." ".$persona->last_name;
                                    
                        }

        $tickets2 = DB::table("ticket_groups")
                        ->join("airlines","airlines.id","=","ticket_groups.airline_id")
                        ->join("flight_stretches","flight_stretches.id","=","ticket_groups.flight_stretch_id")
                        ->join("person_ticket","person_ticket.ticket_group_id","=","ticket_groups.id")
                        ->join("persons","persons.id","=","person_ticket.person_id")
                        ->select("ticket_groups.oc","ticket_groups.oc_date","person_ticket.lost_user_id","persons.role","persons.name as name","persons.last_name as last_name","persons.rut as rut","person_ticket.lost_datetime as lost_datetime","person_ticket.person_id as person_id","ticket_groups.code","ticket_groups.ticket_type","ticket_groups.id","ticket_groups.plus","ticket_groups.arrival_airport_id","ticket_groups.departure_airport_id","ticket_groups.flight_date","ticket_groups.departure_time","ticket_groups.arrival_time","airlines.name as airline","flight_stretches.name as stretch","ticket_groups.extra_price","ticket_groups.price")
                        ->where("person_ticket.ticket_status_id",5)
                        /*->when($search, function ($query, $search) {
                            return $query->where('persons.name', 'like', "%$search%")
                                ->orWhere('persons.last_name', 'like', "%$search%")
                                ->orWhere('ticket_groups.code', 'like', "%$search%")
                                ->orWhere('airlines.name', 'like', "%$search%");
                        })*/
                        ->get();

                        foreach($tickets2 as $t){
                            $airport1 = DB::table("airports")->where("id",$t->departure_airport_id)->first();
                            $airport2 = DB::table("airports")->where("id",$t->arrival_airport_id)->first();
                            if($airport1){
                                $t->airport1 = $airport1->city;
                            }
                            if($airport2){
                                $t->airport2 = $airport2->city;
                            }                       
                            $t->role = $role; 
                            $persona = DB::table("users")->where("id",$t->lost_user_id)->first();
                            $t->lost_name = $persona->name." ".$persona->last_name;       
                        }
      
      $tickets =  $tickets->merge($tickets2);
      
        return $this->sendResponse(
            $tickets,
            'pasajes perdidos'
        );
    }



    public function getAllTicketsInStock(Request $request)
    {

        $search = $request->query('search');
        $tickets = DB::table("stock_tickets")
                        ->join("airlines","airlines.id","=","stock_tickets.airline_id")
                        ->join("flight_stretches","flight_stretches.id","=","stock_tickets.flight_stretch_id")
                        ->select("stock_tickets.original_ticket_id","stock_tickets.oc","stock_tickets.oc_date","stock_tickets.dearness","stock_tickets.penalty","stock_tickets.is_carry","stock_tickets.user_type","stock_tickets.code","stock_tickets.ticket_type","stock_tickets.ticket_user_id","stock_tickets.id","stock_tickets.plus","stock_tickets.arrival_airport_id","stock_tickets.departure_airport_id","stock_tickets.flight_date","stock_tickets.departure_time","stock_tickets.arrival_time","airlines.name as airline","flight_stretches.name as stretch","stock_tickets.extra_price","stock_tickets.price")
                        ->where("stock_tickets.is_voucher",0)
                        ->where("stock_tickets.used",0)
                        /*->when($search, function ($query, $search) {
                            return $query->where('airlines.name', 'like', "%$search%");
                        })*/
                        ->orderBy("stock_tickets.id","desc")
                        ->get();

                        foreach($tickets as $t){

                            $ticketoriginal = DB::table("ticket_groups")->where("id",$t->original_ticket_id)->first();
                            if($ticketoriginal){
                                $t->oc = $ticketoriginal->oc;
                                $t->oc_date = $ticketoriginal->oc_date;
                            }

                            $airport1 = DB::table("airports")->where("id",$t->departure_airport_id)->first();
                            $airport2 = DB::table("airports")->where("id",$t->arrival_airport_id)->first();
                            if($airport1){
                                $t->airport1 = $airport1->city;
                            }
                            if($airport2){
                                $t->airport2 = $airport2->city;
                            }
                            $name = "";
                            $rut ="";
                            $role = "";
                            if($t->user_type=='new_stock'){

                            }
                            if($t->user_type=='person'){
                                $person = DB::table("persons")->where("id",$t->ticket_user_id)->first();
                                $role = $person->role;   
                                $rut = $person->rut;
                                $name = $person->name." ".$person->last_name;
                            }
                            if($t->user_type=='worker'){                    
                                $person = DB::table("workers")->where("id",$t->ticket_user_id)->first();
                                $rut = $person->rut;
                                $name = $person->name." ".$person->last_name;
                                $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$person->id)->get();
                                $rol = "";
                                foreach($roles as $r){
                                    $role = $role." ".$r->name;
                                }
                                 
                            }
                            $t->name = $name;
                            $t->rut = $rut;
                            $t->role = $role; 
                                    
                        }
      
        return $this->sendResponse(
            $tickets,
            'pasajes encontrados'
        );
    }


  /* public function setVoucherTicket(Request $r){
        $voucher = DB::table("stock_tickets")->where("id",$r->stock_ticket_id)->update(['is_voucher'=>1,'date_to'=>date('Y-m-s H:i:s'),'user_id_to'=>Auth::id()]);
        $wich = DB::table("stock_tickets")->where("id",$r->stock_ticket_id)->first();
        $existe = DB::table("airline_vouchers")->where("ticket_group_id",$wich->original_ticket_id)->where("airline_id",$wich->airline_id)->where("code",$wich->code)->first();
        if($existe){
            if($wich->is_carry==0){
                $voucher = DB::table("airline_vouchers")->where("id",$existe->id)->update(['price'=>intval($wich->price)+intval($existe->price),'with_ticket'=>1]);
            }else{
                $voucher = DB::table("airline_vouchers")->where("id",$existe->id)->update(['price'=>intval($wich->price)+intval($existe->price),'with_carry'=>1]);
            }
            
        }else{
            if($wich->is_carry==0){
                $voucher = DB::table("airline_vouchers")->insert(['airline_id'=>$wich->airline_id,'ticket_group_id'=>$wich->original_ticket_id,'price'=>$wich->price,'code'=>$wich->code,'with_ticket'=>1]);
            }else{
                $voucher = DB::table("airline_vouchers")->insert(['airline_id'=>$wich->airline_id,'ticket_group_id'=>$wich->original_ticket_id,'price'=>$wich->price,'code'=>$wich->code,'with_carry'=>1]);
            }
            
        }
        return $this->sendResponse(
           $voucher,
            'pasajes pasado a stock'
        );
    }*/

    public function setVoucherTicket(Request $r){
        $voucher = DB::table("stock_tickets")->where("id",$r->stock_ticket_id)->update(['is_voucher'=>1,'date_to'=>date('Y-m-d H:i:s'),'user_id_to'=>Auth::id()]);
        $wich = DB::table("stock_tickets")->where("id",$r->stock_ticket_id)->first();
        $existe = DB::table("airline_vouchers")->where("airline_id",$wich->airline_id)->where("code",$wich->code)->first();
       
       if($wich->ticket_type=="new_stock"){
            if($existe){
                if($wich->is_carry==0){
                    $voucher = DB::table("airline_vouchers")->where("id",$existe->id)->update(['price'=>intval(($wich->price + $wich->dearness + $wich->penalty))+intval($existe->price),'with_ticket'=>1]);
                }else{
                    $voucher = DB::table("airline_vouchers")->where("id",$existe->id)->update(['price'=>intval(($wich->price + $wich->dearness + $wich->penalty))+intval($existe->price),'with_carry'=>1]);
                }      
            }else{
                if($wich->is_carry==0){
                    $voucher = DB::table("airline_vouchers")->insertGetId(['airline_id'=>$wich->airline_id,'ticket_group_id'=>null,'price'=>($wich->price + $wich->dearness + $wich->penalty),'code'=>$wich->code,'with_ticket'=>1]);
                }else{
                    $voucher = DB::table("airline_vouchers")->insertGetId(['airline_id'=>$wich->airline_id,'ticket_group_id'=>null,'price'=>($wich->price + $wich->dearness + $wich->penalty),'code'=>$wich->code,'with_carry'=>1]);
                }       
            }
       }else{
            if($existe){
                if($wich->is_carry==0){
                    $voucher = DB::table("airline_vouchers")->where("id",$existe->id)->update(['price'=>intval($wich->price)+intval($existe->price),'with_ticket'=>1]);
                }else{
                    $voucher = DB::table("airline_vouchers")->where("id",$existe->id)->update(['price'=>intval($wich->price)+intval($existe->price),'with_carry'=>1]);
                }      
            }else{
                if($wich->is_carry==0){
                    $voucher = DB::table("airline_vouchers")->insertGetId(['airline_id'=>$wich->airline_id,'ticket_group_id'=>null,'price'=>$wich->price,'code'=>$wich->code,'with_ticket'=>1]);
                }else{
                    $voucher = DB::table("airline_vouchers")->insertGetId(['airline_id'=>$wich->airline_id,'ticket_group_id'=>null,'price'=>$wich->price,'code'=>$wich->code,'with_carry'=>1]);
                }       
            }
       }
        




        return $this->sendResponse(
           $voucher,
            'pasajes pasado a stock'
        );
    }





    public function getAllVouchers(){
        $vouchers = DB::table("airline_vouchers")->join("airlines","airlines.id","=","airline_vouchers.airline_id")->select("airline_vouchers.with_carry","airline_vouchers.with_ticket","airline_vouchers.price_used","airlines.name as airline","airline_vouchers.code","airline_vouchers.price","airline_vouchers.id")
                    ->orderBy("airline_vouchers.id","desc")
                    ->get();

                    return $this->sendResponse(
                        $vouchers,
                         'vouchers encontrados'
                     );
    }

    public function getVouchers(int $id){
        $vouchers = DB::table("airline_vouchers")->join("airlines","airlines.id","=","airline_vouchers.airline_id")->select("airline_vouchers.with_carry","airline_vouchers.with_ticket","airline_vouchers.price_used","airlines.name as airline","airline_vouchers.code","airline_vouchers.price","airline_vouchers.id")
                    ->where('airlines.id',$id)
                    ->where('airline_vouchers.status',1)
                    ->get();

                    foreach($vouchers as $v){
                        if($v->with_carry==1 && $v->with_ticket==1){
                            $v->contain_type = ' - (P y M)';
                        }else{
                            $v->contain_type = '';
                        }
                        
                    }

                    return $this->sendResponse(
                        $vouchers,
                         'vouchers encontrados'
                     );
    }


    public function getVouchersWithUsed(int $id,int $voucher_id,int $ticket_id){
       /* $voucher_used = DB::table("airline_vouchers")->join("airlines","airlines.id","=","airline_vouchers.airline_id")->select("airline_vouchers.price_used","airlines.name as airline","airline_vouchers.code","airline_vouchers.price","airline_vouchers.id","airline_vouchers.airline_id")
                    ->where("airline_vouchers.id",$voucher_id)
                    ->get();

        foreach($voucher_used as $v){
            $log = DB::table("log_vouchers")->where("airline_voucher_id",$v->id)->where("ticket_group_id",$ticket_id)->where("status",1)->first();
            if($log){
                $v->price = $log->voucher_price;
            }
        }*/
        $vouchers = DB::table("airline_vouchers")->join("airlines","airlines.id","=","airline_vouchers.airline_id")->select("airline_vouchers.with_carry","airline_vouchers.with_ticket","airline_vouchers.price_used","airlines.name as airline","airline_vouchers.code","airline_vouchers.price","airline_vouchers.id","airline_vouchers.airline_id")
                    ->where('airlines.id',$id)
                    ->where(function ($query) use($voucher_id) {
                        $query->where('airline_vouchers.status',1)
                              ->orWhere('airline_vouchers.id', '=', $voucher_id);
                    })
                    ->get();

                    foreach($vouchers as $v){
                        if($v->id == $voucher_id){
                            $log = DB::table("log_vouchers")->where("airline_voucher_id",$v->id)->where("ticket_group_id",$ticket_id)->where("status",1)->first();
                            if($log){
                                $v->price = $log->voucher_price;
                            }
                        }  

 
                        if($v->with_carry==1 && $v->with_ticket==1){
                            $v->contain_type = ' - (P y M)';
                        }else{
                            $v->contain_type = '';
                        }
                        
                    }

                    //$vouchers = $vouchers->merge($voucher_used);

                    return $this->sendResponse(
                        $vouchers,
                         'vouchers encontrados'
                     );
    }

    public function getWorkers(Request $request){
        $search = $request->query('search');
        $workers = DB::table("workers")->where("deleted_at",null)->when($search, function ($query, $search) {
            return $query->where('name', 'like', "%$search%")
                ->orWhere('last_name', 'like', "%$search%")
                ->orWhere('rut', 'like', "%$search%")
                ->orWhere('phone_number', 'like', "%$search%")
                ->orWhere('code', 'like', "%$search%");
        })->orderBy('name','asc')->get();
        return $this->sendResponse($workers, 'Listado de trabajadores');
    }

    public function getStocks(int $id){
        $tickets = DB::table("stock_tickets")->where('airline_id',$id)->where("is_voucher",0)->where("used",0)->get();
                     $name = "Nuevo Stock";
                    foreach($tickets as $t){
                        if($t->user_type=='person'){
                            $person = DB::table("persons")->where("id",$t->ticket_user_id)->first();
                            if($person){
                                $name = $person->name." ".$person->last_name;
                            }
                        }
                        if($t->user_type=='worker'){
                            $person = DB::table("workers")->where("id",$t->ticket_user_id)->first();
                            if($person){
                                $name = $person->name." ".$person->last_name;
                            }
                        }
                        $t->name = $name;
                    }

                    return $this->sendResponse(
                        $tickets,
                         'stocks encontrados'
                     );
    }


    public function getStocksWithUsed(int $id,int $ticket_id){
        $tickets_used = DB::table("stock_tickets")->where("id",$ticket_id)->get();
        $tickets = DB::table("stock_tickets")->where('airline_id',$id)->where("is_voucher",0)->where("used",0)->get();


                     $name = "Nuevo Stock";
                     $tickets = $tickets->merge($tickets_used);
                    foreach($tickets as $t){
                        if($t->user_type=='person'){
                            $person = DB::table("persons")->where("id",$t->ticket_user_id)->first();
                            if($person){
                                $name = $person->name." ".$person->last_name;
                            }
                        }
                        if($t->user_type=='worker'){
                            $person = DB::table("workers")->where("id",$t->ticket_user_id)->first();
                            if($person){
                                $name = $person->name." ".$person->last_name;
                            }
                        }
                        $t->name = $name;
                    }

                    return $this->sendResponse(
                        $tickets,
                         'stocks encontrados'
                     );
    }


    public function getVoucherDetails(int $id)
    {
 
        $tickets = DB::table("ticket_groups")
                        ->join("person_ticket","person_ticket.ticket_group_id","=","ticket_groups.id")
                        ->join("airlines","airlines.id","=","ticket_groups.airline_id")
                        ->join("flight_stretches","flight_stretches.id","=","ticket_groups.flight_stretch_id")
                        ->select("person_ticket.penalty","person_ticket.person_id","ticket_groups.created_at","ticket_groups.code","ticket_groups.ticket_type","ticket_groups.passengers","ticket_groups.id","ticket_groups.plus","ticket_groups.id","ticket_groups.arrival_airport_id","ticket_groups.departure_airport_id","ticket_groups.flight_date","ticket_groups.departure_time","ticket_groups.arrival_time","airlines.name as airline","flight_stretches.name as stretch","ticket_groups.extra_price","ticket_groups.price")
                        ->where("ticket_groups.voucher_id",$id)
                        ->orderBy("ticket_groups.id","desc")
                        ->get();

                        foreach($tickets as $t){

                            $name = "";
                            $rut ="";
                            $role = "";
                           
                                $person = DB::table("persons")->where("id",$t->person_id)->first();
                                $role = $person->role;   
                                $rut = $person->rut;
                                $name = $person->name." ".$person->last_name;
                           
                            $t->name = $name;
                            $t->rut = $rut;
                            $t->role = $role; 

                            $voucher = DB::table("log_vouchers")->where("ticket_group_id",$t->id)->where("airline_voucher_id",$id)->where("status",1)->first();
                            if($voucher){
                                $t->voucher_required = $voucher->required_price;
                                $t->voucher_price = $voucher->voucher_price;
                                $t->balance = $voucher->balance;
                            }else{
                                $t->voucher_required = 0;
                                $t->voucher_price = 0;
                                $t->balance = 0;
                            }

                            $airport1 = DB::table("airports")->where("id",$t->departure_airport_id)->first();
                            $airport2 = DB::table("airports")->where("id",$t->arrival_airport_id)->first();
                            if($airport1){
                                $t->airport1 = $airport1->city;
                            }
                            if($airport2){
                                $t->airport2 = $airport2->city;
                            }
                            $carry = 0;
                            $passengers = $t->passengers;
                            $t->type = $t->ticket_type;

                            $count = DB::table("person_ticket")->where("status","1")->where("ticket_group_id",$t->id)->get();
                            if(sizeof($count)>0){
                                foreach($count as $c){
                                    $carry = $carry + intval($c->carry_price);
                                }
                            }
                            $count = DB::table("worker_ticket")->where("status","1")->where("ticket_group_id",$t->id)->get();
                            if(sizeof($count)>0){
                                foreach($count as $c){
                                    $carry = $carry + intval($c->carry_price);
                                }
                            }




                            if($t->ticket_type=='crew'){
                                $cantidad = DB::table("tickets")->where("ticket_group_id",$t->id)->where("ticket_status_id","1")->get();
                                foreach($cantidad as $c){
                                    $carry = $carry + intval($c->carry_price);
                                }
                            }
                           
                            $trip = DB::table("crew_tickets")->where("ticket_group_id",$t->id)->first();
                            if($trip){
                                $crew = DB::table("crews")
                                ->join("ships","ships.id","=","crews.ship_id")
                                ->join("clients","clients.id","=","crews.client_id")
                                ->select("clients.name as client","ships.name as ship","crews.upload_date")
                                ->where("crews.id",$trip->crew_id)
                                ->first();
                                if($crew){
                                    $t->upload_date = $crew->upload_date;
                                    $t->crew = $crew->ship." / ".$crew->client.'('.$crew->upload_date.')';
                                }else{
                                    $t->crew = ' - ';
                                    $t->upload_date = '';
                                }
                                
                            }else{
                                $t->crew = ' - ';
                                $t->upload_date = '';
                            }
                            if($t->code==null || $t->code==''){
                                $t->code = ' - ';
                            }

                            $t->total_price = intval($passengers) * intval($t->price);
                            $t->carry = $carry;
                        }

         $ticketsWorkers = DB::table("ticket_groups")
                        ->join("worker_ticket","worker_ticket.ticket_group_id","=","ticket_groups.id")
                        ->join("airlines","airlines.id","=","ticket_groups.airline_id")
                        ->join("flight_stretches","flight_stretches.id","=","ticket_groups.flight_stretch_id")
                        ->select("worker_ticket.penalty","worker_ticket.worker_id","ticket_groups.created_at","ticket_groups.code","ticket_groups.ticket_type","ticket_groups.passengers","ticket_groups.id","ticket_groups.plus","ticket_groups.id","ticket_groups.arrival_airport_id","ticket_groups.departure_airport_id","ticket_groups.flight_date","ticket_groups.departure_time","ticket_groups.arrival_time","airlines.name as airline","flight_stretches.name as stretch","ticket_groups.extra_price","ticket_groups.price")
                        ->where("ticket_groups.voucher_id",$id)
                        ->orderBy("ticket_groups.id","desc")
                        ->get();

                        foreach($ticketsWorkers as $t){

                            $name = "";
                            $rut ="";
                            $role = "";
                          
                                          
                                $person = DB::table("workers")->where("id",$t->worker_id)->first();
                                $rut = $person->rut;
                                $name = $person->name." ".$person->last_name;
                                $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$person->id)->get();
                                $rol = "";
                                foreach($roles as $r){
                                    $role = $role." ".$r->name;
                                }
                                 
                            

                            $t->name = $name;
                            $t->rut = $rut;
                            $t->role = $role; 

                            $voucher = DB::table("log_vouchers")->where("ticket_group_id",$t->id)->where("airline_voucher_id",$id)->where("status",1)->first();
                            if($voucher){
                                $t->voucher_required = $voucher->required_price;
                                $t->voucher_price = $voucher->voucher_price;
                                $t->balance = $voucher->balance;
                            }else{
                                $t->voucher_required = 0;
                                $t->voucher_price = 0;
                                $t->balance = 0;
                            }

                            $airport1 = DB::table("airports")->where("id",$t->departure_airport_id)->first();
                            $airport2 = DB::table("airports")->where("id",$t->arrival_airport_id)->first();
                            if($airport1){
                                $t->airport1 = $airport1->city;
                            }
                            if($airport2){
                                $t->airport2 = $airport2->city;
                            }
                            $carry = 0;
                            $passengers = $t->passengers;
                            $t->type = $t->ticket_type;

                            $count = DB::table("person_ticket")->where("status","1")->where("ticket_group_id",$t->id)->get();
                            if(sizeof($count)>0){
                                foreach($count as $c){
                                    $carry = $carry + intval($c->carry_price);
                                }
                            }
                            $count = DB::table("worker_ticket")->where("status","1")->where("ticket_group_id",$t->id)->get();
                            if(sizeof($count)>0){
                                foreach($count as $c){
                                    $carry = $carry + intval($c->carry_price);
                                }
                            }
                            if($t->ticket_type=='crew'){
                                $cantidad = DB::table("tickets")->where("ticket_group_id",$t->id)->where("ticket_status_id","1")->get();
                                foreach($cantidad as $c){
                                    $carry = $carry + intval($c->carry_price);
                                }
                            }
                           
                            $trip = DB::table("crew_tickets")->where("ticket_group_id",$t->id)->first();
                            if($trip){
                                $crew = DB::table("crews")
                                ->join("ships","ships.id","=","crews.ship_id")
                                ->join("clients","clients.id","=","crews.client_id")
                                ->select("clients.name as client","ships.name as ship","crews.upload_date")
                                ->where("crews.id",$trip->crew_id)
                                ->first();
                                if($crew){
                                    $t->upload_date = $crew->upload_date;
                                    $t->crew = $crew->ship." / ".$crew->client.'('.$crew->upload_date.')';
                                }else{
                                    $t->crew = ' - ';
                                    $t->upload_date = '';
                                }
                                
                            }else{
                                $t->crew = ' - ';
                                $t->upload_date = '';
                            }
                            if($t->code==null || $t->code==''){
                                $t->code = ' - ';
                            }

                            $t->total_price = intval($passengers) * intval($t->price);
                            $t->carry = $carry;
                        }
      
                        $tickets = $tickets->merge($ticketsWorkers);

        return $this->sendResponse(
            $tickets,
            'pasajes de voucher encontrados'
        );
    }


    public function getVouchersOf(int $id){

        $code = DB::table("airline_vouchers")->where("id",$id)->first();
        $tickets = DB::table("stock_tickets")
                        ->join("airlines","airlines.id","=","stock_tickets.airline_id")
                        ->join("users","users.id","=","stock_tickets.user_id_to")
                        ->join("flight_stretches","flight_stretches.id","=","stock_tickets.flight_stretch_id")
                        ->select("stock_tickets.oc","stock_tickets.oc_date","stock_tickets.dearness","stock_tickets.penalty","stock_tickets.is_carry","stock_tickets.date_to","users.name as user_name_to","users.last_name as last_name_to","stock_tickets.user_type","stock_tickets.code","stock_tickets.ticket_type","stock_tickets.ticket_user_id","stock_tickets.id","stock_tickets.plus","stock_tickets.arrival_airport_id","stock_tickets.departure_airport_id","stock_tickets.flight_date","stock_tickets.departure_time","stock_tickets.arrival_time","airlines.name as airline","flight_stretches.name as stretch","stock_tickets.extra_price","stock_tickets.price")
                        ->where("stock_tickets.is_voucher",1)
                        ->where("stock_tickets.code",$code->code)
                        ->orderBy("stock_tickets.id","desc")
                        ->get();

                        foreach($tickets as $t){
                            $airport1 = DB::table("airports")->where("id",$t->departure_airport_id)->first();
                            $airport2 = DB::table("airports")->where("id",$t->arrival_airport_id)->first();
                            if($airport1){
                                $t->airport1 = $airport1->city;
                            }
                            if($airport2){
                                $t->airport2 = $airport2->city;
                            }
                            $name = "";
                            $rut ="";
                            $role = "";
                            if($t->ticket_type =="new_stock"){
                                $t->price = $t->price + $t->dearness + $t->penalty;
                            }
                            if($t->user_type=='new_stock'){
                               
                            }
                            if($t->user_type=='person'){
                                $person = DB::table("persons")->where("id",$t->ticket_user_id)->first();
                                $role = $person->role;   
                                $rut = $person->rut;
                                $name = $person->name." ".$person->last_name;
                            }
                            if($t->user_type=='worker'){                    
                                $person = DB::table("workers")->where("id",$t->ticket_user_id)->first();
                                $rut = $person->rut;
                                $name = $person->name." ".$person->last_name;
                                $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$person->id)->get();
                                $rol = "";
                                foreach($roles as $r){
                                    $role = $role." ".$r->name;
                                }
                                 
                            }
                            $t->name = $name;
                            $t->rut = $rut;
                            $t->role = $role; 
                                    
                        }
      
        return $this->sendResponse(
            $tickets,
            'pasajes encontrados'
        );
    }



    public function setTransfersCrew(Request $request)
    {
        $crew = DB::table("quotations")->where("id",$request->quotation_id)->first();
        if($crew){
            if($request->isedit==1){

                $workers = DB::table("worker_transfer")->join("transfers","transfers.id","=","worker_transfer.transfer_id")->where("transfers.id",$request->snack_id)->where("transfers.crew_id",$crew->crew_id)->get();
                DB::table("transfers")->where("id",$request->transfer_id)->update(['journey_type'=>$request->journey_type,'quotation'=>$request->no_facturar,'price'=>$request->price,'upload_date'=>$request->upload_date,'supplier_id'=>$request->supplier_id,'entry_text'=>$request->entry_text,'exit_text'=>$request->exit_text,'observation'=>$request->observation,'updated_at'=>date('Y-m-d H:i:s')]);        
                foreach($workers as $w){
                    $existe = DB::table("worker_transfer")->join("transfers","transfers.id","=","worker_transfer.transfer_id")->where("worker_transfer.worker_id",$w->worker_id)->where("transfers.crew_id",$crew->crew_id)->count();
                    if($existe>1){
                        DB::table("worker_transfer")->where("worker_id",$w->worker_id)->where("transfer_id",$request->snack_id)->delete();   
                    }
                }

            }else{
               $last =  DB::table("transfers")->insertGetId(['journey_type'=>$request->journey_type,'crew_id'=>$crew->crew_id,'quotation'=>$request->no_facturar,'price'=>$request->price,'upload_date'=>$request->upload_date,'supplier_id'=>$request->supplier_id,'entry_text'=>$request->entry_text,'exit_text'=>$request->exit_text,'observation'=>$request->observation,'created_at'=>date('Y-m-d H:i:s')]);
                DB::table("crew_transfers")->insert(['crew_id'=>$crew->crew_id,'transfer_id'=>$last]);
            }
        }

        return $this->sendResponse(
            $request,
            'traslado guardado'
        );
    }

    public function setAccommodationsCrew(Request $request)
    {
        $crew = DB::table("quotations")->where("id",$request->quotation_id)->first();

        if($crew){
            if($request->isedit==1){

                $workers = DB::table("accommodation_worker")->join("accommodations","accommodations.id","=","accommodation_worker.accommodation_id")->where("accommodations.id",$request->accommodation_id)->where("accommodations.crew_id",$crew->crew_id)->get();
                 DB::table("accommodations")->where("id",$request->accommodation_id)->update(['journey_type'=>$request->journey_type,'price'=>$request->price,'supplier_id'=>$request->supplier_id,'entry_date'=>$request->entry_date,'exit_date'=>$request->exit_date,'observation'=>$request->observation,'updated_at'=>date('Y-m-d H:i:s')]);
                foreach($workers as $w){
                    $existe = DB::table("accommodation_worker")->join("accommodations","accommodations.id","=","accommodation_worker.accommodation_id")->where("accommodation_worker.worker_id",$w->worker_id)->where("accommodations.crew_id",$crew->crew_id)->count();
                    if($existe>1){
                        DB::table("accommodation_worker")->where("worker_id",$w->worker_id)->where("accommodation_id",$request->accommodation_id)->delete();   
                    }
                }
            }else{
                $last = DB::table("accommodations")->insertGetId(['journey_type'=>$request->journey_type,'crew_id'=>$crew->crew_id,'price'=>$request->price,'supplier_id'=>$request->supplier_id,'entry_date'=>$request->entry_date,'exit_date'=>$request->exit_date,'observation'=>$request->observation,'created_at'=>date('Y-m-d H:i:s')]);
                DB::table("crew_accommodations")->insert(['crew_id'=>$crew->crew_id,'accommodation_id'=>$last]);
            }
        }

        return $this->sendResponse(
            $request,
            'traslado guardado'
        );
    }

    public function setSnackCrew(Request $request)
    {
        $crew = DB::table("quotations")->where("id",$request->quotation_id)->first();

        if($crew){
            if($request->isedit==1){
                

                /*$workers = DB::table("worker_snack")->join("snacks","snacks.id","=","worker_snack.snack_id")->where("snacks.crew",$crew->crew_id)->where("snacks.journey_type",$request->journey_type)->get();
                DB::table("snacks")->where("id",$request->snack_id)->update(['journey_type'=>$request->journey_type,'price'=>$request->price,'description'=>$request->description]);
                foreach($workers as $w){
                   DB::table("worker_snack")->where("worker_id",$w->worker_id)->where("snack_id",$request->snack_id)->delete();   
                }*/


                $workers = DB::table("worker_snack")->join("snacks","snacks.id","=","worker_snack.snack_id")->where("snacks.id",$request->snack_id)->where("snacks.crew_id",$crew->crew_id)->get();
                DB::table("snacks")->where("id",$request->snack_id)->update(['journey_type'=>$request->journey_type,'price'=>$request->price,'description'=>$request->description]);
                foreach($workers as $w){
                    $existe = DB::table("worker_snack")->join("snacks","snacks.id","=","worker_snack.snack_id")->where("worker_snack.worker_id",$w->worker_id)->where("snacks.crew_id",$crew->crew_id)->count();
                    if($existe>1){
                        DB::table("worker_snack")->where("worker_id",$w->worker_id)->where("snack_id",$request->snack_id)->delete();   
                    }
                }

                

            }else{
                $last = DB::table("snacks")->insertGetId(['journey_type'=>$request->journey_type,'crew_id'=>$crew->crew_id,'price'=>$request->price,'description'=>$request->description]);
                DB::table("crew_snacks")->insert(['crew_id'=>$crew->crew_id,'snack_id'=>$last]);
            }
        }

        return $this->sendResponse(
            $request,
            'colacion guardado'
        );
    }

    public function getAllTransfersCrew(int $id)
    {
        $crew = DB::table("quotations")->where("id",$id)->first();
        if($crew){
            $transfers = DB::table("transfers")
                        ->join("suppliers","suppliers.id","=","transfers.supplier_id")
                        ->join("crew_transfers","crew_transfers.transfer_id","=","transfers.id")
                        ->select("suppliers.id as supplier_id","transfers.id","suppliers.name as supplier","transfers.price","transfers.observation","transfers.upload_date","transfers.entry_text","transfers.exit_text","transfers.quotation","transfers.created_at")
                        ->where("crew_transfers.crew_id",$crew->crew_id)
                        ->orderBy("transfers.id","desc")
                        ->get();
      
                        foreach($transfers as $a){
                            $cantidad2 = DB::table("worker_transfer")->where("status",1)->where("transfer_id",$a->id)->count();
                            $a->quantity = $cantidad2;
                        }

        }else{
            $transfers = null;
        }
        
        return $this->sendResponse(
            $transfers,
            'traslados encontrados'
        );
    }

    public function getAllAccommodationsCrew(int $id)
    {
        
        $accomodations = DB::table("accommodations")
                        ->join("suppliers","suppliers.id","=","accommodations.supplier_id")
                        ->join("crew_accommodations","crew_accommodations.accommodation_id","=","accommodations.id")
                        ->select("suppliers.id as supplier_id","accommodations.id","suppliers.name as supplier","accommodations.price","accommodations.observation","accommodations.entry_date","accommodations.exit_date","accommodations.created_at")
                        ->where("crew_accommodations.crew_id",$id)
                        ->orderBy("accommodations.id","desc")
                        ->get();

        foreach($accomodations as $a){
            $cantidad2 = DB::table("accommodation_worker")->where("status",1)->where("accommodation_id",$a->id)->count();
            $a->quantity = $cantidad2;
        }
       
        return $this->sendResponse(
            $accomodations,
            'persona encontrada'
        );
    }

    public function deleteTransfer(int $id){
        DB::table("crew_transfers")->where("transfer_id",$id)->delete();
        DB::table("transfers")->where("id",$id)->delete();
        return $this->sendResponse(
            $id,
            'traslado eliminado'
        );
    }

    public function deleteAccommodation(int $id){
        DB::table("crew_accommodations")->where("accommodation_id",$id)->delete();
        DB::table("accommodations")->where("id",$id)->delete();
        return $this->sendResponse(
            $id,
            'aljamiento eliminado'
        );
    }

    public function deleteSnack(int $id){
        DB::table("crew_snacks")->where("snack_id",$id)->delete();
        DB::table("snacks")->where("id",$id)->delete();
        return $this->sendResponse(
            $id,
            'colacion eliminado'
        );
    }

    public function getTransfersWorkers(int $transfer_id,int $quotation_id){
        $crew = DB::table("quotations")->where("id",$quotation_id)->first();
        $workerTransfers = DB::table("crew_worker")->join("workers","workers.id","=","crew_worker.worker_id")->where("crew_worker.crew_id",$crew->crew_id)->where("crew_worker.filtered",0)->select("workers.id","workers.name","workers.last_name","workers.rut",'crew_worker.with_emergency','deleted_at')->get();
        $transfer = DB::table("transfers")->where("id",$transfer_id)->first();
        foreach($workerTransfers as $w){
            $rol = "";
           $roles =  DB::table("roles")->join("role_worker","role_worker.role_id","=","roles.id")->where("role_worker.worker_id",$w->id)->select("roles.name")->get(); 
           foreach($roles as $r){
            $rol = $rol.",".$r->name;
           }
           $w->roles = substr($rol, 1);
          //  $intransfer = DB::table("worker_transfer")->where("worker_transfer.transfer_id",$transfer_id)->where("worker_transfer.worker_id",$w->id)->where("worker_transfer.status",1)->first();
          $intransfer = DB::table("worker_transfer")
          ->join("crew_transfers","crew_transfers.transfer_id","=","worker_transfer.transfer_id")
          ->join("transfers","transfers.id","=","worker_transfer.transfer_id")
          ->where("transfers.journey_type",$transfer->journey_type)
          ->where("crew_transfers.crew_id",$crew->crew_id)
          ->where("worker_transfer.worker_id",$w->id)
          ->where("worker_transfer.status",1)
          ->first();
           
          
            if($intransfer){
                if($intransfer->transfer_id==$transfer_id){
                    $w->used = 1;
                }else{
                    $w->used = 0;
                }
                
            }else{
                $w->used = 2;
            }
            
        }
        return response()->json($workerTransfers);
      
    }

   

    public function getAccommodationWorkers(int $accommodation_id,int $quotation_id){
        $crew = DB::table("quotations")->where("id",$quotation_id)->first();
        $workerTransfers = DB::table("crew_worker")->join("workers","workers.id","=","crew_worker.worker_id")->where("crew_worker.crew_id",$crew->crew_id)->where("crew_worker.filtered",0)->select("workers.id","workers.name","workers.last_name","workers.rut",'crew_worker.with_emergency','deleted_at')->get();
        $accommodation = DB::table("accommodations")->where("id",$accommodation_id)->first();
        foreach($workerTransfers as $w){
            $rol = "";
           $roles =  DB::table("roles")->join("role_worker","role_worker.role_id","=","roles.id")->where("role_worker.worker_id",$w->id)->select("roles.name")->get(); 
           foreach($roles as $r){
            $rol = $rol.",".$r->name;
           }
           $w->roles = substr($rol, 1);
          //  $intransfer = DB::table("worker_transfer")->where("worker_transfer.transfer_id",$transfer_id)->where("worker_transfer.worker_id",$w->id)->where("worker_transfer.status",1)->first();
          $intransfer = DB::table("accommodation_worker")
          ->join("crew_accommodations","crew_accommodations.accommodation_id","=","accommodation_worker.accommodation_id")
          ->join("accommodations","accommodations.id","=","accommodation_worker.accommodation_id")
          ->where("accommodations.journey_type",$accommodation->journey_type)
          ->where("crew_accommodations.crew_id",$crew->crew_id)
          ->where("accommodation_worker.worker_id",$w->id)
          ->where("accommodation_worker.status",1)
          ->first();
           
          
            if($intransfer){
                if($intransfer->accommodation_id==$accommodation_id){
                    $w->used = 1;
                }else{
                    $w->used = 0;
                }
                
            }else{
                $w->used = 2;
            }
            
        }
        return response()->json($workerTransfers);
      
    }


    public function getSnackWorkers(int $snack_id,int $quotation_id){
        $crew = DB::table("quotations")->where("id",$quotation_id)->first();
        $workerTransfers = DB::table("crew_worker")->join("workers","workers.id","=","crew_worker.worker_id")->where("crew_worker.crew_id",$crew->crew_id)->where("crew_worker.filtered",0)->select("workers.id","workers.name","workers.last_name","workers.rut",'crew_worker.with_emergency','deleted_at')->get();
       
        $snack = DB::table("snacks")->where("id",$snack_id)->first();
        foreach($workerTransfers as $w){
            $rol = "";
            $roles =  DB::table("roles")->join("role_worker","role_worker.role_id","=","roles.id")->where("role_worker.worker_id",$w->id)->select("roles.name")->get(); 
            foreach($roles as $r){
             $rol = $rol.",".$r->name;
            }
            $w->roles = substr($rol, 1);
          //  $intransfer = DB::table("worker_transfer")->where("worker_transfer.transfer_id",$transfer_id)->where("worker_transfer.worker_id",$w->id)->where("worker_transfer.status",1)->first();
          $intransfer = DB::table("worker_snack")
          ->join("crew_snacks","crew_snacks.snack_id","=","worker_snack.snack_id")
          ->join("snacks","snacks.id","=","worker_snack.snack_id")
          ->where("snacks.journey_type",$snack->journey_type)
          ->where("crew_snacks.crew_id",$crew->crew_id)
          ->where("worker_snack.worker_id",$w->id)
          ->where("worker_snack.status",1)
          ->first();
           
          
            if($intransfer){
                if($intransfer->snack_id==$snack_id){
                    $w->used = 1;
                }else{
                    $w->used = 0;
                }
                
            }else{
                $w->used = 2;
            }
            
        }
        return response()->json($workerTransfers);
      
    }


    public function setTransferCrewWorker(Request $request)
    {  
        foreach($request->workers as $d){
            if($d['selected']==true){
                if($d['enabled']==true){
                    $existe = DB::table("worker_transfer")->where('worker_id',$d['worker_id'])->where('transfer_id',$request->transfer_id)->first();
                    if($existe){

                    }else{
                        DB::table("worker_transfer")->insert(['worker_id'=>$d['worker_id'],'transfer_id'=>$request->transfer_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
                    }
                 }
            }else{
                DB::table("worker_transfer")->where('worker_id',$d['worker_id'])->where('transfer_id',$request->transfer_id)->delete();
            }               
        }
        return $this->sendResponse(
            $request,
            'traslado guardado'
        );
    }

    public function setAccommodationCrewWorker(Request $request)
    {  
        foreach($request->workers as $d){
            if($d['selected']==true){
                if($d['enabled']==true){
                    $existe = DB::table("accommodation_worker")->where('worker_id',$d['worker_id'])->where('accommodation_id',$request->accommodation_id)->first();
                    if($existe){

                    }else{
                        DB::table("accommodation_worker")->insert(['worker_id'=>$d['worker_id'],'accommodation_id'=>$request->accommodation_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
                    }
                 }
            }else{
                DB::table("accommodation_worker")->where('worker_id',$d['worker_id'])->where('accommodation_id',$request->accommodation_id)->delete();
            }               
        }
        return $this->sendResponse(
            $request,
            'alojamientos guardado'
        );
    }

    public function setSnackCrewWorker(Request $request)
    {  
        foreach($request->workers as $d){
            if($d['selected']==true){
                if($d['enabled']==true){
                    $existe = DB::table("worker_snack")->where('worker_id',$d['worker_id'])->where('snack_id',$request->snack_id)->first();
                    if($existe){

                    }else{
                        DB::table("worker_snack")->insert(['worker_id'=>$d['worker_id'],'snack_id'=>$request->snack_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
                    }
                 }
            }else{
                DB::table("worker_snack")->where('worker_id',$d['worker_id'])->where('snack_id',$request->snack_id)->delete();
            }               
        }
        return $this->sendResponse(
            $request,
            'colaciones guardado'
        );
    }



    public function setTicketsCrew(Request $r)
    {
        $nostockcount = 0;
        $manycount = 0;
        $isstock = 0;
        $havestock = 0;
        if($r->isedit==1){

            $ticket = DB::table("ticket_groups")->where("id",$r->ticket_id)->first();
            $actual = DB::table("ticket_groups")->where("id",$r->ticket_id)->first();

            DB::table("ticket_groups")->where("id",$r->ticket_id)->update(['voucher_id'=>null,'stock_ticket_id'=>null,'payment_type'=>null,'payment_use_price'=>0]);

            if($actual->payment_type==1){
                DB::table("stock_tickets")->where("id",$actual->stock_ticket_id)->update(['used'=>0]);
                DB::table("ticket_groups")->where("id",$r->ticket_id)->update(['stock_ticket_id'=>null]);
            }

            if($r->stock_ticket_id==1){
                if($r->stock_id<>$actual->stock_ticket_id){
                  DB::table("stock_tickets")->where("id",$r->stock_id)->update(['used'=>1,'stock_used_date'=>date('Y-m-d H:i:s')]);
                }
            }
 
            if($actual->payment_type==2 && $r->stock_ticket_id<>2){

                $devolucion = $actual->total_price;
                if($actual->total_price > $actual->payment_use_price){
                    $devolucion = $actual->payment_use_price;
                }

                $last_voucher = DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->first();
                $airlinevoucher = DB::table("airline_vouchers")->where('id',$ticket->voucher_id)->first();
                DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->update(['required_price'=>($last_voucher->required_price - $devolucion),'balance'=>($last_voucher->balance + $devolucion)]);
                DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>0,'price'=>($airlinevoucher->price + $devolucion),'price_used'=>($airlinevoucher->price_used - $devolucion)]);
                $estado =  DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->first();
                if($estado){
                    if($estado->price>0){
                        DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>1]);
                    }
                }
            }

            if($actual->payment_type<>2 && $r->stock_ticket_id==2){
                $devolucion = $r->total_price;
                if($r->total_price > $r->payment_price){
                    $devolucion = $r->payment_price;
                }

                $existe = DB::table("log_vouchers")->where('airline_voucher_id',$r->voucher_id)->where('ticket_group_id',$ticket->id)->first();
                if($existe){

                }else{
                    $voucher = DB::table("airline_vouchers")->where("id",$r->voucher_id)->first();
                    DB::table("log_vouchers")->insert(['date'=>date('Y-m-d H:i:s'),'required_price'=>0,'voucher_price'=>$voucher->price,'balance'=>$voucher->price,'ticket_group_id'=>$ticket->id,'airline_voucher_id'=>$r->voucher_id]);
                }

                 

                $last_voucher = DB::table("log_vouchers")->where('airline_voucher_id',$r->voucher_id)->where('ticket_group_id',$ticket->id)->first();
                $airlinevoucher = DB::table("airline_vouchers")->where('id',$r->voucher_id)->first();
                DB::table("log_vouchers")->where('airline_voucher_id',$r->voucher_id)->where('ticket_group_id',$ticket->id)->update(['required_price'=>($last_voucher->required_price + $devolucion),'balance'=>($last_voucher->balance - $devolucion)]);
                DB::table("airline_vouchers")->where("id",$r->voucher_id)->update(['status'=>0,'price'=>($airlinevoucher->price - $devolucion),'price_used'=>($airlinevoucher->price_used + $devolucion)]);
                $estado =  DB::table("airline_vouchers")->where("id",$r->voucher_id)->first();
                if($estado){
                    if($estado->price>0){
                        DB::table("airline_vouchers")->where("id",$r->voucher_id)->update(['status'=>1]);
                    }
                }
            }
         
            DB::table("ticket_groups")->where("id",$r->ticket_id)->update(['oc'=>$r->purchase_order,'oc_date'=> $r->purchase_order_date,'payment_use_price'=>$r->payment_price,'passengers'=>$r->passengers,'total_price'=>$r->total_price,'unitary_extra_price'=>$r->unitary_extra_price,'stock_ticket_id'=>$r->stock_id,'voucher_id'=>$r->voucher_id,'expiration_date'=>$r->expiration_date,'payment_type'=>$r->stock_ticket_id,'code'=>$r->code,'penalty'=>$r->penalty,'dearness'=>$r->dearness,'flight_date'=>$r->flight_date,'departure_time'=>$r->departure_time,'arrival_time'=>$r->arrival_time,'price'=>$r->price,'airline_id'=>$r->airline_id,'flight_stretch_id'=>$r->flight_stretch_id,'departure_airport_id'=>$r->departure_airport_id,'arrival_airport_id'=>$r->arrival_airport_id,'updated_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id(),'extra_price'=>$r->extra_price,'plus'=>$r->tarifa_plus]);
            
            if($r->stock_ticket_id==1){                
                $cantidad = DB::table("worker_ticket")->where("ticket_group_id",$r->ticket_id)->get();
                if(sizeof($cantidad)>1){
                    DB::table("worker_ticket")->where("ticket_group_id",$r->ticket_id)->delete();
                }     
                DB::table("ticket_groups")->where("id",$r->ticket_id)->update(['passengers'=>sizeof($cantidad)]);   
            }


            /*$workers = DB::table("worker_ticket")->join("ticket_groups","ticket_groups.id","=","worker_ticket.ticket_group_id")->where("ticket_groups.id",$r->ticket_id)->get(); 
            foreach($workers as $w){
                $existe = DB::table("worker_ticket")->join("ticket_groups","ticket_groups.id","=","worker_ticket.ticket_group_id")->where("worker_ticket.worker_id",$w->worker_id)->where("ticket_groups.quotation_id",$r->quotation_id)->count();
                if($existe>1){
                  
                }
            }*/


            return $this->sendResponse(
                $r,
                'pasaje guardado'
            );
        }else{
            $cotizacion = DB::table("quotations")->where("id",$r->quotation_id)->first();
            $last = DB::table("ticket_groups")->insertGetId(['oc'=>$r->purchase_order,'oc_date'=> $r->purchase_order_date,'crew_id'=>$cotizacion->crew_id,'quotation_id'=>$r->quotation_id,'payment_use_price'=>$r->payment_price,'payment_type'=>$r->stock_ticket_id,'passengers'=>$r->passengers,'total_price'=>$r->total_price,'unitary_extra_price'=>$r->unitary_extra_price,'stock_ticket_id'=>$r->stock_id,'voucher_id'=>$r->voucher_id,'expiration_date'=>$r->expiration_date,'ticket_type'=>'crew','code'=>$r->code,'penalty'=>$r->penalty,'dearness'=>$r->dearness,'flight_date'=>$r->flight_date,'departure_time'=>$r->departure_time,'arrival_time'=>$r->arrival_time,'price'=>$r->price,'airline_id'=>$r->airline_id,'flight_stretch_id'=>$r->flight_stretch_id,'departure_airport_id'=>$r->departure_airport_id,'arrival_airport_id'=>$r->arrival_airport_id,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id(),'extra_price'=>$r->extra_price,'plus'=>$r->tarifa_plus]);  
            DB::table("crew_tickets")->insertGetId(['crew_id'=>$cotizacion->crew_id,'ticket_group_id'=>$last,'status'=>1]);  
           
            if($r->stock_ticket_id==1){
                DB::table("stock_tickets")->where("id",$r->stock_id)->update(['used'=>1,'stock_used_date'=>date('Y-m-d H:i:s')]);
                $stock = DB::table("stock_tickets")->where("id",$r->stock_id)->first();
                DB::table("ticket_groups")->where("id",$last)->update(['payment_use_price'=>$stock->price]);
            }
            if($r->stock_ticket_id==2){
                $voucher = DB::table("airline_vouchers")->where("id",$r->voucher_id)->first();
                if($voucher){
                    $value = $r->extra_price;
                    if($r->extra_price>$voucher->price){
                        $value = $voucher->price;
                    }
                    $diff = $voucher->price - $value;
                    $disabled = 1;
                    if($diff == 0){
                        $disabled = 0;
                    }
                    DB::table("log_vouchers")->insert(['date'=>date('Y-m-d H:i:s'),'required_price'=>$value,'voucher_price'=>$voucher->price,'balance'=>$diff,'ticket_group_id'=>$last,'airline_voucher_id'=>$r->voucher_id]);
                    DB::table("airline_vouchers")->where("id",$r->voucher_id)->update(['status'=>$disabled,'price_used'=>($voucher->price_used + $value),'price'=>$diff]);
                }
            }
            return $this->sendResponse(
                ['is_stock'=>$isstock,'have_stock'=>$havestock,'nostock'=>$nostockcount,'many'=>$manycount],
                'pasaje guardado'
            );
        }
        
       
    }

  



    public function getTicketWorkers(int $ticket_id,int $quotation_id){
        //return response()->json($ticket_id);
        $crew = DB::table("quotations")->where("id",$quotation_id)->first();
        $workerTransfers = DB::table("crew_worker")->join("workers","workers.id","=","crew_worker.worker_id")->where("crew_worker.crew_id",$crew->crew_id)->where("crew_worker.filtered",0)->select("workers.id","workers.name","workers.last_name","workers.rut",'crew_worker.with_emergency','deleted_at')->get();
        
        $ticket = DB::table("ticket_groups")->where("id",$ticket_id)->first();
        foreach($workerTransfers as $w){

           $rol = "";
           $roles =  DB::table("roles")->join("role_worker","role_worker.role_id","=","roles.id")->where("role_worker.worker_id",$w->id)->select("roles.name")->get(); 
           foreach($roles as $r){
            $rol = $rol.",".$r->name;
           }
           $w->roles = substr($rol, 1);
          //  $intransfer = DB::table("worker_transfer")->where("worker_transfer.transfer_id",$transfer_id)->where("worker_transfer.worker_id",$w->id)->where("worker_transfer.status",1)->first();
          $intransfer = DB::table("worker_ticket")
                    ->join("crew_tickets","crew_tickets.ticket_group_id","=","worker_ticket.ticket_group_id")
                    ->join("ticket_groups","ticket_groups.id","=","crew_tickets.ticket_group_id")
                    ->select("ticket_groups.id as ticket_group_id")
                    ->where("ticket_groups.flight_stretch_id",$ticket->flight_stretch_id)
                    ->where("crew_tickets.crew_id",$crew->crew_id)
                    ->where("worker_ticket.worker_id",$w->id)
                    ->where("worker_ticket.status",1)
                    ->first();
          
          $w->intransfer = $intransfer;
            if($intransfer){
                if($intransfer->ticket_group_id==$ticket_id){
                    $w->used = 1;
                }else{
                    $w->used = 0;
                }
                
            }else{
                $w->used = 2;
            }
            
        }
        return response()->json($workerTransfers);
      
    }
    
    public function setTicketWorkersCrew(Request $request){

     

         foreach($request->tickets as $c){
            
                    if($c['edited']==1){
                        //if($c['payment_type']==2){    
                            foreach($request->workers as $d){
                                if($c['id']==$request->ticket_id){
                                    if($d['selected']==true){
                                        if($d['enabled']==true){           
                                            $existe = DB::table("worker_ticket")->where('worker_id',$d['worker_id'])->where('ticket_group_id',$request->ticket_id)->first();
                                            if($existe){
                                                $ticket = DB::table("ticket_groups")->where("id",$request->ticket_id)->first();
                                                
                                                 if($ticket->payment_type==2){                               
                                                    $original = DB::table("worker_ticket")->where("ticket_group_id",$ticket->id)->where("worker_id",$d['worker_id'])->first();
                                                    $resta = $original->ticket_price + $original->penalty;
                                                    $last_voucher = DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->first();
                                                    $airlinevoucher = DB::table("airline_vouchers")->where('id',$ticket->voucher_id)->first();
                                                    DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->update(['required_price'=>($last_voucher->required_price - $resta),'balance'=>($last_voucher->balance + $resta)]);
                                                    DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>0,'price'=>($airlinevoucher->price + $resta),'price_used'=>($airlinevoucher->price_used - $resta)]);
                                                    echo "0.- ".$last_voucher->required_price." - ".$resta."//";
                                                    echo "0.- ".$last_voucher->balance ." + ". $resta."//"; 
                                                    echo "0.- ".$airlinevoucher->price." + ". $resta."//";
                                                    echo "0.- ".$airlinevoucher->price_used." - ". $resta."//";
                                                    $estado =  DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->first();
                                                    if($estado){
                                                        if($estado->price>0){
                                                            DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>1]);
                                                        }
                                                    }

                                                    $last_voucher = DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->first();
                                                    $airlinevoucher = DB::table("airline_vouchers")->where('id',$ticket->voucher_id)->first();
                                                    $resta = $c['price'] + $original->penalty;
                                                    DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->update(['required_price'=>($last_voucher->required_price + $resta),'balance'=>($last_voucher->balance - $resta)]);
                                                    DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>0,'price'=>($airlinevoucher->price - $resta),'price_used'=>($airlinevoucher->price_used + $resta)]);
                                                    echo "1.- ".$last_voucher->required_price." + ".$c['price']."//";
                                                    echo "1.- ".$last_voucher->balance ." - ". $c['price']."//"; 
                                                    echo "1.- ".$airlinevoucher->price." - ". $c['price']."//";
                                                    echo "1.- ".$airlinevoucher->price_used." + ". $c['price']."//";
                                                    $estado =  DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->first();
                                                    if($estado){
                                                        if($estado->price>0){
                                                            DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>1]);
                                                        }
                                                    }
                                                }
                                            }else{

                                                DB::table("worker_ticket")->insert(['ticket_price'=>$c['price'],'worker_id'=>$d['worker_id'],'ticket_group_id'=>$request->ticket_id,'status'=>1,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
                                                self::cuenta($request->ticket_id);

                                                $ticket = DB::table("ticket_groups")->where("id",$request->ticket_id)->first();
                                                if($ticket->payment_type==2){                               

                                                    $last_voucher = DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->first();
                                                    $airlinevoucher = DB::table("airline_vouchers")->where('id',$ticket->voucher_id)->first();
                                                    DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->update(['required_price'=>($last_voucher->required_price + $c['price']),'balance'=>($last_voucher->balance - $c['price'])]);
                                                    DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>0,'price'=>($airlinevoucher->price - $c['price']),'price_used'=>($airlinevoucher->price_used + $c['price'])]);
                                                    echo "2.- ".$last_voucher->required_price." -+ ".$c['price']."//";
                                                    echo "2.- ".$last_voucher->balance ." - ". $c['price']."//"; 
                                                    echo "2.- ".$airlinevoucher->price." - ". $c['price']."//";
                                                    echo "2.- ".$airlinevoucher->price_used." + ". $c['price']."//";
                                                    $estado =  DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->first();
                                                    if($estado){
                                                        if($estado->price>0){
                                                            DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>1]);
                                                        }
                                                    }
                                                }
                        
                                            }
                                         }else{
                                            
                                         }
                                    }else{     
                                        $existe = DB::table("worker_ticket")->where('worker_id',$d['worker_id'])->where('ticket_group_id',$request->ticket_id)->get();
                                        
                                        if(sizeof($existe)>0){

                                            echo "adasd//";

                                            $ticket = DB::table("ticket_groups")->where("id",$request->ticket_id)->first();
                                            if($ticket->payment_type==2){                               
                                                $original = DB::table("worker_ticket")->where("ticket_group_id",$ticket->id)->where("worker_id",$d['worker_id'])->first();
                                                $resta = $original->ticket_price + $original->penalty;
                                                $last_voucher = DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->first();
                                                $airlinevoucher = DB::table("airline_vouchers")->where('id',$ticket->voucher_id)->first();
                                                DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->update(['required_price'=>($last_voucher->required_price - $resta),'balance'=>($last_voucher->balance + $resta)]);
                                                DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>0,'price'=>($airlinevoucher->price + $resta),'price_used'=>($airlinevoucher->price_used - $resta)]);
                                                echo "3.- ".$last_voucher->required_price." - ".$resta."//";
                                                echo "3.- ".$last_voucher->balance ." + ". $resta."//"; 
                                                echo "3.- ".$airlinevoucher->price." + ". $resta."//";
                                                echo "3.- ".$airlinevoucher->price_used." - ". $resta."//";
                                                $estado =  DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->first();
                                                if($estado){
                                                    if($estado->price>0){
                                                        DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>1]);
                                                    }
                                                }
                                               // return response()->json($existe);
                                            }
                                            //
                                            DB::table("worker_ticket")->where("worker_id",$d['worker_id'])->where("ticket_group_id",$request->ticket_id)->delete();
                        
                                            self::cuenta($request->ticket_id);
                        
                                        }
                                    }   
                                }
                           // }
                           
                        }

                       DB::table("ticket_groups")->where("id",$request->ticket_id)->update(['extra_price'=>$c['extra_price'],'total_price'=>$c['total_price'],'unitary_extra_price'=>$c['unitary_extra_price']]);
                     }
        }
   
    }

    public function setTicketWorkerCrew(Request $request){
        $ticket = DB::table("ticket_groups")->where("id",$request->ticket_id)->first();

       $existe =  DB::table("worker_ticket")->where("ticket_group_id",$request->ticket_id)->first();
        if($existe){
            if($existe->worker_id==$request->worker_id){

            }else{
                DB::table("worker_ticket")->where("ticket_group_id",$request->ticket_id)->delete();
                DB::table("worker_ticket")->insert(['ticket_price'=>$ticket->price,'worker_id'=>$request->worker_id,'ticket_group_id'=>$request->ticket_id,'status'=>1,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
            }
           
        }else{
            DB::table("worker_ticket")->insert(['ticket_price'=>$ticket->price,'worker_id'=>$request->worker_id,'ticket_group_id'=>$request->ticket_id,'status'=>1,'created_at'=>date('Y-m-d H:i:s'),'uploader_id'=>Auth::id()]);
        }

        DB::table("ticket_groups")->where("id",$request->ticket_id)->update(['extra_price'=>$request->extra_price,'unitary_extra_price'=>$request->unitary_extra_price,'total_price'=>$request->total_price]);

        self::cuenta($request->ticket_id);
    
        return $this->sendResponse(
            $request,
            'pasajes asociados'
        );
    }

    public function cuenta(int $ticket){
        $the_ticket = DB::table("worker_ticket")->where("ticket_group_id",$ticket)->where("status",1)->count();
        DB::table("ticket_groups")->where("id",$ticket)->update(['passengers'=> $the_ticket]);
    }

    public function deleteWorkerTicket(Request $r){

      
        $original = DB::table("worker_ticket")->where("worker_id",$r->worker_id)->where("ticket_group_id",$r->ticket_id)->first();
        DB::table("worker_ticket")->where("worker_id",$r->worker_id)->where("ticket_group_id",$r->ticket_id)->delete();
        self::cuenta($r->ticket_id);
        $ticket = DB::table("ticket_groups")->where("id",$r->ticket_id)->first();


        foreach($r->tickets as $d){
            if($d['id']==$r->ticket_id){
                $resta = $original->ticket_price + $original->penalty;
               
                DB::table("ticket_groups")->where("id",$d["id"])->update(['extra_price'=>$d['extra_price'],'unitary_extra_price'=>$d['unitary_extra_price'],'total_price'=>$d['total_price']]);    
                if($d['payment_type']==2){
                    $last_voucher = DB::table("log_vouchers")->where('airline_voucher_id',$d['voucher_id'])->where('ticket_group_id',$r->ticket_id)->first();
                    $airlinevoucher = DB::table("airline_vouchers")->where('id',$d['voucher_id'])->first();
                    DB::table("log_vouchers")->where('airline_voucher_id',$d['voucher_id'])->where('ticket_group_id',$r->ticket_id)->update(['required_price'=>($last_voucher->required_price - $resta),'balance'=>($last_voucher->balance + $resta)]);
                    DB::table("airline_vouchers")->where("id",$d['voucher_id'])->update(['status'=>0,'price'=>($airlinevoucher->price + $resta),'price_used'=>($airlinevoucher->price_used - $resta)]);
                    $estado =  DB::table("airline_vouchers")->where("id",$d['voucher_id'])->first();
                    if($estado){
                        if($estado->price>0){
                            DB::table("airline_vouchers")->where("id",$d['voucher_id'])->update(['status'=>1]);
                        }
                    }
                } 
            }               
        }
       

       
        return $this->sendResponse(
            $r->worker_id,
            'colacion eliminado'
        );
    }


    public function deleteWorkerTransfer(Request $r){
        DB::table("worker_transfer")->where("worker_id",$r->worker_id)->where("transfer_id",$r->transfer_id)->delete();
        return $this->sendResponse(
            $r->worker_id,
            'traslado eliminado'
        );
    }
    public function deleteWorkerSnack(Request $r){
        DB::table("worker_snack")->where("worker_id",$r->worker_id)->where("snack_id",$r->snack_id)->delete();
        return $this->sendResponse(
            $r->worker_id,
            'colacion eliminado'
        );
    }
    public function deleteWorkerAccommodation(Request $r){
        DB::table("accommodation_worker")->where("worker_id",$r->worker_id)->where("accommodation_id",$r->accommodation_id)->delete();
        return $this->sendResponse(
            $r->worker_id,
            'alojamiento eliminado'
        );
    }

    public function calculatePrices(int $ticket){
        $the_ticket = DB::table("ticket_groups")->where("id",$ticket)->first();        
        $workers = DB::table("worker_ticket")->where("ticket_group_id",$ticket)->get();
        $multa = 0;
        foreach($workers as $w){
            $multa = $multa + intval($w->penalty);
        }
        $extra = $multa + intval($the_ticket->penalty) + intval($the_ticket->dearness);
        $extra_price = $extra;
        $total_price = (intval($the_ticket->price) * intval($the_ticket->passengers)) +intval($extra_price);
        $unitary_extra_price = 0;
        if($the_ticket->passengers>0){
            $unitary_extra_price = round((intval($extra_price) / intval($the_ticket->passengers)),0);
        }
        
        DB::table("ticket_groups")->where("id",$ticket)->update(['extra_price'=> $extra_price,'total_price'=>$total_price,'unitary_extra_price'=>$unitary_extra_price]);

    }


    public function ticketcalculate(int $ticket_id){
        $actual = DB::table("ticket_groups")->where("id",$r->ticket_id)->first();

        DB::table("ticket_groups")->where("id",$r->ticket_id)->update(['voucher_id'=>null,'stock_ticket_id'=>null,'payment_type'=>null,'payment_use_price'=>0]);

        $cambiopago = $actual->payment_type;
        $cambiostockid =$actual->stock_ticket_id;
        $cambiovoucher = $actual->voucher_id;

        if($r->stock_ticket_id<>$actual->payment_type){
            $cambiopago = $r->stock_ticket_id;
            if($actual->payment_type==1){
                DB::table("stock_tickets")->where("id",$actual->stock_ticket_id)->update(['used'=>0]);
                DB::table("ticket_groups")->where("id",$r->ticket_id)->update(['stock_ticket_id'=>null]);
            }
            if($actual->payment_type==2){
                $last_voucher = DB::table("log_vouchers")->where('airline_voucher_id',$actual->voucher_id)->where('ticket_group_id',$r->ticket_id)->first();
                $airlinevoucher = DB::table("airline_vouchers")->where('id',$actual->voucher_id)->first();
                DB::table("log_vouchers")->where('airline_voucher_id',$actual->voucher_id)->where('ticket_group_id',$r->ticket_id)->update(['status' => 0]);
                DB::table("airline_vouchers")->where("id",$actual->voucher_id)->update(['status'=>0,'price'=>($airlinevoucher->price + $last_voucher->required_price),'price_used'=>($airlinevoucher->price_used - $last_voucher->required_price)]);
                DB::table("ticket_groups")->where("id",$r->ticket_id)->update(['voucher_id'=>null]);
                $estado =  DB::table("airline_vouchers")->where("id",$actual->voucher_id)->first();
                if($estado){
                    if($estado->price>0){
                        DB::table("airline_vouchers")->where("id",$actual->voucher_id)->update(['status'=>1]);
                    }
                }
            }
            
        }


        if($r->stock_ticket_id==1){
            if($r->stock_id<>$actual->stock_ticket_id){
              DB::table("stock_tickets")->where("id",$r->stock_id)->update(['used'=>1,'stock_used_date'=>date('Y-m-d H:i:s')]);
            }
        }
        if($r->stock_ticket_id==2){
            if($r->voucher_id<>$actual->voucher_id){                
                $voucher = DB::table("airline_vouchers")->where("id",$r->voucher_id)->first();
                if($voucher){
                    $value = $r->total_price;
                    if($r->total_price>$voucher->price){
                        $value = $voucher->price;
                    }
                    $diff = $voucher->price - $value;
                    $disabled = 1;
                    if($diff == 0){
                        $disabled = 0;
                    }           
                    DB::table("log_vouchers")->insert(['date'=>date('Y-m-d H:i:s'),'required_price'=>$value,'voucher_price'=>$voucher->price,'balance'=>$diff,'ticket_group_id'=>$r->ticket_id,'airline_voucher_id'=>$r->voucher_id,'status'=>1]);
                    DB::table("airline_vouchers")->where("id",$r->voucher_id)->update(['status'=>$disabled,'price_used'=>($voucher->price_used + $value),'price'=>$diff]);
                }
         }
        }
    }


    public function setWorkersTicketData(Request $r){
        foreach($r->workers as $d){
            if($d['ticketida']==null){

            }else{

                $ticket = DB::table("ticket_groups")->where("id",$d['ticketida']['ticket_group_id'])->first();
                if($ticket->payment_type==2){
                    $original = DB::table("worker_ticket")->where("ticket_group_id",$ticket->id)->where("worker_id",$d['ticketida']['worker_id'])->first();

                    $last_voucher = DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->first();
                    $airlinevoucher = DB::table("airline_vouchers")->where('id',$ticket->voucher_id)->first();
                    DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->update(['required_price'=>($last_voucher->required_price - $original->penalty),'balance'=>($last_voucher->balance + $original->penalty)]);
                    DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>0,'price'=>($airlinevoucher->price + $original->penalty),'price_used'=>($airlinevoucher->price_used - $original->penalty)]);
                    $estado =  DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->first();
                    if($estado){
                        if($estado->price>0){
                            DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>1]);
                        }
                    }

                    $last_voucher = DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->first();
                    $airlinevoucher = DB::table("airline_vouchers")->where('id',$ticket->voucher_id)->first();
                    DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->update(['required_price'=>($last_voucher->required_price + $d['ticketida']['penalty']),'balance'=>($last_voucher->balance - $d['ticketida']['penalty'])]);
                    DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>0,'price'=>($airlinevoucher->price - $d['ticketida']['penalty']),'price_used'=>($airlinevoucher->price_used + $d['ticketida']['penalty'])]);
                    $estado =  DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->first();
                    if($estado){
                        if($estado->price>0){
                            DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>1]);
                        }
                    }
                }

               // DB::table("ticket_groups")->where("id",$d['ticketida']['ticket_group_id'])->update(['extra_price'=>$d['ticketida']['extra_price'],'unitary_extra_price'=>$d['ticketida']['unitary_extra_price'],'total_price'=>$d['ticketida']['total_price']]);
              
               DB::table("worker_ticket")->where("ticket_group_id",$d['ticketida']['ticket_group_id'])->where("worker_id",$d['ticketida']['worker_id'])->update(['carry_price'=>$d['ticketida']['carry_price'],'extra_price'=>$d['ticketida']['extra_price'],'observation'=>$d['ticketida']['observation'],'quotation'=>$d['ticketida']['quotation'],'penalty'=>$d['ticketida']['penalty'],'penalty_quotation'=>$d['ticketida']['penalty_quotation']]);
               //self::calculatePrices($d['ticketida']['ticket_group_id']);
            } 
            
            if($d['ticketvuelta']==null){

            }else{
                
                $ticket = DB::table("ticket_groups")->where("id",$d['ticketvuelta']['ticket_group_id'])->first();
                if($ticket->payment_type==2){
                    $original = DB::table("worker_ticket")->where("ticket_group_id",$ticket->id)->where("worker_id",$d['ticketvuelta']['worker_id'])->first();

                    $last_voucher = DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->first();
                    $airlinevoucher = DB::table("airline_vouchers")->where('id',$ticket->voucher_id)->first();
                    DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->update(['required_price'=>($last_voucher->required_price - $original->penalty),'balance'=>($last_voucher->balance + $original->penalty)]);
                    DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>0,'price'=>($airlinevoucher->price + $original->penalty),'price_used'=>($airlinevoucher->price_used - $original->penalty)]);
                    $estado =  DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->first();
                    if($estado){
                        if($estado->price>0){
                            DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>1]);
                        }
                    }

                    

                    $last_voucher = DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->first();
                    $airlinevoucher = DB::table("airline_vouchers")->where('id',$ticket->voucher_id)->first();
                    DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticket->id)->update(['required_price'=>($last_voucher->required_price + $d['ticketvuelta']['penalty']),'balance'=>($last_voucher->balance - $d['ticketvuelta']['penalty'])]);
                    DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>0,'price'=>($airlinevoucher->price - $d['ticketvuelta']['penalty']),'price_used'=>($airlinevoucher->price_used + $d['ticketvuelta']['penalty'])]);
                    $estado =  DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->first();
                    if($estado){
                        if($estado->price>0){
                            DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>1]);
                        }
                    }

                   // return $d['ticketvuelta'];

                    
                }

                
             //  DB::table("ticket_groups")->where("id",$d['ticketvuelta']['ticket_group_id'])->update(['extra_price'=>$d['ticketvuelta']['extra_price'],'unitary_extra_price'=>$d['ticketvuelta']['unitary_extra_price'],'total_price'=>$d['ticketvuelta']['total_price']]);
               DB::table("worker_ticket")->where("ticket_group_id",$d['ticketvuelta']['ticket_group_id'])->where("worker_id",$d['ticketvuelta']['worker_id'])->update(['carry_price'=>$d['ticketvuelta']['carry_price'],'extra_price'=>$d['ticketvuelta']['extra_price'],'observation'=>$d['ticketvuelta']['observation'],'quotation'=>$d['ticketvuelta']['quotation'],'penalty'=>$d['ticketvuelta']['penalty'],'penalty_quotation'=>$d['ticketvuelta']['penalty_quotation']]);
               //self::calculatePrices($d['ticketvuelta']['ticket_group_id']);
               
               
            }
        }

       foreach($r->tickets as $d){
           if($d['flight_stretch_id']==2){
            DB::table("ticket_groups")->where("id",$d['id'])->update(['extra_price'=>$d['extra_price'],'unitary_extra_price'=>$d['unitary_extra_price'],'total_price'=>$d['total_price']]);
          
           }
           if($d['flight_stretch_id']==1){
            DB::table("ticket_groups")->where("id",$d['id'])->update(['extra_price'=>$d['extra_price'],'unitary_extra_price'=>$d['unitary_extra_price'],'total_price'=>$d['total_price']]);
          
             }
              
       }


        
       
    }

    public function setWorkerTicketData(Request $r){     
               DB::table("worker_ticket")->where("ticket_group_id",$d['ticketida']['ticket_group_id'])->where("worker_id",$d['ticketida']['worker_id'])->update(['carry_price'=>$d['ticketida']['carry_price'],'extra_price'=>$d['ticketida']['extra_price'],'observation'=>$d['ticketida']['observation'],'quotation'=>$d['ticketida']['quotation'],'penalty'=>$d['ticketida']['penalty'],'penalty_quotation'=>$d['ticketida']['penalty_quotation']]);
    }


    public function getSuppliers(int $id){
        //if($id==5){
        //    $suppliers = DB::table("airlines")->select("id","name")->orderBy("name","asc")->get();
        //}else{
            $suppliers = DB::table("suppliers")->select("id","name")->orderBy("name","asc")->where("supplier_type_id",$id)->get();
        //}
        return $this->sendResponse(
            $suppliers,
            'colaciones guardado'
        );
    }

    public function getBankAccounts(){
        
            $accounts = DB::table("bank_accounts")->select("account_number","account_name",'id')->where("deleted_at",null)->orderBy("account_name","asc")->get();
        
        return $this->sendResponse(
            $accounts,
            'colaciones guardado'
        );
    }

    public function setInvoice(Request $r){
        $crew = DB::table("quotations")->where("id",$r->quotation_id)->first();  
        if($r->isedit==0){
            $existe = DB::table("invoices")->where("supplier_id",$r->supplier_id)->where("invoice_number",$r->invoice_number)->first();
            if($existe){
                DB::table("invoice_crew")->insert(['crew_id'=>$crew->crew_id,'invoice_id'=>$existe->id]);
            }else{
                $last = DB::table("invoices")->insertGetId(['airline_id'=>null,'supplier_id'=>$r->supplier_id,'account_id'=>$r->account_id,'purchase_order_date'=>$r->purchase_order_date,'purchase_order'=>$r->purchase_order,'invoice_number'=>$r->invoice_number,'uploader_id'=>Auth::id()]);
                DB::table("invoice_crew")->insert(['crew_id'=>$crew->crew_id,'invoice_id'=>$last ]);
            }
        }else{
              DB::table("invoices")->where("id",$r->invoice_id)->update(['airline_id'=>null,'supplier_id'=>$r->supplier_id,'account_id'=>$r->account_id,'purchase_order_date'=>$r->purchase_order_date,'purchase_order'=>$r->purchase_order,'invoice_number'=>$r->invoice_number,'uploader_id'=>Auth::id()]);     
        }
        return $this->sendResponse(
            $r->all(),
            'invice guardado'
        ); 
    }

    public function setInvoiceExternal(Request $r){
   
        if($r->isedit==0){
            $existe = DB::table("invoices")->where("supplier_id",$r->supplier_id)->where("invoice_number",$r->invoice_number)->first();
            if($existe){
                DB::table("invoice_external_ticket")->insert(['ticket_group_id'=>$r->ticket_id,'invoice_id'=>$existe->id]);
            }else{
                $last = DB::table("invoices")->insertGetId(['airline_id'=>null,'supplier_id'=>$r->supplier_id,'account_id'=>$r->account_id,'invoice_number'=>$r->invoice_number,'uploader_id'=>Auth::id()]);
                DB::table("invoice_external_ticket")->insert(['ticket_group_id'=>$r->ticket_id,'invoice_id'=>$last ]);
            }
        }else{
              DB::table("invoices")->where("id",$r->invoice_id)->update(['account_id'=>$r->account_id,'invoice_number'=>$r->invoice_number,'uploader_id'=>Auth::id()]);     
        }
        return $this->sendResponse(
            $r->all(),
            'invice guardado'
        ); 
    }


    public function getInvoiceExternal($id){

        $facturas = DB::table("invoices")->join("invoice_external_ticket","invoice_external_ticket.invoice_id","=","invoices.id")
                                        ->join("bank_accounts","bank_accounts.id","invoices.account_id")
                                        ->join("suppliers","suppliers.id","=","invoices.supplier_id")
                                        ->select("bank_accounts.id as account_id","bank_accounts.account_name","bank_accounts.account_number","invoices.invoice_number","suppliers.name as supplier","invoices.id as index")
                                        ->where("invoice_external_ticket.ticket_group_id",$id)
                                         ->get();

   
    
        return $this->sendResponse(
            $facturas,
            'facturas encontradas'
        ); 
    }

    public function setInvoicePurchase(Request $r){
        
        if($r->isedit==0){
            $existe = DB::table("invoices")->where("supplier_id",$r->supplier_id)->where("invoice_number",$r->invoice_number)->first();
            if($existe){
                $existe2 =  DB::table("invoice_purchase")->where("purchase_id",$r->supply_request_id)->where("invoice_id",$existe->id)->first();
                if($existe2){

                }else{
                    DB::table("invoice_purchase")->insert(['purchase_id'=>$r->supply_request_id,'invoice_id'=>$existe->id,'created_at'=>date('Y-m-d H:i:s') ]);
                }
                
            }else{
                $last = DB::table("invoices")->insertGetId(['airline_id'=>null,'supplier_id'=>$r->supplier_id,'account_id'=>$r->account_id,'purchase_order'=>$r->purchase_order,'purchase_order_date'=>$r->purchase_order_date,'invoice_number'=>$r->invoice_number,'uploader_id'=>Auth::id()]);
                DB::table("invoice_purchase")->insert(['purchase_id'=>$r->supply_request_id,'invoice_id'=>$last,'created_at'=>date('Y-m-d H:i:s')  ]);
            }
        }else{
              DB::table("invoices")->where("id",$r->invoice_id)->update(['airline_id'=>null,'supplier_id'=>$r->supplier_id,'account_id'=>$r->account_id,'purchase_order'=>$r->purchase_order,'purchase_order_date'=>$r->purchase_order_date,'invoice_number'=>$r->invoice_number,'uploader_id'=>Auth::id()]);     

            }
        return $this->sendResponse(
            $r->all(),
            'invice guardado'
        ); 
    }


    public function deleteInvoice(int $id,int $quotation_id){  
        // aqui revisar como se borra la factura
        $crew = DB::table("quotations")->where("id",$quotation_id)->first();  
        DB::table("invoice_crew")->where("invoice_id",$id)->where("crew_id",$crew->id)->delete();

        DB::table("transfers")->where("invoice_id",$id)->where("crew_id",$crew->id)->update(['invoice_id'=>null,'invoice_user_id'=>null,'invoice_date'=>null]);
        DB::table("accommodations")->where("invoice_id",$id)->where("crew_id",$crew->id)->update(['invoice_id'=>null,'invoice_user_id'=>null,'invoice_date'=>null]);
        $tickets = DB::table("ticket_groups")->where("quotation_id",$crew->id)->get();
        foreach($tickets as $t){
            DB::table("worker_ticket")->where("ticket_group_id",$t->id)->where("invoice_id",$id)->update(['invoice_id'=>null,'invoice_user_id'=>null,'invoice_date'=>null]);
            DB::table("worker_ticket")->where("ticket_group_id",$t->id)->where("penalty_invoice_id",$id)->update(['penalty_invoice_id'=>null,'penalty_invoice_user_id'=>null,'penalty_invoice_date'=>null]);
        }
        DB::table("ticket_groups")->where("invoice_id",$id)->where("quotation_id",$crew->id)->update(['invoice_id'=>null,'invoice_user_id'=>null,'invoice_date'=>null]);

   /*$cuantos = DB::table("invoice_crew")->where("invoice_id",$id)->first();
        if($cuantos){

        }else{*/
            DB::table("invoices")->where("id",$id)->delete();
            DB::table("invoice_crew")->where("invoice_id",$id)->delete();
      //  }
        return $this->sendResponse(
            $id,
            'invoice eliminado'
        );
    }


    public function associateWorkerCarryInvoice(Request $r){
        DB::table("worker_ticket")->where("id",$r->id)->update(['invoice_id'=>$r->invoice_id,'invoice_user_id'=>Auth::id(),'invoice_date'=>date('Y-m-d H:i:s')]);

        return $this->sendResponse(
            $r,
            'invoice asociado'
        );
    }  

    public function associateWorkerPenaltyInvoice(Request $r){
        DB::table("worker_ticket")->where("id",$r->id)->update(['penalty_invoice_id'=>$r->invoice_id,'penalty_invoice_user_id'=>Auth::id(),'penalty_invoice_date'=>date('Y-m-d H:i:s')]);

        return $this->sendResponse(
            $r,
            'invoice asociado'
        );
    }  

    public function associateExternalInvoice(Request $r){


       // return response()->json($r->all());

        if($r->type=="Pasaje"){
            DB::table("ticket_groups")->where("id",$r->id)->update(['invoice_id'=>$r->invoice_id,'invoice_user_id'=>Auth::id(),'invoice_date'=>date('Y-m-d H:i:s')]);
        }

        if($r->type=="Multa"){
            if($r->person_type=="Persona"){
                DB::table("person_ticket")->where("id",$r->person_id)->update(['penalty_invoice_id'=>$r->invoice_id,'penalty_invoice_user_id'=>Auth::id(),'penalty_invoice_date'=>date('Y-m-d H:i:s')]);
            }
            if($r->person_type=="Tripulante"){
                DB::table("worker_ticket")->where("id",$r->person_id)->update(['penalty_invoice_id'=>$r->invoice_id,'penalty_invoice_user_id'=>Auth::id(),'penalty_invoice_date'=>date('Y-m-d H:i:s')]);
            }
         }

        if($r->type=="Maleta"){
            if($r->person_type=="Persona"){
                DB::table("person_ticket")->where("id",$r->person_id)->update(['invoice_id'=>$r->invoice_id,'invoice_user_id'=>Auth::id(),'invoice_date'=>date('Y-m-d H:i:s')]);
            }
            if($r->person_type=="Tripulante"){
                DB::table("worker_ticket")->where("id",$r->person_id)->update(['invoice_id'=>$r->invoice_id,'invoice_user_id'=>Auth::id(),'invoice_date'=>date('Y-m-d H:i:s')]);
            }
        }
        
        return $this->sendResponse(
            $r,
            'invoice asociado'
        );
    }  


    public function desassociateInvoiceExternal(Request $r){

        //return response()->json($r->all());
        if($r->type=="Pasaje"){
            DB::table("ticket_groups")->where("id",$r->id)->update(['invoice_id'=>null,'invoice_user_id'=>null,'invoice_date'=>null]);
        }

        if($r->type=="Multa"){
            if($r->person_type=="Persona"){
                DB::table("person_ticket")->where("id",$r->id)->update(['penalty_invoice_id'=>null,'penalty_invoice_user_id'=>null,'penalty_invoice_date'=>null]);
            }
            if($r->person_type=="Tripulante"){
                DB::table("worker_ticket")->where("id",$r->id)->update(['penalty_invoice_id'=>null,'penalty_invoice_user_id'=>null,'penalty_invoice_date'=>null]);
            }
         }

        if($r->type=="Maleta"){
            if($r->person_type=="Persona"){
                DB::table("person_ticket")->where("id",$r->id)->update(['invoice_id'=>null,'invoice_user_id'=>null,'invoice_date'=>null]);
            }
            if($r->person_type=="Tripulante"){
                DB::table("worker_ticket")->where("id",$r->id)->update(['invoice_id'=>null,'invoice_user_id'=>null,'invoice_date'=>null]);
            }
        }
        
        return $this->sendResponse(
            $r,
            'invoice asociado'
        );
    }  

    public function associateAccommodationInvoice(Request $r){
        DB::table("accommodations")->where("id",$r->accommodation_id)->update(['invoice_id'=>$r->invoice_id,'invoice_user_id'=>Auth::id(),'invoice_date'=>date('Y-m-d H:i:s')]);

        return $this->sendResponse(
            $r,
            'invoice asociado'
        );
    }  
    
    public function associateTransferInvoice(Request $r){
        DB::table("transfers")->where("id",$r->transfer_id)->update(['invoice_id'=>$r->invoice_id,'invoice_user_id'=>Auth::id(),'invoice_date'=>date('Y-m-d H:i:s')]);

        return $this->sendResponse(
            $r,
            'invoice asociado'
        );
    }  
    public function associateTicketInvoice(Request $r){
        DB::table("ticket_groups")->where("id",$r->ticket_id)->update(['invoice_id'=>$r->invoice_id,'invoice_user_id'=>Auth::id(),'invoice_date'=>date('Y-m-d H:i:s')]);

        return $this->sendResponse(
            $r,
            'invoice asociado'
        );
    }  

    public function deleteAccommodationInvoice(int $id){
        DB::table("accommodations")->where("id",$id)->update(['invoice_id'=>null,'invoice_user_id'=>null,'invoice_date'=>null]);

        return $this->sendResponse(
            $id,
            'invoice quitado'
        );
    }
    public function deleteTransferInvoice(int $id){
        DB::table("transfers")->where("id",$id)->update(['invoice_id'=>null,'invoice_user_id'=>null,'invoice_date'=>null]);

        return $this->sendResponse(
            $id,
            'invoice quitado'
        );
    }

    public function deleteTicketInvoice(int $id){
        DB::table("ticket_groups")->where("id",$id)->update(['invoice_id'=>null,'invoice_user_id'=>null,'invoice_date'=>null]);

        return $this->sendResponse(
            $id,
            'invoice quitado'
        );
    }

    public function deleteWorkerCarryInvoice(int $id){
        DB::table("worker_ticket")->where("id",$id)->update(['invoice_id'=>null,'invoice_user_id'=>null,'invoice_date'=>null]);

        return $this->sendResponse(
            $id,
            'invoice quitado'
        );
    }


 


    public function deleteExternalInvoice(int $id,int $ticket_id){

        DB::table("invoice_external_ticket")->where("invoice_id",$id)->where("ticket_group_id",$ticket_id)->delete();

        DB::table("ticket_groups")->where("invoice_id",$id)->where("id",$ticket_id)->update(['invoice_id'=>null,'invoice_user_id'=>null,'invoice_date'=>null]);

        DB::table("worker_ticket")->where("invoice_id",$id)->where("ticket_group_id",$ticket_id)->update(['invoice_id'=>null,'invoice_user_id'=>null,'invoice_date'=>null]);
        DB::table("person_ticket")->where("invoice_id",$id)->where("ticket_group_id",$ticket_id)->update(['invoice_id'=>null,'invoice_user_id'=>null,'invoice_date'=>null]);

        DB::table("worker_ticket")->where("penalty_invoice_id",$id)->where("ticket_group_id",$ticket_id)->update(['penalty_invoice_id'=>null,'penalty_invoice_user_id'=>null,'penalty_invoice_date'=>null]);
        DB::table("person_ticket")->where("penalty_invoice_id",$id)->where("ticket_group_id",$ticket_id)->update(['penalty_invoice_id'=>null,'penalty_invoice_user_id'=>null,'penalty_invoice_date'=>null]);


        return $this->sendResponse(
            $id,
            'invoice quitado'
        );
    }



    public function deleteWorkerPenaltyInvoice(int $id){
        DB::table("worker_ticket")->where("id",$id)->update(['penalty_invoice_id'=>null,'penalty_invoice_user_id'=>null,'penalty_invoice_date'=>null]);

        return $this->sendResponse(
            $id,
            'invoice quitado'
        );
    }

    public function quotateAny(Request $r){
        foreach($r->quotations as $q){
            $elid = 0;
            $existe = DB::table("invoices")->where("supplier_id",$q['supplier_id'])->where("invoice_number",$q['invoice_number'])->first();
            if($existe){
                $elid = $existe->id;
            }else{
                $last = DB::table("invoices")->insertGetId(['airline_id'=>null,'supplier_id'=>$q['supplier_id'],'account_id'=>$q['account_id'],'purchase_order'=>$q['purchase_order'],'purchase_order_date'=>$q['purchase_order_date'],'invoice_number'=>$q['invoice_number'],'uploader_id'=>Auth::id()]);
                $elid = $last;
            }
           
            if($r->type=='transfers'){
                foreach($q['transfers'] as $item){
                    DB::table("transfers")->where("id",$item['id'])->update(['invoice_id'=>$elid,'invoice_user_id'=>Auth::id(),'invoice_date'=>date('Y-m-d H:i:s')]);
                }  
            }
            if($r->type=='accommodations'){
                foreach($q['transfers'] as $item){
                    DB::table("accommodations")->where("id",$item['id'])->update(['invoice_id'=>$elid,'invoice_user_id'=>Auth::id(),'invoice_date'=>date('Y-m-d H:i:s')]);
                }  
            }
            if($r->type=='tickets'){
                foreach($q['transfers'] as $item){
                    DB::table("ticket_groups")->where("id",$item['id'])->update(['invoice_id'=>$elid,'invoice_user_id'=>Auth::id(),'invoice_date'=>date('Y-m-d H:i:s')]);
                }  
            }
            
        }
        return response()->json($r->quotations);
    }


    public function getPurchaseInvoice(int $id){
        $invoices = DB::table("invoices")
        ->join("invoice_purchase","invoice_purchase.invoice_id","=","invoices.id")
        ->join("suppliers","invoices.supplier_id","=","suppliers.id")
        ->join("bank_accounts","bank_accounts.id","=","invoices.account_id")
        ->select("suppliers.supplier_type_id","invoice_purchase.purchase_id","bank_accounts.account_name as account_name","bank_accounts.id as account_id","bank_accounts.account_number as account_number","suppliers.id as supplier_id","suppliers.name as supplier","invoices.purchase_order","invoices.purchase_order_date","invoices.invoice_number","invoices.price as price","invoices.id as invoice_id")
        ->where("invoice_purchase.purchase_id","=",$id)
        ->get();
        return response()->json($invoices);
    }

    public function associateInvoiceArticle(Request $r){

       // return $r;
        if($r->type==1){
            DB::table("provisions")->where("id",$r->article_id)->update(['invoice_user_id'=>Auth::id(),'invoice_id'=>$r->invoice_id,'invoice_date'=>date('Y-m-d H:i:s')]);

        }
        if($r->type==2){
            DB::table("purchase_warehouse_item_size")->where("id",$r->article_id)->update(['invoice_user_id'=>Auth::id(),'invoice_id'=>$r->invoice_id,'invoice_date'=>date('Y-m-d H:i:s')]);

        }

        $cuantos1 = DB::table("provisions")->where("id",$r->article_id)->whereNull("invoice_id")->count();
       
        $cuantos2 = DB::table("purchase_warehouse_item_size")->where("purchase_id",$r->purchase_id)->whereNull("invoice_id")->count();

        $many = $cuantos1+$cuantos2;
        if($many==0){
            DB::table("purchases")->where("id",$r->purchase_id)->update(['quoted'=>1]);
        }

    }

    public function desassociateInvoiceArticle(Request $r){

        //return $r;

        if($r->type==1){
            DB::table("provisions")->where("id",$r->id)->update(['invoice_user_id'=>null,'invoice_id'=>null,'invoice_date'=>null]);
        }
        if($r->type==2){
            DB::table("purchase_warehouse_item_size")->where("id",$r->id)->update(['invoice_user_id'=>null,'invoice_id'=>null,'invoice_date'=>null]);
        }

        
        $cuantos1 = DB::table("provisions")->where("id",$r->id)->whereNull("invoice_id")->count();
        $cuantos2 = DB::table("purchase_warehouse_item_size")->where("purchase_id",$r->purchase_id)->whereNull("invoice_id")->count();
        $many = $cuantos1+$cuantos2;
        if($many==0){
            DB::table("purchases")->where("id",$r->purchase_id)->update(['quoted'=>1]);
        }else{
            DB::table("purchases")->where("id",$r->purchase_id)->update(['quoted'=>0]);
        }

    }

    public function deleteInvoicePurchase(Request $r){

        
        DB::table("invoice_purchase")->where("invoice_id",$r->invoice_id)->where("purchase_id",$r->purchase_id)->delete();
        DB::table("purchase_warehouse_item_size")->where("purchase_id",$r->purchase_id)->where("invoice_id",$r->invoice_id)->update(['invoice_user_id'=>null,'invoice_id'=>null,'invoice_date'=>null]);
        DB::table("provisions")->where("id",$r->provisions_id)->where("invoice_id",$r->invoice_id)->update(['invoice_user_id'=>null,'invoice_id'=>null,'invoice_date'=>null]);
 

       $many1 = DB::table("invoice_crew")->where("invoice_id",$r->invoice_id)->count();
       $many2 = DB::table("invoice_purchase")->where("invoice_id",$r->invoice_id)->count();
       $many3 = DB::table("provisions")->where("invoice_id",$r->invoice_id)->count();
        $cuantos = $many1 + $many2 +$many3;
        if($cuantos>0){

        }else{
            DB::table("invoices")->where("id",$r->invoice_id)->delete();
        }
        
        $cuantos1 = DB::table("provisions")->where("id",$r->provisions_id)->whereNull("invoice_id")->count();
        $cuantos2 = DB::table("purchase_warehouse_item_size")->where("purchase_id",$r->purchase_id)->whereNull("invoice_id")->count();
        $many = $cuantos1 + $cuantos2;
        if($many==0){
            DB::table("purchases")->where("id",$r->purchase_id)->update(['quoted'=>1]);
        }else{
            DB::table("purchases")->where("id",$r->purchase_id)->update(['quoted'=>0]);
        }


        return $this->sendResponse(
            $r,
            'invoice eliminado'
        );
    }


    public function purchaseQuoted(Request $r){
        DB::table("purchases")->where("id",$r->purchase_id)->update(['closed'=>1]);
    }


    

    public function getInvoicesHistory(Request $request){
        $search = $request->query('search');
        $facturas = DB::table("invoices")
                    ->join("suppliers","suppliers.id","=","invoices.supplier_id")
        ->select("invoices.*","suppliers.name","suppliers.code")
        ->orderBy("invoices.id","desc")->groupBy("suppliers.code")
        ->groupBy("invoices.invoice_number")->get();

        foreach($facturas as $fa){
            $many = 0;
            $price = 0;
            $proveedor = DB::table("suppliers")->join("invoices","invoices.supplier_id","=","suppliers.id")->where("suppliers.code",$fa->code)->where("invoices.invoice_number",$fa->invoice_number)->select("invoices.id")->get();
            foreach($proveedor as $f){
                $alojamientos = DB::table("accommodations")->where("invoice_id",$f->id)->get();
                foreach($alojamientos as $a){
                    $personas = DB::table("accommodation_person")->where("accommodation_id",$a->id)->count();
                    $tripulantes = DB::table("accommodation_worker")->where("accommodation_id",$a->id)->count();
                    $many = $many + ($personas + $tripulantes);
                   // $price = $price + (($personas + $tripulantes) * $a->price);
                    $price = $price + $a->price;
                }
    
                $traslados = DB::table("transfers")->where("invoice_id",$f->id)->get();
                foreach($traslados as $a){
                    $personas = DB::table("person_transfer")->where("transfer_id",$a->id)->count();
                    $tripulantes = DB::table("worker_transfer")->where("transfer_id",$a->id)->count();
                    $many = $many + ($personas + $tripulantes);
                    //$price = $price + (($personas + $tripulantes) * $a->price);
                    $price = $price + $a->price;
                }


                $person_maleta = DB::table("person_ticket")->where("invoice_id",$f->id)->get();
                $many = $many + sizeof($person_maleta);
                foreach($person_maleta as $p){
                    $price = $price + intval($p->carry_price);
                }
            
                $worker_maleta = DB::table("worker_ticket")->where("invoice_id",$f->id)->get();
                $many = $many + sizeof($worker_maleta);
                foreach($worker_maleta as $p){
                    $price = $price + intval($p->carry_price);
                }

                $person_multa = DB::table("person_ticket")->where("penalty_invoice_id",$f->id)->get();
                $many = $many + sizeof($person_multa);
                foreach($person_multa as $p){
                    $price = $price + intval($p->penalty);
                }
                $worker_multa = DB::table("worker_ticket")->where("penalty_invoice_id",$f->id)->get();
                $many = $many + sizeof($worker_multa);
                foreach($worker_multa as $p){
                    $price = $price + intval($p->penalty);
                }


                $fa->multasymaletas = $price;

                
                $fa->pasajesnormal = 0;
    
                $pasajes = DB::table("ticket_groups")->where("invoice_id",$f->id)->get();
                foreach($pasajes as $p){
                    $many = $many + intval($p->passengers);
                    
                    

                    if($p->payment_type==1){
                        $resta = $p->price - $p->payment_use_price;
                        if($resta<0){
                            $p->price = 0;
                            $fa->pasajesnormal =  $fa->pasajesnormal + 0;
                        }else{
                            $p->price = $resta;
                            $fa->pasajesnormal =  $fa->pasajesnormal + $resta;
                        }

                        
                    }

                    $penal = 0;
                    if($p->payment_type==2){
                       $voucher = DB::table("log_vouchers")->where("ticket_group_id",$p->id)->first();

                       $price = $price - $voucher->required_price;
                       /*$restaprice = 0;

                       $person_multa = DB::table("person_ticket")->where("penalty_invoice_id",$f->id)->where("ticket_group_id",$p->id)->get();
                       foreach($person_multa as $j){
                           $restaprice = $restaprice + intval($j->penalty);
                       }
                       $worker_multa = DB::table("worker_ticket")->where("penalty_invoice_id",$f->id)->where("ticket_group_id",$p->id)->get();
                       foreach($worker_multa as $j){
                           $restaprice = $restaprice + intval($j->penalty);
                       }

                       $restaprice = $restaprice + ($p->price * intval($p->passengers));
                      


                       $fa->restaprice = $restaprice;
                        $penal = $penal + $p->penalty;

                        $fa->penal = $penal;

                       if($restaprice <= $voucher->voucher_price){
                          $price = $price - $restaprice;
                          $fa->pasajevoucher = $price - $restaprice;
                       }else{
                          
                          $price = $price + ($restaprice - $voucher->voucher_price);
                          $fa->pasajevoucher =  $price + ($restaprice - $voucher->voucher_price);
                       }


                       $fa->elprice = $price;*/
                       
                    }

                   
                        $price = $price + ($p->price * intval($p->passengers)) + $p->dearness;
                   
            


                   
                    if($p->dearness>0){
                        $many = $many+1;
                    }
                }


              
             
          
    
                $provisiones = DB::table("provisions")->where("invoice_id",$f->id)->get();
                foreach($provisiones as $p){
                    $many = $many + 1;
                    $price = $price + $p->price;
                }
                
                $compras = DB::table("purchases")->join("invoice_purchase","invoice_purchase.purchase_id","=","purchases.id")->where("invoice_purchase.invoice_id",$f->id)->get();
                foreach($compras as $c){
                   $cuantos = DB::table("purchase_warehouse_item_size")->where("purchase_id",$c->purchase_id)->get();
                   foreach($cuantos as $k){
                        $many = $many + $k->quantity;
                        $price = $price + ($k->price * $k->quantity);
                   }
                 
                }
            }

            $fa->quantity = $many;
            $fa->price = $price;
            
        }
      
        return $this->sendResponse($facturas
            ,
            'person list'
        );

    }

 
    public function getInvoiceDetails(int $id){
                 
            $fa = DB::table("invoices")
                ->join("suppliers","suppliers.id","=","invoices.supplier_id")
                ->select("invoices.*","suppliers.name","suppliers.code")
                ->where("invoices.id",$id)
                ->first();

           // 
            $many = 0;
            $price = 0;
            $proveedor = DB::table("suppliers")->join("invoices","invoices.supplier_id","=","suppliers.id")->where("suppliers.code",$fa->code)->where("invoices.invoice_number",$fa->invoice_number)->select("invoices.id")->get();
           // return response()->json($proveedor);
           $arreglo= [];
           foreach($proveedor as $p){
                array_push($arreglo,$p->id);
           }
          // return response()->json($arreglo);
            foreach($proveedor as $f){
               
                $alojamientos = DB::table("accommodations")->where("invoice_id",$f->id)->get();

                foreach($alojamientos as $a){
                    $personas = DB::table("accommodation_person")->where("accommodation_id",$a->id)->count();
                    $tripulantes = DB::table("accommodation_worker")->where("accommodation_id",$a->id)->count();
                  //  $price = $price + (($personas + $tripulantes) * $a->price);
                    $price = $price + $a->price;
                }
    
                $traslados = DB::table("transfers")->where("invoice_id",$f->id)->get();
                foreach($traslados as $a){
                    $personas = DB::table("person_transfer")->where("transfer_id",$a->id)->count();
                    $tripulantes = DB::table("worker_transfer")->where("transfer_id",$a->id)->count();
                    //$many = $many + ($personas + $tripulantes);
                    $price = $price + $a->price;
                }
    
                $pasajes = DB::table("ticket_groups")->where("invoice_id",$f->id)->get();
                foreach($pasajes as $p){

                    if($p->payment_type==2){
                        $price = $price + $p->extra_price;
                    }else{
                        $price = $price + $p->total_price;
                    }
                  

                    $mperson = DB::table("person_ticket")->where("ticket_group_id",$p->id)->get();
                    foreach($mperson as $m){
                        $price = $price + $m->carry_price;
                    }
                }
    
                $maletas = DB::table("worker_ticket")->where("invoice_id",$f->id)->get();
                foreach($maletas as $m){
                   // $many = $many + 1;
                    $price = $price + $m->carry_price;
                }
    
                $provisiones = DB::table("provisions")->where("invoice_id",$f->id)->get();
                foreach($provisiones as $p){
                   // $many = $many + 1;
                    $price = $price + $p->price;
                }
                
                $compras = DB::table("purchases")->join("invoice_purchase","invoice_purchase.purchase_id","=","purchases.id")->where("invoice_purchase.invoice_id",$f->id)->get();
                foreach($compras as $c){
                   $cuantos = DB::table("purchase_warehouse_item_size")->where("purchase_id",$c->purchase_id)->whereNotNull("invoice_id")->get();
                   foreach($cuantos as $k){
                        //$many = $many + $k->quantity;
                        $price = $price + ($k->price * $k->quantity);
                   }

                   
                 
                }
            }

            //alojamientos
            $alojamientos = DB::table("accommodations")->join("accommodation_worker","accommodation_worker.accommodation_id","=","accommodations.id")->join("workers","workers.id","=","accommodation_worker.worker_id")->whereIn("invoice_id",$arreglo)->get();
            foreach($alojamientos as $a){

                    $personas = DB::table("accommodation_person")->where("accommodation_id",$a->accommodation_id)->count();
                    $tripulantes = DB::table("accommodation_worker")->where("accommodation_id",$a->accommodation_id)->count();

                    $division = $personas + $tripulantes;
                    if($division>0){
                        $a->price = $a->price / $division;
                    }
                    

                $invoiceData = DB::table("invoices")->where("id",$a->invoice_id)->first();
                $a->purchase_order = $invoiceData->purchase_order;
                $a->purchase_order_date = $invoiceData->purchase_order_date;
                $a->crew = " - ";
                if($a->crew_id<>null){
                    $trip = DB::table("crews")->join("ships","ships.id","=","crews.ship_id")->join("clients","clients.id","=","crews.client_id")->select("clients.name as client","ships.name as ship","crews.id")->where("crews.id",$a->crew_id)->first();
                    $a->crew = $trip->ship." - ".$trip->client;
                }
                $a->stretch = " - ";
                if($a->journey_type<>0){
                    if($a->journey_type==1){
                        $a->stretch = "Ida";
                    }
                    if($a->journey_type==2){
                        $a->stretch = "Vuelta";
                    }
                }
                $a->user_type = "Tripulante";
                $usuario = DB::table("users")->where("id",$a->invoice_user_id)->first();
                $a->invoice_user = $usuario->name." ".$usuario->last_name;
            }

            $alojamientosperson = DB::table("accommodations")->join("accommodation_person","accommodation_person.accommodation_id","=","accommodations.id")->join("persons","persons.id","=","accommodation_person.person_id")->whereIn("invoice_id",$arreglo)->get();
            foreach($alojamientosperson as $a){

                $personas = DB::table("accommodation_person")->where("accommodation_id",$a->accommodation_id)->count();
                $tripulantes = DB::table("accommodation_worker")->where("accommodation_id",$a->accommodation_id)->count();

                $division = $personas + $tripulantes;
                if($division>0){
                    $a->price = $a->price / $division;
                }

                $invoiceData = DB::table("invoices")->where("id",$a->invoice_id)->first();
                $a->purchase_order = $invoiceData->purchase_order;
                $a->purchase_order_date = $invoiceData->purchase_order_date;
                $a->crew = " - ";
                $a->stretch = " - ";
                if($a->journey_type<>0){
                    if($a->journey_type==1){
                        $a->stretch = "Ida";
                    }
                    if($a->journey_type==2){
                        $a->stretch = "Vuelta";
                    }
                }
                $a->user_type = "Persona";
                $usuario = DB::table("users")->where("id",$a->invoice_user_id)->first();
                $a->invoice_user = $usuario->name." ".$usuario->last_name;
                
            }

            //fin alojamientos

            //traslados

            $traslados = DB::table("transfers")->join("worker_transfer","worker_transfer.transfer_id","=","transfers.id")->join("workers","workers.id","=","worker_transfer.worker_id")->whereIn("invoice_id",$arreglo)->get();
            foreach($traslados as $a){

                $personas = DB::table("person_transfer")->where("transfer_id",$a->transfer_id)->count();
                $tripulantes = DB::table("worker_transfer")->where("transfer_id",$a->transfer_id)->count();

                $division = $personas + $tripulantes;
                if($division>0){
                    $a->price = $a->price / $division;
                }

                $invoiceData = DB::table("invoices")->where("id",$a->invoice_id)->first();
                $a->purchase_order = $invoiceData->purchase_order;
                $a->purchase_order_date = $invoiceData->purchase_order_date;
                $a->crew = " - ";
                if($a->crew_id<>null){
                    $trip = DB::table("crews")->join("ships","ships.id","=","crews.ship_id")->join("clients","clients.id","=","crews.client_id")->select("clients.name as client","ships.name as ship","crews.id")->where("crews.id",$a->crew_id)->first();
                    $a->crew = $trip->ship." - ".$trip->client;
                }
                $a->stretch = " - ";
                if($a->journey_type<>0){
                    if($a->journey_type==1){
                        $a->stretch = "Ida";
                    }
                    if($a->journey_type==2){
                        $a->stretch = "Vuelta";
                    }
                }
                $a->user_type = "Tripulante";
                $usuario = DB::table("users")->where("id",$a->invoice_user_id)->first();
                $a->invoice_user = $usuario->name." ".$usuario->last_name;
            }

            $trasladosperson = DB::table("transfers")->join("person_transfer","person_transfer.transfer_id","=","transfers.id")->join("persons","persons.id","=","person_transfer.person_id")->whereIn("invoice_id",$arreglo)->get();
            foreach($trasladosperson as $a){

                $personas = DB::table("person_transfer")->where("transfer_id",$a->transfer_id)->count();
                $tripulantes = DB::table("worker_transfer")->where("transfer_id",$a->transfer_id)->count();

                $division = $personas + $tripulantes;
                if($division>0){
                    $a->price = $a->price / $division;
                }

                $invoiceData = DB::table("invoices")->where("id",$a->invoice_id)->first();
                $a->purchase_order = $invoiceData->purchase_order;
                $a->purchase_order_date = $invoiceData->purchase_order_date;
                $a->crew = " - ";
                $a->stretch = " - ";
                if($a->journey_type<>0){
                    if($a->journey_type==1){
                        $a->stretch = "Ida";
                    }
                    if($a->journey_type==2){
                        $a->stretch = "Vuelta";
                    }
                }
                $a->user_type = "Persona";
                $usuario = DB::table("users")->where("id",$a->invoice_user_id)->first();
                $a->invoice_user = $usuario->name." ".$usuario->last_name;
                
            }

            //fin traslados



           // maletas
            $maletas = DB::table("worker_ticket")->join("workers","workers.id","=","worker_ticket.worker_id")->whereIn("invoice_id",$arreglo)->get();
          
            foreach($maletas as $a){
                $invoiceData = DB::table("ticket_groups")->where("id",$a->ticket_group_id)->first();
                $a->purchase_order = $invoiceData->oc;
                $a->purchase_order_date = $invoiceData->oc_date;
                $a->crew = " - ";

                $ticket = DB::table("ticket_groups")->where("id",$a->ticket_group_id)->first();
                
                if($ticket->crew_id<>null){
                    $trip = DB::table("crews")->join("ships","ships.id","=","crews.ship_id")->join("clients","clients.id","=","crews.client_id")->select("clients.name as client","ships.name as ship","crews.id")->where("crews.id",$ticket->crew_id)->first();
                    $a->crew = $trip->ship." - ".$trip->client;
                }
                $a->user_type = "Tripulante";
                $usuario = DB::table("users")->where("id",$a->invoice_user_id)->first();
                $a->invoice_user = $usuario->name." ".$usuario->last_name;
                
            }


            $maletasperson = DB::table("person_ticket")->join("persons","persons.id","=","person_ticket.person_id")->whereIn("invoice_id",$arreglo)->get();
          
            foreach($maletasperson as $a){
                $invoiceData = DB::table("ticket_groups")->where("id",$a->ticket_group_id)->first();
                    $a->purchase_order = $invoiceData->oc;
                    $a->purchase_order_date = $invoiceData->oc_date;
                $a->crew = " - ";

                $ticket = DB::table("ticket_groups")->where("id",$a->ticket_group_id)->first();
                
                if($ticket->crew_id<>null){
                    $trip = DB::table("crews")->join("ships","ships.id","=","crews.ship_id")->join("clients","clients.id","=","crews.client_id")->select("clients.name as client","ships.name as ship","crews.id")->where("crews.id",$ticket->crew_id)->first();
                    $a->crew = $trip->ship." - ".$trip->client;
                }
                $a->user_type = "Persona";
                $usuario = DB::table("users")->where("id",$a->invoice_user_id)->first();
                $a->invoice_user = $usuario->name." ".$usuario->last_name;
                
            }


        
            //fin maletas


                // multas
                $multas = DB::table("worker_ticket")->join("workers","workers.id","=","worker_ticket.worker_id")->whereIn("penalty_invoice_id",$arreglo)->get();
          
                foreach($multas as $a){
                  //  $invoiceData = DB::table("invoices")->where("id",$a->penalty_invoice_id)->first();
                  //$a->purchase_order = $invoiceData->purchase_order;
                  //$a->purchase_order_date = $invoiceData->purchase_order_date;
                    $invoiceData = DB::table("ticket_groups")->where("id",$a->ticket_group_id)->first();
                    $a->payment_type = $invoiceData->payment_type;
                    $a->purchase_order = $invoiceData->oc;
                    $a->purchase_order_date = $invoiceData->oc_date;
                    $a->invoice_date = $invoiceData->invoice_date;
                    $a->crew = " - ";
    
                    $ticket = DB::table("ticket_groups")->where("id",$a->ticket_group_id)->first();
                    
                    if($ticket->crew_id<>null){
                        $trip = DB::table("crews")->join("ships","ships.id","=","crews.ship_id")->join("clients","clients.id","=","crews.client_id")->select("clients.name as client","ships.name as ship","crews.id")->where("crews.id",$ticket->crew_id)->first();
                        $a->crew = $trip->ship." - ".$trip->client;
                    }
                    $a->user_type = "Tripulante";
                    $usuario = DB::table("users")->where("id",$a->penalty_invoice_user_id)->first();
                    $a->invoice_user = $usuario->name." ".$usuario->last_name;
                    
                }


                $multasperson = DB::table("person_ticket")->join("persons","persons.id","=","person_ticket.person_id")->whereIn("penalty_invoice_id",$arreglo)->get();
          
                foreach($multasperson as $a){
                    $invoiceData = DB::table("ticket_groups")->where("id",$a->ticket_group_id)->first();
                    $a->payment_type = $invoiceData->payment_type;
                    $a->purchase_order = $invoiceData->oc;
                    $a->purchase_order_date = $invoiceData->oc_date;
                    $a->invoice_date = $invoiceData->invoice_date;
                    $a->crew = " - ";
    
                    $ticket = DB::table("ticket_groups")->where("id",$a->ticket_group_id)->first();
                    
                    if($ticket->crew_id<>null){
                        $trip = DB::table("crews")->join("ships","ships.id","=","crews.ship_id")->join("clients","clients.id","=","crews.client_id")->select("clients.name as client","ships.name as ship","crews.id")->where("crews.id",$ticket->crew_id)->first();
                        $a->crew = $trip->ship." - ".$trip->client;
                    }
                    $a->user_type = "Persona";
                    $usuario = DB::table("users")->where("id",$a->penalty_invoice_user_id)->first();
                    $a->invoice_user = $usuario->name." ".$usuario->last_name;
                    
                }
                //fin multas


           // viveres
           $provisiones = DB::table("provisions")->whereIn("invoice_id",$arreglo)->get();
           foreach($provisiones as $a){
               $invoiceData = DB::table("invoices")->where("id",$a->invoice_id)->first();
               $a->purchase_order = $invoiceData->purchase_order;
               $a->purchase_order_date = $invoiceData->purchase_order_date;
               $a->crew = " - ";
               
               if($a->crew_id<>null){
                   $trip = DB::table("crews")->join("ships","ships.id","=","crews.ship_id")->join("clients","clients.id","=","crews.client_id")->select("clients.name as client","ships.name as ship","crews.id")->where("crews.id",$a->crew_id)->first();
                   $a->crew = $trip->ship." - ".$trip->client;
               }
               $usuario = DB::table("users")->where("id",$a->invoice_user_id)->first();
               $a->invoice_user = $usuario->name." ".$usuario->last_name;
               
           }
           //fin viveres

           //compras
           $arreglopurchase = [];
           $idcompras = DB::table("purchases")->join("invoice_purchase","invoice_purchase.purchase_id","=","purchases.id")->whereIn("invoice_purchase.invoice_id",$arreglo)->select("purchases.id")->get();
           foreach($idcompras as $c){
            array_push($arreglopurchase,$c->id);
           }
           $compras = Purchase::with('warehouse_item_sizes')->whereIn("id",$arreglopurchase)->get();
           foreach($compras as $c){
               
               $c->crew = " - ";
               if($c->supply_request_id!=null){
              
                    $trip = DB::table("crews")->join("ships","ships.id","=","crews.ship_id")->join("clients","clients.id","=","crews.client_id")->select("clients.name as client","ships.name as ship","crews.id")->where("crews.id",$c->supply_request_id)->first();
                    $c->crew = $trip->ship." - ".$trip->client;
             
               }
                foreach($c->warehouse_item_sizes as $key => $d){
                    if($d['invoice_id']==null){
                        $c->warehouse_item_sizes ->forget($key);
                    }else{
                        $invoiceData = DB::table("invoices")->where("id",$d['invoice_id'])->first();
                        $d['purchase_order'] = $invoiceData->purchase_order;
                        $d['purchase_order_date'] = $invoiceData->purchase_order_date;
                        $d['invoice_user_name'] = "Sin Usuario";
                        if($d['invoice_user_id']<>null){
                            $usuario = DB::table("users")->where("id",$d['invoice_user_id'])->first();
                            $d['invoice_user_name'] = $usuario->name." ".$usuario->last_name;
                        }
                    }
                    /*$invoiceData = DB::table("invoices")->where("id",$d['invoice_id'])->first();
                    $d['purchase_order'] = $invoiceData->purchase_order;*/
                    /*$d['invoice_user_name'] = "Sin Usuario";
                    if($d['invoice_user_id']<>null){
                        $usuario = DB::table("users")->where("id",$d['invoice_user_id'])->first();
                        $d['invoice_user_name'] = $usuario->name." ".$usuario->last_name;
                    }*/
                }
           }
           //fin compras


           /* $pasajes = DB::table("ticket_groups")->join("worker_ticket","worker_ticket","=","ticket_groups.id")->join("workers","workers.id","=","worker_ticket.worker_id")->whereIn("ticket_groups.invoice_id",$arreglo)
                        ->select("workers.name","workers.last_name","ticket_groups.price as price","worker_ticket.penalty")->get();  */
                  
            //$compras = DB::table("purchases")->join("invoice_purchase","invoice_purchase.purchase_id","=","purchases.id")->whereIn("invoice_purchase.invoice_id",$arreglo)->get();

            $pasajes = DB::table("ticket_groups")
            ->join("airlines","airlines.id","=","ticket_groups.airline_id")
            ->join("flight_stretches","flight_stretches.id","=","ticket_groups.flight_stretch_id")
            ->join("worker_ticket","worker_ticket.ticket_group_id","=","ticket_groups.id")
            ->join("workers","workers.id","=","worker_ticket.worker_id")
            ->select("ticket_groups.payment_use_price","worker_ticket.penalty_quotation","ticket_groups.oc","ticket_groups.oc_date","ticket_groups.payment_type","ticket_groups.invoice_user_id","ticket_groups.invoice_date","worker_ticket.carry_price","worker_ticket.quotation","ticket_groups.invoice_id","workers.name as worker_name","workers.last_name as worker_last_name","workers.rut as rut","ticket_groups.airline_id","ticket_groups.code","ticket_groups.ticket_type","ticket_groups.passengers","ticket_groups.id","ticket_groups.plus","ticket_groups.id","ticket_groups.arrival_airport_id","ticket_groups.departure_airport_id","ticket_groups.flight_date","ticket_groups.departure_time","ticket_groups.arrival_time","airlines.name as airline","flight_stretches.name as stretch","ticket_groups.extra_price","ticket_groups.price")
            ->whereIn("ticket_groups.invoice_id",$arreglo)
            ->orderBy("ticket_groups.id","desc")
            ->get();

            foreach($pasajes as $t){
                
                //$invoiceData = DB::table("invoices")->where("id",$t->invoice_id)->first();
                $t->purchase_order = $t->oc;
                $t->purchase_order_date = $t->oc_date;

                $usuario = DB::table("users")->where("id",$t->invoice_user_id)->first();
                $t->invoice_user = $usuario->name." ".$usuario->last_name;

                $supplier = DB::table("suppliers")->where("code",$t->airline_id)->where("supplier_type_id",5)->first();
                $t->supplier_id = $supplier->id;

                $airport1 = DB::table("airports")->where("id",$t->departure_airport_id)->first();
                $airport2 = DB::table("airports")->where("id",$t->arrival_airport_id)->first();
                if($airport1){
                    $t->airport1 = $airport1->city;
                }
                if($airport2){
                    $t->airport2 = $airport2->city;
                }
                $t->type = $t->ticket_type;

                if($t->ticket_type=='crew'){
                    $cantidad = DB::table("tickets")->where("ticket_group_id",$t->id)->where("ticket_status_id","1")->get();
                    foreach($cantidad as $c){
                        $carry = $carry + intval($c->carry_price);
                    }
                }
               
                $trip = DB::table("crew_tickets")->where("ticket_group_id",$t->id)->first();
                if($trip){
                    $crew = DB::table("crews")
                    ->join("ships","ships.id","=","crews.ship_id")
                    ->join("clients","clients.id","=","crews.client_id")
                    ->select("clients.name as client","ships.name as ship","crews.upload_date")
                    ->where("crews.id",$trip->crew_id)
                    ->first();
                    if($crew){
                        $t->upload_date = $crew->upload_date;
                        $t->crew = $crew->ship." / ".$crew->client.'('.$crew->upload_date.')';
                    }else{
                        $t->crew = '-';
                        $t->upload_date = '';
                    }
                    
                }else{
                    $t->crew = '-';
                    $t->upload_date = '';
                }
                if($t->code==null || $t->code==''){
                    $t->code = '-';
                }
            }

            $pasajes2 = DB::table("ticket_groups")
            ->join("airlines","airlines.id","=","ticket_groups.airline_id")
            ->join("flight_stretches","flight_stretches.id","=","ticket_groups.flight_stretch_id")
            ->join("person_ticket","person_ticket.ticket_group_id","=","ticket_groups.id")
            ->join("persons","persons.id","=","person_ticket.person_id")
            ->select("ticket_groups.payment_use_price","person_ticket.penalty_quotation","ticket_groups.oc","ticket_groups.oc_date","ticket_groups.payment_type","ticket_groups.invoice_user_id","ticket_groups.invoice_date","person_ticket.carry_price","person_ticket.quotation","ticket_groups.invoice_id","persons.name as worker_name","persons.last_name as worker_last_name","persons.rut as rut","ticket_groups.airline_id","ticket_groups.code","ticket_groups.ticket_type","ticket_groups.passengers","ticket_groups.id","ticket_groups.plus","ticket_groups.id","ticket_groups.arrival_airport_id","ticket_groups.departure_airport_id","ticket_groups.flight_date","ticket_groups.departure_time","ticket_groups.arrival_time","airlines.name as airline","flight_stretches.name as stretch","ticket_groups.extra_price","ticket_groups.price")
            ->whereIn("ticket_groups.invoice_id",$arreglo)
            ->orderBy("ticket_groups.id","desc")
            ->get();

            foreach($pasajes2 as $t){
                
              //  $invoiceData = DB::table("invoices")->where("id",$t->invoice_id)->first();
                $t->purchase_order = $t->oc;
                $t->purchase_order_date = $t->oc_date;

                $usuario = DB::table("users")->where("id",$t->invoice_user_id)->first();
                $t->invoice_user = $usuario->name." ".$usuario->last_name;

                $supplier = DB::table("suppliers")->where("code",$t->airline_id)->where("supplier_type_id",5)->first();
                $t->supplier_id = $supplier->id;

                $airport1 = DB::table("airports")->where("id",$t->departure_airport_id)->first();
                $airport2 = DB::table("airports")->where("id",$t->arrival_airport_id)->first();
                if($airport1){
                    $t->airport1 = $airport1->city;
                }
                if($airport2){
                    $t->airport2 = $airport2->city;
                }
                $t->type = $t->ticket_type;

                if($t->ticket_type=='crew'){
                    $cantidad = DB::table("tickets")->where("ticket_group_id",$t->id)->where("ticket_status_id","1")->get();
                    foreach($cantidad as $c){
                        $carry = $carry + intval($c->carry_price);
                    }
                }
               
                $trip = DB::table("crew_tickets")->where("ticket_group_id",$t->id)->first();
                if($trip){
                    $crew = DB::table("crews")
                    ->join("ships","ships.id","=","crews.ship_id")
                    ->join("clients","clients.id","=","crews.client_id")
                    ->select("clients.name as client","ships.name as ship","crews.upload_date")
                    ->where("crews.id",$trip->crew_id)
                    ->first();
                    if($crew){
                        $t->upload_date = $crew->upload_date;
                        $t->crew = $crew->ship." / ".$crew->client.'('.$crew->upload_date.')';
                    }else{
                        $t->crew = '-';
                        $t->upload_date = '';
                    }
                    
                }else{
                    $t->crew = '-';
                    $t->upload_date = '';
                }
                if($t->code==null || $t->code==''){
                    $t->code = '-';
                }
            }


            $extra = DB::table("ticket_groups")
            ->join("airlines","airlines.id","=","ticket_groups.airline_id")
            ->join("flight_stretches","flight_stretches.id","=","ticket_groups.flight_stretch_id")
            ->select("ticket_groups.oc","ticket_groups.oc_date","ticket_groups.dearness","ticket_groups.penalty","ticket_groups.invoice_user_id","ticket_groups.invoice_date","ticket_groups.invoice_id","ticket_groups.airline_id","ticket_groups.code","ticket_groups.ticket_type","ticket_groups.passengers","ticket_groups.id","ticket_groups.plus","ticket_groups.id","ticket_groups.arrival_airport_id","ticket_groups.departure_airport_id","ticket_groups.flight_date","ticket_groups.departure_time","ticket_groups.arrival_time","airlines.name as airline","flight_stretches.name as stretch","ticket_groups.extra_price","ticket_groups.price")
            ->whereIn("ticket_groups.invoice_id",$arreglo)
            ->where("ticket_groups.dearness",">","0")
            ->orderBy("ticket_groups.id","desc")
            ->groupBy("ticket_groups.id")
            ->get();

            foreach($extra as $t){
                
                //$invoiceData = DB::table("invoices")->where("id",$t->invoice_id)->first();
                $t->purchase_order = $t->oc;
                $t->purchase_order_date = $t->oc_date;

                $usuario = DB::table("users")->where("id",$t->invoice_user_id)->first();
                $t->invoice_user = $usuario->name." ".$usuario->last_name;

                $supplier = DB::table("suppliers")->where("code",$t->airline_id)->where("supplier_type_id",5)->first();
                $t->supplier_id = $supplier->id;

                $airport1 = DB::table("airports")->where("id",$t->departure_airport_id)->first();
                $airport2 = DB::table("airports")->where("id",$t->arrival_airport_id)->first();
                if($airport1){
                    $t->airport1 = $airport1->city;
                }
                if($airport2){
                    $t->airport2 = $airport2->city;
                }
                $t->type = $t->ticket_type;

                if($t->ticket_type=='crew'){
                    $cantidad = DB::table("tickets")->where("ticket_group_id",$t->id)->where("ticket_status_id","1")->get();
                    foreach($cantidad as $c){
                        $carry = $carry + intval($c->carry_price);
                    }
                }
               
                $trip = DB::table("crew_tickets")->where("ticket_group_id",$t->id)->first();
                if($trip){
                    $crew = DB::table("crews")
                    ->join("ships","ships.id","=","crews.ship_id")
                    ->join("clients","clients.id","=","crews.client_id")
                    ->select("clients.name as client","ships.name as ship","crews.upload_date")
                    ->where("crews.id",$trip->crew_id)
                    ->first();
                    if($crew){
                        $t->upload_date = $crew->upload_date;
                        $t->crew = $crew->ship." / ".$crew->client.'('.$crew->upload_date.')';
                    }else{
                        $t->crew = '-';
                        $t->upload_date = '';
                    }
                    
                }else{
                    $t->crew = '-';
                    $t->upload_date = '';
                }
                if($t->code==null || $t->code==''){
                    $t->code = '-';
                }
            }



            $alojamientos =$alojamientos->merge($alojamientosperson);
            $traslados = $traslados->merge($trasladosperson);
            $pasajes = $pasajes->merge($pasajes2);
            $maletas = $maletas->merge($maletasperson);
            $multas = $multas->merge($multasperson);


            $ticketgroup = [];
            $ticketvalue = [];
            $voucheruso = [];
            $uso = 1;
            foreach($pasajes as $m){
                if($m->payment_type==2){
                    if(!in_array($m->id,$ticketgroup)){
                        $voucher = DB::table("log_vouchers")->where("ticket_group_id",$m->id)->first();
                        array_push($ticketgroup,$m->id);
                        array_push($voucheruso,$voucher->id);
                        array_push($ticketvalue,$voucher->voucher_price);      
                        $uso = $uso +1;
                    }
                }
            }


            $many = 0;
            $price = 0;

            if(sizeof($maletas)==0){
                $maletas = null;
            }else{
                $many = $many + sizeof($maletas);
                foreach($maletas as $m){
                    $price = $price + $m->carry_price;
                }
            }

            if(sizeof($alojamientos)==0){
                $alojamientos = null;
            }else{
                $many = $many + sizeof($alojamientos);
                foreach($alojamientos as $m){
                    $price = $price + $m->price;
                }
            }

            if(sizeof($traslados)==0){
                $traslados = null;
            }else{
                $many = $many + sizeof($traslados);
                foreach($traslados as $m){
                    $price = $price + $m->price;
                }
            }

            if(sizeof($provisiones)==0){
                $provisiones = null;
            }else{
                $many = $many + sizeof($provisiones);
                foreach($provisiones as $m){
                    $price = $price + $m->price;
                }
            }

            if(sizeof($compras)==0){
                $compras = null;
            }else{
                $many = $many + sizeof($compras);
                foreach($compras as $m){
                    $price = $price + ($m->price * $m->quantity);
                }
            }

            if(sizeof($pasajes)==0){
                $pasajes = null;
            }else{
                $many = $many + sizeof($pasajes);
                foreach($pasajes as $m){

                    $m->payment_current = 0;
                    $m->real_price = $m->price;
                    $m->voucher = 0;

                    if($m->payment_type==1){
                        $resta = $m->price - $m->payment_use_price;
                        if($resta<0){
                            $m->price = 0;
                        }else{
                            $m->price = $resta;
                        }
                    }
                    if($m->payment_type==2){
                       $index =  array_search($m->id, $ticketgroup);
                       if($m->price<=$ticketvalue[$index]){
                            $m->payment_current = $ticketvalue[$index];
                            $ticketvalue[$index] = ($ticketvalue[$index] - $m->price);
                            $m->price = 0;
                            $m->voucher = $voucheruso[$index];
                       }else{
                           if($ticketvalue[$index]>0){

                            
                            $m->payment_current = $m->price - $ticketvalue[$index];
                            $m->price = $m->price - $ticketvalue[$index];
                            $ticketvalue[$index] = $m->price - $ticketvalue[$index];
                            $m->voucher = $voucheruso[$index];
                            
                           }else{
                            $m->payment_current = 0;
                            $ticketvalue[$index] = 0;
                            $m->voucher = $voucheruso[$index];
                           }
                        
                       
                       }
                    }
                    $price = $price + $m->price;
                }
            }

            if(sizeof($extra)==0){
                $extra = null;
            }else{
                $many = $many + sizeof($extra);
                foreach($extra as $m){
                    $price = $price + $m->dearness;
                }
            }

            if(sizeof($multas)==0){
                $multas = null;
            }else{
                $many = $many + sizeof($multas);
                /*foreach($multas as $m){
                    $price = $price + $m->penalty;
                }*/

                foreach($multas as $m){

                    $m->payment_current = 0;
                    $m->real_price = $m->penalty;
                    $m->voucher = 0;

                    if($m->payment_type==2){
                       $index =  array_search($m->ticket_group_id, $ticketgroup);
                       if($m->penalty<=$ticketvalue[$index]){
                            $m->payment_current = $ticketvalue[$index];
                            $ticketvalue[$index] = ($ticketvalue[$index] - $m->penalty);
                            $m->penalty = 0;
                            $m->voucher = $voucheruso[$index];
                       }else{
                          if($ticketvalue[$index]>0){
                            $m->payment_current = $m->penalty - $ticketvalue[$index];
                            $m->price = $m->penalty - $ticketvalue[$index];
                            $ticketvalue[$index] = $m->penalty - $ticketvalue[$index];
                            $m->voucher = $voucheruso[$index];
                            
                           }else{
                            $m->payment_current = 0;
                            $ticketvalue[$index] = 0;
                            $m->voucher = $voucheruso[$index];
                           }
                        
                         $m->payment_current = 0;
                         $ticketvalue[$index] = 0;
                       }
                    }
                    $price = $price + $m->penalty;
                }
            }
               
            $fa->accommodations = $alojamientos;
            $fa->transfers = $traslados;
            $fa->carry = $maletas;
            $fa->provisions = $provisiones;
            $fa->purchases = $compras;
            $fa->tickets = $pasajes; 
            $fa->extra = $extra; 
            $fa->penaltys = $multas;
            
            $fa->supplier_data = $proveedor;
            $fa->quantity =$many;
            $fa->price = $price;    

            $fa->gru = $ticketgroup;
            $fa->grv = $ticketvalue;
            
            return $this->sendResponse($fa
            ,
            'detalle de factura'
        );
        
    }

    public function closeQuotation(Request $r){
        DB::table("quotations")->where("id",$r->id)->update(['closed'=>1]);
    }

    public function openQuotation(Request $r){
        DB::table("quotations")->where("id",$r->id)->update(['closed'=>0]);
    }


    public function sendsms2(Request $r){
        return $r;
        
       // NotifyWorker::dispatch($r->id);

       $ticketId = $r->id;
       $data = DB::table('ticket_groups')
        ->join('quotations', 'quotations.id', '=', 'ticket_groups.quotation_id')
        ->join('crews', 'crews.id', '=', 'quotations.crew_id')
        ->join('crew_worker',"crew_worker.crew_id","=","crews.id")
        ->join('workers', 'workers.id', '=', 'crew_worker.worker_id')
        ->select(
            'workers.phone_number',
            'workers.name',
            'workers.last_name',
            'crews.upload_date',
            'crews.observations',
            'crews.download_date',
            'crew_worker.id as crew_worker_id',
            'crew_worker.lodging',
            'crew_worker.relocation',
            'ticket_groups.flight_date',
            'ticket_groups.code',
            'crew_worker.worker_id',
            'crews.id',
            'ticket_groups.flight_stretch_id'
        )
        ->where('ticket_groups.id',$ticketId)
        ->first();

     
      //  return response()->json($data);

    if($data->flight_stretch_id==1){
                    //ida
                

                // obtiene jefe de flota
                $f = DB::table('users')
                    ->select('name', 'last_name')
                    ->where('position_id', function ($query) {
                        // TODO: pasar a types
                        return $query->select('id')->from('positions')->where('name', 'Administrador');
                    })
                    ->first();

                // configuración de Twilio
                $account_sid = getenv("TWILIO_SID");
                $auth_token = getenv("TWILIO_AUTH_TOKEN");
                $twilio_number = getenv("TWILIO_NUMBER");

                $client = new Client($account_sid, $auth_token);
                $debugNumber = env('DEBUG_NUMBER', null);

                $observation = $this->cleanString($data->observations);

                // mensaje a enviar
                $text = "Hola {$data->name} {$data->last_name}.\n";
                $text .= "Debe presentarse el dia {$data->upload_date} {$observation}, su vuelo es el dia {$data->flight_date},";
                if($data->code<>null){
                    $text .= " su codigo de pasaje es {$data->code}.\n";
                }
                

                // en caso de requerir traslado...
              //  if ($data->relocation) {
                    $relocation = DB::table("transfers")
                    ->join("worker_transfer","worker_transfer.transfer_id","=","transfers.id")
                    ->join("suppliers","suppliers.id","=","transfers.supplier_id")
                    ->where('transfers.crew_id', $data->id)
                    ->where('worker_transfer.worker_id', $data->worker_id)
                    ->where('transfers.journey_type', 1)
                    ->select("suppliers.name","transfers.upload_date","transfers.entry_text")
                    ->first();
                    if($relocation){
                        $text .= "Tiene un traslado el dia {$relocation->upload_date} en {$relocation->entry_text}\n";
                    }
                    
              //  }

                // en caso de requerir alojamiento...
              //  if ($data->lodging) {
                    $lodging =  DB::table("accommodations")
                    ->join("accommodation_worker","accommodation_worker.accommodation_id","=","accommodations.id")
                    ->join("suppliers","suppliers.id","=","accommodations.supplier_id")
                    ->where('accommodations.crew_id', $data->id)
                    ->where('accommodation_worker.worker_id', $data->worker_id)
                    ->where('accommodations.journey_type', 1)
                    ->select("suppliers.name","accommodations.entry_date","accommodations.exit_date")
                    ->first();
                    if($lodging){  
                        $nombre = $this->cleanString($lodging->name);
                        $text .= "Su alojamiento sera en {$nombre} el dia {$lodging->entry_date}.\n";
                    }
                    
               // }

               
                $text .= "Favor confirmar su asistencia con Ignacio Duran via Whatsapp.";

                $client->messages->create(
                // telefono de quien recibe
                // $debugNumber ? : "+56{$data->phone_number}",
                $debugNumber ? : "+56991560195",
                    [
                        'from' => $twilio_number,
                        'body' => $text
                    ]
                );

                DB::table("crew_worker")->where("id",$data->crew_worker_id)->update(['sms_go'=>1]);

    }else{
        //vuelta


                            // obtiene jefe de flota
                            $f = DB::table('users')
                            ->select('name', 'last_name')
                            ->where('position_id', function ($query) {
                                // TODO: pasar a types
                                return $query->select('id')->from('positions')->where('name', 'Administrador');
                            })
                            ->first();

                        // configuración de Twilio
                        $account_sid = getenv("TWILIO_SID");
                        $auth_token = getenv("TWILIO_AUTH_TOKEN");
                        $twilio_number = getenv("TWILIO_NUMBER");

                        $client = new Client($account_sid, $auth_token);
                        $debugNumber = env('DEBUG_NUMBER', null);

                        $observation = $this->cleanString($data->observations);

                        // mensaje a enviar
                        $text = "Hola {$data->name} {$data->last_name}.\n";
                        $text .= "Su fecha de bajada es el dia {$data->download_date}, su vuelo es el dia {$data->flight_date},";
                        if($data->code<>null){
                            $text .= " su codigo de pasaje es {$data->code}.\n";
                        }
                        

                        // en caso de requerir traslado...
                        //if ($data->relocation) {
                            $relocation = DB::table("transfers")
                            ->join("worker_transfer","worker_transfer.transfer_id","=","transfers.id")
                            ->join("suppliers","suppliers.id","=","transfers.supplier_id")
                            ->where('transfers.crew_id', $data->id)
                            ->where('worker_transfer.worker_id', $data->worker_id)
                            ->where('transfers.journey_type', 2)
                            ->select("suppliers.name","transfers.upload_date","transfers.entry_text")
                            ->first();
                            
                            if($relocation){
                                $text .= "Tiene un traslado el dia {$relocation->upload_date} en {$relocation->entry_text}\n";
                            }
                           
                        //}

                        // en caso de requerir alojamiento...
                        //if ($data->lodging) {
                            $lodging =  DB::table("accommodations")
                            ->join("accommodation_worker","accommodation_worker.accommodation_id","=","accommodations.id")
                            ->join("suppliers","suppliers.id","=","accommodations.supplier_id")
                            ->where('accommodations.crew_id', $data->id)
                            ->where('accommodation_worker.worker_id', $data->worker_id)
                            ->where('accommodations.journey_type', 2)
                            ->select("suppliers.name","accommodations.entry_date","accommodations.exit_date")
                            ->first();
                            if($lodging){
                                $nombre = $this->cleanString($lodging->name);
                                $text .= "Su alojamiento sera en {$nombre} entre {$lodging->entry_date} y {$lodging->exit_date}.\n";
                            }
                           
                        //}

                        //$text .= "Favor confirmar su asistencia con el jefe de flota {$f->name} {$f->last_name} via Whatsapp.";

                       

                        $client->messages->create(
                        // telefono de quien recibe
                        // $debugNumber ? : "+56{$data->phone_number}",
                        $debugNumber ? : "+56991560195",
                            [
                                'from' => $twilio_number,
                                'body' => $text
                            ]
                        );


                        DB::table("crew_worker")->where("id",$data->crew_worker_id)->update(['sms_back'=>1]);
                      

        } 
    }

    public function cleanString($text) {
        $utf8 = array(
            '/[áàâãªä]/u'   =>   'a',
            '/[ÁÀÂÃÄ]/u'    =>   'A',
            '/[ÍÌÎÏ]/u'     =>   'I',
            '/[íìîï]/u'     =>   'i',
            '/[éèêë]/u'     =>   'e',
            '/[ÉÈÊË]/u'     =>   'E',
            '/[óòôõºö]/u'   =>   'o',
            '/[ÓÒÔÕÖ]/u'    =>   'O',
            '/[úùûü]/u'     =>   'u',
            '/[ÚÙÛÜ]/u'     =>   'U',
            '/ç/'           =>   'c',
            '/Ç/'           =>   'C',
        );
        return preg_replace(array_keys($utf8), array_values($utf8), $text);
    }


    public function sendsms(Request $r){
   
        
       // NotifyWorker::dispatch($r->id);

       $q = DB::table("quotations")->where("id",$r->crew_id)->first();
        $crew_id = $q->crew_id;
       if($r->type==1){
                    $crew = DB::table("crews")->where("id",$crew_id)->first();
                    $worker = DB::table("workers")->where("id",$r->worker)->first();
                    $ticket = DB::table("ticket_groups")->join("worker_ticket","worker_ticket.ticket_group_id","=","ticket_groups.id")->where("worker_ticket.worker_id","=",$worker->id)->where("quotation_id",$q->id)->where("flight_stretch_id",1)->first();
                        // obtiene jefe de flota
                        $f = DB::table('users')
                        ->select('name', 'last_name')
                        ->where('position_id', function ($query) {
                            // TODO: pasar a types
                            return $query->select('id')->from('positions')->where('name', 'Administrador');
                        })
                        ->first();

                    // configuración de Twilio
                    $account_sid = getenv("TWILIO_SID");
                    $auth_token = getenv("TWILIO_AUTH_TOKEN");
                    $twilio_number = getenv("TWILIO_NUMBER");

                    $client = new Client($account_sid, $auth_token);
                    $debugNumber = env('DEBUG_NUMBER', null);

                    $observation = $this->cleanString($crew->observations);

                    // mensaje a enviar
                    $text = "Hola {$worker->name} {$worker->last_name}.\n";
                    $text .= "Debe presentarse el dia {$crew->upload_date} {$observation},";
                    
                    if($ticket){
                        $text .= " su vuelo es el dia {$ticket->flight_date},";

                        if($ticket->code<>null){
                            $text .= " su codigo de pasaje es {$ticket->code}.\n";
                        }
                    }

                        $relocation = DB::table("transfers")
                        ->join("worker_transfer","worker_transfer.transfer_id","=","transfers.id")
                        ->join("suppliers","suppliers.id","=","transfers.supplier_id")
                        ->where('transfers.crew_id', $crew->id)
                        ->where('worker_transfer.worker_id', $worker->id)
                        ->where('transfers.journey_type', 1)
                        ->select("suppliers.name","transfers.upload_date","transfers.entry_text")
                        ->first();

   
                        if($relocation){
                            $text .= "Tiene un traslado el dia {$relocation->upload_date} en {$relocation->entry_text},\n";
                        }
                        
                        $lodging =  DB::table("accommodations")
                        ->join("accommodation_worker","accommodation_worker.accommodation_id","=","accommodations.id")
                        ->join("suppliers","suppliers.id","=","accommodations.supplier_id")
                        ->where('accommodations.crew_id', $crew->id)
                        ->where('accommodation_worker.worker_id', $worker->id)
                        ->where('accommodations.journey_type', 1)
                        ->select("suppliers.name","accommodations.entry_date","accommodations.exit_date")
                        ->first();
                        if($lodging){  
                            $nombre = $this->cleanString($lodging->name);
                            $text .= "Su alojamiento sera en {$nombre} el dia {$lodging->entry_date}.\n";
                        }


                    $text .= "Favor confirmar su asistencia con Ignacio Duran via Whatsapp.";
                        
                  //  return $text;
                    $client->messages->create(
                    // telefono de quien recibe
                     $debugNumber ? : "+56".$worker->phone_number,
                    //$debugNumber ? : "+56994936519",
                        [
                            'from' => $twilio_number,
                            'body' => $text
                        ]
                    );

                    DB::table("crew_worker")->where("crew_id",$crew->id)->where("worker_id",$worker->id)->update(['sms_go'=>1]);
       }
       if($r->type==2){
        $crew = DB::table("crews")->where("id",$crew_id)->first();
        $worker = DB::table("workers")->where("id",$r->worker)->first();
        $ticket = DB::table("ticket_groups")->join("worker_ticket","worker_ticket.ticket_group_id","=","ticket_groups.id")->where("worker_ticket.worker_id","=",$worker->id)->where("quotation_id",$q->id)->where("flight_stretch_id",2)->first();
            // obtiene jefe de flota
            $f = DB::table('users')
            ->select('name', 'last_name')
            ->where('position_id', function ($query) {
                // TODO: pasar a types
                return $query->select('id')->from('positions')->where('name', 'Administrador');
            })
            ->first();

        // configuración de Twilio
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_number = getenv("TWILIO_NUMBER");

        $client = new Client($account_sid, $auth_token);
        $debugNumber = env('DEBUG_NUMBER', null);

        $observation = $this->cleanString($crew->observations);

        // mensaje a enviar
        $text = "Hola {$worker->name} {$worker->last_name}.\n";
   // $text .= "Su bajada es el dia {$crew->upload_date} {$observation},";
        $text .= "Su bajada es el dia {$crew->upload_date},";
        
        if($ticket){
            $text .= " su vuelo es el dia {$ticket->flight_date},";

            if($ticket->code<>null){
                $text .= " su codigo de pasaje es {$ticket->code}.\n";
            }
        }

            $relocation = DB::table("transfers")
            ->join("worker_transfer","worker_transfer.transfer_id","=","transfers.id")
            ->join("suppliers","suppliers.id","=","transfers.supplier_id")
            ->where('transfers.crew_id', $crew->id)
            ->where('worker_transfer.worker_id', $worker->id)
            ->where('transfers.journey_type', 2)
            ->select("suppliers.name","transfers.upload_date","transfers.entry_text")
            ->first();


            if($relocation){
                $text .= " Tiene un traslado el dia {$relocation->upload_date} en {$relocation->entry_text},\n";
            }
            
            $lodging =  DB::table("accommodations")
            ->join("accommodation_worker","accommodation_worker.accommodation_id","=","accommodations.id")
            ->join("suppliers","suppliers.id","=","accommodations.supplier_id")
            ->where('accommodations.crew_id', $crew->id)
            ->where('accommodation_worker.worker_id', $worker->id)
            ->where('accommodations.journey_type', 2)
            ->select("suppliers.name","accommodations.entry_date","accommodations.exit_date")
            ->first();
            if($lodging){  
                $nombre = $this->cleanString($lodging->name);
                $text .= "Su alojamiento sera en {$nombre} el dia {$lodging->entry_date}.\n";
            }


    
       // return $text;
        $client->messages->create(
        // telefono de quien recibe
         $debugNumber ? : "+56".$worker->phone_number,
        //$debugNumber ? : "+56994936519",
            [
                'from' => $twilio_number,
                'body' => $text
            ]
        );

        DB::table("crew_worker")->where("crew_id",$crew->id)->where("worker_id",$worker->id)->update(['sms_back'=>1]);
       }

    
    }

    

  


}
