<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Crew;
use App\Models\Position;
use DB;

/**
 * Class CrewDetailController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class CrewDetailController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de cargos disponibles
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(int $id)
    {
        return $this->sendResponse(Crew::with('client', 'ship', 'status')->findOrFail($id), 'Crew details');
    }

    /**
     * Obtiene los trabajadores de una tripulación
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getWorkers(int $id)
    {
        return $this->sendResponse(Crew::select('id')->with('full_workers')->findOrFail($id), 'Crew workers');
    }

    /**
     * Obtiene los trabajadores filtrados de una tripulación
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getFilteredWorkers(int $id)
    {
        return $this->sendResponse(Crew::select('id')->with('filtered_workers')->findOrFail($id), 'Filtered workers');
    }

    /**
     * Obtiene las cotizaciones de la tripulación
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getQuotations(int $id)
    {
        $dato = DB::table('quotations')->where("crew_id",$id)->orderBy('id','desc')->first();
       
            $pasajes = DB::table("ticket_groups")
            ->join("airlines","airlines.id","=","ticket_groups.airline_id")
            ->join("flight_stretches","flight_stretches.id","=","ticket_groups.flight_stretch_id")
            ->select("ticket_groups.quotation_id","ticket_groups.payment_type","ticket_groups.invoice_user_id","ticket_groups.invoice_date","ticket_groups.invoice_id","ticket_groups.airline_id","ticket_groups.code","ticket_groups.ticket_type","ticket_groups.passengers","ticket_groups.id","ticket_groups.plus","ticket_groups.id","ticket_groups.arrival_airport_id","ticket_groups.departure_airport_id","ticket_groups.flight_date","ticket_groups.departure_time","ticket_groups.arrival_time","airlines.name as airline","flight_stretches.name as stretch","ticket_groups.extra_price","ticket_groups.price")
            ->where("ticket_groups.crew_id",$id)
            ->orderBy("ticket_groups.id","desc")
            ->get();
    
            foreach($pasajes as $t){
    
                $supplier = DB::table("suppliers")->where("code",$t->airline_id)->where("supplier_type_id",5)->first();
                $t->supplier_id = $supplier->id;
    
                $airport1 = DB::table("airports")->where("id",$t->departure_airport_id)->first();
                $airport2 = DB::table("airports")->where("id",$t->arrival_airport_id)->first();
                if($airport1){
                    $t->airport1 = $airport1->city;
                }
                if($airport2){
                    $t->airport2 = $airport2->city;
                }
                $trip = DB::table("crew_tickets")->where("ticket_group_id",$t->id)->first();
                if($trip){
                    $crew = DB::table("crews")
                    ->join("ships","ships.id","=","crews.ship_id")
                    ->join("clients","clients.id","=","crews.client_id")
                    ->select("clients.name as client","ships.name as ship","crews.upload_date")
                    ->where("crews.id",$trip->crew_id)
                    ->first();
                    if($crew){
                        $t->upload_date = $crew->upload_date;
                        $t->crew = $crew->ship." / ".$crew->client.'('.$crew->upload_date.')';
                    }else{
                        $t->crew = '-';
                        $t->upload_date = '';
                    }
                    
                }else{
                    $t->crew = '-';
                    $t->upload_date = '';
                }
                if($t->code==null || $t->code==''){
                    $t->code = '-';
                }
            }
            //$c->tickets = $pasajes;
        

                $data['tickets'] = $pasajes;
    
                $transfers = DB::table("transfers")
                    ->join("suppliers","suppliers.id","=","transfers.supplier_id")
                    ->where('transfers.crew_id', $id)
                    ->select("suppliers.name","transfers.upload_date","transfers.entry_text","transfers.price","transfers.id","transfers.exit_text")
                    ->get();

                $accommodations =  DB::table("accommodations")
                    ->join("suppliers","suppliers.id","=","accommodations.supplier_id")
                    ->where('accommodations.crew_id', $id)
                    ->select("suppliers.name","accommodations.entry_date","accommodations.exit_date","accommodations.price","accommodations.id")
                    ->get();
     
                
                foreach($accommodations as $a){
                    $cuantos = DB::table("accommodation_worker")->where("accommodation_id",$a->id)->count();
                    $a->cuantos = $cuantos;
                }

                foreach($transfers as $a){
                    $cuantos = DB::table("worker_transfer")->where("transfer_id",$a->id)->count();
                    $a->cuantos = $cuantos;
                }
       
                $data['alojamientos'] = $accommodations;
                $data['traslados'] = $transfers;

                $workerTransfers = DB::table("crew_worker")->join("workers","workers.id","=","crew_worker.worker_id")->where("crew_worker.crew_id",$id)->where("crew_worker.filtered",0)->select("workers.id","workers.name","workers.last_name","workers.rut",'crew_worker.lodging','crew_worker.lodging_food','crew_worker.relocation','crew_worker.entry_date','crew_worker.with_emergency','crew_worker.filtered')->get();

                foreach($workerTransfers as $w){
                    $rol = "";
                    $roles =  DB::table("roles")->join("role_worker","role_worker.role_id","=","roles.id")->where("role_worker.worker_id",$w->id)->select("roles.name")->get(); 
                    foreach($roles as $r){
                     $rol = $rol.",".$r->name;
                    }
                    $w->roles = substr($rol, 1);
                }
                
                $data['tripulantes'] = $workerTransfers;

                if($dato){
                    $data['cotizacion'] = $dato->id;
                }else{
                    $data['cotizacion'] = null;
                }
                
                
        return $this->sendResponse($data, 'Crew quotations');
    }

    /**
     * Obtiene información de la solicitud de abastecimiento
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getSupplyRequest(int $id)
    {
        return $this->sendResponse(Crew::select('id')->with('workers', 'supply_request')->findOrFail($id), 'Supply request');
    }

    /**
     * Obtiene las compras asociadas a una tripulación
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getPurchases(int $id)
    {
        return $this->sendResponse(Crew::select('id')->with('purchases')->findOrFail($id), 'Purchases list');
    }

    /**
     * Obtiene los botones de emergencia activados en una tripulación
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getEmergencyButtons(int $id)
    {
        return $this->sendResponse(Crew::select('id')->with('emergency_buttons')->findOrFail($id), 'Emergencies list');
    }
}
