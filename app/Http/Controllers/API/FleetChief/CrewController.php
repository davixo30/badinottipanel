<?php

namespace App\Http\Controllers\API\FleetChief;

use App\Http\Controllers\API\V1\BaseController;
use App\Jobs\NewClientCaseFilter;
use App\Models\Crew;
use App\Models\CrewStatus;
use App\Models\CrewWorker;
use App\Models\EmergencyButton;
use App\Models\Worker;
use App\Models\WorkerAttribute;
use App\Types\CrewStatusesTypes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
/**
 * Class CrewController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\FleetChief
 */
class CrewController extends BaseController
{
    /**
     * Create a new controller instance.
     * TODO: verificar si esto se debería sacar o cambiar
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el listado de tripulaciones
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return $this->sendResponse(Crew::latest()->with(
            'client:id,name',
            'ship:id,name,supervisor_id',
            'status'
        )->get(), 'Crews list');
    }

    public function updateuploaddate(Request $r) {
        $newdate = Carbon::parse($r->new_upload_date);
        $newdatedownload = Carbon::parse($r->new_download_date);
        $newdatedownloadreal = Carbon::parse($r->new_real_download_date);
        $crew = DB::table("crews")->where("id",$r->crew_id)->first();
        DB::insert('INSERT into upload_date_changes(reason,crew_id,original_upload_date,new_upload_date,user_id, original_download_date, new_download_date, original_real_download_date, new_real_download_date, original_extra_days, new_extra_days) values(?,?,?,?,?,?,?,?,?,?,?)', array($r->reason,$r->crew_id,$crew->upload_date,$newdate,Auth::id(),$crew->download_date,$newdatedownload, $crew->real_download_date, $newdatedownloadreal,$crew->extra_days, $r->new_extra_days ));
        DB::update('UPDATE crews set upload_date=?, download_date=?, real_download_date=?, extra_days=? where id=?', array($newdate, $newdatedownload, $newdatedownloadreal, $r->new_extra_days, $r->crew_id));
        $crewupdated = DB::table("crews")->where("id",$r->crew_id)->first();
        return $this->sendResponse($crewupdated, 'Fecha presentacion actualizada correctamente');
    }

    /**
     * Almacena una nueva tripulación
     *
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'client_id' => 'required|exists:clients,id',
            'ship_id' => 'required|exists:ships,id',
            'upload_date' => 'required|date',
            'real_download_date' => 'required|date',
            'observations' => 'present|nullable|max:80'
        ]);

        $real = Carbon::parse($request->real_download_date);
        $protocolar = Carbon::create($request->get('upload_date'))->addDays(24);
        $diff = $real->diffInDays($protocolar);

        if($real < $protocolar){
            $diff = "-".$diff;
        }

        $data = array_merge($request->only('client_id', 'ship_id', 'upload_date', 'real_download_date', 'observations'), [
            'status_id' => CrewStatus::getDefaultStatusId(),
            'download_date' => Carbon::create($request->get('upload_date'))->addDays(24),
            'extra_days' => $diff
        ]);

        // TODO: falta validar que el cliente tenga inducciones

        return $this->sendResponse(Crew::create($data), 'Tripulación creada correctamente');
    }

    /**
     * Obtiene los datos de una tripulación para su edición
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getForEdition(int $id)
    {
        $crew = Crew::with(
            'client:id,name',
            'ship:id,name,capacity',
            'status',
            'full_workers'
        )->findOrFail($id);
        
       /* foreach ($crew->full_workers as $key => $worker) {
            if($worker->worker==null){
                $crew->full_workers->forget($key);
            }
        }*/
        
        //return $this->sendResponse($crew, 'Trabajadores encontrados');
        foreach ($crew->full_workers as $key => $worker) {

           // if($worker->worker==null){
               // $theworker = Worker::find($worker->worker_id);
                //$theworker = DB::table("workers")->where("id",$worker->worker_id)->first();

               // $worker->newworker = $theworker;
               // $worker->worker = DB::table("workers")->where("id",$worker->worker_id)->first();
           // }
                $worker->status = Worker::getWorkerEvaluation($worker->worker_id,$crew->client_id);

                $last = DB::table('crews')
                ->leftJoin('crew_worker', 'crew_worker.crew_id', '=', 'crews.id')
                ->leftJoin('ships', 'crews.ship_id', '=', 'ships.id')
                ->where('crew_worker.worker_id', $worker->worker_id)
                ->where('crews.status_id', CrewStatus::getInCalendarStatusId())
                ->select('crews.id', 'ships.name','upload_date','real_download_date')
                ->orderBy('crews.upload_date')
                ->first();

            
                $worker->last_crew = $last;
         

           
            
        }


        // renombra el atributo para evitar conflictos en front
        $crew->workers = $crew->full_workers;
        unset($crew->full_workers);

        return $this->sendResponse($crew, 'Trabajadores encontrados');
    }

    /**
     * Obtiene el listado de trabajadores disponibles según la tripulación que se está generando
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getAvailableWorkers(int $id, Request $request)
    {
        $client = Crew::where("id",$id)->first();
        $search = $request->query('search');
        $workers = Worker::whereIn("attribute_id",array(2,10,1,20))->when($search, function ($query, $search) {
           /* return $query->where('name', 'like', "%$search%")
               // ->orWhere('last_name', 'like', "%$search%");
                ->orWhere('last_name', 'like', "%$search%")
                ->orWhere('rut', 'like', "%$search%")
                ->orWhere('phone_number', 'like', "%$search%")
                ->orWhere('code', 'like', "%$search%");*/
        })->inCrew($id)->withoutErrors($client->client_id)->with('roles')->withCount('headline')->orderBy('name','asc')->get();

        foreach ($workers as $key => $worker) {
            $worker->attribute = "(S/A)";  
            if($worker->attribute_id==2){
                $worker->attribute = "(Des)";
                $worker->order = 2;
            }
            if($worker->attribute_id==10){
                $worker->attribute = "(Dis)";
                $worker->order = 1;
            }
            if($worker->attribute_id==1){
                $worker->attribute = "(Emb)";
                $worker->order = 3;
            }
            if($worker->attribute_id==20){
                $worker->attribute = "(Vac)";
                $worker->order = 4;
            }
            
            $status = Worker::getWorkerEvaluation($worker->id,$client->client_id);
            $last_crew = Crew::getWorkerLastCrew($worker->id);
            $added = Crew::getWorkerLastCrewAddedStatus($worker->id);
            $worker->status = $status;
            $worker->last_crew = $last_crew;
            
            if($status['status']=="Error"){
                $workers->forget($key);
            }else{
                if($added){
                    $fecha_bajada = date('Y-m-d', strtotime('-1 day', strtotime($added->real_download_date)));
                    $fecha_subida = date('Y-m-d', strtotime($added->upload_date));
                    $fecha_actual = date('Y-m-d', strtotime($client->upload_date));
                    if((($fecha_actual>=$fecha_subida) && ($fecha_actual<=$fecha_bajada)) && $added->filtered==0 && $added->with_emergency==0 ){
                        $workers->forget($key);
                    }
                }
            }
        }

        $ids = [ 1,2,3,4];
        $workers = $workers->sortBy(function($model) use ($ids) {
            return array_search($model->order, $ids);
        });
        //$workers = $workers->paginate(5);

        return $this->sendResponse($workers, 'Listado de trabajadores disponibles');
    }

    /**
     * Obtiene trabajadores disponibles para hacer reemplazos
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getReplacementWorkers(Request $request)
    {
        $crew = DB::table("crews")->where("id",$request->crew_id)->first();
        $search = $request->query('search');
        $workers = Worker::when($search, function ($query, $search) {
           /* return $query->where('name', 'like', "%$search%")
                //->orWhere('last_name', 'like', "%$search%");
                ->orWhere('last_name', 'like', "%$search%")
                ->orWhere('rut', 'like', "%$search%")
                ->orWhere('phone_number', 'like', "%$search%")
                ->orWhere('code', 'like', "%$search%");*/
        })->with('roles')->orderBy('name','asc')->get();

        foreach ($workers as $key => $worker) {

            $worker->attribute = "(S/A)";  
            if($worker->attribute_id==2){
                $worker->attribute = "(Des)";
                $worker->order = 2;
            }
            if($worker->attribute_id==10){
                $worker->attribute = "(Dis)";
                $worker->order = 1;
            }
            if($worker->attribute_id==1){
                $worker->attribute = "(Emb)";
                $worker->order = 3;
            }
            if($worker->attribute_id==20){
                $worker->attribute = "(Vac)";
                $worker->order = 4;
            }


            $worker->status = Worker::getWorkerEvaluation($worker->id,$crew->client_id);
            $added = DB::table("crew_worker")->where("worker_id",$worker->id)->where("crew_id",$crew->id)->first();
            if($added){
                $workers->forget($key);
            }
        }

         $ids = [ 1,2,3,4];
        $workers = $workers->sortBy(function($model) use ($ids) {
            return array_search($model->order, $ids);
        });
       // $workers = $workers->paginate(5);
        return $this->sendResponse($workers, 'Listado de trabajadores reemplazo');
    }

   /* public function getReplacementWorkersNoAdded(Request $request)
    {
        $crew = DB::table("crews")->where("id",$request->crew_id)->first();
        $search = $request->query('search');
        $workers = Worker::when($search, function ($query, $search) {
            return $query->where('name', 'like', "%$search%")
                ->orWhere('last_name', 'like', "%$search%");
        })->with('roles')->orderBy('name','asc')->get();

        foreach ($workers as $key => $worker) {
            $worker->status = Worker::getWorkerEvaluation($worker->id,$crew->client_id);
            $added = DB::table("crew_worker")->where("worker_id",$worker->id)->where("crew_id",$request->crew_id)->first();
            if($added){
                $workers->forget($key);
            }
        }

        $workers = $workers->paginate(5);

        return $this->sendResponse($workers, 'Listado de trabajadores');
    }*/

    /**
     * Obtiene el listado de trabajadores en sala de espera
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getWaitingWorkers(int $id, Request $request)
    {
        $client = Crew::where("id",$id)->first();
        $search = $request->query('search');
        $workers = Worker::whereIn("attribute_id",array(2,10,1,20))->when($search, function ($query, $search) {
               /* return $query->where('workers.name', 'like', "%$search%")
                    //->orWhere('workers.last_name', 'like', "%$search%");
                    ->orWhere('workers.last_name', 'like', "%$search%")
                    ->orWhere('workers.rut', 'like', "%$search%")
                    ->orWhere('workers.phone_number', 'like', "%$search%")
                    ->orWhere('workers.code', 'like', "%$search%");*/
            })->inCrew($id)->with('roles')->withCount('headline')->orderBy('name','asc')->get();

        foreach ($workers as $key => $worker) {
            if($worker->attribute_id==2){
                $worker->attribute = "(Des)";
                $worker->order = 2;
            }
            if($worker->attribute_id==10){
                $worker->attribute = "(Dis)";
                $worker->order = 1;
            }
            if($worker->attribute_id==1){
                $worker->attribute = "(Emb)";
                $worker->order = 3;
            }
            if($worker->attribute_id==20){
                $worker->attribute = "(Vac)";
                $worker->order = 4;
            }
            
            $status = Worker::getWorkerEvaluation($worker->id,$client->client_id);
            $last_crew = Crew::getWorkerLastCrew($worker->id);
            $added = Crew::getWorkerLastCrewAddedStatus($worker->id);
            $worker->status = $status;
            $worker->last_crew = $last_crew;

            if($status['status']=="Ok" || $status['status']=="Observaciones"){
                $workers->forget($key);
            }else{
                if($added){
                    $fecha_bajada = date('Y-m-d', strtotime('-1 day', strtotime($added->real_download_date)));
                    $fecha_subida = date('Y-m-d', strtotime($added->upload_date));
                    $fecha_actual = date('Y-m-d', strtotime($client->upload_date));
                    if(($fecha_actual>=$fecha_subida) && ($fecha_actual<=$fecha_bajada) && $added->filtered==0 && $added->with_emergency==0 ){
                        $workers->forget($key);
                    }
                }
            }
            
        }
        $ids = [ 1,2,3,4];
        $workers = $workers->sortBy(function($model) use ($ids) {
            return array_search($model->order, $ids);
        });
       // $workers = $workers->paginate(5);

        return $this->sendResponse($workers, 'Listado de trabajadores en espera');
    }

    /**
     * Asocia trabajadores a una tripulación
     *
     * @param int $id
     * @param Request $request
     *
     * @throws \Exception
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function setWorkers(int $id, Request $request)
    {
        $this->validate($request, [
            'workers' => 'required|array',
            'workers.*' => 'int|exists:workers,id'
        ]);

        Crew::setWorkers($id, $request->get('workers'));

        return $this->sendResponse('Ok', 'Trabajadores guardados');
    }

    /**
     * Elimina un trabajador de una tripulación
     *
     * @param int $id
     * @param int $workerId
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function deleteWorker(int $id, int $workerId)
    {
        // TODO: verificar si esta validación está bien
        $this->validate(new Request(['crew_id' => $id, 'worker_id' => $workerId]), [
            'crew_id' => 'exists:crews,id',
            'worker_id' => 'exists:workers,id'
        ]);

        return $this->sendResponse(
            CrewWorker::where(['crew_id' => $id, 'worker_id' => $workerId])->delete(),
            'Trabajador quitado'
        );
    }

    /**
     * Confirma la tripulación y sus trabajadores
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function confirm(int $id, Request $request)
    {
        $this->validate($request, [
            'lodgings' => 'present',
            'lodgings.*' => 'int|exists:crew_worker,id',
            'lodgingsFood' => 'present',
            'lodgingsFood.*' => 'int|exists:crew_worker,id',
            'relocations' => 'present',
            'relocations.*' => 'int|exists:crew_worker,id',
        ]);

        Crew::confirmCrew($id, $request->get('lodgings'), $request->get('lodgingsFood'), $request->get('relocations'));

        $crewworker = DB::table("crew_worker")
        ->join("workers","crew_worker.worker_id","=","workers.id")
        ->where("crew_id",$id)
        ->where("filtered","0")
        ->where("workers.deleted_at",null)
        ->get();

        $crew = Crew::find($id);
        foreach($crewworker as $c){
            $atributo = $c->attribute_id;
            if($c->attribute_id == 10){
            $atributo = 14;
            }
            if($c->attribute_id == 2){
            $atributo = 15;
            }
            if($c->attribute_id<>10 && $c->attribute_id<>2){
            $atributo = 16;
            }

            $wk = Worker::find($c->worker_id);
            $wk->attribute_id = $atributo;
            $wk->save();

            $w = new WorkerAttribute;
            $w->worker_id = $c->worker_id;
            $w->attribute_id = $atributo;
            $w->date_start = date("Y-m-d");
            $w->date_end = date("Y-m-d",strtotime($crew->upload_date." - 1 days"));
            $w->crew_id = $id;
            $w->save();
        }

        // notifica a supervisor para aplicar filtro caso cliente
        // REMOVIDO SOLICITUD ELIMINACION FILTRO SUPERVISOR
        // NewClientCaseFilter::dispatch($id);

        return $this->sendResponse('Ok', 'Tripulación confirmada');
    }

    /**
     * Elimina una tripulación solo si está en estado borrador
     *
     * @param int $id
     *
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $crew = Crew::with('status')->findOrFail($id);

        // TODO: excepcion debería ir en modelo?
        if ($crew->status->name !== CrewStatusesTypes::$BORRADOR) {
            throw new \Exception("Solo puede eliminar tripulaciones en estado 'Borrador'");
        }

        return $this->sendResponse($crew->delete(), 'Tripulación eliminada');
    }

    /**
     * Obtiene un botón de emergencia según tripulación y crewWorker
     *
     * @param int $crewId
     * @param int $crewWorkerId
     *
     * @return \Illuminate\Http\Response
     */
    public function getEmergencyButton(int $crewId, int $crewWorkerId)
    {
        return $this->sendResponse(
            EmergencyButton::where('emergency_crew_worker_id', $crewWorkerId)->where('crew_id', $crewId)->first(),
            'Emergency button found'
        );
    }

    /**
     * Crea un nuevo botón de emergencia
     *
     * @param int $crewId
     * @param int $crewWorkerId
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function setEmergencyButton(Request $request)
    {
        $this->validate($request, [
            'reason' => 'required|string',
            'emergency_crew_worker_id' => 'required|exists:crew_worker,id',
            'replacement_worker_id' => 'nullable|exists:workers,id',
            'services' => 'present',
            'services.lodging' => 'required|bool',
            'services.lodging_food' => 'required|bool',
            'services.relocation' => 'required|bool',
            'with_ticket' => 'required|bool',
        ]);

        $button = EmergencyButton::create($request->only([
            'reason', 'emergency_crew_worker_id', 'replacement_worker_id', 'services','entry_date','with_ticket'
        ]));

        // TODO emite eventos

        return $this->sendResponse($button, 'Emergency button created');
    }


    public function setEmergencyButtonAddWorker(Request $request)
    {

       // return $request;
        $this->validate($request, [
            'reason' => 'required|string',
            'replacement_worker_id' => 'nullable|exists:workers,id',
            'services' => 'present',
            'services.lodging' => 'required|bool',
            'services.lodging_food' => 'required|bool',
            'services.relocation' => 'required|bool',
            'crew_id' => 'required',
            'entry_date' => 'required',
            'with_ticket' => 'required|bool',
        ]);

        $button = EmergencyButton::createAddWorker($request->only([
            'reason', 'replacement_worker_id', 'services','crew_id','entry_date','with_ticket'
        ]));

        // TODO emite eventos

        return $this->sendResponse($button, 'Emergency button created');
    }
}
