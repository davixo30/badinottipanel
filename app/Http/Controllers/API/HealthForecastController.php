<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\HealthForecast;
use Illuminate\Http\Response;

/**
 * Class HealthForecastController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class HealthForecastController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el listado de afps
     *
     * @return Response
     */
    public function __invoke()
    {
        return $this->sendResponse(HealthForecast::all(), 'AFP list');
    }
}
