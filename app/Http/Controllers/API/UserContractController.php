<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\UserContract;
use Illuminate\Http\Request;

/**
 * Class UserContractController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class UserContractController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el contrato de un usuario según su ID
     *
     * @param int $userId
     *
     * @return \Illuminate\Http\Response
     */
    public function get(int $userId)
    {
        return $this->sendResponse(UserContract::find($userId), 'Contrato de usuario encontrado');
    }

    /**
     * Guarda el contrato de un usuario
     * TODO: es complicado por que utilizar una suerte de 'upsert', una vez bien modelado el contrato, corregir
     *
     * @param int $userId
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function set(int $userId, Request $request)
    {
        $this->validate(new Request(['id' => $userId]), ['id' => 'exists:users,id']);
        $this->validate($request, [
            'date' => 'required|date',
            'afp_id' => 'required|exists:afps,id',
            'health_forecast_id' => 'required|exists:health_forecasts,id',
            'contract_type_id' => 'required|exists:contract_types,id',
            'identity_card_copy' => 'required|file|mimes:pdf|max:10240'
        ]);

        // almacena los archivos
        $fileName = time() . '.pdf';
        $request->file('identity_card_copy')->storeAs('public/user-identity-card-copies', $fileName);

        // guarda el contrato del usuario
        $contract = UserContract::find($userId);
        $data = array_merge($request->only(
            ['date', 'salary', 'afp_id', 'health_forecast_id', 'contract_type_id']
        ), ['user_id' => $userId, 'identity_card_copy' => $fileName]);

        if ($contract) {
            $contract->fill($data);
        } else {
            $contract = new UserContract($data);
        }

        $contract->save();

        return $this->sendResponse($contract, 'Contrato de usuario guardado');
    }
}
