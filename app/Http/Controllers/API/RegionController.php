<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Region;
use Illuminate\Http\Request;

/**
 * Class RegionController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class RegionController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de regiones disponibles
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->sendResponse(Region::all(), 'Regions list');
    }

    /**
     * Obtiene región con sus comunas
     *
     * @param int $id
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function getWithCommunes(int $id)
    {
        $this->validate(new Request(['id' => $id]), ['id' => 'exists:regions,id']);

        return $this->sendResponse(Region::with('communes')->findOrFail($id), 'Region with communes');
    }
}
