<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Client;

/**
 * Class ClientController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class ClientController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de clientes
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->sendResponse(Client::all(), 'Clients list');
    }
}
