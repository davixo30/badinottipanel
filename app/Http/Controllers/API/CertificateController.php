<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Certificate;
use Illuminate\Http\Request;

/**
 * Class CertificateController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class CertificateController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de certificados disponibles para una embarcación
     *
     * @param int $id ID embarcación
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function __invoke(int $id)
    {
        $this->validate(new Request(['ship_id' => $id]), ['ship_id' => 'required|exists:ships,id']);

        return $this->sendResponse(
            Certificate::getByShipId($id),
            'Certificates list'
        );
    }
}
