<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Role;
use Illuminate\Http\Response;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use DateTime;
use DB;
use App\Models\Worker;
use App\Models\Crew;
use PHPExcel_Worksheet_MemoryDrawing;


class ReportController extends BaseController
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    function get_nombre_mes($fecha){
        $fechats = strtotime($fecha);
        switch (date('m', $fechats)){
            case 1: return "Enero"; break;
            case 2: return "Febrero"; break;
            case 3: return "Marzo"; break;
            case 4: return "Abril"; break;
            case 5: return "Mayo"; break;
            case 6: return "Junio"; break;
            case 7: return "Julio"; break;
            case 8: return "Agosto"; break;
            case 9: return "Septiembre"; break;
            case 10: return "Octubre"; break;
            case 11: return "Noviembre"; break;
            case 12: return "Diciembre"; break;
        }
     }

    public function exportReport($report_type,$d1,$d2){

        if($report_type==0){
            self::generateExpired();
        }
        if($report_type==1){
            self::generateExpireNextMonth();
        }
        if($report_type==2){
            self::generateAttributes();
        }   

        if($report_type==3){
            self::generateAntiguedad();
        }  
        
        if($report_type==4){
            self::generateDisponibles();
        } 
        
        if($report_type==5){
            self::generateHistorialpasajes($d1,$d2);
        } 

        if($report_type==6){
            self::generateLicenciaPermiso();
        } 

        if($report_type==7){
            self::generateRelevo($d1);
        }  

        if($report_type==8){
            self::revista($d1);
        }  

    
    }


    public function revista($anio){
        $dt1 = new DateTime();
        $today = $dt1->format("m");

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $plomo = [
            'fill' => [
                'fillType' =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => 'C6C3BF',
                ]
            ],
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        ];

        $rojo = [
            'font' => [
                'color' => array('rgb' => 'FFFFFF'),
            ],
            'fill' => [
                'fillType' =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => 'FF0000',
                ]
            ],
        ];

        $azul = [
            'font' => [
                'color' => array('rgb' => 'FFFFFF'),
            ],
            'fill' => [
                'fillType' =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => '0000CC',
                ]
            ],
        ];

        $botes = DB::connection('eskuadconn')->table('cicles')->join('activity_task','cicles.task_id','=','activity_task.task_id')->join('activities','activities.id','=','activity_task.activity_id')->join('sub_reports_values','sub_reports_values.activity_id','=','activities.id')->join('sub_reports','sub_reports.id','=','sub_reports_values.subreport_id')->where('sub_reports.code','857w4tgy4ax')->where('cicles.date','like',$anio.'%')->select('cicles.task_id','cicles.number','cicles.user_id','cicles.date')->groupBy('cicles.task_id','cicles.number')->orderBy('cicles.id','asc')->get();

        $boat = 0;
        

        foreach($botes as $b){

            $lafecha = self::get_nombre_mes(date("Y-m",strtotime($b->date)));

            $barco = self::getValueSubreport($b->task_id,$b->number,'1','857w4tgy4ax');
            $fin = 11;
            $inicio = 0;
            if($boat == 0){
            }else{
                $spreadsheet->createSheet();
                $spreadsheet->setActiveSheetIndex($boat);
                $sheet = $spreadsheet->getActiveSheet();
            }

            $sheet->setTitle($barco.' - '.$lafecha.' '.$anio);

            $url = public_path().'/images/badinotimarine.jpg';

            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setPath($url); // put your path and image here
            $drawing->setWidthAndHeight(300, 120);
            $drawing->setCoordinates('B1');
            $drawing->setWorksheet($sheet);

            $sheet->setCellValue('B4', 'Embarcación');
            $sheet->setCellValue('B5', 'Tipo de Inspección');
            $sheet->setCellValue('B6', 'Fecha');
            $sheet->setCellValue('B7', 'Puerto');
            $sheet->setCellValue('B8', 'Inspector');
            $sheet->setCellValue('B9', 'Otros');

            $sheet->getStyle('B4:B9')->applyFromArray($plomo);
            $sheet->getStyle('C4:C9')->applyFromArray($styleArray);

            $sheet->setCellValue('F3', 'Boletas');
            $sheet->getStyle('F3')->applyFromArray($plomo);

            $sheet->setCellValue('B11', 'Nr°');
            $sheet->setCellValue('C11', 'Ítem');
            $sheet->setCellValue('D11', 'N°boleta');
            $sheet->setCellValue('E11', 'Cod.');
            $sheet->setCellValue('F11', 'Vencimiento');
            $sheet->setCellValue('G11', 'Área encargada');
            $sheet->setCellValue('H11', 'Plan acción');
            $sheet->setCellValue('I11', 'Estado');
            $sheet->setCellValue('J11', 'Fecha del Levantamiento');
            
            
            
                $sheet->setCellValue('C4', self::getValueSubreport($b->task_id,$b->number,'1','857w4tgy4ax'));
                $sheet->setCellValue('C5', self::getValueSubreport($b->task_id,$b->number,'1','cej6nquvhrt'));
                $sheet->setCellValue('C6', date("d/m/Y",strtotime(self::getValueSubreport($b->task_id,$b->number,'1','fnbpimwl6m0'))));
                $sheet->setCellValue('C7', self::getValueSubreport($b->task_id,$b->number,'1','eb8p1xfngun'));
                $sheet->setCellValue('C8', self::getValueSubreport($b->task_id,$b->number,'1','auo71k5lg9y'));
                $sheet->setCellValue('C9', self::getValueSubreport($b->task_id,$b->number,'1','wpmik11ch7j'));


                $max = DB::connection('eskuadconn')->table("sub_reports_values")
                ->join("sub_reports","sub_reports_values.subreport_id","=","sub_reports.id")
                ->where("sub_reports_values.task_id", $b->task_id)
                ->where("sub_reports_values.number",$b->number)
                ->where("sub_reports.code","v29sb6ihe31")
                ->select("sub_reports_values.value","sub_reports_values.cicle","sub_reports_values.clone","sub_reports_values.task_id","sub_reports_values.number")
                ->get();
      

               
                    $foto1 = self::getValueSubreport($b->task_id,$b->number,'1','2h83xmml8tb');
                    $foto2 = self::getValueSubreport($b->task_id,$b->number,'1','9e4cvmn3nqv');
                    $foto3 = self::getValueSubreport($b->task_id,$b->number,'1','07k862xjjob');

                    if($foto1<>""){
                        $ruta = "https://eskuadfiles.fra1.digitaloceanspaces.com/resources/eskuad_images/tasks/107/".$b->task_id."/".$foto1;
                        $sheet->setCellValue("G3","Link");
                        $sheet->getHyperlink('G3')->setUrl($ruta);
                        $sheet->getStyle('G3')->applyFromArray($styleArray); 
                    }

                    if($foto2<>""){
                        $ruta = "https://eskuadfiles.fra1.digitaloceanspaces.com/resources/eskuad_images/tasks/107/".$b->task_id."/".$foto2;
                        $sheet->setCellValue("G4","Link");
                        $sheet->getHyperlink('G4')->setUrl($ruta);
                        $sheet->getStyle('G4')->applyFromArray($styleArray); 
                    }

                    if($foto3<>""){
                        $ruta = "https://eskuadfiles.fra1.digitaloceanspaces.com/resources/eskuad_images/tasks/107/".$b->task_id."/".$foto3;
                        $sheet->setCellValue("G5","Link");
                        $sheet->getHyperlink('G5')->setUrl($ruta);
                        $sheet->getStyle('G5')->applyFromArray($styleArray); 
                    }
       
                $indice =  12;
                $correlativo = 1;
                foreach($max as $m){
                    $sheet->setCellValue('B'.$indice, $correlativo);
                    $sheet->setCellValue('C'.$indice, self::getValueSubreportClone($b->task_id,$b->number,$m->cicle,$m->clone,'v29sb6ihe31'));
                    $sheet->setCellValue('D'.$indice, self::getValueSubreportClone($b->task_id,$b->number,$m->cicle,$m->clone,'a5p6n5m47xr'));
                    $sheet->setCellValue('E'.$indice, self::getValueSubreportClone($b->task_id,$b->number,$m->cicle,$m->clone,'7uva8mh3a4k'));
                    $sheet->setCellValue('F'.$indice, date("d/m/Y",strtotime(self::getValueSubreportClone($b->task_id,$b->number,$m->cicle,$m->clone,'405o2lqu60j'))));

                    $areas = [];
                    $area1 = self::getValueSubreportClone($b->task_id,$b->number,$m->cicle,$m->clone,'5yxnekoukua');
                    $area2 = self::getValueSubreportClone($b->task_id,$b->number,$m->cicle,$m->clone,'sda2ykkonff');
                    $area3 = self::getValueSubreportClone($b->task_id,$b->number,$m->cicle,$m->clone,'jdhn9wlu2mn');
                    if(!in_array($area1,$areas)){
                        if($area1<>""){
                            array_push($areas,$area1);
                        }  
                    }
                    if(!in_array($area2,$areas)){
                        if($area2<>""){
                            array_push($areas,$area2);
                        }  
                    }
                    if(!in_array($area3,$areas)){
                        if($area3<>""){
                            array_push($areas,$area3);
                        }  
                    }

                    $respuesta = "";
                    for($a = 0;$a<sizeof($areas);$a++){
                        $respuesta = $respuesta.ucfirst($areas[$a]).",";
                    }
                    if($respuesta<>""){
                        $respuesta = substr($respuesta, 0, -1);
                    }
                    
                
                    $sheet->setCellValue('G'.$indice, $respuesta);
                    $sheet->getStyle('G'.$indice)->applyFromArray($azul); 
                    $sheet->setCellValue('H'.$indice, self::getValueSubreportClone($b->task_id,$b->number,$m->cicle,$m->clone,'444nki4uz6q'));
                    $sheet->setCellValue('I'.$indice, 'Pendiente');
                    $sheet->getStyle('I'.$indice)->applyFromArray($rojo); 
                    $sheet->setCellValue('J'.$indice, '');
                    $indice ++;
                    $correlativo ++;
                }
                $fin = $indice;
            
            
            $sheet->getStyle('B11:J11')->applyFromArray($plomo);

            $fin = $fin-1;

            $sheet->getStyle('B12:J'.$fin)->applyFromArray($styleArray); 

            foreach(range('B','J') as $columnID) {
                $sheet->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $boat ++;
        }

       

        try {
            $writer = new Xlsx($spreadsheet);
            $file = 'Revista '.$anio.'.xlsx';
            header("Content-Description: File Transfer");
            header('Content-Disposition: attachment; filename="' . $file . '"');
            header('Content-type: application/vnd.ms-excel');
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Expires: 0');     
            $writer->save("php://output");
        } catch (\PHPExcel_Reader_Exception $e) {
            return response("Error al generar documento solicitado", 500);
        }
    }

    public function exportSupplyrequest(int $id){
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $dt1 = new DateTime();
        $today = $dt1->format("Y-m-d");


        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $inicio = 1;
        $provisiones = DB::table("provisions")->where("crew_id",$id)->first();
        if($provisiones){
            $sheet->setCellValue('A'.$inicio, "Viveres para ".$provisiones->workers_quantity." persona(s)");
            $sheet->mergeCells('A'.$inicio.':E'.$inicio);
            $sheet->getStyle('A'.$inicio)->getFont()->setBold(true);
            $sheet->getStyle('A'.$inicio)->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A'.$inicio.":E".$inicio)->applyFromArray($styleArray); 
            $sheet->getStyle("A".$inicio)->getFont()->setSize(18);
            $inicio = $inicio +2;
        }
      
      

        

        $supply = DB::table("supply_request_item as e")
        ->where("e.supply_request_id",$id)
        ->get();
        foreach($supply as $e){
               $latalla = "";
                $elitem = "";
                $launidad = "";
                
                $talla = DB::table("warehouse_item_sizes")->where("id",$e->warehouse_item_size_id)->first();
                if($talla){
                    $latalla = $talla->name;
                    $itemepp = DB::table("warehouse_items")->where("id",$talla->warehouse_item_id)->first();
                    if($itemepp){
                        $elitem = $itemepp->name;
                        $unidad = DB::table("measurement_units")->where("id",$itemepp->measurement_unit_id)->first();
                        if($unidad){
                            $launidad = $unidad->name;
                        }
                    }
                }
                $e->item = $elitem;
                $e->size = $latalla;
                $e->measurement = $launidad;

                
                
        }

        $inicioarticulos = $inicio;

        $sheet->setCellValue('A'.$inicio, "Artículos de bodega");
        $sheet->mergeCells('A'.$inicio.':E'.$inicio);
        $sheet->getStyle('A'.$inicio)->getFont()->setBold(true);
        $sheet->getStyle('A'.$inicio)->getAlignment()->setHorizontal('center');
        $inicio = $inicio +1;
        $sheet->setCellValue('A'.$inicio, "Articulo");
        $sheet->setCellValue('B'.$inicio, "Talla");
        $sheet->setCellValue('C'.$inicio, "Cantidad");
        $sheet->setCellValue('D'.$inicio, "Unidad Medida");
        $sheet->setCellValue('E'.$inicio, "Observacion");
        $sheet->getStyle('A'.$inicio.':E'.$inicio)->getFont()->setBold(true);
        $sheet->getStyle('A'.$inicio.':E'.$inicio)->getAlignment()->setHorizontal('center');
        $inicio = $inicio +1;
        foreach($supply as $e){
            $sheet->setCellValue('A'.$inicio, $e->item);
            $sheet->setCellValue('B'.$inicio, $e->size);
            $sheet->setCellValue('C'.$inicio, $e->quantity);
            $sheet->setCellValue('D'.$inicio, $e->measurement);
            $sheet->setCellValue('E'.$inicio, $e->observations);
            $inicio = $inicio +1;
        }

        $sheet->getStyle('A'.$inicioarticulos.":E".($inicio-1))->applyFromArray($styleArray); 

        $inicio = $inicio +2;
        $inicioepp = $inicio;

       $epp = DB::table("epp_delivery_supply_request as e")
               ->join("workers as w","w.id","=","e.worker_id")
               ->where("e.supply_request_id",$id)
               ->select("w.name","w.last_name","w.rut","w.id","e.warehouse_item_size_id")
               ->get();
        
               foreach($epp as $e){
                $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$e->id)->get();
                $rol = "Sin Rol";
                foreach($roles as $role){
                    if($rol == "Sin Rol"){
                        $rol = "";
                    }
                    if($rol==""){
                        $rol = $role->name;
                    }else{
                        $rol = $rol.",".$role->name;
                    }
                }
                $e->role = $rol;

                $latalla = "";
                $elitem = "";
                
                $talla = DB::table("warehouse_item_sizes")->where("id",$e->warehouse_item_size_id)->first();
                if($talla){
                    $latalla = $talla->name;
                    $itemepp = DB::table("warehouse_items")->where("id",$talla->warehouse_item_id)->first();
                    if($itemepp){
                        $elitem = $itemepp->name;
                    }
                }
                $e->item = $elitem;
                $e->size = $latalla;
               }

            
            $sheet->setCellValue('A'.$inicio, "Entrega de EPP");
            $sheet->mergeCells('A'.$inicio.":E".$inicio);
            $sheet->getStyle('A'.$inicio)->getFont()->setBold(true);
            $sheet->getStyle('A'.$inicio)->getAlignment()->setHorizontal('center');
            $inicio = $inicio +1;
            $sheet->setCellValue('A'.$inicio, "Nombre");
            $sheet->setCellValue('B'.$inicio, "Rut");
            $sheet->setCellValue('C'.$inicio, "Rol");
            $sheet->setCellValue('D'.$inicio, "EPP");
            $sheet->setCellValue('E'.$inicio, "Talla");
            $sheet->getStyle('A'.$inicio.':E'.$inicio)->getFont()->setBold(true);
            $sheet->getStyle('A'.$inicio.':E'.$inicio)->getAlignment()->setHorizontal('center');
            $inicio = $inicio +1;
            foreach($epp as $e){
                $sheet->setCellValue('A'.$inicio, $e->name." ".$e->last_name);
                $sheet->setCellValue('B'.$inicio, $e->rut);
                $sheet->setCellValue('C'.$inicio, $e->role);
                $sheet->setCellValue('D'.$inicio, $e->item);
                $sheet->setCellValue('E'.$inicio, $e->size);
                $inicio = $inicio +1;
            }

            $sheet->getStyle('A'.$inicioepp.":E".($inicio-1))->applyFromArray($styleArray); 
      
            foreach(range('A','E') as $columnID) {
                $sheet->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

    

        try {
            $writer = new Xlsx($spreadsheet);
            $file = 'Solicitud Abastecimiento '.$today.'.xlsx';
            header("Content-Description: File Transfer");
            header('Content-Disposition: attachment; filename="' . $file . '"');
            header('Content-type: application/vnd.ms-excel');
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Expires: 0');     
            $writer->save("php://output");
        } catch (\PHPExcel_Reader_Exception $e) {
            return response("Error al generar documento solicitado", 500);
        }
    }

    public function exportSupplyrequestOld($id){
        /*$dt1 = new DateTime();
        $today = $dt1->format("Y-m-d");

        $atributos =  DB::table("workers")
                    ->select("workers.attribute_id","workers.id","workers.rut","workers.name as nombre","workers.last_name as apellido")
                    ->whereIn("workers.attribute_id",array(3,4,5,6))
                    //->orderBy("worker_attributes.created_at","desc")
                    //->orderBy("worker_attributes.id","desc")
                    ->get();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $styleArray = array(
            'borders' => array(
                'outline' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        //inicio documentos
        $sheet->setCellValue('A1', "Nombre");
        $sheet->setCellValue('B1', "Apellido");
        $sheet->setCellValue('C1', "Rut");
        $sheet->setCellValue('D1', "Rol");
        $sheet->setCellValue('E1', "Atributo");
        $sheet->setCellValue('F1', "Fecha Inicio");
        $sheet->setCellValue('G1', "Fecha Fin");
        $sheet->setCellValue('H1', "Observacion");
        $sheet->getStyle('A1:H1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:H1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:H1')->getFont()->setBold(true);
        $start = 2;
        foreach($atributos as $d){

            $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$d->id)->get();
                $rol = "Sin Rol";
                foreach($roles as $role){
                    if($rol == "Sin Rol"){
                        $rol = "";
                    }
                    if($rol==""){
                        $rol = $role->name;
                    }else{
                        $rol = $rol.",".$role->name;
                    }
                }
                $rut = self::rut($d->rut);
               $cual = DB::table("document_types")->where("attribute_id",$d->attribute_id)->first();
               $fechas = DB::table("extra_documents")->where("document_type_id",$cual->id)->where("worker_id",$d->id)->orderBy("id","desc")->first();
               $atributo = DB::table("attributes")->where("id",$d->attribute_id)->first();
               $inicio = "";
               $fin ="";
               $obs = "";
               if($fechas){
                   $inicio = $fechas->date_start;
                   $fin = $fechas->date_end;
                   $obs = $fechas->observation;
               }

                
            $sheet->setCellValue('A'.$start, $d->nombre);
            $sheet->setCellValue('B'.$start, $d->apellido);
            $sheet->setCellValue('C'.$start, $rut);
            $sheet->setCellValue('D'.$start, $rol);
            $sheet->setCellValue('E'.$start, $atributo->name);
            $sheet->setCellValue('F'.$start, $inicio);
            $sheet->setCellValue('G'.$start, $fin);
            $sheet->setCellValue('H'.$start, $obs);
            $sheet->getStyle('A'.$start.':H'.$start)->applyFromArray($styleArray);
            $sheet->getStyle('A'.$start.':H'.$start)->getAlignment()->setHorizontal('center');
            $start = $start +1;

        }
        foreach(range('A','H') as $columnID) {
            $sheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        $sheet->setTitle("Licencias y Permisos");*/
        //fin documentos
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $dt1 = new DateTime();
        $today = $dt1->format("Y-m-d");


        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $inicio = 1;  

        $supply = DB::table("supply_request_item as e")
        ->where("e.supply_request_id",$id)
        ->get();
        foreach($supply as $e){
               $latalla = "";
                $elitem = "";
                $launidad = "";
                
                $talla = DB::table("warehouse_item_sizes")->where("id",$e->warehouse_item_size_id)->first();
                if($talla){
                    $latalla = $talla->name;
                    $itemepp = DB::table("warehouse_items")->where("id",$talla->warehouse_item_id)->first();
                    if($itemepp){
                        $elitem = $itemepp->name;
                        $unidad = DB::table("measurement_units")->where("id",$itemepp->measurement_unit_id)->first();
                        if($unidad){
                            $launidad = $unidad->name;
                        }
                    }
                }
                $e->item = $elitem;
                $e->size = $latalla;
                $e->measurement = $launidad;

                
                
        }


        $sheet->setCellValue('A'.$inicio, "Artículos de bodega");
        $sheet->mergeCells('A1:E1');
        $sheet->getStyle('A'.$inicio)->getFont()->setBold(true);
        $sheet->getStyle('A'.$inicio)->getAlignment()->setHorizontal('center');
        $inicio = $inicio +1;
        $sheet->setCellValue('A'.$inicio, "Articulo");
        $sheet->setCellValue('B'.$inicio, "Talla");
        $sheet->setCellValue('C'.$inicio, "Cantidad");
        $sheet->setCellValue('D'.$inicio, "Unidad Medida");
        $sheet->setCellValue('E'.$inicio, "Observacion");
        $sheet->getStyle('A'.$inicio.':E'.$inicio)->getFont()->setBold(true);
        $sheet->getStyle('A'.$inicio.':E'.$inicio)->getAlignment()->setHorizontal('center');
        $inicio = $inicio +1;
        foreach($supply as $e){
            $sheet->setCellValue('A'.$inicio, $e->item);
            $sheet->setCellValue('B'.$inicio, $e->size);
            $sheet->setCellValue('C'.$inicio, $e->quantity);
            $sheet->setCellValue('D'.$inicio, $e->measurement);
            $sheet->setCellValue('E'.$inicio, $e->observations);
            $inicio = $inicio +1;
        }

        $sheet->getStyle('A1'.":E".($inicio-1))->applyFromArray($styleArray); 

        $inicio = $inicio +2;
        $inicioepp = $inicio;

       $epp = DB::table("epp_delivery_supply_request as e")
               ->join("workers as w","w.id","=","e.worker_id")
               ->where("e.supply_request_id",$id)
               ->select("w.name","w.last_name","w.rut","w.id","e.warehouse_item_size_id")
               ->get();
        
               foreach($epp as $e){
                $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$e->id)->get();
                $rol = "Sin Rol";
                foreach($roles as $role){
                    if($rol == "Sin Rol"){
                        $rol = "";
                    }
                    if($rol==""){
                        $rol = $role->name;
                    }else{
                        $rol = $rol.",".$role->name;
                    }
                }
                $e->role = $rol;

                $latalla = "";
                $elitem = "";
                
                $talla = DB::table("warehouse_item_sizes")->where("id",$e->warehouse_item_size_id)->first();
                if($talla){
                    $latalla = $talla->name;
                    $itemepp = DB::table("warehouse_items")->where("id",$talla->warehouse_item_id)->first();
                    if($itemepp){
                        $elitem = $itemepp->name;
                    }
                }
                $e->item = $elitem;
                $e->size = $latalla;
               }

            
            $sheet->setCellValue('A'.$inicio, "Entrega de EPP");
            $sheet->mergeCells('A'.$inicio.":E".$inicio);
            $sheet->getStyle('A'.$inicio)->getFont()->setBold(true);
            $sheet->getStyle('A'.$inicio)->getAlignment()->setHorizontal('center');
            $inicio = $inicio +1;
            $sheet->setCellValue('A'.$inicio, "Nombre");
            $sheet->setCellValue('B'.$inicio, "Rut");
            $sheet->setCellValue('C'.$inicio, "Rol");
            $sheet->setCellValue('D'.$inicio, "EPP");
            $sheet->setCellValue('E'.$inicio, "Talla");
            $sheet->getStyle('A'.$inicio.':E'.$inicio)->getFont()->setBold(true);
            $sheet->getStyle('A'.$inicio.':E'.$inicio)->getAlignment()->setHorizontal('center');
            $inicio = $inicio +1;
            foreach($epp as $e){
                $sheet->setCellValue('A'.$inicio, $e->name." ".$e->last_name);
                $sheet->setCellValue('B'.$inicio, $e->rut);
                $sheet->setCellValue('C'.$inicio, $e->role);
                $sheet->setCellValue('D'.$inicio, $e->item);
                $sheet->setCellValue('E'.$inicio, $e->size);
                $inicio = $inicio +1;
            }

            $sheet->getStyle('A'.$inicioepp.":E".($inicio-1))->applyFromArray($styleArray); 
      
            foreach(range('A','E') as $columnID) {
                $sheet->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

    

        try {
            $writer = new Xlsx($spreadsheet);
            $file = 'Solicitud Abastecimiento '.$today.'.xlsx';
            header("Content-Description: File Transfer");
            header('Content-Disposition: attachment; filename="' . $file . '"');
            header('Content-type: application/vnd.ms-excel');
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Expires: 0');     
            $writer->save("php://output");
        } catch (\PHPExcel_Reader_Exception $e) {
            return response("Error al generar documento solicitado", 500);
        }
    }



    public function generateRelevo($d1){
        $dt1 = new DateTime();
        $today = $dt1->format("Y-m-d");

        set_time_limit ( 300 );
        ini_set('memory_limit','256M');

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $styleArray = array(
            'borders' => array(
                'outline' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $styleArrayAlineado = array(
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' =>  \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ],
            'borders' => array(
                'outline' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $colorrojo = [
            'font' => [
                'bold'  =>  true,
                'color' => array('rgb' => 'FF0000'),
            ],
        ];

        $embarcado = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' =>  \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ],
            'fill' => [
                'fillType' =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => 'CCCCCC',
                ]
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' =>  \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']
                ]
            ]
        ];

        $descanso = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' =>  \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ],
            'fill' => [
                'fillType' =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => 'FE7E7E',
                ]
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' =>  \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']
                ]
            ]
        ];


        $extras = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' =>  \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ],
            'fill' => [
                'fillType' =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => 'F7FA4A',
                ]
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' =>  \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']
                ]
            ]
        ];

        $permiso = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' =>  \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ],
            'fill' => [
                'fillType' =>  \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => 'FFA500',
                ]
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' =>  \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']
                ]
            ]
        ];

        $dateString = $d1;
        $lafecha = date("Y-m-", strtotime($d1));
        $ultimodia = date("t", strtotime($dateString));
        $dia_start = 4;
        $nfinaldia = 4;

        $letras = ['D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK'];
        $numero = 2;
        $barcos = DB::table("ships")->whereNull("deleted_at")->get();

        $arrayletras = [];
        $arraydias = [];

        $arrayletras_copia = [];
        $arraydias_copia = [];
        $elk = 0;
        for($k=1;$k<$ultimodia+1;$k++){
            $letra = $this->num_to_letters($dia_start);   
            array_push($arrayletras,$letra);
            array_push($arraydias,$k);
            $elk=$k;
            $dia_start++;
        }
        $ultimaletra = $this->num_to_letters($dia_start);  
        array_push($arrayletras,$ultimaletra);
        array_push($arraydias,($elk+1));
        

        

        $dia_start = 4;
        foreach($barcos as $b){
            $sheet->setCellValue('A'.$numero, strtoupper($b->name." ".$b->call_identifier));
            $sheet->getStyle('A'.$numero)->applyFromArray($styleArray); 
            $sheet->getStyle('A'.$numero)->getFont()->setBold(true);
            $sheet->getStyle('A'.$numero)->getAlignment()->setHorizontal('center');
            $numero++;
            $sheet->setCellValue('A'.$numero, "TRIPULACION");
            $sheet->setCellValue('B'.$numero, "RUT");
            $sheet->setCellValue('C'.$numero, "ROL");
            $sheet->getStyle('A'.$numero.':C'.$numero)->getFont()->setBold(true);
            $sheet->getStyle('A'.$numero.':C'.$numero)->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A'.$numero)->applyFromArray($styleArray); 
            $sheet->getStyle('B'.$numero)->applyFromArray($styleArray); 
            $sheet->getStyle('C'.$numero)->applyFromArray($styleArray); 

            $sheet->getColumnDimension("A")->setAutoSize(true);
            $sheet->getColumnDimension("B")->setAutoSize(true);
            $sheet->getColumnDimension("C")->setAutoSize(true);
            
            for($k=1;$k<$ultimodia+1;$k++){
                $letra = $this->num_to_letters($dia_start);
                $nfinaldia = $dia_start;
                if($k<10){
                    $dia = $lafecha."0".$k;
                }else{
                    $dia = $lafecha.$k;
                } 

          
                if($this->isWeekend($dia)){
                    $sheet->setCellValue($letra.$numero,$k);
                    $sheet->getStyle($letra.$numero)->applyFromArray($colorrojo);
                   // $sheet->getStyle($letra.$numero)->getFont()->setBold(true);
                }else{
                     $sheet->setCellValue($letra.$numero,$k);
                }
                $sheet->getStyle($letra.$numero)->applyFromArray($styleArray); 
                $dia_start++;
            }
            $dia_start = 4;
        
            $numero++;
         
            $crews = DB::table("crews")
                    ->join("crew_worker","crew_worker.crew_id","=","crews.id")
                    ->join("workers","workers.id","=","crew_worker.worker_id")
                    ->where("crews.ship_id",$b->id)
                    ->where(function ($q) use($lafecha) {
                            $q->where("crews.upload_date",'like',$lafecha."%")->orWhere("crews.download_date",'like',$lafecha."%");
                    })
                    ->groupBy("workers.id")
                    ->orderBy("workers.name","asc")
                    ->select("workers.*")
                    ->get();

                    $primer_dia_mes = date("Y-m-", strtotime($d1));
                    $ultimo_dia_mes = date("t", strtotime($d1));

                    $primer_dia_mes = $primer_dia_mes."01";
                    if($ultimo_dia_mes<10){
                        $ultimo_dia_mes = "0".$ultimo_dia_mes;
                    }
                    $ultimo_dia_mes = date("Y-m-", strtotime($d1)).$ultimo_dia_mes;

                    foreach($crews as $w){

                        $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$w->id)->get();
                        $rol = "Sin Rol";
                        foreach($roles as $role){
                            if($rol == "Sin Rol"){
                                $rol = "";
                            }
                            if($rol==""){
                                $rol = $role->name;
                            }else{
                                $rol = $rol.",".$role->name;
                            }
                        }


                        $sheet->setCellValue('A'.$numero, $w->name." ".$w->last_name);
                        $sheet->setCellValue('B'.$numero, $w->rut);
                        $sheet->setCellValue('C'.$numero, $rol);
                        $sheet->getStyle('A'.$numero.':C'.$numero)->getAlignment()->setHorizontal('center');

                        $sheet->getStyle('A'.$numero)->applyFromArray($styleArray); 
                        $sheet->getStyle('B'.$numero)->applyFromArray($styleArray); 
                        $sheet->getStyle('C'.$numero)->applyFromArray($styleArray); 

                        $tripulaciones =  DB::table("crews")
                        ->join("crew_worker","crew_worker.crew_id","=","crews.id")
                        ->where("crews.ship_id",$b->id)
                        ->where("crew_worker.worker_id",$w->id)
                        ->where(function ($q) use($lafecha) {
                                $q->where("crews.upload_date",'like',$lafecha."%")->orWhere("crews.download_date",'like',$lafecha."%");
                        })
                        ->select('crews.id','crews.upload_date','crews.download_date','crew_worker.is_replacement','crew_worker.with_emergency','crew_worker.late_entry','crew_worker.entry_date',"crews.extra_days")
                        ->get();


                        $arrayletras_copia = $arrayletras;
                        $arraydias_copia = $arraydias;
                        $dia_uno = [];
                        

                       // $sheet->setCellValue("AO".$numero, json_encode($tripulaciones));

                       $que_es = [];
                       $que_letra = [];

                       $que_es_ultima = "";
                       $ultimo_correlativo = 0;

                        foreach($tripulaciones as $trip){           
                            $fecha_de_subida = $trip->upload_date;
                            $fecha_de_bajada = $trip->download_date;

                            if($trip->entry_date<>null){
                                $fecha_de_subida = $trip->entry_date;
                            }

                            if($trip->with_emergency==1){
                                $emergency = DB::table("pending_attributes")->where("crew_id",$trip->id)->where("worker_id",$w->id)->where("attribute_id",2)->first();    
                                $trip->emergency = $emergency;     
                                $fecha_de_bajada = $emergency->date;

                               // $sheet->setCellValue("AK".$numero, json_encode($emergency));
                            }else{
                                $trip->emergency = null;    
                            }
                            
                            $index_subida = intval(date("d", strtotime($fecha_de_subida))) - 1;
                            $index_bajada = intval(date("d", strtotime($fecha_de_bajada))) - 1;

                            if($fecha_de_subida<$primer_dia_mes){
                                $index_subida = 0;
                            }
                            if($fecha_de_bajada>$ultimo_dia_mes){
                                $index_bajada = $ultimodia-1;
                            }

                            if($fecha_de_bajada<$primer_dia_mes){
                                $index_bajada = 0;
                            }

                            $correlativo_dias = 1;

                            $fecha1= new DateTime($fecha_de_subida);
                            $fecha2= new DateTime($fecha_de_bajada);
                            $dias_mes = $fecha1->diff($fecha2)->days; 
                            $bajo=0;
                            if($index_subida==0){
                                $fecha1= new DateTime($primer_dia_mes);
                                $fecha2= new DateTime($fecha_de_bajada);
                                $diff = $fecha1->diff($fecha2);
                                $dias_mes = $dias_mes - $diff->days;
                                $correlativo_dias = $dias_mes+1;
                                
                            }

                            if($index_bajada==0){
                                $fecha1= new DateTime($fecha_de_bajada);
                                $fecha2= new DateTime($primer_dia_mes);
                                $diff = $fecha1->diff($fecha2);
                                $dias_mes = $diff->days;
                                $correlativo_dias = $dias_mes+1;
                                $bajo=1;
                               
                            } 

                           
                         //   $que_es_ultima_subida = date('Y-m-d', strtotime($fecha_de_subida));

                            if($que_es_ultima == $fecha_de_subida){
                                $correlativo_dias = $ultimo_correlativo;
                                $ultimo_correlativo = $ultimo_correlativo + 25;
                            }
                           // $sheet->setCellValue("AL".$numero, $fecha_de_subida);
                           // $sheet->setCellValue("AM".$numero, $que_es_ultima);
                           // $sheet->setCellValue("AM".$numero, json_encode($fecha_de_subida));
                           // $sheet->setCellValue("AN".$numero, json_encode($fecha_de_bajada));
                           // $sheet->setCellValue("AL".$numero, json_encode($correlativo_dias));
                            //$correlativo_dias = 1;

                            $extradays = $trip->extra_days+1;
                            $many_extra = 0;

                            for($m=$index_subida;$m<$index_bajada+1;$m++){
                                if($bajo==0){

                                    if($que_es_ultima == $fecha_de_subida){
                                        $sheet->setCellValue($arrayletras[$m].$numero, $correlativo_dias);
                                        if($correlativo_dias<=25){
                                            $sheet->getStyle($arrayletras[$m].$numero)->applyFromArray($embarcado); 
                                        }else{
                                            //if($many_extra<=$extradays){
                                                $sheet->getStyle($arrayletras[$m].$numero)->applyFromArray($extras); 
                                            //    $many_extra = $many_extra +1;
                                            //}else{
                                            //    $sheet->getStyle($arrayletras[$m].$numero)->applyFromArray($embarcado); 
                                            //}
                                            
                                        }    
                                        $indice = array_search($arrayletras[$m], $arrayletras_copia);
                                        array_splice($arrayletras_copia, $indice, 1);
                                        array_splice($arraydias_copia, $indice, 1);     
                                        $correlativo_dias++;
                                    }else{
                                        $sheet->setCellValue($arrayletras[$m].$numero, $correlativo_dias);
                                        if($correlativo_dias<=25){
                                            $sheet->getStyle($arrayletras[$m].$numero)->applyFromArray($embarcado); 
                                        }else{
                                            $sheet->getStyle($arrayletras[$m].$numero)->applyFromArray($extras); 
                                        }    
                                        $indice = array_search($arrayletras[$m], $arrayletras_copia);
                                        array_splice($arrayletras_copia, $indice, 1);
                                        array_splice($arraydias_copia, $indice, 1);     
                                        $correlativo_dias++;
                                    }

                                    



                                }
                                if($bajo==1){
                                    $sheet->setCellValue($arrayletras[$m].$numero, $correlativo_dias);
                                    if($correlativo_dias<=21){
                                        $sheet->getStyle($arrayletras[$m].$numero)->applyFromArray($descanso); 
                                    }else{
                                        $sheet->getStyle($arrayletras[$m].$numero)->applyFromArray($extras); 
                                    }    
                                    $indice = array_search($arrayletras[$m], $arrayletras_copia);
                                    array_splice($arrayletras_copia, $indice, 1);
                                    array_splice($arraydias_copia, $indice, 1);    
                                    
                                    $correlativo_dias++;
                                }

                                $ultimo_correlativo = $correlativo_dias;
                             }

                             array_push($que_es,$bajo); 

                             // aqui dio error en real asi que hubo que ponerle -1 , revisar con tiempo
                             array_push($que_letra,$arrayletras[$m]); 
                             //


                             array_push($dia_uno,$arraydias[$m-1]);

                             $que_es_ultima = date('Y-m-d', strtotime($fecha_de_bajada.' +1 day'));

                        }


                        //inicio de atributos
                        $atributos = DB::table("worker_attributes")->where("worker_id",$w->id)
                        ->where(function ($q) use($lafecha) {
                           $q->where("date_start",'like',$lafecha."%")->orWhere("date_end",'like',$lafecha."%");
                       })
                       ->whereIn("attribute_id",array(3,4,5,6))
                       ->select("attribute_id","date_start","date_end")
                       ->get();

                       //$sheet->setCellValue('AK'.$numero, json_encode($atributos));

                       foreach($atributos as $trip){
                        $fecha_de_subida = $trip->date_start;
                        $fecha_de_bajada = $trip->date_end;
                        
                        $index_subida = intval(date("d", strtotime($fecha_de_subida))) - 1;
                        $index_bajada = intval(date("d", strtotime($fecha_de_bajada))) - 1;

                        if($fecha_de_subida<$primer_dia_mes){
                            $index_subida = 0;
                        }
                        if($fecha_de_bajada>$ultimo_dia_mes){
                            $index_bajada = $ultimodia-1;
                        }

                        $correlativo_dias = 1;

                        $fecha1= new DateTime($fecha_de_subida);
                        $fecha2= new DateTime($fecha_de_bajada);
                        $dias_mes = $fecha1->diff($fecha2)->days; 
                        if($index_subida==0){
                            $fecha1= new DateTime($primer_dia_mes);
                            $fecha2= new DateTime($fecha_de_bajada);
                            $diff = $fecha1->diff($fecha2);
                            $dias_mes = $dias_mes - $diff->days;
                            $correlativo_dias = $dias_mes+1;
                        }

                        //$correlativo_dias = 1;
                        for($m=$index_subida;$m<$index_bajada+1;$m++){
                            $sheet->setCellValue($arrayletras[$m].$numero, $correlativo_dias);
                            $sheet->getStyle($arrayletras[$m].$numero)->applyFromArray($permiso);   
                            $indice = array_search($arrayletras[$m], $arrayletras_copia);
                            array_splice($arrayletras_copia, $indice, 1);
                            array_splice($arraydias_copia, $indice, 1);     
                            $correlativo_dias++; 
                         }
                         array_push($dia_uno,$arraydias[$m-1]);
                       }

                       //fin de atributos

                        $correlativo_dias = 1;
                        $correlativo_libres = 1;
                        $que_entro = 0;
                        for($m=0;$m<sizeof($arrayletras_copia);$m++){

                                if($que_entro == 0){
                                    $posicion = array_search($arrayletras_copia[$m],$arrayletras);
                                    $letrabusca = $arrayletras[$posicion];
                                    $ultimo_estado = 0;
                                    if(in_array($letrabusca,$que_letra)){
                                        $posicionletra = array_search($letrabusca,$que_letra);
                                        $ultimo_estado = $que_es[$posicionletra];
                                    }
                                    $que_entro =1;
                                }
                                

                                if($ultimo_estado==0){
                                    $index  = array_search($arraydias_copia[$m],$arraydias);
                                    if(in_array($index,$dia_uno)){
                                        $correlativo_dias=1;
                                    }
         
                                    if($correlativo_dias<=21){
                                        $sheet->setCellValue($arrayletras_copia[$m].$numero, $correlativo_dias);
                                        $sheet->getStyle($arrayletras_copia[$m].$numero)->applyFromArray($descanso); 
                                        $correlativo_dias++; 
                                    } else{
                                        $sheet->setCellValue($arrayletras_copia[$m].$numero, $correlativo_libres);
                                        $sheet->getStyle($arrayletras_copia[$m].$numero)->applyFromArray($styleArray); 
                                        $correlativo_libres++; 
                                    }    
                                }


                                if($ultimo_estado==1){
                            
                             
                                        $sheet->setCellValue($arrayletras_copia[$m].$numero, $correlativo_libres);
                                        $sheet->getStyle($arrayletras_copia[$m].$numero)->applyFromArray($styleArray); 
                                        $correlativo_libres++; 
                                      
                                }

                                

                         
                                  
                         }
                        // $sheet->setCellValue("AK".$numero, json_encode($arrayletras_copia));
                        // $sheet->setCellValue("AM".$numero, json_encode($que_es));
                         $numero++;
                    }

                $numero++;
         
        }

        $sheet->removeColumn($ultimaletra);

        //inicio documentos
        
        foreach(range('A','AK') as $columnID) {
            $sheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(1);
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('B2', "Color");
        $sheet->setCellValue('C2', "Atributo");

        $sheet->setCellValue('B3', "Gris");
        $sheet->setCellValue('C3', "Trabajando");
        $sheet->getStyle('B3')->applyFromArray($embarcado); 

        $sheet->setCellValue('B4', "Rojo");
        $sheet->setCellValue('C4', "Descanso");
        $sheet->getStyle('B4')->applyFromArray($descanso); 

        $sheet->setCellValue('B5', "Amarillo");
        $sheet->setCellValue('C5', "Dias Extra");
        $sheet->getStyle('B5')->applyFromArray($extras); 

        $sheet->setCellValue('B6', "Naranjo");
        $sheet->setCellValue('C6', "Licencia o Permiso");
        $sheet->getStyle('B6')->applyFromArray($permiso); 

        $sheet->setCellValue('B7', "Blanco");
        $sheet->setCellValue('C7', "Disponible");

        $sheet->getStyle('B2')->applyFromArray($styleArrayAlineado); 
        $sheet->getStyle('B3')->applyFromArray($styleArrayAlineado); 
        $sheet->getStyle('B4')->applyFromArray($styleArrayAlineado); 
        $sheet->getStyle('B5')->applyFromArray($styleArrayAlineado); 
        $sheet->getStyle('B6')->applyFromArray($styleArrayAlineado); 
        $sheet->getStyle('B7')->applyFromArray($styleArrayAlineado);
        $sheet->getStyle('C2')->applyFromArray($styleArrayAlineado); 
        $sheet->getStyle('C3')->applyFromArray($styleArrayAlineado); 
        $sheet->getStyle('C4')->applyFromArray($styleArrayAlineado); 
        $sheet->getStyle('C5')->applyFromArray($styleArrayAlineado); 
        $sheet->getStyle('C6')->applyFromArray($styleArrayAlineado); 
        $sheet->getStyle('C7')->applyFromArray($styleArrayAlineado); 

        foreach(range('B','C') as $columnID) {
            $sheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
       
        $spreadsheet->setActiveSheetIndex(0);



    
        try {
            $writer = new Xlsx($spreadsheet);
            $mes = $this->get_nombre_mes($d1);
            $anio = date("Y", strtotime($d1));
            $file = 'Calendario Relevos '.$mes.' '.$anio.'.xlsx';
            header("Content-Description: File Transfer");
            header('Content-Disposition: attachment; filename="' . $file . '"');
            header('Content-type: application/vnd.ms-excel');
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Expires: 0');     
            $writer->save("php://output");
        } catch (\PHPExcel_Reader_Exception $e) {
            return response("Error al generar documento solicitado", 500);
        }
    }

  
    function letradia($letras,$dias,$busqueda){
      $index  = array_search($busqueda, $dias);
      return $letras[$index];
    }

    function indexletradia($letras,$dias,$busqueda){
        $index  = array_search($busqueda, $dias);
        return $index;
      }

    function dia_de_subida($subida,$entry_date){
        if($entry_date<>null){
            $fecha1= new DateTime($subida);
            $fecha2= new DateTime($entry_date);
            $diff = $fecha1->diff($fecha2);
            $subida = date('Y-m-d', strtotime($subida. ' + '.$diff->days.' days'));
            return $subida;
        }   
        return $subida;
    }

    function dia_de_bajada_real($subida,$extra,$with_emergency,$emergency,$ultimodia,$fecha_bajada){
        if($with_emergency==1){
            return $emergency->date;
        }
        //$bajada = date('Y-m-d', strtotime($subida. ' + 24 days'));
        //$bajada = date('Y-m-d', strtotime($bajada. ' + '.$extra.' days'));

        $bajada = $fecha_bajada;

        $fecha1= new DateTime($subida);
        $fecha2= new DateTime($bajada);
        $diff = $fecha1->diff($fecha2);
        $ultimodia_dia = intval(date('d', strtotime($ultimodia)));

        
        if($diff->days >$ultimodia_dia){
           return $ultimodia;
        }
        return $bajada;
    }

    function dia_de_bajada_fin_mes($subida,$extra,$with_emergency,$emergency,$ultimodia,$fecha_bajada){
        if($with_emergency==1){

           // $bajada = date('Y-m-d', strtotime($subida. ' + 24 days'));
           // $bajada = date('Y-m-d', strtotime($bajada. ' + '.$extra.' days'));
            
           $bajada = $fecha_bajada;
    
            if($this->mismomes($subida,$bajada)){ 
                return $emergency->date;
            }else{

                $fecha1= new DateTime($subida);
                $fecha2= new DateTime($bajada);
                $diff = $fecha1->diff($fecha2);
                $ultimodia_dia = $ultimodia;
                $diasubida_dia = intval(date('d', strtotime($subida)));

                $diasmenos = $ultimodia_dia - $diasubida_dia;
                $bajada = date('Y-m-d', strtotime($subida. ' + '.$diasmenos.' days'));
                return $bajada;
            }
           


        }
       // $bajada = date('Y-m-d', strtotime($subida. ' + 24 days'));
       // $bajada = date('Y-m-d', strtotime($bajada. ' + '.$extra.' days'));
        $bajada = $fecha_bajada;

        $fecha1= new DateTime($subida);
        $fecha2= new DateTime($bajada);
        $diff = $fecha1->diff($fecha2);
        //$ultimodia_dia = intval(date('d', strtotime($ultimodia)));
        $ultimodia_dia = $ultimodia;
        $diasubida_dia = intval(date('d', strtotime($subida)));

        if($this->mismomes($subida,$bajada)){ 
                return $bajada;
        }else{
            $diasmenos = $ultimodia_dia - $diasubida_dia;
            $bajada = date('Y-m-d', strtotime($subida. ' + '.$diasmenos.' days'));
            return $bajada;
        }
    }

    

    function dias_de_trabajo($subida,$diasmes,$with_emergency,$bajada){
        if($with_emergency==1){
            $subida = date('Y-m-d', strtotime($subida));
            $bajada = date('Y-m-d', strtotime($bajada));
            $fecha1= new DateTime($subida);
            $fecha2= new DateTime($bajada);
            $diff = $fecha1->diff($fecha2);
            $diff = $diff->days + 1;
            if($diff > $diasmes){
                return $diasmes;
            }
            return $diff;
        }
        $subida = date('d', strtotime($subida));
        $calculo = $diasmes - $subida;
        if($calculo > $diasmes){
            return $diasmes;
        }
        return $calculo;
    }

    function mismomes($subida,$bajada){
        $subida = date('Y-m', strtotime($subida));
        $bajada = date('Y-m', strtotime($bajada));
        if($subida==$bajada){
            return true;
        }
        return false;
    }


    function isWeekend($date) {
        return (date('N', strtotime($date)) > 6);
    }


    function num_to_letters($n)
{
    $n -= 1;
    for ($r = ""; $n >= 0; $n = intval($n / 26) - 1)
        $r = chr($n % 26 + 0x41) . $r;
    return $r;
}

    public function generateLicenciaPermiso(){
        $dt1 = new DateTime();
        $today = $dt1->format("Y-m-d");

        $atributos =  DB::table("workers")
                    ->select("workers.attribute_id","workers.id","workers.rut","workers.name as nombre","workers.last_name as apellido")
                    ->whereIn("workers.attribute_id",array(3,4,5,6))
                    //->orderBy("worker_attributes.created_at","desc")
                    //->orderBy("worker_attributes.id","desc")
                    ->get();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $styleArray = array(
            'borders' => array(
                'outline' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        //inicio documentos
        $sheet->setCellValue('A1', "Nombre");
        $sheet->setCellValue('B1', "Apellido");
        $sheet->setCellValue('C1', "Rut");
        $sheet->setCellValue('D1', "Rol");
        $sheet->setCellValue('E1', "Atributo");
        $sheet->setCellValue('F1', "Fecha Inicio");
        $sheet->setCellValue('G1', "Fecha Fin");
        $sheet->setCellValue('H1', "Observacion");
        $sheet->getStyle('A1:H1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:H1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:H1')->getFont()->setBold(true);
        $start = 2;
        foreach($atributos as $d){

            $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$d->id)->get();
                $rol = "Sin Rol";
                foreach($roles as $role){
                    if($rol == "Sin Rol"){
                        $rol = "";
                    }
                    if($rol==""){
                        $rol = $role->name;
                    }else{
                        $rol = $rol.",".$role->name;
                    }
                }
                $rut = self::rut($d->rut);
               $cual = DB::table("document_types")->where("attribute_id",$d->attribute_id)->first();
               $fechas = DB::table("extra_documents")->where("document_type_id",$cual->id)->where("worker_id",$d->id)->orderBy("id","desc")->first();
               $atributo = DB::table("attributes")->where("id",$d->attribute_id)->first();
               $inicio = "";
               $fin ="";
               $obs = "";
               if($fechas){
                   $inicio = $fechas->date_start;
                   $fin = $fechas->date_end;
                   $obs = $fechas->observation;
               }

                
            $sheet->setCellValue('A'.$start, $d->nombre);
            $sheet->setCellValue('B'.$start, $d->apellido);
            $sheet->setCellValue('C'.$start, $rut);
            $sheet->setCellValue('D'.$start, $rol);
            $sheet->setCellValue('E'.$start, $atributo->name);
            $sheet->setCellValue('F'.$start, $inicio);
            $sheet->setCellValue('G'.$start, $fin);
            $sheet->setCellValue('H'.$start, $obs);
            $sheet->getStyle('A'.$start.':H'.$start)->applyFromArray($styleArray);
            $sheet->getStyle('A'.$start.':H'.$start)->getAlignment()->setHorizontal('center');
            $start = $start +1;

        }
        foreach(range('A','H') as $columnID) {
            $sheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        $sheet->setTitle("Licencias y Permisos");
        //fin documentos


    

        try {
            $writer = new Xlsx($spreadsheet);
            $file = 'Licencias y Permisos '.$today.'.xlsx';
            header("Content-Description: File Transfer");
            header('Content-Disposition: attachment; filename="' . $file . '"');
            header('Content-type: application/vnd.ms-excel');
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Expires: 0');     
            $writer->save("php://output");
        } catch (\PHPExcel_Reader_Exception $e) {
            return response("Error al generar documento solicitado", 500);
        }
    }


    public function generateHistorialpasajes(string $d1,string $d2){
     
        

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();


        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ],
            ],
        ];

        $sheet
            ->getStyle('A1:AD1')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('c6e0b4');

        $sheet->setCellValue('A1', "Nº CTA");
        $sheet->setCellValue('B1', "NOMBRE CTA");
        $sheet->setCellValue('C1', "MES OC");
        $sheet->setCellValue('D1', "FECHA OC");
        $sheet->setCellValue('E1', "O/C");
        $sheet->setCellValue('F1', "NUMERO FACTURA");
        $sheet->setCellValue('G1', "TARIFA");
        $sheet->setCellValue('H1', "MULTA");
        $sheet->setCellValue('I1', "AFECTO");
        $sheet->setCellValue('J1', "TOTAL");
        $sheet->setCellValue('K1', "C.C");
        $sheet->setCellValue('L1', "C.C CARGO");
        $sheet->setCellValue('M1', "PASAJERO");
        $sheet->setCellValue('N1', "RUT");
        $sheet->setCellValue('O1', "CARGO");
        $sheet->setCellValue('P1', "TRAMO");
        $sheet->setCellValue('Q1', "LINEA");
        $sheet->setCellValue('R1', "CODIGO");
        $sheet->setCellValue('S1', "RUTA");
        $sheet->setCellValue('T1', "MES VUELO");
        $sheet->setCellValue('U1', "FECHA VUELO");
        $sheet->setCellValue('V1', "SALIDA");
        $sheet->setCellValue('W1', "LLEGADA");
        $sheet->setCellValue('X1', "USO REAL");
        $sheet->setCellValue('Y1', "C.C FINAL");
        $sheet->setCellValue('Z1', "CONDICION");
        $sheet->setCellValue('AA1', "FECHA SOLICITUD");
        $sheet->setCellValue('AB1', "OBSERVACION");
        $sheet->setCellValue('AC1', "PASAJE COMPRADO/STOCK/VOUCHER");
        $sheet->setCellValue('AD1', "TIPO");
        $sheet->setCellValue('AE1', "PRECIO VOUCHER");
        $sheet->setCellValue('AF1', "VOUCHER");
        
    
        $start = 2;

        $workerTickets = DB::table("ticket_groups as t")
                    ->join("airlines as a","a.id","=","t.airline_id")
                    ->join("worker_ticket as w","w.ticket_group_id","=","t.id")
                    ->join('flight_stretches as f',"f.id","=","t.flight_stretch_id")
                    ->select("w.penalty","w.penalty_quotation","t.oc_date as oc_date","t.oc as oc","t.ticket_type","w.penalty as wpenalty","w.carry_price","w.stock","t.is_stock","t.ticket_status_id","w.lost_datetime","t.voucher_id","t.stock_ticket_id","w.observation","t.created_at as fecha","w.worker_id as pid","t.invoice_id","t.passengers","t.dearness","t.penalty","t.price","t.id","t.crew_id","t.flight_date","a.name as aerolinea","f.name as tramo","t.code as codigo","t.departure_airport_id","t.arrival_airport_id","t.departure_time","t.arrival_time")
                    ->whereBetween("t.flight_date",array($d1,$d2))
                   // ->orderBy("t.id","desc")
                    ->get();
        
        $personTickets = DB::table("ticket_groups as t")
                    ->join("airlines as a","a.id","=","t.airline_id")
                    ->join("person_ticket as w","w.ticket_group_id","=","t.id")
                    ->join('flight_stretches as f',"f.id","=","t.flight_stretch_id")
                    ->select("w.penalty","w.penalty_quotation","t.oc_date as oc_date","t.oc as oc","t.ticket_type","w.penalty as wpenalty","w.carry_price","w.stock","t.is_stock","t.ticket_status_id","w.lost_datetime","t.voucher_id","t.stock_ticket_id","w.observation","t.created_at as fecha","w.person_id as pid","t.invoice_id","t.passengers","t.dearness","t.penalty","t.price","t.id","t.crew_id","t.flight_date","a.name as aerolinea","f.name as tramo","t.code as codigo","t.departure_airport_id","t.arrival_airport_id","t.departure_time","t.arrival_time")
                    ->whereBetween("t.flight_date",array($d1,$d2))
                    //->orderBy("t.id","desc")
                    ->get();

        $stockTickets = DB::table("stock_tickets as t")
                    ->join("airlines as a","a.id","=","t.airline_id")
                    ->join('flight_stretches as f',"f.id","=","t.flight_stretch_id")
                    ->select("t.is_voucher as isvoucher","t.is_carry","t.original_ticket_id","t.ticket_user_id","t.user_type","t.ticket_type","t.stock_ticket_id","t.created_at as fecha","t.dearness","t.penalty","t.price","t.id","t.flight_date","a.name as aerolinea","f.name as tramo","t.code as codigo","t.departure_airport_id","t.arrival_airport_id","t.departure_time","t.arrival_time")
                    ->whereBetween("t.flight_date",array($d1,$d2))
                   // ->where("t.ticket_type","new_stock")
                    ->where("t.used",0)
                   // ->orderBy("t.id","desc")
                    ->get();

                    $ids = [];
                    foreach($workerTickets as $p){

                        $p->isvoucher = 0;
                 
                        $logvoucher = DB::table("log_vouchers")->where("ticket_group_id",$p->id)->first();
                        if($logvoucher){
                            $p->voucher_value = $logvoucher->voucher_price;
                        }else{
                            $p->voucher_value = 0;
                        }


                        if (!in_array($p->id, $ids)) {
                            array_push($ids,$p->id);
                        }

                        $p->is_carry = 0;
                        $p->tipoticket = "tripulante";

                        $a1 =  DB::table("airports")->where("id",$p->departure_airport_id)->first();
                        $a2 =  DB::table("airports")->where("id",$p->arrival_airport_id)->first();
                        $p->ruta = $a1->iata." - ".$a2->iata;

                        $p->mesvuelo =  self::get_nombre_mes(date("Y-m",strtotime($p->flight_date)));
                        $p->fechavuelo = date("d-m-Y",strtotime($p->flight_date));

                        $p->rut = "";
                        $p->nombre = "";
                        $p->cargo = "";
                        $p->barco= "";
                        $p->condicion = "";
                        $p->fsolicitud ="";

                            $worker = DB::table("worker_ticket")->join("workers","workers.id","=","worker_ticket.worker_id")->where("worker_ticket.ticket_group_id",$p->id)->where("worker_ticket.worker_id",$p->pid)->select("workers.last_name","workers.name","workers.rut","workers.id")->first();
                            if($worker){
                                $p->rut = self::rut($worker->rut);
                                $p->nombre = $worker->name." ".$worker->last_name;
                                    $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$worker->id)->get();
                                    $role = "";
                                    foreach($roles as $r){
                                        $role = $role." ".$r->name;
                                    }
                                $p->cargo = $role;
                            }

                        if($p->crew_id<>null){
                            $crew = DB::table("crews")->join("ships","ships.id","=","crews.ship_id")->where("crews.id",$p->crew_id)->select("ships.name as ship","crews.upload_date")->first();
                            if($crew){
                                $p->barco = $crew->ship;
                                $p->fsolicitud = $crew->upload_date;
                            }
                        }else{  
                            $p->fsolicitud = date("Y-m-d",strtotime($p->fecha));
                        }
                       
                        $p->total = "";

                        if(intval($p->passengers)>0){
                            $p->penalty = round($p->penalty / $p->passengers,0) + ($p->wpenalty);
                            $p->dearness = round($p->dearness / $p->passengers,0);
                            $p->total = $p->price + $p->penalty + $p->dearness;
                        }

                        $p->ncta ="";
                        $p->nombrecta="";
                       // $p->oc="";
                        $p->nfactura="";
                        $p->mesoc="";
                        $p->fechaoc="";
                        
                        if($p->invoice_id==null){

                        }else{
                            $invoice = DB::table("invoices")->where("id",$p->invoice_id)->first();
                            if($invoice){
                                $account = DB::table("bank_accounts")->where("id",$invoice->account_id)->first();
                                if($account){
                                    $p->ncta = $account->account_number;
                                    $p->nombrecta= $account->account_name;
                                }
                                //$p->oc= $invoice->purchase_order;
                                $p->nfactura= $invoice->invoice_number;

                                /*if($invoice->purchase_order_date<>null){
                                    $p->mesoc =  self::get_nombre_mes(date("Y-m",strtotime($invoice->purchase_order_date)));
                                    $p->fechaoc =  $invoice->purchase_order_date;
                                }*/
                            }
                        }    
                        
                        if($p->oc!=null && $p->oc!=""){
                            $p->mesoc =  self::get_nombre_mes(date("Y-m",strtotime($p->oc_date)));
                            $p->fechaoc =  $p->oc_date;
                        }
                        
                        
                        if($p->observation==null){
                            $p->observation = "";
                        }
                       
                        
                        $p->cccargo = "";
                        if($p->ncta=="6103011"){
                            $p->cccargo = "Embarcado";
                        }
                        if($p->ncta=="6105007"){
                            $p->cccargo = "Mantención";
                        }
                        if($p->ncta=="6103010"){
                            $p->cccargo = "Operaciones";
                        }

                        $voucher = "";
                        if($p->voucher_id<>null && $p->voucher_id<>""){
                            $voucher = $p->voucher_id;
                        }

                        $stockid = "";
                        if($p->stock_ticket_id<>null && $p->stock_ticket_id<>""){
                            $stockid = $p->stock_ticket_id;
                        }

                        $p->estadopasaje = "";
                        if($stockid =="" && $voucher==""){
                            $p->estadopasaje = "Comprado";
                        }else{
                            if($stockid<>"" && $voucher==""){
                                $elstock = DB::table("stock_tickets")->where("id",$stockid)->first();
                                if($elstock){
                                    $p->estadopasaje = $elstock->code. " - Stock";
                                }          
                            }
                            if($stockid=="" && $voucher<>""){
                                $elvoucher = DB::table("airline_vouchers")->where("id",$voucher)->first();
                                if($elvoucher){
                                    $p->estadopasaje = $elvoucher->code. " - Voucher";
                                    $p->isvoucher =1;
                                }          
                            }
                        }

                    }

                 


                    foreach($personTickets as $p){

                        $logvoucher = DB::table("log_vouchers")->where("ticket_group_id",$p->id)->first();
                        if($logvoucher){
                            $p->voucher_value = $logvoucher->voucher_price;
                        }else{
                            $p->voucher_value = 0;
                        }
                        
                        $p->isvoucher = 0;

                        if (!in_array($p->id, $ids)) {
                            array_push($ids,$p->id);
                        }
                        $p->is_carry = 0;

                        $p->tipoticket = "persona";
                        $a1 =  DB::table("airports")->where("id",$p->departure_airport_id)->first();
                        $a2 =  DB::table("airports")->where("id",$p->arrival_airport_id)->first();
                        $p->ruta = $a1->iata." - ".$a2->iata;

                        $p->mesvuelo =  self::get_nombre_mes(date("Y-m",strtotime($p->flight_date)));
                        $p->fechavuelo = date("d-m-Y",strtotime($p->flight_date));

                        

                        $p->rut = "";
                        $p->nombre = "";
                        $p->cargo = "";
                        $p->barco= "";
                        $p->condicion = "";
                        

                        $person = DB::table("person_ticket")->join("persons","persons.id","=","person_ticket.person_id")->where("person_ticket.ticket_group_id",$p->id)->where("person_ticket.person_id",$p->pid)->select("persons.last_name","persons.name","persons.rut","persons.role")->first();
                        if($person){
                         $p->rut = self::rut($person->rut);
                         $p->nombre = $person->name." ".$person->last_name;
                         $p->cargo = $person->role;
                        }
          
                        $p->total = "";

                        if(intval($p->passengers)>0){
                            $p->penalty = round($p->penalty / $p->passengers,0) + ($p->wpenalty);
                            $p->dearness = round($p->dearness / $p->passengers,0);
                            $p->total = $p->price + $p->penalty + $p->dearness;
                        }


                        $p->ncta ="";
                        $p->nombrecta="";
                       // $p->oc="";
                        $p->nfactura="";
                        $p->mesoc="";
                        $p->fechaoc="";
                        $p->fsolicitud = "";


                        if($p->invoice_id==null){

                        }else{
                            $invoice = DB::table("invoices")->where("id",$p->invoice_id)->first();
                            if($invoice){
                                $account = DB::table("bank_accounts")->where("id",$invoice->account_id)->first();
                                if($account){
                                    $p->ncta = $account->account_number;
                                    $p->nombrecta= $account->account_name;
                                }
                               // $p->oc= $invoice->purchase_order;
                                $p->nfactura= $invoice->invoice_number;

                                /*if($invoice->purchase_order_date<>null){
                                    $p->mesoc =  self::get_nombre_mes(date("Y-m",strtotime($invoice->purchase_order_date)));
                                    $p->fechaoc =  $invoice->purchase_order_date;
                                }*/
                            }
                        }  

                        if($p->oc!=null && $p->oc!=""){
                            $p->mesoc =  self::get_nombre_mes(date("Y-m",strtotime($p->oc_date)));
                            $p->fechaoc =  $p->oc_date;  
                        } 

                        if($p->observation==null){
                            $p->observation = "";
                        }

                        $p->fsolicitud = date("Y-m-d",strtotime($p->fecha));
                       
                        
                        $p->cccargo = "";
                        if($p->ncta=="6103011"){
                            $p->cccargo = "Embarcado";
                        }
                        if($p->ncta=="6105007"){
                            $p->cccargo = "Mantención";
                        }
                        if($p->ncta=="6103010"){
                            $p->cccargo = "Operaciones";
                        }


                        $voucher = "";
                        if($p->voucher_id<>null && $p->voucher_id<>""){
                            $voucher = $p->voucher_id;
                        }

                        $stockid = "";
                        if($p->stock_ticket_id<>null && $p->stock_ticket_id<>""){
                            $stockid = $p->stock_ticket_id;
                        }

                        $p->estadopasaje = "";
                        if($stockid =="" && $voucher==""){
                            $p->estadopasaje = "Comprado";
                        }else{
                            if($stockid<>"" && $voucher==""){
                                $elstock = DB::table("stock_tickets")->where("id",$stockid)->first();
                                if($elstock){
                                    $p->estadopasaje = $elstock->code." - Stock";
                                }          
                            }
                            if($stockid=="" && $voucher<>""){
                                $elvoucher = DB::table("airline_vouchers")->where("id",$voucher)->first();
                                if($elvoucher){
                                    $p->estadopasaje = $elvoucher->code." - Voucher";
                                  //  $p->isvoucher =1;
                                }          
                            }
                        }
                    }


                    $workerTickets = $workerTickets->merge($personTickets);


                    foreach($stockTickets as $p){

                        $p->penalty_quotation =0 ;

                                
                        $logvoucher = DB::table("log_vouchers")->where("ticket_group_id",$p->original_ticket_id)->first();
                        if($logvoucher){
                            $p->voucher_value = $logvoucher->voucher_price;
                        }else{
                            $p->voucher_value = 0;
                        }
                        

                        if($p->user_type=="worker"){
                            $tic = DB::table("worker_ticket")->where("id",$p->original_ticket_id)->first();
                            if($tic){
                                if($tic->penalty_quotation==1){
                                    $p->penalty_quotation==1;
                                }
                            }
                        }

                        if($p->user_type=="person"){
                            $tic = DB::table("person_ticket")->where("id",$p->original_ticket_id)->first();
                            if($tic){
                                if($tic->penalty_quotation==1){
                                    $p->penalty_quotation==1;
                                }
                            }
                        }

                        $p->tipoticket = "stock";
                        $a1 =  DB::table("airports")->where("id",$p->departure_airport_id)->first();
                        $a2 =  DB::table("airports")->where("id",$p->arrival_airport_id)->first();
                        $p->ruta = $a1->iata." - ".$a2->iata;

                        $p->mesvuelo =  self::get_nombre_mes(date("Y-m",strtotime($p->flight_date)));
                        $p->fechavuelo = date("d-m-Y",strtotime($p->flight_date));


                        $p->rut = "";
                        $p->nombre = "";
                        $p->cargo = "";
                        $p->barco= "";
                        $p->condicion = "";
                        $p->total = "";
                        $p->observation ="";
                        $p->carry_price ="";

                        $original = DB::table("ticket_groups")->where("id",$p->original_ticket_id)->first();

                        if($p->user_type=="worker"){
                            $worker = DB::table("worker_ticket")->join("workers","workers.id","=","worker_ticket.worker_id")->where("worker_ticket.ticket_group_id",$p->original_ticket_id)->where("worker_ticket.worker_id",$p->ticket_user_id)->select("workers.last_name","workers.name","workers.rut","workers.id")->first();
                            if($worker){
                                $p->rut = self::rut($worker->rut);
                                $p->nombre = $worker->name." ".$worker->last_name;
                                    $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$worker->id)->get();
                                    $role = "";
                                    foreach($roles as $r){
                                        $role = $role." ".$r->name;
                                    }
                                $p->cargo = $role;
                            }

                            

                            if($original->crew_id<>null){
                                $crew = DB::table("crews")->join("ships","ships.id","=","crews.ship_id")->where("crews.id",$original->crew_id)->select("ships.name as ship","crews.upload_date")->first();
                                if($crew){
                                    $p->barco = $crew->ship;
                                    $p->fsolicitud = $crew->upload_date;
                                }
                            }else{  
                                $p->fsolicitud = date("Y-m-d",strtotime($p->fecha));
                            }

                        }

                        if($p->user_type=="person"){
                            $person = DB::table("person_ticket")->join("persons","persons.id","=","person_ticket.person_id")->where("person_ticket.ticket_group_id",$p->original_ticket_id)->where("person_ticket.person_id",$p->ticket_user_id)->select("persons.last_name","persons.name","persons.rut","persons.role")->first();
                            if($person){
                            $p->rut = self::rut($person->rut);
                            $p->nombre = $person->name." ".$person->last_name;
                            $p->cargo = $person->role;
                            }
                        }
                        
                        
                        $p->total = $p->price + $p->penalty + $p->dearness;

                        $p->ncta ="";
                        $p->nombrecta="";
                        $p->oc="";
                        $p->nfactura="";
                        $p->mesoc="";
                        $p->fechaoc="";
                        $p->fsolicitud = "";

                       if($original->invoice_id==null){

                        }else{
                            $invoice = DB::table("invoices")->where("id",$original->invoice_id)->first();
                            if($invoice){
                                $account = DB::table("bank_accounts")->where("id",$invoice->account_id)->first();
                                if($account){
                                    $p->ncta = $account->account_number;
                                    $p->nombrecta= $account->account_name;
                                }
                              //  $p->oc= $invoice->purchase_order;
                                $p->nfactura= $invoice->invoice_number;

                                /*if($invoice->purchase_order_date<>null){
                                    $p->mesoc =  self::get_nombre_mes(date("Y-m",strtotime($invoice->purchase_order_date)));
                                    $p->fechaoc =  $invoice->purchase_order_date;
                                }*/
                            }
                        }

                        if($original){
                            if($original->oc!=null && $original->oc!=""){
                                $p->mesoc =  self::get_nombre_mes(date("Y-m",strtotime($original->oc_date)));
                                $p->fechaoc = $original->oc_date;  
                                $p->oc = $original->oc;
                            } 
                        }
                       
                        


                        $p->fsolicitud = date("Y-m-d",strtotime($p->fecha));
                       
                        
                        $p->cccargo = "";
                        if($p->ncta=="6103011"){
                            $p->cccargo = "Embarcado";
                        }
                        if($p->ncta=="6105007"){
                            $p->cccargo = "Mantención";
                        }
                        if($p->ncta=="6103010"){
                            $p->cccargo = "Operaciones";
                        }


                        $voucher = "";
                        if($original->voucher_id<>null && $original->voucher_id<>""){
                            $voucher = $original->voucher_id;
                        }

                        $stockid = "";
                        if($original->stock_ticket_id<>null && $original->stock_ticket_id<>""){
                            $stockid = $original->stock_ticket_id;
                        }

                        $p->estadopasaje = "";
                        if($stockid =="" && $voucher==""){
                            $p->estadopasaje = "Comprado";
                        }else{
                            if($stockid<>"" && $voucher==""){
                                $elstock = DB::table("stock_tickets")->where("id",$stockid)->first();
                                if($elstock){
                                    $p->estadopasaje = $elstock->code." - Stock";
                                }          
                            }
                            if($stockid=="" && $voucher<>""){
                                $elvoucher = DB::table("airline_vouchers")->where("id",$voucher)->first();
                                if($elvoucher){
                                    $p->estadopasaje = $elvoucher->code." - Voucher";
                                  //  $p->isvoucher = 1;
                                }          
                            }
                        }
                    }

                    $workerTickets = $workerTickets->merge($stockTickets);


                    foreach($workerTickets as $p){

                        $vouchervalue = 0;
                        $negativo = "";
                        $esvoucher="";
                        if($p->isvoucher==0){
                            if($p->tipoticket=="stock"){
                                if($p->ticket_type<>"new_stock"){
                                    $negativo = "-";
                                }
                                $condicion = "Stock";

                            }else{
                                
                                if($p->lost_datetime<>null){
                                    $condicion="Perdido";
                                }else{
                                    if($p->stock=="1"){
                                        $condicion="Stock";
                                    }else{
                                        if($p->ticket_status_id=="2"){
                                            $condicion="Por Volar";
                                        }
                                        if($p->ticket_status_id=="3"){
                                            $condicion="Volado";
                                        }
                                    }
                                   
                                }
                            }
                        }else{
                            $condicion="Cambiado";
                            $esvoucher= "Voucher";
                            $vouchervalue = $p->voucher_value;
                            $negativo = "-";
                        }

                        $vo = strpos($p->estadopasaje, "- Voucher");
                        if($vo){
                            $esvoucher= "Voucher";
                            $vouchervalue = $p->voucher_value;
                        }
                        
                       

                        $sheet->setCellValue('A'.$start, $p->ncta);//Nº CTA
                        $sheet->setCellValue('B'.$start, $p->nombrecta);//NOMBRE CTA
                        $sheet->setCellValue('C'.$start, $p->mesoc);//mes oc
                        $sheet->setCellValue('D'.$start, $p->fechaoc);//FECHA OC
                        $sheet->setCellValue('E'.$start, $p->oc);//O/C
                        $sheet->setCellValue('F'.$start, $p->nfactura);//NUMERO FACTURA
                        if($p->is_carry==0){
                            if($p->penalty_quotation==1){
                                $sheet->setCellValue('G'.$start, $negativo.$p->price);
                                $sheet->setCellValue('H'.$start, "");
                                $sheet->setCellValue('I'.$start, $negativo.$p->dearness);
                                $sheet->setCellValue('J'.$start, $negativo.($p->price + $p->dearness));
                            }else{
                                $sheet->setCellValue('G'.$start, $negativo.$p->price);
                                $sheet->setCellValue('H'.$start, $negativo.$p->penalty);
                                $sheet->setCellValue('I'.$start, $negativo.$p->dearness);
                                $sheet->setCellValue('J'.$start, $negativo.$p->total);
                            }
                         
                        }else{
                            $sheet->setCellValue('G'.$start, "");
                            $sheet->setCellValue('H'.$start, "");
                            $sheet->setCellValue('I'.$start, $negativo.$p->price);
                            $sheet->setCellValue('J'.$start, $negativo.$p->price);
                        }  
                        $sheet->setCellValue('K'.$start, $p->barco);//barco
                        $sheet->setCellValue('L'.$start, $p->cccargo);// "C.C CARGO"
                        $sheet->setCellValue('M'.$start, $p->nombre);
                        $sheet->setCellValue('N'.$start, $p->rut);
                        $sheet->setCellValue('O'.$start, $p->cargo);
                        $sheet->setCellValue('P'.$start, $p->tramo);
                        $sheet->setCellValue('Q'.$start, $p->aerolinea);
                        $sheet->setCellValue('R'.$start, $p->codigo);
                        $sheet->setCellValue('S'.$start, $p->ruta);
                        $sheet->setCellValue('T'.$start, $p->mesvuelo);
                        $sheet->setCellValue('U'.$start, $p->fechavuelo);
                        $sheet->setCellValue('V'.$start, date("H:i",strtotime($p->departure_time )));
                        $sheet->setCellValue('W'.$start, date("H:i",strtotime($p->arrival_time )));
                        $sheet->setCellValue('X'.$start, $p->mesvuelo);//uso real
                        $sheet->setCellValue('Y'.$start, $p->barco);//cc final
                        $sheet->setCellValue('Z'.$start, $condicion);//CONDICION
                        $sheet->setCellValue('AA'.$start, $p->fsolicitud);//FECHA SOLICITUD
                        $sheet->setCellValue('AB'.$start, $p->observation);//OBSERVACION
                        $sheet->setCellValue('AC'.$start, $p->estadopasaje);//PASAJE COMPRADO/STOCK/VOUCHER
                        if($p->is_carry==0){
                            $sheet->setCellValue('AD'.$start, "Pasaje");//PASAJE COMPRADO/STOCK/VOUCHER
                        }else{
                            $sheet->setCellValue('AD'.$start, "Maleta");//PASAJE COMPRADO/STOCK/VOUCHER
                        }
                        $sheet->setCellValue('AE'.$start, $vouchervalue);//es voucher
                        $sheet->setCellValue('AF'.$start, $esvoucher);//es voucher
                        $start = $start +1;


                        /*if($condicion=="Stock" || $condicion=="Cambiado"){
                            $sheet->setCellValue('A'.$start, $p->ncta);//Nº CTA
                            $sheet->setCellValue('B'.$start, $p->nombrecta);//NOMBRE CTA
                            $sheet->setCellValue('C'.$start, $p->mesoc);//mes oc
                            $sheet->setCellValue('D'.$start, $p->fechaoc);//FECHA OC
                            $sheet->setCellValue('E'.$start, $p->oc);//O/C
                            $sheet->setCellValue('F'.$start, $p->nfactura);//NUMERO FACTURA
                            if($p->is_carry==0){
                                $sheet->setCellValue('G'.$start, "-".$p->price);
                                $sheet->setCellValue('H'.$start, "-".$p->penalty);
                                $sheet->setCellValue('I'.$start, "-".$p->dearness);
                                $sheet->setCellValue('J'.$start, "-".$p->total);
                            }else{
                                $sheet->setCellValue('G'.$start, "");
                                $sheet->setCellValue('H'.$start, "");
                                $sheet->setCellValue('I'.$start, "-".$p->price);
                                $sheet->setCellValue('J'.$start, "-".$p->price);
                            }  
                            $sheet->setCellValue('K'.$start, $p->barco);//barco
                            $sheet->setCellValue('L'.$start, $p->cccargo);// "C.C CARGO"
                            $sheet->setCellValue('M'.$start, $p->nombre);
                            $sheet->setCellValue('N'.$start, $p->rut);
                            $sheet->setCellValue('O'.$start, $p->cargo);
                            $sheet->setCellValue('P'.$start, $p->tramo);
                            $sheet->setCellValue('Q'.$start, $p->aerolinea);
                            $sheet->setCellValue('R'.$start, $p->codigo);
                            $sheet->setCellValue('S'.$start, $p->ruta);
                            $sheet->setCellValue('T'.$start, $p->mesvuelo);
                            $sheet->setCellValue('U'.$start, $p->fechavuelo);
                            $sheet->setCellValue('V'.$start, date("H:i",strtotime($p->departure_time )));
                            $sheet->setCellValue('W'.$start, date("H:i",strtotime($p->arrival_time )));
                            $sheet->setCellValue('X'.$start, $p->mesvuelo);//uso real
                            $sheet->setCellValue('Y'.$start, $p->barco);//cc final
                            $sheet->setCellValue('Z'.$start, $condicion);//CONDICION
                            $sheet->setCellValue('AA'.$start, $p->fsolicitud);//FECHA SOLICITUD
                            $sheet->setCellValue('AB'.$start, $p->observation);//OBSERVACION
                            $sheet->setCellValue('AC'.$start, $p->estadopasaje);//PASAJE COMPRADO/STOCK/VOUCHER
                            if($p->is_carry==0){
                                $sheet->setCellValue('AD'.$start, "Pasaje");//PASAJE COMPRADO/STOCK/VOUCHER
                            }else{
                                $sheet->setCellValue('AD'.$start, "Maleta");//PASAJE COMPRADO/STOCK/VOUCHER
                            }
                            $sheet->setCellValue('AE'.$start, $vouchervalue);//es voucher
                            $sheet->setCellValue('AF'.$start, $esvoucher);//es voucher
                            $start = $start +1;
    
                        } */

                        if($p->penalty_quotation==1){
                            $sheet->setCellValue('A'.$start, $p->ncta);//Nº CTA
                            $sheet->setCellValue('B'.$start, $p->nombrecta);//NOMBRE CTA
                            $sheet->setCellValue('C'.$start, $p->mesoc);//mes oc
                            $sheet->setCellValue('D'.$start, $p->fechaoc);//FECHA OC
                            $sheet->setCellValue('E'.$start, $p->oc);//O/C
                            $sheet->setCellValue('F'.$start, $p->nfactura);//NUMERO FACTURA
                            $sheet->setCellValue('G'.$start, "");
                            $sheet->setCellValue('H'.$start, $p->penalty);
                            $sheet->setCellValue('I'.$start, "");
                            $sheet->setCellValue('J'.$start, $p->penalty);
                            $sheet->setCellValue('K'.$start, $p->barco);//barco
                            $sheet->setCellValue('L'.$start, $p->cccargo);// "C.C CARGO"
                            $sheet->setCellValue('M'.$start, $p->nombre);
                            $sheet->setCellValue('N'.$start, $p->rut);
                            $sheet->setCellValue('O'.$start, $p->cargo);
                            $sheet->setCellValue('P'.$start, $p->tramo);
                            $sheet->setCellValue('Q'.$start, $p->aerolinea);
                            $sheet->setCellValue('R'.$start, $p->codigo);
                            $sheet->setCellValue('S'.$start, $p->ruta);
                            $sheet->setCellValue('T'.$start, $p->mesvuelo);
                            $sheet->setCellValue('U'.$start, $p->fechavuelo);
                            $sheet->setCellValue('V'.$start, date("H:i",strtotime($p->departure_time )));
                            $sheet->setCellValue('W'.$start, date("H:i",strtotime($p->arrival_time )));
                            $sheet->setCellValue('X'.$start, $p->mesvuelo);//uso real
                            $sheet->setCellValue('Y'.$start, $p->barco);//cc final
                            $sheet->setCellValue('Z'.$start, $condicion);//CONDICION
                            $sheet->setCellValue('AA'.$start, $p->fsolicitud);//FECHA SOLICITUD
                            $sheet->setCellValue('AB'.$start, $p->observation);//OBSERVACION
                            $sheet->setCellValue('AC'.$start, $p->estadopasaje);//PASAJE COMPRADO/STOCK/VOUCHER
                            $sheet->setCellValue('AD'.$start, "Multa");//PASAJE COMPRADO/STOCK/VOUCHER
                            $sheet->setCellValue('AE'.$start, $vouchervalue);//es voucher
                            $sheet->setCellValue('AF'.$start, $esvoucher);//es voucher
                            $start = $start +1;
                        }

                        if($p->carry_price<>"" && $p->carry_price>0){
                            $sheet->setCellValue('A'.$start, $p->ncta);//Nº CTA
                            $sheet->setCellValue('B'.$start, $p->nombrecta);//NOMBRE CTA
                            $sheet->setCellValue('C'.$start, $p->mesoc);//mes oc
                            $sheet->setCellValue('D'.$start, $p->fechaoc);//FECHA OC
                            $sheet->setCellValue('E'.$start, $p->oc);//O/C
                            $sheet->setCellValue('F'.$start, $p->nfactura);//NUMERO FACTURA
                            $sheet->setCellValue('G'.$start, "");
                            $sheet->setCellValue('H'.$start, "");
                            $sheet->setCellValue('I'.$start, $negativo.$p->carry_price);
                            $sheet->setCellValue('J'.$start, $negativo.$p->carry_price);
                            $sheet->setCellValue('K'.$start, $p->barco);//barco
                            $sheet->setCellValue('L'.$start, $p->cccargo);// "C.C CARGO"
                            $sheet->setCellValue('M'.$start, $p->nombre);
                            $sheet->setCellValue('N'.$start, $p->rut);
                            $sheet->setCellValue('O'.$start, $p->cargo);
                            $sheet->setCellValue('P'.$start, $p->tramo);
                            $sheet->setCellValue('Q'.$start, $p->aerolinea);
                            $sheet->setCellValue('R'.$start, $p->codigo);
                            $sheet->setCellValue('S'.$start, $p->ruta);
                            $sheet->setCellValue('T'.$start, $p->mesvuelo);
                            $sheet->setCellValue('U'.$start, $p->fechavuelo);
                            $sheet->setCellValue('V'.$start, date("H:i",strtotime($p->departure_time )));
                            $sheet->setCellValue('W'.$start, date("H:i",strtotime($p->arrival_time )));
                            $sheet->setCellValue('X'.$start, $p->mesvuelo);//uso real
                            $sheet->setCellValue('Y'.$start, $p->barco);//cc final
                            $sheet->setCellValue('Z'.$start, $condicion);//CONDICION
                            $sheet->setCellValue('AA'.$start, $p->fsolicitud);//FECHA SOLICITUD
                            $sheet->setCellValue('AB'.$start, $p->observation);//OBSERVACION
                            $sheet->setCellValue('AC'.$start, $p->estadopasaje);//PASAJE COMPRADO/STOCK/VOUCHER
                            $sheet->setCellValue('AD'.$start, "Maleta");//PASAJE COMPRADO/STOCK/VOUCHER
                            $sheet->setCellValue('AE'.$start, $vouchervalue);//es voucher
                            $sheet->setCellValue('AF'.$start, $esvoucher);//es voucher
                            $start = $start +1;

                           /* if($condicion=="Stock" || $condicion=="Cambiado"){
                                $sheet->setCellValue('A'.$start, $p->ncta);//Nº CTA
                                $sheet->setCellValue('B'.$start, $p->nombrecta);//NOMBRE CTA
                                $sheet->setCellValue('C'.$start, $p->mesoc);//mes oc
                                $sheet->setCellValue('D'.$start, $p->fechaoc);//FECHA OC
                                $sheet->setCellValue('E'.$start, $p->oc);//O/C
                                $sheet->setCellValue('F'.$start, $p->nfactura);//NUMERO FACTURA
                                $sheet->setCellValue('G'.$start, "");
                                $sheet->setCellValue('H'.$start, "");
                                $sheet->setCellValue('I'.$start, "-".$p->carry_price);
                                $sheet->setCellValue('J'.$start, "-".$p->carry_price);
                                $sheet->setCellValue('K'.$start, $p->barco);//barco
                                $sheet->setCellValue('L'.$start, $p->cccargo);// "C.C CARGO"
                                $sheet->setCellValue('M'.$start, $p->nombre);
                                $sheet->setCellValue('N'.$start, $p->rut);
                                $sheet->setCellValue('O'.$start, $p->cargo);
                                $sheet->setCellValue('P'.$start, $p->tramo);
                                $sheet->setCellValue('Q'.$start, $p->aerolinea);
                                $sheet->setCellValue('R'.$start, $p->codigo);
                                $sheet->setCellValue('S'.$start, $p->ruta);
                                $sheet->setCellValue('T'.$start, $p->mesvuelo);
                                $sheet->setCellValue('U'.$start, $p->fechavuelo);
                                $sheet->setCellValue('V'.$start, date("H:i",strtotime($p->departure_time )));
                                $sheet->setCellValue('W'.$start, date("H:i",strtotime($p->arrival_time )));
                                $sheet->setCellValue('X'.$start, $p->mesvuelo);//uso real
                                $sheet->setCellValue('Y'.$start, $p->barco);//cc final
                                $sheet->setCellValue('Z'.$start, $condicion);//CONDICION
                                $sheet->setCellValue('AA'.$start, $p->fsolicitud);//FECHA SOLICITUD
                                $sheet->setCellValue('AB'.$start, $p->observation);//OBSERVACION
                                $sheet->setCellValue('AC'.$start, $p->estadopasaje);//PASAJE COMPRADO/STOCK/VOUCHER
                                $sheet->setCellValue('AD'.$start, "Maleta");//PASAJE COMPRADO/STOCK/VOUCHER
                                $sheet->setCellValue('AE'.$start, $vouchervalue);//es voucher
                                $sheet->setCellValue('AF'.$start, $esvoucher);//es voucher
                                $start = $start +1;
                            }*/

                           
                        }


                    }
                   

            $sheet->getColumnDimension("A")->setAutoSize(true);
            $sheet->getColumnDimension("B")->setAutoSize(true);
            $sheet->getColumnDimension("C")->setAutoSize(true);
            $sheet->getColumnDimension("D")->setAutoSize(true);
            $sheet->getColumnDimension("E")->setAutoSize(true);
            $sheet->getColumnDimension("F")->setAutoSize(true);
            $sheet->getColumnDimension("G")->setAutoSize(true);
            $sheet->getColumnDimension("H")->setAutoSize(true);
            $sheet->getColumnDimension("I")->setAutoSize(true);
            $sheet->getColumnDimension("J")->setAutoSize(true);
            $sheet->getColumnDimension("K")->setAutoSize(true);
            $sheet->getColumnDimension("L")->setAutoSize(true);
            $sheet->getColumnDimension("M")->setAutoSize(true);
            $sheet->getColumnDimension("N")->setAutoSize(true);
            $sheet->getColumnDimension("O")->setAutoSize(true);
            $sheet->getColumnDimension("P")->setAutoSize(true);
            $sheet->getColumnDimension("Q")->setAutoSize(true);
            $sheet->getColumnDimension("R")->setAutoSize(true);
            $sheet->getColumnDimension("S")->setAutoSize(true);
            $sheet->getColumnDimension("T")->setAutoSize(true);
            $sheet->getColumnDimension("U")->setAutoSize(true);
            $sheet->getColumnDimension("V")->setAutoSize(true);
            $sheet->getColumnDimension("W")->setAutoSize(true);
            $sheet->getColumnDimension("X")->setAutoSize(true);
            $sheet->getColumnDimension("Y")->setAutoSize(true);
            $sheet->getColumnDimension("Z")->setAutoSize(true);
            $sheet->getColumnDimension("AA")->setAutoSize(true);
            $sheet->getColumnDimension("AB")->setAutoSize(true);
            $sheet->getColumnDimension("AC")->setAutoSize(true);
            $sheet->getColumnDimension("AD")->setAutoSize(true);
            $sheet->getColumnDimension("AE")->setAutoSize(true);
            $sheet->getColumnDimension("AF")->setAutoSize(true);
            
            $start = $start -1;
            $sheet->getStyle('A1:AF'.$start)->applyFromArray($styleArray);
            $sheet->getStyle('A1:AF'.$start)->getAlignment()->setHorizontal('center');

        


        $dt1 = new DateTime();
        $today = $dt1->format("Y-m-d");
        try {
            $writer = new Xlsx($spreadsheet);
            $file = 'Historial pasajes entre '.$d1.' y '.$d2.'.xlsx';
            header("Content-Description: File Transfer");
            header('Content-Disposition: attachment; filename="' . $file . '"');
            header('Content-type: application/vnd.ms-excel');
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Expires: 0');     
            $writer->save("php://output");
        } catch (\PHPExcel_Reader_Exception $e) {
            return response("Error al generar documento solicitado", 500);
        }
        
    }

    public function generateDisponibles(){
     
        
        $workers = Worker::waitingRoom()->orderBy('name','asc')->get();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $styleArray = array(
            'borders' => array(
                'outline' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $sheet->setCellValue('A1', "Estado");
        $sheet->setCellValue('B1', "Nombre");
        $sheet->setCellValue('C1', "Rut");
        $sheet->setCellValue('D1', "Cargo");
        $sheet->setCellValue('E1', "Ultima Embarcacion");
        $sheet->setCellValue('F1', "Fecha Ultima Embarcación");
        $sheet->getStyle('A1:F1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:F1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:F1')->getFont()->setBold(true);
        $start = 2;

        foreach ($workers as $key => $worker) {
            $worker->attribute_name = "(Sin Atributo)";
            if($worker->attribute_id<>10){
                
                $workers->forget($key);
            }else{
               // $atributo = DB::table("attributes")->where("id",$worker->attribute_id)->first();
                $worker->attribute_name = "Disponible";
            }
            


            /*$status = Worker::getWorkerEvaluation($worker->id,0);
            $worker->status = $status;

            if($status['status']=="Ok" || $status['status']=="Observaciones"){
                $workers->forget($key);
            }*/

        }

        foreach($workers as $w){
            $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$w->id)->get();
            $rol = "Sin Rol";
            foreach($roles as $role){
                if($rol == "Sin Rol"){
                    $rol = "";
                }
                if($rol==""){
                    $rol = $role->name;
                }else{
                    $rol = $rol.",".$role->name;
                }
            }

            $barco = "";
            $fecha = "";
            $last = DB::table("crew_worker")->where("worker_id",$w->id)->orderBy("id","desc")->first();
            if($last){
                $crew = DB::table("crews")->where("id",$last->crew_id)->first();
                if($crew){
                    $fecha = $crew->download_date;
                    $ship = DB::table("ships")->where("id",$crew->ship_id)->first();
                    if($ship){
                        $barco = $ship->name;
                    }
                }
            }

            $rut = self::rut($w->rut);
            $sheet->setCellValue('A'.$start, $w->attribute_name);
            $sheet->setCellValue('B'.$start, $w->name." ".$w->last_name);
            $sheet->setCellValue('C'.$start, $rut);
            $sheet->setCellValue('D'.$start, $rol);
            $sheet->setCellValue('E'.$start, $barco);
            $sheet->setCellValue('F'.$start, $fecha);
            $sheet->getStyle('A'.$start.':F'.$start)->applyFromArray($styleArray);
            $sheet->getStyle('A'.$start.':F'.$start)->getAlignment()->setHorizontal('center');
            $start = $start +1;
        }

        foreach(range('A','F') as $columnID) {
            $sheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }


        $dt1 = new DateTime();
        $today = $dt1->format("Y-m-d");
        try {
            $writer = new Xlsx($spreadsheet);
            $file = 'Tripulantes Disponibles '.$today.'.xlsx';
            header("Content-Description: File Transfer");
            header('Content-Disposition: attachment; filename="' . $file . '"');
            header('Content-type: application/vnd.ms-excel');
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Expires: 0');     
            $writer->save("php://output");
        } catch (\PHPExcel_Reader_Exception $e) {
            return response("Error al generar documento solicitado", 500);
        }
        
    }

    public function generateExpired(){
        $dt1 = new DateTime();
        $today = $dt1->format("Y-m-d");

        $documents = DB::table("documents")->join("workers","workers.id","=","documents.worker_id")->join("document_types","document_types.id","=","documents.document_type_id")
                ->select("workers.rut","workers.id","workers.name","workers.last_name","document_types.name as document","documents.expiration_date")->where("workers.deleted_at",null)->where("expiration_date","<=",$today)->groupBy("document_types.id")->orderBy('expiration_date','asc')->get();
    

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $styleArray = array(
            'borders' => array(
                'outline' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        //inicio documentos
        $sheet->setCellValue('A1', "Nombre");
        $sheet->setCellValue('B1', "Apellido");
        $sheet->setCellValue('C1', "Rut");
        $sheet->setCellValue('D1', "Rol");
        $sheet->setCellValue('E1', "Documento");
        $sheet->setCellValue('F1', "Fecha Expiracion");
        $sheet->getStyle('A1:F1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:F1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:F1')->getFont()->setBold(true);
        $start = 2;
        foreach($documents as $d){
            $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$d->id)->get();
            $rol = "Sin Rol";
            foreach($roles as $role){
                if($rol == "Sin Rol"){
                    $rol = "";
                }
                if($rol==""){
                    $rol = $role->name;
                }else{
                    $rol = $rol.",".$role->name;
                }
            }
            $rut = self::rut($d->rut);
            $sheet->setCellValue('A'.$start, $d->name);
            $sheet->setCellValue('B'.$start, $d->last_name);
            $sheet->setCellValue('C'.$start, $rut);
            $sheet->setCellValue('D'.$start, $rol);
            $sheet->setCellValue('E'.$start, $d->document);
            $sheet->setCellValue('F'.$start, $d->expiration_date);
            $sheet->getStyle('A'.$start.':F'.$start)->applyFromArray($styleArray);
            $sheet->getStyle('A'.$start.':F'.$start)->getAlignment()->setHorizontal('center');
            $start = $start +1;
        }
        foreach(range('A','F') as $columnID) {
            $sheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        $sheet->setTitle("Documentos");
        //fin documentos

        //examen ocupacional
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(1);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Examenes Ocupacionales");

        $examens = DB::table("occupational_exams")->join("workers","workers.id","=","occupational_exams.worker_id")
        ->select("workers.id","workers.rut","workers.name","workers.last_name","occupational_exams.expiration_date")->where("workers.deleted_at",null)->where("expiration_date","<=",$today)->groupBy("workers.id")->orderBy('expiration_date','asc')->get();

        $sheet->setCellValue('A1', "Nombre");
        $sheet->setCellValue('B1', "Apellido");
        $sheet->setCellValue('C1', "Rut");
        $sheet->setCellValue('D1', "Rol");
        $sheet->setCellValue('E1', "Fecha Expiracion");
        $sheet->getStyle('A1:E1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:E1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:E1')->getFont()->setBold(true);
        $start = 2;
        foreach($examens as $d){
            $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$d->id)->get();
            $rol = "Sin Rol";
            foreach($roles as $role){
                if($rol == "Sin Rol"){
                    $rol = "";
                }
                if($rol==""){
                    $rol = $role->name;
                }else{
                    $rol = $rol.",".$role->name;
                }
            }
            $rut = self::rut($d->rut);
            $sheet->setCellValue('A'.$start, $d->name);
            $sheet->setCellValue('B'.$start, $d->last_name);
            $sheet->setCellValue('C'.$start, $rut);
            $sheet->setCellValue('D'.$start, $rol);
            $sheet->setCellValue('E'.$start, $d->expiration_date);
            $sheet->getStyle('A'.$start.':E'.$start)->applyFromArray($styleArray);
            $sheet->getStyle('A'.$start.':E'.$start)->getAlignment()->setHorizontal('center');
            $start = $start +1;
        }
        foreach(range('A','E') as $columnID) {
            $sheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        //fin examen ocupacional


         //documentos faltates
         $spreadsheet->createSheet();
         $spreadsheet->setActiveSheetIndex(2);
         $sheet = $spreadsheet->getActiveSheet();
         $sheet->setTitle("Documentos Faltantes");
 
         $workers = DB::table("workers")->where("deleted_at",null)->get();
         $sheet->setCellValue('A1', "Nombre");
         $sheet->setCellValue('B1', "Apellido");
         $sheet->setCellValue('C1', "Rut");
         $sheet->setCellValue('D1', "Rol");
         $sheet->setCellValue('E1', "Documento");
         $sheet->getStyle('A1:E1')->applyFromArray($styleArray);
         $sheet->getStyle('A1:E1')->getAlignment()->setHorizontal('center');
         $sheet->getStyle('A1:E1')->getFont()->setBold(true);
         $start = 2;
         foreach($workers as $d){
            $carnet = DB::table("documents")->where("worker_id",$d->id)->where("document_type_id","1")->first();
            if(!$carnet){
                $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$d->id)->get();
                $rol = "Sin Rol";
                foreach($roles as $role){
                    if($rol == "Sin Rol"){
                        $rol = "";
                    }
                    if($rol==""){
                        $rol = $role->name;
                    }else{
                        $rol = $rol.",".$role->name;
                    }
                }
                $rut = self::rut($d->rut);
                $sheet->setCellValue('A'.$start, $d->name);
                $sheet->setCellValue('B'.$start, $d->last_name);
                $sheet->setCellValue('C'.$start, $rut);
                $sheet->setCellValue('D'.$start, $rol);
                $sheet->setCellValue('E'.$start, "Carnet de Identidad");
                $sheet->getStyle('A'.$start.':E'.$start)->applyFromArray($styleArray);
                $sheet->getStyle('A'.$start.':E'.$start)->getAlignment()->setHorizontal('center');
                $start = $start+1;
            }
            $matricula = DB::table("documents")->where("worker_id",$d->id)->where("document_type_id","2")->first();
            if(!$matricula){
                $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$d->id)->get();
                $rol = "Sin Rol";
                foreach($roles as $role){
                    if($rol == "Sin Rol"){
                        $rol = "";
                    }
                    if($rol==""){
                        $rol = $role->name;
                    }else{
                        $rol = $rol.",".$role->name;
                    }
                }
                $rut = self::rut($d->rut);
                $sheet->setCellValue('A'.$start, $d->name);
                $sheet->setCellValue('B'.$start, $d->last_name);
                $sheet->setCellValue('C'.$start, $rut);
                $sheet->setCellValue('D'.$start, $rol);
                $sheet->setCellValue('E'.$start, "Matrícula de embarque");
                $sheet->getStyle('A'.$start.':E'.$start)->applyFromArray($styleArray);
                $sheet->getStyle('A'.$start.':E'.$start)->getAlignment()->setHorizontal('center');
                $start = $start+1;
            }
            $reglamento = DB::table("documents")->where("worker_id",$d->id)->where("document_type_id","3")->first();
            if(!$reglamento){
                $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$d->id)->get();
                $rol = "Sin Rol";
                foreach($roles as $role){
                    if($rol == "Sin Rol"){
                        $rol = "";
                    }
                    if($rol==""){
                        $rol = $role->name;
                    }else{
                        $rol = $rol.",".$role->name;
                    }
                }
                $rut = self::rut($d->rut);
                $sheet->setCellValue('A'.$start, $d->name);
                $sheet->setCellValue('B'.$start, $d->last_name);
                $sheet->setCellValue('C'.$start, $rut);
                $sheet->setCellValue('D'.$start, $rol);
                $sheet->setCellValue('E'.$start, "Reglamento interno");
                $sheet->getStyle('A'.$start.':E'.$start)->applyFromArray($styleArray);
                $sheet->getStyle('A'.$start.':E'.$start)->getAlignment()->setHorizontal('center');
                $start = $start+1;
            }
            $seguro = DB::table("documents")->where("worker_id",$d->id)->where("document_type_id","4")->first();
            if(!$seguro){
                $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$d->id)->get();
                $rol = "Sin Rol";
                foreach($roles as $role){
                    if($rol == "Sin Rol"){
                        $rol = "";
                    }
                    if($rol==""){
                        $rol = $role->name;
                    }else{
                        $rol = $rol.",".$role->name;
                    }
                }
                $rut = self::rut($d->rut);
                $sheet->setCellValue('A'.$start, $d->name);
                $sheet->setCellValue('B'.$start, $d->last_name);
                $sheet->setCellValue('C'.$start, $rut);
                $sheet->setCellValue('D'.$start, $rol);
                $sheet->setCellValue('E'.$start, "Seguro de vida");
                $sheet->getStyle('A'.$start.':E'.$start)->applyFromArray($styleArray);
                $sheet->getStyle('A'.$start.':E'.$start)->getAlignment()->setHorizontal('center');
                $start = $start+1;
            }
            $seguro = DB::table("occupational_exams")->where("worker_id",$d->id)->first();
            if(!$seguro){
                $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$d->id)->get();
                $rol = "Sin Rol";
                foreach($roles as $role){
                    if($rol == "Sin Rol"){
                        $rol = "";
                    }
                    if($rol==""){
                        $rol = $role->name;
                    }else{
                        $rol = $rol.",".$role->name;
                    }
                }
                $rut = self::rut($d->rut);
                $sheet->setCellValue('A'.$start, $d->name);
                $sheet->setCellValue('B'.$start, $d->last_name);
                $sheet->setCellValue('C'.$start, $rut);
                $sheet->setCellValue('D'.$start, $rol);
                $sheet->setCellValue('E'.$start, "Examen Ocupacional");
                $sheet->getStyle('A'.$start.':E'.$start)->applyFromArray($styleArray);
                $sheet->getStyle('A'.$start.':E'.$start)->getAlignment()->setHorizontal('center');
                $start = $start+1;
            }

         }
         foreach(range('A','E') as $columnID) {
             $sheet->getColumnDimension($columnID)
                 ->setAutoSize(true);
         }
         //fin documentos faltantes

    

        try {
            $writer = new Xlsx($spreadsheet);
            $file = 'Documentos Vencidos y Faltantes'.$today.'.xlsx';
            header("Content-Description: File Transfer");
            header('Content-Disposition: attachment; filename="' . $file . '"');
            header('Content-type: application/vnd.ms-excel');
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Expires: 0');     
            $writer->save("php://output");
        } catch (\PHPExcel_Reader_Exception $e) {
            return response("Error al generar documento solicitado", 500);
        }
    }

    public function generateExpireNextMonth(){
        $dt1 = new DateTime();
        $today = $dt1->format("Y-m-d");

        $dt2 = new DateTime("+1 month");
        $date = $dt2->format("Y-m");
        $date_full = $dt2->format("Y-m-d");
        $documents = DB::table("documents")->join("workers","workers.id","=","documents.worker_id")->join("document_types","document_types.id","=","documents.document_type_id")
                ->select("workers.id","workers.rut","workers.name","workers.last_name","document_types.name as document","documents.expiration_date","documents.document_type_id")->where("expiration_date","like",$date."%")->groupBy("document_types.id")->orderBy('expiration_date','asc')->get();

             
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $styleArray = array(
            'borders' => array(
                'outline' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
  

        //inicio documentos
        $sheet->setCellValue('A1', "Nombre");
        $sheet->setCellValue('B1', "Apellido");
        $sheet->setCellValue('C1', "Rut");
        $sheet->setCellValue('D1', "Rol");
        $sheet->setCellValue('E1', "Documento");
        $sheet->setCellValue('F1', "Fecha Expiracion");
        $sheet->getStyle('A1:F1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:F1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:F1')->getFont()->setBold(true);
        $start = 2;
        foreach($documents as $d){
            $existe = DB::table("documents")->join("workers","workers.id","=","documents.worker_id")->join("document_types","document_types.id","=","documents.document_type_id")
            ->select("workers.id","workers.rut","workers.name","workers.last_name","document_types.name as document","documents.expiration_date")->where("document_types.id",$d->document_type_id)->where("workers.id",$d->id)->where("expiration_date",">",$date_full)->groupBy("document_types.id")->orderBy('expiration_date','asc')->first();
            if($existe){

            }else{
                
            
            $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$d->id)->get();
            $rol = "Sin Rol";
            foreach($roles as $role){
                if($rol == "Sin Rol"){
                    $rol = "";
                }
                if($rol==""){
                    $rol = $role->name;
                }else{
                    $rol = $rol.",".$role->name;
                }
            }
            $rut = self::rut($d->rut);
            $sheet->setCellValue('A'.$start, $d->name);
            $sheet->setCellValue('B'.$start, $d->last_name);
            $sheet->setCellValue('C'.$start, $rut);
            $sheet->setCellValue('D'.$start, $rol);
            $sheet->setCellValue('E'.$start, $d->document);
            $sheet->setCellValue('F'.$start, $d->expiration_date);
            $sheet->getStyle('A'.$start.':F'.$start)->applyFromArray($styleArray);
            $sheet->getStyle('A'.$start.':F'.$start)->getAlignment()->setHorizontal('center');
            $start = $start +1;
            }
        }
        foreach(range('A','F') as $columnID) {
            $sheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        $sheet->setTitle("Documentos");
        //fin documentos

        //inicio inducciones
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(1);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Inducciones");

        $examens = DB::table("induction_workers")->join("workers","workers.id","=","induction_workers.worker_id")->join("inductions","inductions.id","=","induction_workers.induction_id")
        ->select("workers.rut","workers.id","induction_workers.id","workers.id as id_worker","workers.name","workers.last_name",'inductions.name as induction',DB::raw("DATE_ADD(induction_workers.induction_date,INTERVAL 1 YEAR) as expiration_date"))->whereRaw("DATE_ADD(induction_workers.induction_date,INTERVAL 1 YEAR) like '".$date."%'")
        ->groupBy("workers.id")->groupBy("inductions.id")->orderBy('expiration_date','asc')->get();

        $sheet->setCellValue('A1', "Nombre");
        $sheet->setCellValue('B1', "Apellido");
        $sheet->setCellValue('C1', "Rut");
        $sheet->setCellValue('D1', "Rol");
        $sheet->setCellValue('E1', "Inducción");
        $sheet->setCellValue('F1', "Fecha Expiracion");
        $sheet->getStyle('A1:F1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:F1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:F1')->getFont()->setBold(true);
        $start = 2;
        foreach($examens as $d){
            $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$d->id)->get();
            $rol = "Sin Rol";
            foreach($roles as $role){
                if($rol == "Sin Rol"){
                    $rol = "";
                }
                if($rol==""){
                    $rol = $role->name;
                }else{
                    $rol = $rol.",".$role->name;
                }
            }
            $rut = self::rut($d->rut);
            $sheet->setCellValue('A'.$start, $d->name);
            $sheet->setCellValue('B'.$start, $d->last_name);
            $sheet->setCellValue('C'.$start, $rut);
            $sheet->setCellValue('D'.$start, $rol);
            $sheet->setCellValue('E'.$start, $d->induction);
            $sheet->setCellValue('F'.$start, $d->expiration_date);
            $sheet->getStyle('A'.$start.':F'.$start)->applyFromArray($styleArray);
            $sheet->getStyle('A'.$start.':F'.$start)->getAlignment()->setHorizontal('center');
            $start = $start +1;
        }
        foreach(range('A','F') as $columnID) {
            $sheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        //fin inducciones




        //examen ocupacional
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(2);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Examenes Ocupacionales");

        $examens = DB::table("occupational_exams")->join("workers","workers.id","=","occupational_exams.worker_id")
        ->select("workers.rut","workers.id","workers.name","workers.last_name","occupational_exams.expiration_date")->where("expiration_date","like",$date."%")->groupBy("workers.id")->orderBy('expiration_date','asc')->get();

        $sheet->setCellValue('A1', "Nombre");
        $sheet->setCellValue('B1', "Apellido");
        $sheet->setCellValue('C1', "Rut");
        $sheet->setCellValue('D1', "Rol");
        $sheet->setCellValue('E1', "Fecha Expiracion");
        $sheet->getStyle('A1:E1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:E1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:E1')->getFont()->setBold(true);
        $start = 2;
        foreach($examens as $d){
            $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$d->id)->get();
            $rol = "Sin Rol";
            foreach($roles as $role){
                if($rol == "Sin Rol"){
                    $rol = "";
                }
                if($rol==""){
                    $rol = $role->name;
                }else{
                    $rol = $rol.",".$role->name;
                }
            }
            $rut = self::rut($d->rut);
            $sheet->setCellValue('A'.$start, $d->name);
            $sheet->setCellValue('B'.$start, $d->last_name);
            $sheet->setCellValue('C'.$start, $rut);
            $sheet->setCellValue('D'.$start, $rol);
            $sheet->setCellValue('E'.$start, $d->expiration_date);
            $sheet->getStyle('A'.$start.':E'.$start)->applyFromArray($styleArray);
            $sheet->getStyle('A'.$start.':E'.$start)->getAlignment()->setHorizontal('center');
            $start = $start +1;
        }
        foreach(range('A','E') as $columnID) {
            $sheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        //fin examen ocupacional

        try {
            $writer = new Xlsx($spreadsheet);
            $file = 'Documentos por vencer mes '.self::get_nombre_mes($date).'.xlsx';
            header("Content-Description: File Transfer");
            header('Content-Disposition: attachment; filename="' . $file . '"');
            header('Content-type: application/vnd.ms-excel');
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Expires: 0');     
            $writer->save("php://output");
        } catch (\PHPExcel_Reader_Exception $e) {
            return response("Error al generar documento solicitado", 500);
        }
    }



    public function generateAttributes(){
        $dt1 = new DateTime();
        $today = $dt1->format("Y-m-d");

        $atributos =  DB::table("worker_attributes")->join("workers","workers.id","=","worker_attributes.worker_id")->join("attributes","attributes.id","=","worker_attributes.attribute_id")
                    ->join("crews","crews.id","=","worker_attributes.crew_id")->join("ships","ships.id","=","crews.ship_id")->join("clients","clients.id","=","crews.client_id")
                    ->select("workers.id","workers.rut","attributes.name as atributo","workers.name as nombre","workers.last_name as apellido","ships.name as barco","clients.name as cliente","worker_attributes.date_start as inicio","worker_attributes.date_end as fin","worker_attributes.created_at as creado")
                    ->orderBy("worker_attributes.created_at","desc")
                    ->orderBy("worker_attributes.id","desc")
                    ->get();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $styleArray = array(
            'borders' => array(
                'outline' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        //inicio documentos
        $sheet->setCellValue('A1', "Nombre");
        $sheet->setCellValue('B1', "Apellido");
        $sheet->setCellValue('C1', "Rut");
        $sheet->setCellValue('D1', "Rol");
        $sheet->setCellValue('E1', "Atributo");
        $sheet->setCellValue('F1', "Fecha Inicio");
        $sheet->setCellValue('G1', "Fecha Fin");
        $sheet->setCellValue('H1', "Embarcación");
        $sheet->setCellValue('I1', "Cliente");
        $sheet->setCellValue('J1', "Agregado");
        $sheet->getStyle('A1:J1')->applyFromArray($styleArray);
        $sheet->getStyle('A1:J1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:J1')->getFont()->setBold(true);
        $start = 2;
        foreach($atributos as $d){

            $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$d->id)->get();
                $rol = "Sin Rol";
                foreach($roles as $role){
                    if($rol == "Sin Rol"){
                        $rol = "";
                    }
                    if($rol==""){
                        $rol = $role->name;
                    }else{
                        $rol = $rol.",".$role->name;
                    }
                }
                $rut = self::rut($d->rut);

                
            $sheet->setCellValue('A'.$start, $d->nombre);
            $sheet->setCellValue('B'.$start, $d->apellido);
            $sheet->setCellValue('C'.$start, $rut);
            $sheet->setCellValue('D'.$start, $rol);
            $sheet->setCellValue('E'.$start, $d->atributo);
            $sheet->setCellValue('F'.$start, $d->inicio);
            $sheet->setCellValue('G'.$start, $d->fin);
            $sheet->setCellValue('H'.$start, $d->barco);
            $sheet->setCellValue('I'.$start, $d->cliente);
            $sheet->setCellValue('J'.$start, $d->creado);
            $sheet->getStyle('A'.$start.':J'.$start)->applyFromArray($styleArray);
            $sheet->getStyle('A'.$start.':J'.$start)->getAlignment()->setHorizontal('center');
            $start = $start +1;
        }
        foreach(range('A','J') as $columnID) {
            $sheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        $sheet->setTitle("Atributos");
        //fin documentos


    

        try {
            $writer = new Xlsx($spreadsheet);
            $file = 'Atributos '.$today.'.xlsx';
            header("Content-Description: File Transfer");
            header('Content-Disposition: attachment; filename="' . $file . '"');
            header('Content-type: application/vnd.ms-excel');
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Expires: 0');     
            $writer->save("php://output");
        } catch (\PHPExcel_Reader_Exception $e) {
            return response("Error al generar documento solicitado", 500);
        }
    }

    public function getColumnRange($start, $end) {
        $columns = array();
        while ($start !== $end) {
            $columns[] = $start;
            if ($start === 'Z') {
                $start = 'AA';
            } else {
                $start++;
            }
        }
        $columns[] = $end;
        return $columns;
    }

    public function incrementLetters($input) {
        $length = strlen($input);
        $carry = 1;
    
        for ($i = $length - 1; $i >= 0; $i--) {
            $char = $input[$i];
    
            if ($carry) {
                if ($char === 'Z') {
                    $input[$i] = 'A';
                } else {
                    $input[$i] = chr(ord($char) + 1);
                    $carry = 0;
                }
            }
        }
    
        if ($carry) {
            $input = 'A' . $input;
        }
    
        return $input;
    }

    public function generateAntiguedad(){
        $dt1 = new DateTime();
        $today = $dt1->format("Y-m-d");

        $atributos =  DB::table("workers")->join("contracts","contracts.worker_id","=","workers.id")->select('workers.*','contracts.worker_id','contracts.date','contracts.salary','contracts.identity_card_copy','contracts.boarding_license','contracts.afp_id','contracts.health_forecast_id','contracts.contract_type_id','contracts.uploader_id')->get();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $styleArray = array(
            'borders' => array(
                'outline' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $styleArrayHeader = array(
            'borders' => array(
                'outline' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
            'fill' => array(
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => array('rgb' => '002060'), // Blue background color
            ),
            'font' => array(
                'color' => array('rgb' => 'FFFFFF'), // White text color
            ),
        );
        $styleArrayCap = array(
            'borders' => array(
                'outline' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
            'fill' => array(
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => array('rgb' => 'FFEA00'), // Blue background color
            ),
            'font' => array(
                'color' => array('rgb' => '000000'), // White text color
            ),
        );
        $inducciones = DB::table("inductions")->whereNull("deleted_at")->get();

        //inicio documentos
        $sheet->setCellValue('A2', "Nombre");
        $sheet->setCellValue('B2', "Apellido");
        $sheet->setCellValue('C2', "Rut");
        $sheet->setCellValue('D2', "Cargo");
        $sheet->setCellValue('E2', "Edad");
        $sheet->setCellValue('F2', "Antiguedad Laboral");
        $sheet->setCellValue('G2', "Telefono");
        $sheet->setCellValue('H2', "Dirección");
        $sheet->setCellValue('I2', "Ciudad");
        $sheet->setCellValue('J2', "Vcto.CI");
        $sheet->setCellValue('K2', "Fecha contrato");
        $sheet->setCellValue('L2', "Término"); //no
        $sheet->setCellValue('M2', "Tipo contrato");
        $sheet->setCellValue('N2', "Licencia medica");
        $sheet->setCellValue('O2', "Descanso");
        $sheet->setCellValue('P2', "Vacaciones");
        $sheet->setCellValue('Q2', "Permiso sin gose de sueldo");
        $sheet->setCellValue('R2', "Permiso con gose de sueldo");
        $sheet->setCellValue('S2', "Pacto horas extras"); // no
        $sheet->setCellValue('T2', "Fecha Vencimiento reglamento interno");
        $sheet->setCellValue('U2', "AFP");
        $sheet->setCellValue('V2', "Salud");
        $sheet->setCellValue('W2', "Vcto seguro de vida");
        $sheet->setCellValue('X2', "Vcto. Matricula");
        $sheet->setCellValue('Y2', "Vcto radio operador"); //no
        $sheet->setCellValue('Z2', "Permiso embarco"); //no
        $sheet->setCellValue('AA2', "Vcto. Examen Ocupacional");

        $sheet->getRowDimension(2)->setRowHeight(30);

        $letter = "AA";
        foreach($inducciones as $induccion){
            $letter = self::incrementLetters($letter);
            $sheet->setCellValue($letter."2", $induccion->name);
            $sheet->setCellValue($letter."1", 'Capacitaciones');
        }
        $sheet->mergeCells('AB1:'.$letter.'1');
        $sheet->getStyle('AB1')->applyFromArray($styleArrayCap);
        $sheet->getStyle('AB1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('AB1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('A2:'.$letter.'2')->applyFromArray($styleArrayHeader);
        $sheet->getStyle('A2:'.$letter.'2')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A2:'.$letter.'2')->getFont()->setBold(true);
        $start = 3;
        foreach($atributos as $d){
            $letterStart = "AA";
            if($d->date<>null){
                $roles = DB::table("role_worker")->join("roles","roles.id","=","role_worker.role_id")->where("role_worker.worker_id",$d->id)->get();
                $rol = "Sin Rol";
                foreach($roles as $role){
                    if($rol == "Sin Rol"){
                        $rol = "";
                    }
                    if($rol==""){
                        $rol = $role->name;
                    }else{
                        $rol = $rol.",".$role->name;
                    }
                }
                $rut = self::rut($d->rut);
            
                $firstDate  = new DateTime($d->birthdate);
                $secondDate = new DateTime(date("Y-m-d"));
                $intvl = $firstDate->diff($secondDate);

                 //  $edad = $intvl->y . " años, " . $intvl->m." meses y ".$intvl->d." días";
                $edad = $intvl->y . " años";
                    
                $sheet->setCellValue('A'.$start, $d->name);
                $sheet->setCellValue('B'.$start, $d->last_name);
                $sheet->setCellValue('C'.$start, $rut);
                $sheet->setCellValue('D'.$start, $rol);
                $sheet->setCellValue('E'.$start, $edad);


                $firstDate  = new DateTime($d->date);
                $secondDate = new DateTime(date("Y-m-d"));
                $intvl = $firstDate->diff($secondDate);
                $año = $intvl->y . " año";
                if($intvl->y>1){
                    $año = $intvl->y . " años";
                }else{
                    if($intvl->y==0){
                        $año="";
                    }
                }
                $mes = $intvl->m." mes";
                if($intvl->m>1){
                    $mes = $intvl->m . " meses";
                }else{
                    if($intvl->m==0){
                        $mes="";
                    }
                }
                $dia = $intvl->d." día";
                if($intvl->d>1){
                    $dia = $intvl->d . " días";
                }else{
                    if($intvl->d==0){
                        $dia="";
                    }
                }
                $antiguedad = $año." ".$mes." ".$dia;
                

                $sheet->setCellValue('F'.$start, $antiguedad);

                //datos nuevos
                $sheet->setCellValue('G'.$start, $d->phone_number);
                $sheet->setCellValue('H'.$start, $d->address);

                $ciudad = DB::table("communes")->where("id",$d->commune_id)->first();
                $sheet->setCellValue('I'.$start, $ciudad->name);

                $cedula = DB::table("documents")->where("document_type_id","1")->where("worker_id",$d->id)->first();
                if ($cedula) {
                    $sheet->setCellValue('J'.$start, $cedula->expiration_date);
                }

                $contrato = DB::table("contracts")->where("worker_id",$d->id)->first();
                if ($contrato) {
                    $sheet->setCellValue('K'.$start, $contrato->date);
                    $tipocontrato = DB::table("contract_types")->where("id",$contrato->contract_type_id)->first();
                    if ($tipocontrato ) {
                        $sheet->setCellValue('M'.$start, $tipocontrato->name);
                    }
                    $afp = DB::table("afps")->where("id",$contrato->afp_id)->first();
                    if ($afp) {
                        $sheet->setCellValue('U'.$start, $afp->name);
                    }
                    $salud = DB::table("health_forecasts")->where("id",$contrato->health_forecast_id)->first();
                    if ($salud) {
                        $sheet->setCellValue('V'.$start, $salud->name);
                    }
                }

                if ($d->attribute_id == "5" || $d->attribute_id == "6") {
                    $sheet->setCellValue('N'.$start, "X");
                }
                if ($d->attribute_id == "2") {
                    $sheet->setCellValue('O'.$start, "X");
                }
                if ($d->attribute_id == "20") {
                    $sheet->setCellValue('P'.$start, "X");
                }
                if ($d->attribute_id == "4") {
                    $sheet->setCellValue('Q'.$start, "X");
                }
                if ($d->attribute_id == "3") {
                    $sheet->setCellValue('R'.$start, "X");
                }

                $reglamento = DB::table("documents")->where("document_type_id","3")->where("worker_id",$d->id)->first();
                if ($reglamento) {
                    $sheet->setCellValue('T'.$start, $reglamento->expiration_date);
                }

                //agregado desde documentos pero al parecer no es y corresponde a documentos empresa
                $seguro = DB::table("documents")->where("document_type_id","4")->where("worker_id",$d->id)->first();
                if ($seguro) {
                    $sheet->setCellValue('W'.$start, $seguro->expiration_date);
                }
                
                $matricula = DB::table("documents")->where("document_type_id","2")->where("worker_id",$d->id)->first();
                if ($matricula) {
                    $sheet->setCellValue('X'.$start, $matricula->expiration_date);
                }

                $ocupacional = DB::table("occupational_exams")->where("worker_id",$d->id)->first();
                if ($ocupacional) {
                    $sheet->setCellValue('AA'.$start, $ocupacional->expiration_date);
                }

                foreach($inducciones as $induccion){
                    $letterStart = self::incrementLetters($letterStart);
                    $induccion_trabajador = DB::table("induction_workers")->where("induction_id",$induccion->id)->where("worker_id",$d->id)->first();
                    if ($induccion_trabajador) {
                        $sheet->setCellValue($letterStart.$start, $induccion_trabajador->induction_date);
                    }
                }
                //fin datos nuevos

                $sheet->getStyle('A'.$start.':'.$letter.$start)->applyFromArray($styleArray);
                $sheet->getStyle('A'.$start.':'.$letter.$start)->getAlignment()->setHorizontal('center');
                $start = $start +1;
            }
            
        }

        $columns = self::getColumnRange('A', $letter);
        foreach ($columns as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
            $sheet->getStyle($columnID.'2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $sheet->getStyle($columnID.'2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        }
        $sheet->setTitle("Base De Datos Personal");
        //fin documentos

        try {
            $writer = new Xlsx($spreadsheet);
            $file = 'Base datos personal '.$today.'.xlsx';
            header("Content-Description: File Transfer");
            header('Content-Disposition: attachment; filename="' . $file . '"');
            header('Content-type: application/vnd.ms-excel');
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Expires: 0');     
            $writer->save("php://output");
        } catch (\PHPExcel_Reader_Exception $e) {
            return response("Error al generar documento solicitado", 500);
        }
    }

    function rut( $elrut ) {
                $rut = str_replace(".","",$elrut);
                $rut = str_replace("-","",$rut);
                $rut = str_replace(",","",$rut);
        try{
            return number_format( substr ( $rut, 0 , -1 ) , 0, "", ".") . '-' . substr ( $rut, strlen($rut) -1 , 1 );
        }catch(Exception $e){
            return "Rut Inválido";
        }
        
    }


    public function getValueSubreportClone($task_id,$number,$cicle,$clone,$code){
        
        $valor = DB::connection("eskuadconn")->table("sub_reports_values")
                                    ->join("sub_reports","sub_reports.id","=","sub_reports_values.subreport_id")
                                    ->where("sub_reports_values.task_id",$task_id)
                                    ->where("sub_reports_values.number",$number)
                                    ->where("sub_reports_values.cicle",$cicle)
                                    ->where("sub_reports_values.clone",$clone)
                                    ->where("sub_reports.code",$code)
                                    ->select("sub_reports_values.value","sub_reports.type","sub_reports_values.subreport_id","sub_reports_values.cicle")
                                    ->first();


        if($valor){
            if($valor->type<>"3" && $valor->type<>"4" && $valor->type<>"10"){
                return ucfirst($valor->value);
            }

            if($valor->type=="3"){
                $opcion = DB::connection("eskuadconn")->table("options")->where("id",$valor->value)->first();
                if($opcion){
                    return ucfirst($opcion->name);
                }else{
                    return ucfirst($valor->value);
                }
                
            }

            if($valor->type=="4"){
                $opciones = DB::connection("eskuadconn")->table("options_values")
                ->join("options","options.id","=","options_values.option_id")
                ->where("options_values.subreport_id",$valor->subreport_id)
                ->where("options_values.cicle",$valor->cicle)
                ->where("options_values.task_id",$task_id)
                ->where("options_values.number",$number)
                ->select("options.name")
                ->get();
                $respuesta ="";
                foreach($opciones as $o){
                    $respuesta = $respuesta.ucfirst($o->name).",";
                }
                $respuesta = substr($respuesta, 0, -1);
                return $respuesta;
            }

            $opcion = DB::connection("eskuadconn")->table("options")->where("id",$valor->value)->first();
            return ucfirst($opcion->name);
        }else{
            return "";
        }
    }


    public function getValueSubreport($task_id,$number,$cicle,$code){
        
        $valor = DB::connection("eskuadconn")->table("sub_reports_values")
                                    ->join("sub_reports","sub_reports.id","=","sub_reports_values.subreport_id")
                                    ->where("sub_reports_values.task_id",$task_id)
                                    ->where("sub_reports_values.number",$number)
                                    ->where("sub_reports_values.cicle",$cicle)
                                    ->where("sub_reports.code",$code)
                                    ->select("sub_reports_values.value","sub_reports.type")
                                    ->first();


        if($valor){
            if($valor->type<>"3" && $valor->type<>"4"){
                return ucfirst($valor->value);
            }
            

            $opcion = DB::connection("eskuadconn")->table("options")->where("id",$valor->value)->first();
            if($opcion){
                return ucfirst($opcion->name);
            }else{
                return ucfirst($valor->value);
            }
            
        }else{
            return "";
        }
    }

    public function getValueReport($task_id,$number,$cicle,$code){
        
        $valor = DB::connection("eskuadconn")->table("report_task_user")
                                    ->join("reports","reports.id","=","report_task_user.report_id")
                                    ->where("report_task_user.task_id",$task_id)
                                    ->where("report_task_user.number",$number)
                                    ->where("report_task_user.cicle",$cicle)
                                    ->where("reports.code",$code)
                                    ->select("report_task_user.value","reports.type","report_task_user.report_id","report_task_user.cicle")
                                    ->first();


        if($valor){
            if($valor->type<>"3" && $valor->type<>"4" && $valor->type<>"10"){
                return ucfirst($valor->value);
            }

            if($valor->type=="3"){
                $opcion = DB::connection("eskuadconn")->table("options")->where("id",$valor->value)->first();
                if($opcion){
                    return ucfirst($opcion->name);
                }else{
                    return ucfirst($valor->value);
                }
                
            }
            if($valor->type=="4"){
                $opciones = DB::connection("eskuadconn")->table("options_values")
                ->join("options","options.id","=","options_values.option_id")
                ->where("options_values.report_id",$valor->report_id)
                ->where("options_values.cicle",$valor->cicle)
                ->where("options_values.task_id",$task_id)
                ->where("options_values.number",$number)
                ->select("options.name")
                ->get();
                $respuesta ="";
                foreach($opciones as $o){
                    $respuesta = $respuesta.ucfirst($o->name).",";
                }
                $respuesta = substr($respuesta, 0, -1);
                return $respuesta;
            }


            $opcion = DB::connection("eskuadconn")->table("options")->where("id",$valor->value)->first();
            return ucfirst($opcion->name);
           
        }else{
            return "";
        }
    }


}
