<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\ContractType;
use Illuminate\Http\Response;

/**
 * Class ContractTypeController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class ContractTypeController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el listado de roles
     *
     * @return Response
     */
    public function __invoke()
    {
        return $this->sendResponse(ContractType::all(), 'Contract type list');
    }
}
