<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\DocumentType;

/**
 * Class DocumentTypeController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class DocumentTypeController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de cargos disponibles
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->sendResponse(DocumentType::where('extra_document','0')->get(), 'Document types list');
    }

    public function extraDocuments()
    {
        return $this->sendResponse(DocumentType::where('extra_document','1')->get(), 'Document types list');
    }
}
