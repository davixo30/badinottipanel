<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\User;

/**
 * Class SupervisorController
 *
 * @author Juan Gamonal H <juangamonal@gmail.com>
 * @package App\Http\Controllers\API
 */
class SupervisorController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el listado de supervisores
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->sendResponse(
            User::supervisor()->get(),
            'Supervisors list'
        );
    }
}
