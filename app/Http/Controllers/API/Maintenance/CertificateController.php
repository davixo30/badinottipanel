<?php

namespace App\Http\Controllers\API\Maintenance;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Certificate;
use Illuminate\Http\Request;

/**
 * Class CertificateController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Maintenance
 */
class CertificateController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de certificaciones
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->sendResponse(Certificate::orderBy('name','asc')->with('ship_type')->get(), 'Certifications list');
    }

    /**
     * Crea una nueva certificación
     *
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:certificates,name',
            'ship_type_id' => 'required|int|exists:ship_types,id'
        ]);

        return $this->sendResponse(
            Certificate::create($request->only('name', 'ship_type_id')),
            'Certificate created'
        );
    }

    /**
     * Modifica una certificación de embarcación
     *
     * @param int $id
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function update(int $id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:certificates,name,' . $id,
            'ship_type_id' => 'required|int|exists:ship_types,id'
        ]);

        return $this->sendResponse(
            Certificate::findOrFail($id)->update($request->only('name', 'ship_type_id')),
            'Certificate created'
        );
    }

    /**
     * Elimina una certificación
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        return $this->sendResponse(Certificate::findOrFail($id)->delete(), 'Certificate deleted');
    }
}
