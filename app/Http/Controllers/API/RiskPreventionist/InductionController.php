<?php

namespace App\Http\Controllers\API\RiskPreventionist;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Induction;
use Illuminate\Http\Request;

/**
 * Class InductionController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\RiskPreventionist
 */
class InductionController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de inducciones disponibles
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $inductions = Induction::orderBy('name','asc')->with('clients')->get();

       // if (count($request->query()) > 0) {
       //     $inductions = $inductions->paginate(10);
       // } else {
          //  $inductions = $inductions->get();
        //}

        return $this->sendResponse($inductions, 'Inductions list');
    }

    /**
     * Crea una nueva inducción
     *
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:inductions',
            'clients' => 'present|array',
            'clients.*' => 'int|exists:clients,id'
        ]);

        $this->sendResponse(Induction::create($request->only('name', 'clients')), 'Induction created');
    }

    /**
     * Modifica una inducción
     *
     * @param int $id
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(int $id, Request $request)
    {
        $this->validate($request, [
            'name' => "required|string|max:255|unique:inductions,name,$id",
            'clients' => 'present|array',
            'clients.*' => 'int|exists:clients,id'
        ]);

        $this->sendResponse(
            Induction::edit($id, $request->get('name'), $request->get('clients')),
            'Induction created'
        );
    }

    /**
     * Elimina una inducción
     *
     * @param int $id
     */
    public function destroy(int $id)
    {
        $this->sendResponse(Induction::find($id)->delete(), 'Induction deleted');
    }
}
