<?php

namespace App\Http\Controllers\API\RiskPreventionist;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\IMC;
use App\Models\OccupationalExam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * Class WorkerController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\RiskPreventionist
 */
class WorkerController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Registra un nuevo IMC
     *
     * @param int $workerId
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function setIMC(int $workerId, Request $request)
    {
        $this->validate($request, [
            'month' => 'required|int|min:0|max:11',
            'year' => 'required|int|min:2020',
            'height' => 'required|numeric|min:0',
            'weight' => 'required|numeric|min:0',
            'imc' => 'required|numeric|min:0'
        ]);

        return $this->sendResponse(
            IMC::create(array_merge($request->only(['month', 'year', 'height', 'weight', 'imc']), [
                'worker_id' => $workerId
            ])),
            'IMC Created'
        );
    }

    /**
     * Guarda un examen ocupacional a un trabajador
     *
     * @param int $workerId
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function setOccupationalExam(int $workerId, Request $request)
    {
        // primero valida campos básicos
        $this->validate($request, [
            'condition' => 'required|in:true,false',
            'exam_date' => 'required|date'
        ]);
        $condition = $request->get('condition');

        // datos que se persistirán
        $data = $request->only('exam_date');
        $data['worker_id'] = $workerId;

        if ($condition === 'true') {
            // luego valida los datos de apto
            $this->validate($request, ['expiration_date' => 'required|date', 'certificate' => 'required|file']);
            $data['expiration_date'] = $request->get('expiration_date');

            // guarda el certificado
            $certificate = $request->file('certificate');
            $fileName = time() . '.' . $certificate->extension();
            $spaceEnv = env('DO_SPACE_ENV');
            Storage::putFileAs("$spaceEnv/occupational-exams/$workerId", $certificate, $fileName);

            $data['observations'] = $request->get('observations');
            $data['certificate'] = $fileName;
            $data['condition'] = true;
        } else {
            // luego valida los datos de contraindicado
            $this->validate($request, ['observations' => 'required|string']);
            $data['observations'] = $request->get('observations');
            $data['condition'] = false;
        }

        return $this->sendResponse(OccupationalExam::create($data), 'Occupational exam created');
    }
}
