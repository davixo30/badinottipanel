<?php

namespace App\Http\Controllers\API\Operations;

use App\Http\Controllers\API\V1\BaseController;
use App\Jobs\QuotationRejected;
use App\Jobs\QuotationValidated;
use App\Models\Position;
use App\Models\Quotation;
use Illuminate\Http\Request;
use DB;

/**
 * Class QuotationController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Operations
 */
class QuotationController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de cotizaciones por aprobar
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $many = Quotation::operations()->latest()->get();
        foreach($many as $key => $m){
            $emergencia = DB::table("crew_worker")->where("crew_id",$m->crew_id)->where("with_emergency",1)->count();
            $reemplazo = DB::table("crew_worker")->where("crew_id",$m->crew_id)->where("is_replacement",1)->count();
            $total = DB::table("crew_worker")->where("crew_id",$m->crew_id)->count();

            $suma = $emergencia + $reemplazo;
            if($suma == $total){
                $many->forget($key);
            }

        }
        $many = $many->paginate(10);

        return $this->sendResponse($many, 'Quotations list');
    }

    /**
     * Muestra el detalle de una cotización
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return $this->sendResponse(Quotation::with('crew', 'tickets')->find($id), 'Quotation details');
    }


    public function showWithoutEmergencyAndReplace(int $id)
    {
        return $this->sendResponse(Quotation::with('crewWithoutEmergencyAndReplace', 'tickets')->find($id), 'Quotation details');
    }

    /**
     * Rechaza una cotización
     *
     * @param int $id
     * @param Request $request
     *
     * @throws \Exception
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function rejectQuotation(int $id, Request $request)
    {
        // TODO: aplicar max
        $this->validate($request, [
            'reason' => 'required|string'
        ]);

        $reason = $request->get('reason');

        // guarda el rechazo de la cotización
        $quotation = Quotation::rejectQuotation($id, $reason);

        // notifica a bodega que la cotización fue rechazada
        QuotationRejected::dispatch($reason, $quotation->id);

        return $this->sendResponse('Ok', 'Quotation refused');
    }

    /**
     * Valida una cotización
     *
     * @param int $id
     *
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function validateQuotation(int $id)
    {
        // guarda la validación de la cotización
        Quotation::validateQuotation($id);

        // notifica a logística que la cotización fue aceptada y que debe facturarla
        QuotationValidated::dispatch($id);

        return $this->sendResponse('Ok', 'Quotation validated');
    }
}
