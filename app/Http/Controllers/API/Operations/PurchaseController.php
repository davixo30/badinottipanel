<?php

namespace App\Http\Controllers\API\Operations;

use App\Http\Controllers\API\V1\BaseController;
use App\Jobs\PurchaseRejected;
use App\Jobs\PurchaseValidated;
use App\Models\Purchase;
use Illuminate\Http\Request;
use DB;
/**
 * Class PurchaseController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Operations
 */
class PurchaseController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de compras
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $validate = Purchase::where("only_warehouse",1)->get();
        foreach($validate as $v){
            $v->status = true;
            $v->save();
        }
        return $this->sendResponse(Purchase::latest()->with('supplier', 'supply_request')->where("only_warehouse",0)->where("supply_request_id",null) ->orderBy("purchases.id","desc")->get(), 'Purchases list');
    }

    public function indexCrew()
    {
        $validate = Purchase::where("only_warehouse",1)->get();
        foreach($validate as $v){
            $v->status = true;
            $v->save();
        }

        $requests = DB::table("supply_requests")
        ->join("crews","crews.id","=","supply_requests.id")
        ->join("ships","ships.id","=","crews.ship_id")
        ->join("purchases","purchases.supply_request_id","=","supply_requests.id")
        ->join("clients","clients.id","=","crews.client_id")
        ->select("purchases.status","purchases.confirmed_at","ships.name as ship","clients.name as client","crews.upload_date","crews.real_download_date","crews.download_date","purchases.id")
        ->where("purchases.only_warehouse",0)
        ->orderBy("purchases.id","desc")
        ->get();
       // Purchase::latest()->with('supplier', 'supply_request')->where("only_warehouse",0)->paginate(10)
        return $this->sendResponse($requests, 'Purchases list');
    }

    /**
     * Muestra el detalle de una compra
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return $this->sendResponse(Purchase::with('supplier', 'warehouse_item_sizes')->find($id), 'Purchase details');
    }

    public function showCrew(int $id)
    {   
        $crew = Purchase::with('supplier', 'warehouse_item_sizes')->find($id);
        if($crew){

            $thecrew = DB::table("supply_requests")
            ->join("crews","crews.id","=","supply_requests.id")
            ->join("ships","ships.id","=","crews.ship_id")
            ->join("purchases","purchases.supply_request_id","=","supply_requests.id")
            ->join("clients","clients.id","=","crews.client_id")
            ->select("crews.id as crew_id","purchases.status","purchases.confirmed_at","ships.name as ship","clients.name as client","crews.upload_date","crews.real_download_date","crews.download_date","supply_requests.id")
            ->where("purchases.id",$crew->id)
            ->first();
            $crew->crew = $thecrew;
            $provisions = DB::table("provisions")->where("provisions.crew_id",$thecrew->crew_id)->first();
            
            if($provisions){
                $provisions->supplier = "Sin Proveedor";
                if($provisions->supplier_id<>null){
                    $supplier = DB::table("suppliers")->where("id",$provisions->supplier_id)->first();
                    if($supplier){
                        $provisions->supplier = $supplier->name;
                    }

                }
            }

            $crew->provisions = $provisions;
            foreach($crew->warehouse_item_sizes as $w){
                $dato = DB::table("suppliers")->where("id",$w->supplier_id)->first();
                if($dato){
                    $w->supplier_name = $dato->name;
                }else{
                    $w->supplier_name = "Sin Proveedor";
                }
               
            }
            
        }
        return $this->sendResponse($crew, 'Purchase details');
    }

    /**
     * Rechaza una compra
     *
     * @param int $id ID de la compra
     * @param Request $request
     *
     * @throws \Exception
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function rejectPurchase(int $id, Request $request)
    {
        // TODO: aplicar max
        $this->validate($request, [
            'reason' => 'required|string'
        ]);

        $reason = $request->get('reason');

        // guarda el rechazo de la compra
        Purchase::rejectPurchase($id, $reason);

        // notifica a bodega que la compra fue rechazada
        PurchaseRejected::dispatch($reason);

        return $this->sendResponse('Ok', 'Purchase refused');
    }

    /**
     * Valida una compra
     *
     * @param int $id
     *
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function validatePurchase(int $id)
    {
        // guarda la validación de la compra
        Purchase::validatePurchase($id);

        // notifica a bodega que la compra fue aceptada y que debe facturarla
        PurchaseValidated::dispatch($id);

        return $this->sendResponse('Ok', 'Purchase validated');
    }
}
