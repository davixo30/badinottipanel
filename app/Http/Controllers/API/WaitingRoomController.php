<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Worker;
use Illuminate\Http\Request;
use DB;

/**
 * Class WaitingRoomController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class WaitingRoomController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de trabajadores en sala de espera
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $search = $request->query('search');
        $workers = Worker::waitingRoom()->with('roles')->orderBy('name','asc')->get();

        foreach ($workers as $key => $worker) {
            $worker->attribute_name = "(Sin Atributo)";
            if($worker->attribute_id<>null){
                $atributo = DB::table("attributes")->where("id",$worker->attribute_id)->first();
                $worker->attribute_name = "(".$atributo->name.")";
            }
            


            $status = Worker::getWorkerEvaluation($worker->id,0);
            $worker->status = $status;

            if($status['status']=="Ok" || $status['status']=="Observaciones"){
                $workers->forget($key);
            }

        }

       // $workers = $workers->get();


        return $this->sendResponse($workers, 'Workers list');
    }
}
