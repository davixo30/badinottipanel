<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Http\Requests\Workers\WorkerRequest;
use App\Models\Crew;
use App\Models\Document;
use App\Models\EPPDelivery;
use App\Models\Expiration;
use App\Models\IMC;
use App\Models\InductionWorker;
use App\Models\OccupationalExam;
use App\Models\Qualification;
use App\Models\Role;
use App\Models\Worker;
use App\Models\WorkerAttribute;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Models\PendingAttribute;
use App\Jobs\NewWorkerCreated;



/**
 * Responde generalmente a solicitudes de el mantenedor de trabajadores y la ficha del mismo.
 *
 * @author Juan Gamonal H <juangamonal@gmail.com>
 * @package App\Http\Controllers\API
 */
class WorkerController extends BaseController
{
    /** @var string[] */
    private $requestOnly = [
        'name', 'last_name', 'code', 'rut', 'birthdate', 'phone_number',
        'address', 'attribute_id', 'commune_id', 'marital_status_id'
    ];

    public function sendEmail(Request $request){
        /*$epps =  EPPDelivery::byWorkerId($request->id)->with('group', 'size')->orderBy("id","desc")->get();
        foreach($epps as $epp){
            echo $epp['size']['item']['name']."<br>";
        }*/

 
       NewWorkerCreated::dispatch($request->id);
       $worker = Worker::find($request->id);
       $worker->send_email = 1;
       $worker->save();
       return response()->json($worker);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el listado de trabajadores
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $search = $request->query('search');

        return $this->sendResponse(
            Worker::when($search, function ($query, $search) {
               /* return $query->where('name', 'like', "%$search%")
                    ->orWhere('last_name', 'like', "%$search%")
                    ->orWhere('rut', 'like', "%$search%")
                    ->orWhere('phone_number', 'like', "%$search%")
                    ->orWhere('code', 'like', "%$search%");*/
            })->with('roles:id,name')->orderBy('name','asc')->get(),
            'Workers list'
        );
    }

    /**
     * Obtiene la ficha de un trabajador
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(int $id)
    {
        $worker = Worker::withTrashed()->with('attribute', 'commune', 'contract', 'roles')->findOrFail($id);
        $worker->last_crew = Crew::getWorkerLastCrew($id);

        return $this->sendResponse($worker, 'Trabajador encontrado');
    }

    /**
     * Almacena un nuevo trabajador
     *
     * @param WorkerRequest $request
     *
     * @throws \Exception
     * @return Response
     */
    public function store(WorkerRequest $request)
    {
        $existe = DB::table("workers")->where("rut",$request->rut)->first();
        if($existe){
            DB::table("workers")->where("id",$existe->id)->update(['deleted_at'=>null,'attribute_id'=>10]);

            $w = new WorkerAttribute;
            $w->worker_id = $existe->id;
            $w->attribute_id = 10;
            $w->save();
                return $this->sendResponse($existe
                    ,
                    'Trabajador creado correctamente'
                );
        }else{
            $worker = Worker::create($request->only($this->requestOnly));
            DB::UPDATE("update workers set send_email=?, red_day=? where id=?",array(0,0,$worker->id));
            $w = new WorkerAttribute;
            $w->worker_id = $worker->id;
            $w->attribute_id = $worker->attribute_id;
            $w->save();
                return $this->sendResponse($worker
                    ,
                    'Trabajador creado correctamente'
                );
        }
      
    }

    /**
     * Modifica un trabajador existente
     *
     * @param WorkerRequest $request
     * @param int $id
     *
     * @return Response
     */
    public function update(WorkerRequest $request, int $id)
    {

       // return response()->json($request);
        return $this->sendResponse(
            Worker::findOrFail($id)->update($request->only($this->requestOnly)),
            'Trabajador editado correctamente'
        );
    }

    /**
     * Cambia la foto de un trabajador
     *
     * @param Request $request
     * @param int $id
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return Response
     */
    public function updateAvatar(Request $request, int $id)
    {
        $this->validate($request, [
            'avatar' => 'required|image|mimes:jpeg,png,jpg|max:10240',
        ]);

        // almacena el archivo
        $avatar = $request->file('avatar');
        $fileName = time() . '.' . $avatar->extension();
        $spaceEnv = env('DO_SPACE_ENV');
        Storage::putFileAs("$spaceEnv/worker-avatar/$id", $avatar, $fileName);

        // guarda el nuevo nombre de archivo en el trabajador
        Worker::find($id)->update(['avatar' => $fileName]);

        return $this->sendResponse(['file' => $fileName], 'Foto cambiada correctamente');
    }

    /**
     * Elimina un trabajador existente
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy(int $id)
    {
        return $this->sendResponse(
            Worker::findOrFail($id)->delete(),
            'Trabajador eliminado correctamente'
        );
    }

    /**
     * Obtiene listado de roles asociados a un trabajador
     *
     * @param int $id
     *
     * @return Response
     */
    public function getRoles(int $id)
    {
        return $this->sendResponse(
            Role::getByWorkerId($id)->pluck('id'),
            'Trabajador encontrado'
        );
    }

    /**
     * Asocia roles a un trabajador
     *
     * @param int $id
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return Response
     */
    public function setRoles(int $id, Request $request)
    {
        $this->validate($request, [
            'roles' => 'required|array',
            'roles.*' => 'int|min:1|exists:roles,id'
        ]);

        return $this->sendResponse(
            Worker::setRoles($id, $request->get('roles')),
            'Roles asociados'
        );
    }

    /**
     * Guarda un nuevo documento a un trabajador
     *
     * @param int $id
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return Response
     */
    public function setDocument(int $id, Request $request)
    {
        $this->validate($request, [
            'document_type_id' => 'required|exists:document_types,id',
            'expiration_date' => 'required|date'
        ]);

        return $this->sendResponse(Document::create([
            'document_type_id' => $request->get('document_type_id'),
            'expiration_date' => $request->get('expiration_date'),
            'worker_id' => $id
        ]), 'Document saved');
    }

    public function setDocumentExtra(int $id, Request $request)
    {
        $this->validate($request, [
            'document_type_id' => 'required|exists:document_types,id',
        ]);

        $certificate = $request->file('certificate');
        $fileName = time() . '.' . $certificate->extension();
        $spaceEnv = env('DO_SPACE_ENV');
        Storage::putFileAs("$spaceEnv/worker-documents-extra/$id", $certificate, $fileName);

        $document = DB::table("document_types")->where("id",$request->document_type_id)->first();
        if($document->attribute_id<>"0"){
            if(date("Y-m-d")>=$request->date_start && date("Y-m-d")<=$request->date_end){

                DB::table("pending_attributes")->where("worker_id",$id)->where("status","0")->delete();

                $w = Worker::find($id);
                $w->attribute_id = $document->attribute_id;
                $w->save();

                $w = new WorkerAttribute;
                $w->worker_id = $id;
                $w->crew_id = 0;
                $w->attribute_id = $document->attribute_id;
                $w->date_start = $request->date_start;
                $w->date_end = $request->date_end;
                $w->save();

                $np = new PendingAttribute;
                $np->attribute_id = 10;
                $np->worker_id = $id;
                $np->date = date("Y-m-d",strtotime($request->date_end." + 1 days"));
                $np->crew_id = 0;
                $np->status = 0;
                $np->save();
            }else{
                $np = new PendingAttribute;
                $np->attribute_id = $document->attribute_id;
                $np->worker_id = $id;
                $np->date = $request->date_start;
                $np->crew_id = 0;
                $np->status = 0;
                $np->save();
                $np = new PendingAttribute;
                $np->attribute_id = 10;
                $np->worker_id = $id;
                $np->date = date("Y-m-d",strtotime($request->date_end." + 1 days"));
                $np->crew_id = 0;
                $np->status = 0;
                $np->save();
            }
        }

        $data = ['worker_id'=>$id,'document_type_id'=>$request->document_type_id,'date_start'=>$request->date_start,'date_end'=>$request->date_end,'observation'=>$request->observation,'certificate'=>$fileName,'uploader_id'=>Auth::id(),'created_at'=>date("Y-m-d H:i:s")];


        return $this->sendResponse(
            DB::table('extra_documents')->insert($data),
            'Document Extra saved'
        );
    }

    /**
     * Obtiene los documentos de cierto trabajador
     *
     * @param int $id
     *
     * @return Response
     */
    public function getDocuments(int $id)
    {
        return $this->sendResponse(Document::where('worker_id', $id)->with(
            'type',
            'uploader:id,name,last_name'
        )->latest()->paginate(10), 'Documents list');
    }

    public function getDocumentsExtra(int $id)
    {   
        $data = DB::table("extra_documents")->join("users","users.id","=","extra_documents.uploader_id")
                ->join("document_types","document_types.id","=","extra_documents.document_type_id")
                ->select("users.name as uploader_name","users.last_name as uploader_last_name","document_types.name as type","extra_documents.date_start","extra_documents.date_end","extra_documents.certificate","extra_documents.created_at","extra_documents.observation","extra_documents.id","extra_documents.worker_id")
                ->where("extra_documents.worker_id",$id)
                ->orderBy("extra_documents.id","desc")
                ->paginate(10);
        return $this->sendResponse($data, 'Documents extra list');
    }

    /**
     * Obtiene las calificaciones registradas de un trabajador
     *
     * @param int $id
     *
     * @return Response
     */
    public function getQualifications(int $id)
    {
        $data = [
            'qualifications' => Qualification::with(
                'qualifier:id,name,last_name'
            )->latest()->where('worker_id', $id)->paginate(10),
            'general_qualification' => Qualification::getGeneralQualification($id)
        ];

        return $this->sendResponse($data, 'Qualifications found');
    }

    /**
     * Obtiene inducciones de un tripulante
     *
     * @param int $id
     *
     * @return Response
     */
    public function getInductions(int $id)
    {
        return $this->sendResponse(
            InductionWorker::join("inductions","inductions.id","induction_workers.induction_id")->with('induction', 'uploader')->where("inductions.deleted_at",null)->where('induction_workers.worker_id', $id)
                ->latest('induction_date')->paginate(10),
            'Inductions found'
        );
    }

    /**
     * Crea una nueva inducción a un trabajador
     * TODO: mover a controlador de prevencionista
     *
     * @param int $id
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return Response
     */
    public function setInduction(int $id, Request $request)
    {
        $this->validate(new Request(['id' => $id]), ['id' => 'exists:workers,id']);
        // TODO: comprobar tipos y peso máximo
        $this->validate($request, [
            'certificate' => 'required|file|max:10240',
            'induction_date' => 'required|date',
            'induction_id' => 'required|exists:inductions,id'
        ]);

        // almacena el archivo
        $certificate = $request->file('certificate');
        $fileName = time() . '.' . $certificate->extension();
        $spaceEnv = env('DO_SPACE_ENV');
        Storage::putFileAs("$spaceEnv/inductions-certificates/$id", $certificate, $fileName);

        return $this->sendResponse(
            InductionWorker::create(array_merge($request->only('induction_date', 'induction_id'), [
                'certificate' => $fileName,
                'worker_id' => $id
            ])),
            'Inducción creada'
        );
    }

    /**
     * Obtiene las entregas de EPP de un trabajador
     *
     * @param int $id
     *
     * @return Response
     */
    public function getEPPDeliveries(int $id)
    {
        return $this->sendResponse(
            EPPDelivery::byWorkerId($id)->with('group', 'size')->orderBy("id","desc")->paginate(10),
            'EPP deliveries list'
        );
    }

    /**
     * Obtiene los registros de IMC por año de un trabajador
     *
     * @param int $id
     * @param int $year
     *
     * @return Response
     */
    public function getIMC(int $id, int $year)
    {
        return $this->sendResponse(
            IMC::with('uploader')->where('worker_id', $id)->where('year', $year)->get(),
            'IMC found'
        );
    }

    /**
     * Obtiene el listado de exámenes ocupacionales de un trabajador
     *
     * @param int $id
     *
     * @return Response
     */
    public function getOccupationalExams(int $id)
    {
        return $this->sendResponse(
            OccupationalExam::where('worker_id', $id)->with('uploader')->latest()->paginate(10),
            'Exams list'
        );
    }
}
