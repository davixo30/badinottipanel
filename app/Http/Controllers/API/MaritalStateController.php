<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\MaritalStatus;

/**
 * Class MaritalStateController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class MaritalStateController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de estados civiles disponibles
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->sendResponse(MaritalStatus::all(), 'Marital states list');
    }
}
