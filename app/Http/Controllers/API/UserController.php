<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Http\Requests\Users\UserRequest;
use App\Models\User;
use App\Models\Worker;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

/**
 * Class UserController
 *
 * @author Juan Gamonal H <juangamonal@gmail.com>
 * @package App\Http\Controllers\API
 */
class UserController extends BaseController
{
    /** @var string */
    private $defaultPassword = '123456';

    /** @var string[] */
    private $requestOnly = [
        'name', 'last_name', 'code', 'rut', 'address', 'email', 'birthdate',
        'phone_number', 'position_id', 'commune_id', 'marital_status_id'
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function updatePass(Request $request)
    {
        $user = User::where('id',$request->user_id)->first();
        $error = 0;
       // if (Hash::check($request->old_pass, $user->password)){
            $user->password = Hash::make($request->new_pass);
            if($user->save()){
                return response()->json(1);
            }else{
                return response()->json(2);
            }
       // }
        return response()->json($error);
    }


    /**
     * Obtiene el listado de usuarios
     *
     * @return Response
     */
    public function index()
    {
        /*$user1 = User::where('rut','18164351-K')->first();
        $user1->password = Hash::make(765391);
        $user1->save();
        $user5 = User::where('rut','18164351-k')->first();
        $user5->password = Hash::make(765391);
        $user5->save();
        $user2 = User::where('rut','19181996-9')->first();
        $user2->password = Hash::make(309112);
        $user2->save();
        $user3 = User::where('rut','17416296-4')->first();
        $user3->password = Hash::make(657483);
        $user3->save();
        $user4 = User::where('rut','13525916-0')->first();
        $user4->password = Hash::make(918234);
        $user4->save();*/
        return $this->sendResponse(
            User::orderBy('name','asc')->with('position')->get(),
            'Users list'
        );
    }

    /**
     * Obtiene la ficha de un usuario
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(int $id)
    {
        return $this->sendResponse(
            User::with('commune', 'contract', 'position')->findOrFail($id),
            'Trabajador encontrado'
        );
    }

    /**
     * Almacena un nuevo usuario
     *
     * @param UserRequest $request
     *
     * @return Response
     */
    public function store(UserRequest $request)
    {
        return $this->sendResponse(
            User::create(array_merge(
                $request->only($this->requestOnly),
                ['password' => Hash::make($this->defaultPassword)]
            )),
            'Usuario creado correctamente'
        );
    }

    /**
     * Modifica un usuario existente
     *
     * @param UserRequest $request
     * @param int $id
     *
     * @return Response
     */
    public function update(UserRequest $request, int $id)
    {
        return $this->sendResponse(
            User::findOrFail($id)->update($request->only($this->requestOnly)),
            'Usuario editado correctamente'
        );
    }

    /**
     * Elimina un usuario existente
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy(int $id)
    {
        return $this->sendResponse(User::findOrFail($id)->delete(), 'Usuario eliminado correctamente');
    }

    /**
     * Cambia la foto de un usuario
     *
     * @param Request $request
     * @param int $id
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return Response
     */
    public function updateAvatar(Request $request, int $id)
    {
        $this->validate($request, [
            'avatar' => 'required|image|mimes:jpeg,png,jpg|max:10240',
        ]);

        // almacena el archivo
        $avatar = $request->file('avatar');
        $fileName = time() . '.' . $avatar->extension();
        $spaceEnv = env('DO_SPACE_ENV');
        Storage::putFileAs("$spaceEnv/user-avatar/$id", $avatar, $fileName);

        // guarda el nuevo nombre de archivo en el trabajador
        User::find($id)->update(['avatar' => $fileName]);

        return $this->sendResponse(['file' => $fileName], 'Foto cambiada correctamente');
    }



    
}
