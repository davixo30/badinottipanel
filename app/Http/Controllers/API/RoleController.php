<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Role;
use Illuminate\Http\Response;

/**
 * Class RoleController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class RoleController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el listado de roles
     *
     * @return Response
     */
    public function __invoke()
    {
        return $this->sendResponse(Role::all(), 'Roles list');
    }
}
