<?php

namespace App\Http\Controllers\API\Logistics;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Crew;
use DB;

/**
 * Class CrewController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Logistics
 */
class CrewController extends BaseController
{
    /**
     * Create a new controller instance.
     * TODO: verificar si esto se debería sacar o cambiar
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el listado de tripulaciones
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->sendResponse(Crew::logistics()->latest()->with(
            'client:id,name',
            'ghost_quotation:id,crew_id',
            'quotation:id,crew_id',
            'ship:id,name,supervisor_id',
            'status'
        )->with('workers')->paginate(10), 'Crews list 1');
    }


    public function crewWithoutEmpy()
    {
        $crews = Crew::logistics()->latest()->with(
            'client:id,name',
            'ghost_quotation:id,crew_id',
            //'quotation:id,crew_id',
            'ship:id,name,supervisor_id',
            'status'
        )->with('workers')->get();
        foreach($crews as $key => $c){
            if(count($c->workers)==0){
                $crews->forget($key);
            }
            $db = DB::table("quotations")->where("crew_id",$c->id)->first();
            $c->quotation = $db;   


            $tickets = DB::table("ticket_groups")->where("quotation_id",$db->id)->get();
            $c->tickets = "No";
                if($tickets){

                    foreach($tickets as $ticket){
                        $workers = DB::table("worker_ticket")->where("ticket_group_id",$ticket->id)->where("status","1")->where("stock","0")->where("lost_user_id",null)->count();
                        if($workers>0){
                            $c->tickets = "Si";
                        }
                    }
                }
                
           
         
             
        }
        //$crews = $crews->paginate(10);
        return $this->sendResponse($crews, 'Crews list 2');
    }

    /**
     * Obtiene el listado de tripulaciones que tienen alguna emergencia
     *
     * @return \Illuminate\Http\Response
     */
    public function withEmergency()
    {
        return $this->sendResponse(Crew::emergency()->latest()->with(
            'client:id,name',
            'ghost_quotation:id,crew_id',
            'quotation:id,crew_id',
            'ship:id,name',
            'status'
        )->paginate(10), 'Crews list 3');
    }
}
