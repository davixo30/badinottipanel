<?php

namespace App\Http\Controllers\API\Logistics;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Relocation;
use App\Models\RelocationProvider;
use Illuminate\Http\Request;

/**
 * Class RelocationController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Logistics
 */
class RelocationController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de proveedores disponibles
     *
     * @return \Illuminate\Http\Response
     */
    public function getProviders()
    {
        return $this->sendResponse(RelocationProvider::all(), 'Relocation providers list');
    }

    /**
     * Guarda la información con respecto al traslado de un trabajador
     *
     * @param int $crewWorkerId
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function store(int $crewWorkerId, Request $request)
    {
        $this->validate($request, [
            'arrival' => 'required|string|max:255',
            'departure' => 'required|string|max:255',
            // TODO: validar formato de fecha y hora
            'pickup' => 'required|string',
            'price' => 'required|int|min:1',
            'relocation_provider_id' => 'required|exists:suppliers,id'
        ]);

        $relocation = Relocation::where('crew_worker_id', $crewWorkerId)->first();
        $fill = [
            'arrival' => $request->get('arrival'),
            'departure' => $request->get('departure'),
            'pickup' => $request->get('pickup'),
            'price' => $request->get('price'),
            'relocation_provider_id' => $request->get('relocation_provider_id'),
            'crew_worker_id' => $crewWorkerId
        ];

        if ($relocation === null) {
            $relocation = new Relocation($fill);
        } else {
            $relocation->fill($fill);
        }

        return $this->sendResponse($relocation->save(), 'Relocation saved');
    }
}
