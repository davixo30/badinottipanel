<?php

namespace App\Http\Controllers\API\Logistics;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Crew;
use App\Models\EmergencyButton;
use App\Models\EmergencyQuotation;
use Illuminate\Http\Request;

/**
 * Class EmergencyButtonController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Logistics
 */
class EmergencyButtonController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de botones de emergencia disponibles
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->sendResponse(
            EmergencyButton::latest()->withReplacement()->withTicket()->with('crew','quotation')->paginate(10),
            'Emergency button list'
        );
    }

    /**
     * Obtiene el botón de emergencia juntp a su cotización
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getQuotation(int $id)
    {
        return $this->sendResponse(
            EmergencyButton::with('quotation', 'replacement')->findOrFail($id),
            'Quotation found'
        );
    }

    /**
     * Guarda una cotización de emergencia que corresponde a pasajes de un reemplazante
     *
     * @param int $id
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function setQuotation(int $id, Request $request)
    {
        $this->validate($request, [
            'observations' => 'present|nullable|string'
        ]);

        return $this->sendResponse(
            EmergencyQuotation::enter($id, $request->get('observations')),
            'Cotización ingresada'
        );
    }
}
