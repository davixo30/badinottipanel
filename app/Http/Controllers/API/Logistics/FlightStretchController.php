<?php

namespace App\Http\Controllers\API\Logistics;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\FlightStretch;

/**
 * Class FlightStretchController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Logistics
 */
class FlightStretchController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de tramos
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->sendResponse(FlightStretch::all(), 'Flight stretches list');
    }
}
