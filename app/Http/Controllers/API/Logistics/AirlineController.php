<?php

namespace App\Http\Controllers\API\Logistics;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Airline;
use App\Models\Ticket;
use DB;

/**
 * Class AirlineController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Logistics
 */
class AirlineController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de aerolíneas disponibles
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->sendResponse(Airline::all(), 'Airlines list');
    }

    /**
     * Obtiene los pasajes que se encuentren en stock de cierta aerolínea
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getStock(int $id)
    {
        $tickets = Ticket::inStock($id)->get();
        foreach($tickets as $t){
            $codigo = DB::table("ticket_purchases")->where("id",$t->id)->first();
            if($codigo){
                $t->code = $codigo->code;
            }else{
                $t->code = "S/C";
            }

        }

        return $this->sendResponse($tickets, 'Stock tickets list');
    }
}
