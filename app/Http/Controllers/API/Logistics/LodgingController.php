<?php

namespace App\Http\Controllers\API\Logistics;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Lodging;
use App\Models\LodgingProvider;
use Illuminate\Http\Request;

/**
 * Class LodgingController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Logistics
 */
class LodgingController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de proveedores disponibles
     *
     * @return \Illuminate\Http\Response
     */
    public function getProviders()
    {
        return $this->sendResponse(LodgingProvider::all(), 'Lodging providers list');
    }

    /**
     * Guarda la información con respecto al alojamiento de un trabajador
     *
     * @param int $crewWorkerId
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function store(int $crewWorkerId, Request $request)
    {
        $this->validate($request, [
            'arrival_date' => 'required|date',
            'departure_date' => 'required|date',
            'lodging_provider_id' => 'required|exists:suppliers,id',
            'price' => 'required|int|min:1'
        ]);

        $lodging = Lodging::where('crew_worker_id', $crewWorkerId)->first();
        $fill = [
            'arrival_date' => $request->get('arrival_date'),
            'departure_date' => $request->get('departure_date'),
            'lodging_provider_id' => $request->get('lodging_provider_id'),
            'price' => $request->get('price'),
            'crew_worker_id' => $crewWorkerId
        ];

        if ($lodging === null) {
            $lodging = new Lodging($fill);
        } else {
            $lodging->fill($fill);
        }

        return $this->sendResponse($lodging->save(), 'Lodging saved');
    }
}
