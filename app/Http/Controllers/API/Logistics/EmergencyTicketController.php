<?php

namespace App\Http\Controllers\API\Logistics;

use App\Http\Controllers\API\V1\BaseController;
use App\Jobs\NotifyWorker;
use App\Models\Ticket;
use App\Models\TicketGroup;
use App\Models\TicketPurchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class EmergencyTicketController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Logistics
 */
class EmergencyTicketController extends BaseController
{
    /** @var string[] */
    private $requestOnly = [
        'price', 'flight_date', 'departure_time', 'arrival_time', 'flight_stretch_id',
        'departure_airport_id', 'arrival_airport_id', 'airline_id', 'stock_ticket_id'
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Guarda un nuevo pasaje
     *
     * @param int $quotationId
     * @param Request $request
     *
     * @throws \Exception
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function store(int $quotationId, Request $request)
    {
        $this->validate($request, [
            'flight_stretch_id' => 'required|exists:flight_stretches,id',
            'flight_date' => 'required|date',
            'departure_time' => 'required|date_format:H:i',
            'arrival_time' => 'required|date_format:H:i',
            'departure_airport_id' => 'required|exists:airports,id',
            'arrival_airport_id' => 'required|exists:airports,id',
            'airline_id' => 'required|exists:airlines,id',
            'price' => 'required|int|min:0',
            'stock_ticket_id' => 'present|nullable|exists:tickets,id'
        ]);

        return $this->sendResponse(
            TicketGroup::createEmergency(
                array_merge($request->only($this->requestOnly), ['emergency_quotation_id' => $quotationId])
            ),
            'Pasaje creado'
        );
    }

    /**
     * Modifica un pasaje
     *
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'flight_stretch_id' => 'required|exists:flight_stretches,id',
            'flight_date' => 'required|date',
            'departure_time' => 'required|date_format:H:i',
            'arrival_time' => 'required|date_format:H:i',
            'departure_airport_id' => 'required|exists:airports,id',
            'arrival_airport_id' => 'required|exists:airports,id',
            'airline_id' => 'required|exists:airlines,id',
            'price' => 'required|int|min:1'
        ]);

        return $this->sendResponse(
            TicketGroup::findOrFail($request->get('id'))->update($request->only($this->requestOnly)),
            'Pasaje modificado'
        );
    }

    /**
     * Elimina un pasaje
     *
     * @param int $quotationId
     * @param int $ticketId
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteTicket(int $quotationId, int $ticketId)
    {
        return $this->sendResponse(TicketGroup::find($ticketId)->delete(), 'Pasaje eliminado');
    }
}
