<?php

namespace App\Http\Controllers\API\Logistics;

use App\Http\Controllers\API\V1\BaseController;
use App\Jobs\NotifyWorker;
use App\Models\Ticket;
use App\Models\TicketGroup;
use App\Models\TicketPurchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class TicketController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Logistics
 */
class TicketController extends BaseController
{
    /** @var string[] */
    private $requestOnly = [
        'price', 'flight_date', 'departure_time', 'arrival_time', 'flight_stretch_id',
        'departure_airport_id', 'arrival_airport_id', 'airline_id', 'stock_ticket_id'
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene el listado de pasajes disponibles
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->sendResponse(TicketGroup::ableToBuy()->with(
            'airline',
            'arrival_airport',
            'departure_airport',
            'flight_stretch'
        )->latest()->paginate(10), 'Tickets list');
    }

    /**
     * Obtiene el detalle de un pasaje
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return $this->sendResponse(TicketGroup::getDetails($id), 'Ticket found');
    }

    /**
     * Compra una pasaje
     *
     * @param int $id
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function buy(int $id, Request $request)
    {
        $this->validate($request, [
            'dearness' => 'required|int|min:0',
            'penalty' => 'required|int|min:0',
            'flight_code' => 'required|string|max:255'
        ]);

        // genera la compra
        $purchase = TicketPurchase::create([
            'id' => $id,
            'dearness' => $request->get('dearness'),
            'penalty' => $request->get('penalty'),
            'flight_code' => $request->get('flight_code'),
        ]);

        // envía el evento para notificar a usuarios vía SMS
        $ticket = Ticket::findOrFail($id);
        NotifyWorker::dispatch($ticket->id);

        return $this->sendResponse($purchase, 'Ticket bought');
    }

    /**
     * Guarda un nuevo pasaje
     *
     * @param int $quotationId
     * @param Request $request
     *
     * @throws \Exception
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function store(int $quotationId, Request $request)
    {
        $this->validate($request, [
            'flight_stretch_id' => 'required|exists:flight_stretches,id',
            'flight_date' => 'required|date',
            'departure_time' => 'required|date_format:H:i',
            'arrival_time' => 'required|date_format:H:i',
            'departure_airport_id' => 'required|exists:airports,id',
            'arrival_airport_id' => 'required|exists:airports,id',
            'airline_id' => 'required|exists:airlines,id',
            'price' => 'required|int|min:0',
            'stock_ticket_id' => 'present|nullable|exists:tickets,id'
        ]);

        return $this->sendResponse(
            TicketGroup::create(array_merge($request->only($this->requestOnly), ['quotation_id' => $quotationId])),
            'Pasaje creado'
        );
    }

    /**
     * Modifica un pasaje
     *
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'flight_stretch_id' => 'required|exists:flight_stretches,id',
            'flight_date' => 'required|date',
            'departure_time' => 'required|date_format:H:i',
            'arrival_time' => 'required|date_format:H:i',
            'departure_airport_id' => 'required|exists:airports,id',
            'arrival_airport_id' => 'required|exists:airports,id',
            'airline_id' => 'required|exists:airlines,id',
            'price' => 'required|int|min:1'
        ]);

        return $this->sendResponse(
            TicketGroup::findOrFail($request->get('id'))->update($request->only($this->requestOnly)),
            'Pasaje modificado'
        );
    }

    /**
     * Envía un pasaje a stock
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function toStock(int $id)
    {
        return $this->sendResponse(Ticket::toStock($id), 'Pasaje modificado');
    }

    /**
     * Elimina un pasaje
     *
     * @param int $quotationId
     * @param int $ticketId
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteTicket(int $quotationId, int $ticketId)
    {
        $ticket = DB::table("ticket_groups")->where("id",$ticketId)->first();
        DB::table("worker_ticket")->where("ticket_group_id",$ticketId)->delete();
        

                        if($ticket->payment_type==1){
                            DB::table("stock_tickets")->where("id",$ticket->stock_ticket_id)->update(['used'=>0]); 
                        }

                        if($ticket->payment_type==2){
                                $last_voucher = DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticketId)->first();
                                $airlinevoucher = DB::table("airline_vouchers")->where('id',$ticket->voucher_id)->first();
                                DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['price'=>($airlinevoucher->price + $last_voucher->required_price),'price_used'=>($airlinevoucher->price_used - $last_voucher->required_price)]);
                                $estado =  DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->first();
                                if($estado){
                                    if($estado->price<>0){
                                        DB::table("airline_vouchers")->where("id",$ticket->voucher_id)->update(['status'=>1]);
                                    }
                                }  
                                DB::table("log_vouchers")->where('airline_voucher_id',$ticket->voucher_id)->where('ticket_group_id',$ticketId)->delete();
                        }
                                

        return $this->sendResponse(TicketGroup::find($ticketId)->delete(), 'Pasaje eliminado');
    }

    /**
     * Asocia trabajadores a un pasaje ya creado
     *
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function associateWorkers(Request $request)
    {
        $this->validate($request, [
            'ticket_id' => 'required|exists:ticket_groups,id',
            'workers' => 'required|array',
            'workers.*.crew_worker_id' => 'required|int|exists:crew_worker,id'
        ]);

        return $this->sendResponse(
            TicketGroup::associateWorkers($request->get('ticket_id'), $request->get('workers')),
            'Pasaje asociado'
        );
    }

    /**
     * Elimina una asociación entre un pasaje y un crew_worker
     *
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function disassociateWorker(Request $request)
    {
        $this->validate($request, [
            'crew_worker_id' => 'required|int|exists:crew_worker,id',
        ]);

        return $this->sendResponse(
            Ticket::where('crew_worker_id', $request->get('crew_worker_id'))
                ->delete(),
            'Pasajes desasociados'
        );
    }

    /**
     * Asigna un pasaje de stock a un trabajador
     *
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function assignStock(Request $request)
    {
        $this->validate($request, [
            'ticket_id' => 'required|exists:ticket_groups,id',
            'crew_worker_id' => 'required|int|exists:crew_worker,id'
        ]);

        return $this->sendResponse(
            TicketGroup::assignStock($request->get('crew_worker_id'), $request->get('ticket_id')),
            'Pasaje asociado'
        );
    }
}
