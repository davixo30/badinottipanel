<?php

namespace App\Http\Controllers\API\Logistics;

use App\Http\Controllers\API\V1\BaseController;
use App\Jobs\NewQuotation;
use App\Models\InvoicedQuotation;
use App\Models\Quotation;
use Illuminate\Http\Request;
use DB;

/**
 * Class QuotationController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Logistics
 */
class QuotationController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Obtiene una cotización independiente del estado en que se encuentre
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $quotation =  Quotation::with('crew', 'invoice', 'tickets')->find($id);
        //return $quotation;

        foreach($quotation->tickets as $t){
            $t->clone_extra_price = $t->extra_price;
            $t->clone_dearness = $t->dearness;
            $t->temporal_extra_price = 0;
            $t->edited = 0;

            $f = DB::table("suppliers")->where("code",$t->airline_id)->first();
            $t->supplier_id = $f->id;
        }


        $invoices = DB::table("invoices")->join("invoice_crew","invoices.id","=","invoice_crew.invoice_id")->join("suppliers","suppliers.id","=","invoices.supplier_id")->join("supplier_types","supplier_types.id","=","suppliers.supplier_type_id")->join("bank_accounts","bank_accounts.id","=","invoices.account_id")->select('invoices.*','suppliers.name as supplier','supplier_types.name as supplier_type','suppliers.supplier_type_id as supplier_type_id','bank_accounts.account_number','bank_accounts.account_name')->where("invoice_crew.crew_id",$quotation->crew_id)->get();

        $quotation->quotations = $invoices;

        /*foreach($quotation->crew->workers as $w){
            $w->with_emergency = 0;
            $ladb = DB::table("crew_worker")->where("crew_id",$quotation->crew->crew_id)->where("worker_id",$w->worker_id)->first();
            if($ladb){
                if($ladb->with_emergency==1){
                    $w->with_emergency = 1;
                }
            }
        }*/


        foreach($quotation->crew->workers as $w){


            //$intransfer = DB::table("worker_transfer")->where("worker_transfer.worker_id",$w->worker_id)->where("worker_transfer.status",1)->first();
            $intransfer = DB::table("worker_transfer")->join("transfers","transfers.id","=","worker_transfer.transfer_id")->where("transfers.journey_type","1")->where("worker_transfer.worker_id",$w->worker_id)->where("worker_transfer.status",1)->where("transfers.crew_id",$quotation->crew_id)->select('worker_transfer.*','transfers.invoice_id as transfer_invoice_id')->first();
            
           if($intransfer){
            $w->relocation_id = $intransfer->transfer_id;
            $w->invoice_id_ida = $intransfer->transfer_invoice_id;
           } else{
            $w->relocation_id = 0;
            $w->invoice_id_ida = null;
           }

           $intransfer2 = DB::table("worker_transfer")->join("transfers","transfers.id","=","worker_transfer.transfer_id")->where("transfers.journey_type","2")->where("worker_transfer.worker_id",$w->worker_id)->where("worker_transfer.status",1)->where("transfers.crew_id",$quotation->crew_id)->select('worker_transfer.*','transfers.invoice_id as transfer_invoice_id')->first();
            
           if($intransfer2){
            $w->relocation_id_vuelta = $intransfer2->transfer_id;
            $w->invoice_id_vuelta = $intransfer2->transfer_invoice_id;
           } else{
            $w->relocation_id_vuelta = 0;
            $w->invoice_id_vuelta = null;
           }
        }

        foreach($quotation->crew->workers as $w){
            //$inlodging = DB::table("accommodation_worker")->where("accommodation_worker.worker_id",$w->worker_id)->where("accommodation_worker.status",1)->first();
            $inlodging = DB::table("accommodation_worker")->join("accommodations","accommodations.id","=","accommodation_worker.accommodation_id")->where("accommodations.journey_type","1")->where("accommodation_worker.worker_id",$w->worker_id)->where("accommodation_worker.status",1)->where("accommodations.crew_id",$quotation->crew_id)->select('accommodation_worker.*','accommodations.invoice_id as accommodation_invoice_id')->first();
            
           if($inlodging){
            $w->lodging_id = $inlodging->accommodation_id;
            $w->invoice_id_ida = $inlodging->accommodation_invoice_id;
           } else{
            $w->lodging_id = 0;
            $w->invoice_id_ida = null;
           }

           $inlodging2 = DB::table("accommodation_worker")->join("accommodations","accommodations.id","=","accommodation_worker.accommodation_id")->where("accommodations.journey_type","2")->where("accommodation_worker.worker_id",$w->worker_id)->where("accommodation_worker.status",1)->where("accommodations.crew_id",$quotation->crew_id)->select('accommodation_worker.*','accommodations.invoice_id as accommodation_invoice_id')->first();
            
           
           if($inlodging2){
            $w->lodging_id_vuelta = $inlodging2->accommodation_id;
            $w->invoice_id_vuelta = $inlodging2->accommodation_invoice_id;
           } else{
            $w->lodging_id_vuelta = 0;
            $w->invoice_id_vuelta = null;
           }
        }

        foreach($quotation->crew->workers as $w){
            $insnacks = DB::table("worker_snack")->join("snacks","snacks.id","=","worker_snack.snack_id")->where("snacks.journey_type","1")->where("worker_snack.worker_id",$w->worker_id)->where("snacks.crew_id",$quotation->crew_id)->where("worker_snack.status",1)->select('worker_snack.*')->first();
           
           if($insnacks){
            $w->snack_id = $insnacks->snack_id;
           } else{
            $w->snack_id = 0;
           }

           $insnacks2 = DB::table("worker_snack")->join("snacks","snacks.id","=","worker_snack.snack_id")->where("snacks.journey_type","2")->where("worker_snack.worker_id",$w->worker_id)->where("snacks.crew_id",$quotation->crew_id)->where("worker_snack.status",1)->select('worker_snack.*')->first();
           
           if($insnacks2){
            $w->snack_id_vuelta = $insnacks2->snack_id;
           } else{
            $w->snack_id_vuelta = 0;
           }
        }

        foreach($quotation->crew->workers as $w){

            $ticketida = DB::table("worker_ticket")->join("ticket_groups","ticket_groups.id","=","worker_ticket.ticket_group_id")->where("ticket_groups.flight_stretch_id","1")->where("worker_ticket.worker_id",$w->worker_id)->where("ticket_groups.crew_id",$quotation->crew_id)->select('worker_ticket.*','ticket_groups.invoice_id as ticket_invoice_id','worker_ticket.quotation as qclone','worker_ticket.penalty_quotation as penalty_quotation')->first();
           
           if($ticketida){
            $ticketvuelta = ['penalty_invoice_id'=>$ticketida->penalty_invoice_id,'penalty_quotation'=>$ticketida->penalty_quotation,'worker_id'=>$w->worker_id,'ticket_group_id'=>$ticketida->ticket_group_id,'carry_price'=>$ticketida->carry_price,'observation'=>$ticketida->observation,'penalty'=>$ticketida->penalty,'quotation'=>$ticketida->quotation,'qclone'=>$ticketida->qclone,'extra_price'=>$ticketida->extra_price,'invoice_id'=>$ticketida->invoice_id,'id'=>$ticketida->id,'ticket_invoice_id'=>$ticketida->ticket_invoice_id];
            $w->ticketida = $ticketida;
           } else{
           // $ticketida = ['worker_id'=>$w->worker_id,'ticket_group_id'=>null,'carry_price'=>0,'observation'=>'','penalty'=>0,'quotation'=>0,'extra_price'=>0];
           $ticketida = null;
           $w->ticketida = $ticketida;
           }

           $ticketvuelta = DB::table("worker_ticket")->join("ticket_groups","ticket_groups.id","=","worker_ticket.ticket_group_id")->where("ticket_groups.flight_stretch_id","2")->where("worker_ticket.worker_id",$w->worker_id)->where("ticket_groups.crew_id",$quotation->crew_id)->select('worker_ticket.*','ticket_groups.invoice_id as ticket_invoice_id','worker_ticket.quotation as qclone','worker_ticket.penalty_quotation as penalty_quotation')->first();
           
           if($ticketvuelta){
            $ticketvuelta = ['penalty_invoice_id'=>$ticketvuelta->penalty_invoice_id,'penalty_quotation'=>$ticketvuelta->penalty_quotation,'worker_id'=>$w->worker_id,'ticket_group_id'=>$ticketvuelta->ticket_group_id,'carry_price'=>$ticketvuelta->carry_price,'observation'=>$ticketvuelta->observation,'penalty'=>$ticketvuelta->penalty,'quotation'=>$ticketvuelta->quotation,'qclone'=>$ticketvuelta->qclone,'extra_price'=>$ticketvuelta->extra_price,'invoice_id'=>$ticketvuelta->invoice_id,'id'=>$ticketvuelta->id,'ticket_invoice_id'=>$ticketvuelta->ticket_invoice_id];
            $w->ticketvuelta = $ticketvuelta;
           } else{
            //$ticketvuelta = ['worker_id'=>$w->worker_id,'ticket_group_id'=>null,'carry_price'=>0,'observation'=>'','penalty'=>0,'quotation'=>0,'extra_price'=>0];
            $ticketvuelta = null;
            $w->ticketvuelta = $ticketvuelta;
           }
        }

      
        

        $transfers = DB::table("transfers")
        ->join("suppliers","suppliers.id","=","transfers.supplier_id")
        ->join("crew_transfers","crew_transfers.transfer_id","=","transfers.id")
        ->select("transfers.invoice_id","transfers.journey_type","suppliers.id as supplier_id","transfers.id","suppliers.name as supplier","transfers.price","transfers.observation","transfers.upload_date","transfers.entry_text","transfers.exit_text","transfers.quotation","transfers.created_at")
        ->where("crew_transfers.crew_id",$quotation->crew_id)
        ->orderBy("transfers.id","asc")
        ->get();

        foreach($transfers as $a){
            $cantidad2 = DB::table("worker_transfer")->where("status",1)->where("transfer_id",$a->id)->count();
            $a->quantity = $cantidad2;
        }

        $accomodations = DB::table("accommodations")
                        ->join("suppliers","suppliers.id","=","accommodations.supplier_id")
                        ->join("crew_accommodations","crew_accommodations.accommodation_id","=","accommodations.id")
                        ->select("accommodations.invoice_id","accommodations.journey_type","suppliers.id as supplier_id","accommodations.id","suppliers.name as supplier","accommodations.price","accommodations.observation","accommodations.entry_date","accommodations.exit_date","accommodations.created_at")
                        ->where("crew_accommodations.crew_id",$quotation->crew_id)
                        ->orderBy("accommodations.id","asc")
                        ->get();

        foreach($accomodations as $a){
            $cantidad2 = DB::table("accommodation_worker")->where("status",1)->where("accommodation_id",$a->id)->count();
            $a->quantity = $cantidad2;
        }

        $snacks = DB::table("snacks")
                        ->join("crew_snacks","crew_snacks.snack_id","=","snacks.id")
                        ->select("snacks.journey_type","snacks.description","snacks.price","snacks.id")
                        ->where("crew_snacks.crew_id",$quotation->crew_id)
                        ->orderBy("snacks.id","asc")
                        ->get();

        foreach($snacks as $a){
            $cantidad2 = DB::table("worker_snack")->where("snack_id",$a->id)->count();
            $a->quantity = $cantidad2;
        }

        $quotation->transfers = $transfers;
        $quotation->accommodations = $accomodations;
        $quotation->snacks = $snacks;

        return $this->sendResponse($quotation, 'Quotation casi foind');
    }

    /**
     * Crea una nueva cotización a partir de una tripulación
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(int $id, Request $request)
    {
        $this->validate($request, [
            'observations' => 'present|array',
            'observations.*.crew_worker_id' => 'required|exists:crew_worker,id',
            'observations.*.observation' => 'required|max:255',
        ]);

        // guarda el ingreso de la cotización
        Quotation::enter($id, $request->get('observations'));

        // notifica a supervisor de que se ha generado una cotización y debe confirmarla
        NewQuotation::dispatch($id);

        return $this->sendResponse('Ok', 'Cotización ingresada');
    }

    /**
     * Ingresa la factura y order de compra de una cotización
     *
     * @param int $id
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function invoice(int $id, Request $request)
    {
        $this->validate($request, [
            'bank_account_id' => 'required|exists:bank_accounts,id',
            'bill_number' => 'required|string|max:50|unique:invoiced_quotations,bill_number',
            'purchase_order' => 'required|string|max:50|unique:invoiced_quotations,purchase_order',
            'purchase_order_date' => 'required'
        ]);

        return $this->sendResponse(InvoicedQuotation::create([
            'quotation_id' => $id,
            'bank_account_id' => $request->get('bank_account_id'),
            'bill_number' => $request->get('bill_number'),
            'purchase_order' => $request->get('purchase_order'),
            'purchase_order_date' => $request->get('purchase_order_date'),
        ]), 'Quotation invoiced');
    }
}
