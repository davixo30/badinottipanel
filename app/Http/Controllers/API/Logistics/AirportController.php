<?php

namespace App\Http\Controllers\API\Logistics;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Airport;

/**
 * Class AirportController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API\Logistics
 */
class AirportController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de aeropuertos
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->sendResponse(Airport::all(), 'Airports list');
    }
}
