<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\V1\BaseController;
use App\Models\Attribute;
use DB;
use Illuminate\Http\Request;

/**
 * Class AttributeController
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Http\Controllers\API
 */
class AttributeController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Muestra el listado de atributos disponibles
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->sendResponse(Attribute::all(), 'Attributes list');
    }

  

    public function pendingList(Request $request)
    {

        $search = $request->query('search');

        $query = DB::table("pending_attributes")->join("workers","workers.id","=","pending_attributes.worker_id")
                ->join("attributes","attributes.id","=","pending_attributes.attribute_id")
                ->select("workers.rut","workers.code","workers.phone_number","workers.name","workers.last_name","attributes.name as attribute","pending_attributes.date","pending_attributes.late_entry","pending_attributes.date_entry","pending_attributes.status","pending_attributes.crew_id")
                ->where("pending_attributes.status","0")
               /* ->when($search, function ($query, $search) {
                    return $query->where('workers.name', 'like', "%$search%")
                    ->orWhere('workers.last_name', 'like', "%$search%")
                    ->orWhere('workers.rut', 'like', "%$search%")
                    ->orWhere('workers.phone_number', 'like', "%$search%")
                    ->orWhere('workers.code', 'like', "%$search%");
                })*/
                ->orderBy("pending_attributes.date","desc")
                ->get();
             
        foreach($query as $l){
            $ingresotardio = "No";
            if($l->late_entry==1){
                $l->date = $l->date_entry;
                $ingresotardio = "Si";
            }
            $l->late_entry = $ingresotardio;
            $barco = "";
            $cliente ="";
            $crew = DB::table("crews")->where("id",$l->crew_id)->first();
            if($crew){
                $elbarco = DB::table("ships")->where("id",$crew->ship_id)->first();
                if($elbarco){
                    $barco = $elbarco->name;
                }
                $elcliente = DB::table("clients")->where("id",$crew->client_id)->first();
                if($elcliente){
                    $cliente = $elcliente->name;
                }
            }
            $l->ship = $barco;
            $l->client = $cliente;
        }

      
       
        

     

        return $this->sendResponse($query, 'Attributes Pending list');
    }
}
