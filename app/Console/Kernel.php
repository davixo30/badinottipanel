<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
         Commands\EmailDownload::class,
         Commands\EmailExpireDocuments::class,
         Commands\UpdateCrewEskuad::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('crew:eskuad')->hourly();
        $schedule->command('crew:eskuad')->everyMinute();
        $schedule->command('worker:attribute')->everyMinute();
        $schedule->command('update:tickets')->everyMinute();
        $schedule->command('email:download')->dailyAt('00:10');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
