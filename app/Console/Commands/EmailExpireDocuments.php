<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Http\Requests\Eskuad\NewCrewRequest;
use App\Models\Crew;
use App\Jobs\EmailDocuments;

class EmailExpireDocuments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:documents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia email 1 mes antes de que los documentos expiren';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        
        EmailDocuments::dispatch();     
        \Log::info("envia email documentos vencidos");
        return 0;
    }
}



