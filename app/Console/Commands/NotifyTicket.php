<?php

namespace App\Console\Commands;

use App\Models\Lodging;
use App\Models\Relocation;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Twilio\Rest\Client;

/**
 * Class NotifyTicket
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com
 * @package App\Console\Commands
 */
class NotifyTicket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms:send {ticketId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envía una notificación vía SMS al trabajador para embarcarse';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \Twilio\Exceptions\ConfigurationException
     * @throws \Twilio\Exceptions\TwilioException
     * @return void
     */
    public function handle()
    {
        $ticketId = $this->argument('ticketId');


       $data = DB::table('ticket_groups')
       ->join('quotations', 'quotations.id', '=', 'ticket_groups.quotation_id')
       ->join('crews', 'crews.id', '=', 'quotations.crew_id')
       ->join('crew_worker',"crew_worker.crew_id","=","crews.id")
       ->join('workers', 'workers.id', '=', 'crew_worker.worker_id')
       ->select(
           'workers.phone_number',
           'workers.name',
           'workers.last_name',
           'crews.upload_date',
           'crews.observations',
           'crew_worker.id as crew_worker_id',
           'crew_worker.lodging',
           'crew_worker.relocation',
           'ticket_groups.flight_date',
           'ticket_groups.code',
           'crew_worker.worker_id',
           'crews.id',
           'ticket_groups.flight_stretch_id'
       )
       ->where('ticket_groups.id', $ticketId)
       ->first();


     //  return response()->json($data);

   if($data->flight_stretch_id==1){
                   //ida


               // obtiene jefe de flota
               $f = DB::table('users')
                   ->select('name', 'last_name')
                   ->where('position_id', function ($query) {
                       // TODO: pasar a types
                       return $query->select('id')->from('positions')->where('name', 'Administrador');
                   })
                   ->first();

               // configuración de Twilio
               $account_sid = getenv("TWILIO_SID");
               $auth_token = getenv("TWILIO_AUTH_TOKEN");
               $twilio_number = getenv("TWILIO_NUMBER");

               $client = new Client($account_sid, $auth_token);
               $debugNumber = env('DEBUG_NUMBER', null);

               $observation = $this->cleanString($data->observations);

               // mensaje a enviar
               $text = "Hola {$data->name} {$data->last_name}.\n";
               $text .= "Badinotti Marine informa que debe presentarse el dia {$data->upload_date} {$observation}, su vuelo es el dia {$data->flight_date},";
               if($data->code<>null){
                   $text .= " su codigo de pasaje es {$data->code}.\n";
               }


               // en caso de requerir traslado...
               if ($data->relocation) {
                   $relocation = DB::table("transfers")
                   ->join("worker_transfer","worker_transfer.transfer_id","=","transfers.id")
                   ->join("suppliers","suppliers.id","=","transfers.supplier_id")
                   ->where('worker_transfer.crew_id', $data->id)
                   ->where('worker_transfer.worker_id', $data->worker_id)
                   ->select("suppliers.name","transfers.upload_date","transfers.entry_text")
                   ->first();

                   $text .= "Tiene un traslado el día {$relocation->upload_date} en {$relocation->entry_text}\n";
               }

               // en caso de requerir alojamiento...
               if ($data->lodging) {
                   $lodging =  DB::table("accommodations")
                   ->join("accommodation_worker","accommodation_worker.accommodation_id","=","accommodations.id")
                   ->join("suppliers","suppliers.id","=","accommodations.supplier_id")
                   ->where('accommodation_worker.crew_id', $data->id)
                   ->where('accommodation_worker.worker_id', $data->worker_id)
                   ->select("suppliers.name","accommodations.entry_date","accommodations.exit_date")
                   ->first();
                   $text .= "Su alojamiento sera en {$lodging->name} el dia {$lodging->entry_date}.\n";
               }

               $text .= "Favor confirmar su asistencia con Erik Ojeda via Whatsapp.";

               if (!$debugNumber) {
                        $number = "+56{$data->phone_number}";
                }else{
                        $number = $debugNumber;
                }

               $client->messages->create(
               // telefono de quien recibe
               // $debugNumber ? : "+56{$data->phone_number}",
               $debugNumber ? : $number,
                   [
                       'from' => $twilio_number,
                       'body' => $text
                   ]
               );

   }else{
       //vuelta


                           // obtiene jefe de flota
                           $f = DB::table('users')
                           ->select('name', 'last_name')
                           ->where('position_id', function ($query) {
                               // TODO: pasar a types
                               return $query->select('id')->from('positions')->where('name', 'Administrador');
                           })
                           ->first();

                       // configuración de Twilio
                       $account_sid = getenv("TWILIO_SID");
                       $auth_token = getenv("TWILIO_AUTH_TOKEN");
                       $twilio_number = getenv("TWILIO_NUMBER");

                       $client = new Client($account_sid, $auth_token);
                       $debugNumber = env('DEBUG_NUMBER', null);

                       $observation = $this->cleanString($data->observations);

                       // mensaje a enviar
                       $text = "Hola {$data->name} {$data->last_name}.\n";
                       $text .= "Badinotti Marine informa que su fecha de bajada es el dia {$data->upload_date} {$observation}, su vuelo es el dia {$data->flight_date},";
                       if($data->code<>null){
                           $text .= " su codigo de pasaje es {$data->code}.\n";
                       }


                       // en caso de requerir traslado...
                       if ($data->relocation) {
                           $relocation = DB::table("transfers")
                           ->join("worker_transfer","worker_transfer.transfer_id","=","transfers.id")
                           ->join("suppliers","suppliers.id","=","transfers.supplier_id")
                           ->where('worker_transfer.crew_id', $data->id)
                           ->where('worker_transfer.worker_id', $data->worker_id)
                           ->select("suppliers.name","transfers.upload_date","transfers.entry_text")
                           ->first();

                           $text .= "Tiene un traslado el día {$relocation->upload_date} en {$relocation->entry_text}\n";
                       }

                       // en caso de requerir alojamiento...
                       if ($data->lodging) {
                           $lodging =  DB::table("accommodations")
                           ->join("accommodation_worker","accommodation_worker.accommodation_id","=","accommodations.id")
                           ->join("suppliers","suppliers.id","=","accommodations.supplier_id")
                           ->where('accommodation_worker.crew_id', $data->id)
                           ->where('accommodation_worker.worker_id', $data->worker_id)
                           ->select("suppliers.name","accommodations.entry_date","accommodations.exit_date")
                           ->first();
                           $text .= "Su alojamiento sera en {$lodging->name} entre {$lodging->entry_date} y {$lodging->exit_date}.\n";
                       }



                       if (!$debugNumber) {
                            $number = "+56{$data->phone_number}";
                       }else{
                            $number = $debugNumber;
                       }

                       $client->messages->create(
                       // telefono de quien recibe
                       // $debugNumber ? : "+56{$data->phone_number}",
                       $debugNumber ? : $number,
                           [
                               'from' => $twilio_number,
                               'body' => $text
                           ]
                       );


            }
    }


    public function cleanString($text) {
        $utf8 = array(
            '/[áàâãªä]/u'   =>   'a',
            '/[ÁÀÂÃÄ]/u'    =>   'A',
            '/[ÍÌÎÏ]/u'     =>   'I',
            '/[íìîï]/u'     =>   'i',
            '/[éèêë]/u'     =>   'e',
            '/[ÉÈÊË]/u'     =>   'E',
            '/[óòôõºö]/u'   =>   'o',
            '/[ÓÒÔÕÖ]/u'    =>   'O',
            '/[úùûü]/u'     =>   'u',
            '/[ÚÙÛÜ]/u'     =>   'U',
            '/ç/'           =>   'c',
            '/Ç/'           =>   'C',
        );
        return preg_replace(array_keys($utf8), array_values($utf8), $text);
    }
}
