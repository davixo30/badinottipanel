<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Http\Requests\Eskuad\NewCrewRequest;
use App\Models\Crew;
use App\Models\Worker;
use App\Models\PendingAttribute;
use App\Models\WorkerAttribute;
use App\Models\Ship;
use App\Http\Controllers\EskuadController;

class ChangeWorkerAttribute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'worker:attribute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualoza atributos de tripulantes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
     
        $pending = PendingAttribute::where("date","<=",date("Y-m-d"))->where("status","0")->get();
        foreach($pending as $p){
           // \Log::info("Pending ".$p->id);
            $worker = DB::table("workers")->where("id",$p->worker_id)->whereNull('deleted_at')->first();
            if($worker){
            DB::table("workers")->where("id",$p->worker_id)->update(['attribute_id'=>$p->attribute_id]);
            //$worker->attribute_id = $p->attribute_id;
            //$worker->save();

            $w = new WorkerAttribute;
            $w->worker_id = $p->worker_id;
            $w->attribute_id = $p->attribute_id;
            $w->crew_id = $p->crew_id;
            if($p->attribute_id==1){
               // \Log::info("Pending attribute 1");
                $crew = Crew::find($p->crew_id);
                if($p->late_entry==1){
                    $w->date_start = $p->date_entry;
                }else{
                    $w->date_start = $crew->upload_date;
                }             
                $w->date_end = $crew->real_download_date;
                $w->crew_id = $crew->id;

                $existe = DB::table("pending_attributes")->where("crew_id",$crew->id)->where("status","1")->where("worker_id",$p->worker_id)->where("late_entry","1")->where("attribute_id","2")->first();
                if(!$existe){
                    $np = new PendingAttribute;
                    $np->attribute_id = 2;
                    $np->worker_id = $p->worker_id;
                    $np->crew_id = $crew->id;
                    $np->date = date('Y-m-d', strtotime($crew->real_download_date. ' + 1 days'));
                    $np->status = 0;
                    $np->save();
                }
                
            }

            if($p->attribute_id==2){

                $crew = Crew::find($p->crew_id);

                $inicio = date('Y-m-d', strtotime($crew->real_download_date. ' + 1 days'));
                $fin = date('Y-m-d', strtotime($crew->real_download_date.' + 21 days'));
                $disponible = date('Y-m-d', strtotime($crew->real_download_date. ' + 22 days'));
                if($p->late_entry==1){
                    $inicio = date('Y-m-d', strtotime($p->date_entry. ' + 1 days'));
                    $fin = date('Y-m-d', strtotime($p->date_entry.' + 21 days'));
                    $disponible = date('Y-m-d', strtotime($p->date_entry. ' + 22 days'));
                }

                
                $w->date_start = $inicio;
                $w->date_end = $fin;
                $w->crew_id = $crew->id;
               // Log::info("Pending attribute 2");
                $np = new PendingAttribute;
                $np->attribute_id = 10;
                $np->worker_id = $p->worker_id;
                $np->date = $disponible;
                $np->crew_id = $crew->id;
                $np->status = 0;
                $np->save();
            }
            $w->save();

            if($p->late_entry==1 && $p->attribute_id==2){
                                $crew = Crew::with('ship:id,code', 'workers')->findOrFail($p->crew_id);
                                if ($crew) {
                                    EskuadController::updateDatasource($crew->id, false); 
                                }else{
                                    \Log::info("Not found crew update datasource 1 - crewId ".$p->crew_id . " // ".$p->worker_id);
                                }                         
            }
            

            if($p->late_entry==1 && $p->attribute_id==1){
                    $crew = Crew::findOrFail($p->crew_id);
                    if ($crew) {
                        EskuadController::updateDatasource($crew->id, true);
                    }else{
                        \Log::info("Not found crew update datasource 2 - crewId ".$p->crew_id . " // ".$p->worker_id);
                    }
            }


            if($p->attribute_id==3){
                DB::table("pending_attributes")->where("worker_id",$p->worker_id)->where("status","0")->delete();
            }
            if($p->attribute_id==4){
                DB::table("pending_attributes")->where("worker_id",$p->worker_id)->where("status","0")->delete();
            }
            if($p->attribute_id==5){
                DB::table("pending_attributes")->where("worker_id",$p->worker_id)->where("status","0")->delete();
            }
            if($p->attribute_id==6){
                DB::table("pending_attributes")->where("worker_id",$p->worker_id)->where("status","0")->delete();
            }
            if($p->attribute_id==20){
                DB::table("pending_attributes")->where("worker_id",$p->worker_id)->where("status","0")->delete();
            }


            $pendiente = PendingAttribute::find($p->id);
            $pendiente->status = 1;
            $pendiente->save();


        }
        }
        return 0;
    }
}
