<?php

namespace App\Console\Commands;

use App\Http\Requests\Eskuad\NewCrewRequest;
use App\Models\Crew;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

/**
 * Class CrewToEskuad
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com
 * @package App\Console\Commands
 */
class CrewToEskuad extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eskuad:new_crew {crewId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envía una tripulación a Eskuad para generar tareas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @return void
     */
    public function handle()
    {
        $crew = Crew::with('ship:id,code', 'workers')->findOrFail($this->argument('crewId'));
        $payload = new NewCrewRequest($crew);
        $url = env('ESKUAD_NEW_CREW_URL');
        $client = new Client();
        $res = $client->request('POST', $url, ['json' => $payload]);
        dd($res->getBody()->getContents());
        $this->info('Data sent successfully');
    }
}
