<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Http\Requests\Eskuad\NewCrewRequest;
use App\Models\Crew;
use App\Jobs\EmailDownloadDate;

class EmailDownload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:download';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia email 1 semana antes de la fecha de bajada';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = date('Y-m-d', strtotime('+7 days'));
        $crews = DB::table("crews")->where("download_date","=",$date)->get();
        foreach($crews as $t){
            EmailDownloadDate::dispatch($t->id);     
            \Log::info("envia email tripulacion ".$t->id);
        } 
    
        return 0;
    }
}



