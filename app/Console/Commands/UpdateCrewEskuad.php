<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Http\Requests\Eskuad\NewCrewRequest;
use App\Models\Crew;
use GuzzleHttp\Client;
use App\Http\Controllers\EskuadController;

class UpdateCrewEskuad extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crew:eskuad';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza tripulacion de eskuad';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info("Running cron");
            $dia = DB::table("crews")->where("upload_date",'<=',date("Y-m-d"))->where("update_eskuad","0")->where("status_id","3")->get();
            if($dia){
                foreach($dia as $d){
                    EskuadController::updateDatasource($d->id, true);
                }
            }  else {
                \Log::info("Nothing to update ".date("Y-m-d"));
            }
        return 0;
    }
}
