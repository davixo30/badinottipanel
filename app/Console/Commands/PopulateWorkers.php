<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class PopulateWorkers
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com
 * @package App\Console\Commands
 */
class PopulateWorkers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'populate:workers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Carga el listado de trabajadores en JSON a la base de datos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @return int
     */
    public function handle()
    {
        // primero vacía las tablas
        DB::table('workers')->delete();
        DB::table('contracts')->delete();
        DB::table('expirations')->delete();
        DB::table('role_worker')->delete();

        $workers = json_decode(Storage::disk('local')->get('database/workers.json'));

        // random...
        $communes = DB::table('communes')->select('id')->get()->pluck('id');
        $maritalStatuses = DB::table('marital_statuses')->select('id')->get()->pluck('id');

        DB::transaction(function () use ($workers, $communes, $maritalStatuses) {
            // escoje a Ignacio como que el que carga la información inicial
            $uploaderId = User::select('id')->where('rut', '19090413-K')->first()->id;

            foreach ($workers as $i => $worker) {
                // obtiene la comuna a la que pertenece
                $commune = DB::table('communes')
                    ->where('name', 'like', $worker->commune)->first();

                // obtiene estado civil
                $maritalStatus = DB::table('marital_statuses')
                    ->where('name', 'like', $worker->marital_status)->first();

                // inserta el trabajador
                // TODO: code
                $workerId = DB::table('workers')
                    ->insertGetId([
                        'name' => $worker->name,
                        'last_name' => $worker->father_last_name . ' ' . $worker->mother_last_name,
                        'code' => $i,
                        'rut' => $worker->rut,
                        'birthdate' => (new Carbon($worker->birthdate))->format('Y-m-d'),
                        'phone_number' => $worker->phone_number,
                        'address' => $worker->address,
                        'commune_id' => $commune ? $commune->id : $communes->random(),
                        'marital_status_id' => $maritalStatus ? $maritalStatus->id : $maritalStatuses->random(),
                        'created_at' => Carbon::now()->format('Y-m-d')
                    ]);

                // inserta el contrato del trabajador
                // TODO: salary
                DB::table('contracts')
                    ->insert([
                        'salary' => 1,
                        'date' => (new Carbon($worker->contract_date))->format('Y-m-d'),
                        'worker_id' => $workerId,
                        'afp_id' => DB::table('afps')
                            ->where('name', 'like', $worker->afp)->first()->id,
                        'health_forecast_id' => DB::table('health_forecasts')
                            ->where('name', $worker->health_forecast)->first()->id,
                        'contract_type_id' => DB::table('contract_types')
                            ->where('name', strtolower($worker->contract_type))->first()->id,
                        'uploader_id' => $uploaderId
                    ]);

                $arr = explode('/', $worker->roles);

                foreach ($arr as $role) {
                    // inserta rol del trabajador
                    DB::table('role_worker')
                        ->insert([
                            'worker_id' => $workerId,
                            'role_id' => DB::table('roles')
                                ->select('id')
                                ->where('name', $role)
                                ->first()
                                ->id
                        ]);
                }
            }
        });

        return 1;
    }
}
