<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Http\Requests\Eskuad\NewCrewRequest;
use App\Models\Crew;

class UpdateTicketStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:tickets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza pasajes a volado';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $tickets = DB::table("ticket_groups")->where("flight_date","<=",date("Y-m-d"))->where("departure_time","<=",date("H:i:s"))->where("ticket_status_id",2)->get();
        foreach($tickets as $t){
            DB::table("ticket_groups")->where("id",$t->id)->update(['ticket_status_id'=>3]);
            DB::table("worker_ticket")->where("ticket_group_id",$t->id)->update(['ticket_status_id'=>3]);
            DB::table("person_ticket")->where("ticket_group_id",$t->id)->update(['ticket_status_id'=>3]);
            \Log::info("actualizar pasaje ".$t->id);
        } 
    
        return 0;
    }
}



