<?php

namespace App\Mail;

use App\Models\Client;
use App\Models\Crew;
use App\Models\Ship;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

/**
 * Correo para notificar al jefe de flota que algunos trabajadores fueron filtrados. Debe volver a armar la tripulación.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com
 * @package App\Mail
 */
class CrewWorkersFiltered extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var Client
     */
    public $client;

    /**
     * @var Ship
     */
    public $ship;

    /**
     * @var Crew
     */
    public $crew;

    /**
     * @var array
     */
    public $workers = [];

    /** @var string */
    public $subject = 'Tripulantes han sido filtrados';

    /**
     * Create a new message instance.
     *
     * @param Crew $crew
     * @param Client $client
     * @param Ship $ship
     */
    public function __construct(Crew $crew, Client $client, Ship $ship)
    {
        $this->crew = $crew;
        $this->client = $client;
        $this->ship = $ship;
        $tripulantes = DB::table('workers')
            ->select('id', 'name', 'last_name', 'rut')
            ->whereIn('id', function ($query) use ($crew) {
                return $query->select('worker_id')->from('crew_worker')
                    ->where('crew_id', $crew->id)->where('filtered', 1);
            })
            ->get();
            foreach($tripulantes as $t){
               $observacion = DB::table("crew_worker")->where("worker_id",$t->id)->where("crew_id",$crew->id)->first();
               if($observacion){
                $t->observation = $observacion->filter_reason;
               }else{
                $t->observation = "";
               }
            }
        $this->workers = $tripulantes;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.crew_workers_filtered');
    }
}
