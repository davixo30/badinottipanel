<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Correo para notificar a bodega que su compra fue validada y que debe facturarla.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Mail
 */
class PurchaseValidated extends Mailable
{
    use Queueable, SerializesModels;

    /** @var int */
    public $purchaseId;

    /** @var string */
    public $subject = 'Su compra ha sido validada';

    /**
     * Create a new message instance.
     *
     * @param int $purchaseId
     */
    public function __construct(int $purchaseId)
    {
        $this->purchaseId = $purchaseId;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.purchase_validated');
    }
}
