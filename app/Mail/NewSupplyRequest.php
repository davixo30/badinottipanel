<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Correo para notificar a Bodega de que se generó una solicitud de abastecimiento.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Mail
 */
class NewSupplyRequest extends Mailable
{
    use Queueable, SerializesModels;

    /** @var string */
    public $subject = 'Nueva solicitud de abastecimiento';

    /** @var int */
    public $supplyRequestId;

    /**
     * Create a new message instance.
     *
     * @param int $supplyRequestId
     */
    public function __construct(int $supplyRequestId)
    {
        $this->supplyRequestId = $supplyRequestId;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.new_supply_request');
    }
}
