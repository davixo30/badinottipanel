<?php

namespace App\Mail;

use App\Models\Client;
use App\Models\Crew;
use App\Models\Ship;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

/**
 * Correo para notificar a recursos humanos de la nueva tripulación. En caso de no incluir los titulares, debe verificar
 * sus respectivos documentos.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com
 * @package App\Mail
 */
class ScheduledCrewHumanResources extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var Client
     */

    public $client;
    /**
     * @var Ship
     */
    public $ship;

    /**
     * @var Crew
     */
    public $crew;

    /**
     * @var array|\Illuminate\Support\Collection
     */
    public $workers = [];

    /** @var string */
    public $subject = 'Nueva tripulación calendarizada';

    /**
     * Create a new message instance.
     *
     * @param Crew $crew
     * @param Client $client
     * @param Ship $ship
     */
    public function __construct(Crew $crew, Client $client, Ship $ship)
    {
        $this->crew = $crew;
        $this->client = $client;
        $this->ship = $ship;
        $this->workers = DB::table('workers')
            ->select('workers.id', 'name', 'last_name', 'rut')
            ->join('crew_worker', 'crew_worker.worker_id', '=', 'workers.id')
            ->where('crew_id', $crew->id)
            ->where('crew_worker.filtered', 0)
            ->get();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.scheduled_crew_human_resources');
    }
}
