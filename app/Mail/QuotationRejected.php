<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Correo para notificar a bodega que la cotización solicitada fue rechazada.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Mail
 */
class QuotationRejected extends Mailable
{
    use Queueable, SerializesModels;

    /** @var string */
    public $reason;

    /** @var int */
    public $quotationId;

    /** @var string */
    public $subject = 'Su cotización ha sido rechazada';

    /**
     * Create a new message instance.
     *
     * @param string $reason
     * @param int $quotationId
     */
    public function __construct(string $reason, int $quotationId)
    {
        $this->reason = $reason;
        $this->quotationId = $quotationId;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.quotation_rejected');
    }
}
