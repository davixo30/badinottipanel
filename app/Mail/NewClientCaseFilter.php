<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Correo para notificar a Supervisor de que se confirmó una tripulación y debe aplicar filtro caso cliente.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Mail
 */
class NewClientCaseFilter extends Mailable
{
    use Queueable, SerializesModels;

    /** @var string */
    public $subject = 'Nueva tripulación para aplicar filtros';

    /** @var int */
    public $crewId;

    /**
     * Create a new message instance.
     *
     * @param int $crewId
     */
    public function __construct(int $crewId)
    {
        $this->crewId = $crewId;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.new_client_case_filter');
    }
}
