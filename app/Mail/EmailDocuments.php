<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Correo para notificar a Bodega de que se generó una solicitud de abastecimiento.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Mail
 */
class EmailDocuments extends Mailable
{
    use Queueable, SerializesModels;

    /** @var string */
    public $subject = 'Aviso de vencimiendo de documentos';

    /** @var int */
    public $inducciones;

    public $examenes;

    public $documentos;

    public $perfil;

    public $fecha;

    public $carenas;

    public $revistas;

    public $seguro;

    public $epp;

    /**
     * Create a new message instance.
     *
     * @param int $supplyRequestId
     */
    public function __construct($inducciones,$examenes,$documentos,$perfil,$fecha,$carenas,$revistas,$seguro,$epp)
    {
        $this->inducciones = $inducciones;
        $this->examenes = $examenes;
        $this->documentos = $documentos;
        $this->perfil = $perfil;
        $this->fecha = $fecha;
        $this->carenas = $carenas;
        $this->revistas = $revistas;
        $this->seguro = $seguro;
        $this->epp = $epp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.EmailDocuments');
    }
}
