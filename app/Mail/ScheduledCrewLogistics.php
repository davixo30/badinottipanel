<?php

namespace App\Mail;

use App\Models\Client;
use App\Models\Crew;
use App\Models\Quotation;
use App\Models\Ship;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

/**
 * Correo para notificar a logística que debe crear una cotización para una nueva tripulación
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com
 * @package App\Mail
 */
class ScheduledCrewLogistics extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var Client
     */
    public $client;

    /**
     * @var Ship
     */
    public $ship;

    /**
     * @var Crew
     */
    public $crew;

    /**
     * @var Quotation
     */
    public $quotation;

    /**
     * @var array|\Illuminate\Support\Collection
     */
    public $workers = [];

    /** @var string */
    public $subject = 'Nueva tripulación calendarizada';

    /**
     * Create a new message instance.
     *
     * @param Crew $crew
     * @param Client $client
     * @param Ship $ship
     * @param Quotation $quotation
     */
    public function __construct(Crew $crew, Client $client, Ship $ship, Quotation $quotation)
    {
        $this->crew = $crew;
        $this->client = $client;
        $this->ship = $ship;
        $this->quotation = $quotation;
        $this->workers = DB::table('workers')
            ->select('workers.id', 'name', 'last_name', 'rut')
            ->join('crew_worker', 'crew_worker.worker_id', '=', 'workers.id')
            ->where('crew_id', $crew->id)
            ->where('crew_worker.filtered', 0)
            ->get();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.scheduled_crew_logistics');
    }
}
