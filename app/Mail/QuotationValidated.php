<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Correo para notificar a logística que su cotización fue validada y que debe facturarla.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Mail
 */
class QuotationValidated extends Mailable
{
    use Queueable, SerializesModels;

    /** @var int */
    public $quotationId;

    /** @var string */
    public $subject = 'Su cotización ha sido validada';

    /**
     * Create a new message instance.
     *
     * @param int $quotationId
     */
    public function __construct(int $quotationId)
    {
        $this->quotationId = $quotationId;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.quotation_validated');
    }
}
