<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Correo para notificar a Bodega de que se generó una solicitud de abastecimiento.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Mail
 */
class EmailDownloadDate extends Mailable
{
    use Queueable, SerializesModels;

    /** @var string */
    public $subject = 'Compra de pasajes de vuelta para tripulación';

    /** @var int */
    public $crewid;

    public $ship;

    public $date;

    public $client;

    /**
     * Create a new message instance.
     *
     * @param int $supplyRequestId
     */
    public function __construct(int $crewid, string $ship,string $thedate,string $client)
    {
        $this->crewid = $crewid;
        $this->ship = $ship;
        $this->date = $thedate;
        $this->client = $client;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.EmailDownloadDate');
    }
}
