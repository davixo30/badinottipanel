<?php

namespace App\Mail;

use App\Models\WarehouseItemSize;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

/**
 * Correo para notificar al jefe de flota que algunos trabajadores fueron filtrados. Debe volver a armar la tripulación.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com
 * @package App\Mail
 */
class CriticalStock extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $items;

    /** @var string */
    public $subject = 'Nuevo Stock Crítico';

    /**
     * Create a new message instance.
     *
     * @param WarehouseItemSize $items
     */
    public function __construct($items)
    {
        $this->items = $items;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.critical_stock');
    }
}
