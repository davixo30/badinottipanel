<?php

namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\Models\EPPDelivery;

/**
 * Correo para notificar al jefe de flota que algunos trabajadores fueron filtrados. Debe volver a armar la tripulación.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com
 * @package App\Mail
 */
class NewWorkerMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $tripulante;

    /** @var string */
    public $subject = 'Nuevo Tripulante Creado';

    public function __construct(int $id)
    {
        $tripulante = DB::table('workers')->where("id",$id)->first();
        $contrato = DB::table("contracts")->where("worker_id",$id)->first();

        $rol = "Sin información";
        $roles =  DB::table("roles")->join("role_worker","role_worker.role_id","=","roles.id")->where("role_worker.worker_id",$id)->select("roles.name")->get(); 
        if($roles){
            $rol = "";
        }
        foreach($roles as $r){
         $rol = $rol.",".$r->name;
        }
        $rol = substr($rol, 1); 

        $estadocivil = "Sin información";     
        $marital = DB::table("marital_statuses")->where("id",$tripulante->marital_status_id)->first();
        if($marital){
           $estadocivil = $marital->name;
        }

            $headlines = DB::table("headlines")->where("worker_id",$id)->first();
            $barco = "Sin Asignar";
            if($headlines){
                $ship = DB::table("ships")->where("id",$headlines->ship_id)->first();
                if($ship){
                    $barco = $ship->name;
                }
            }
    
        $salud = "Sin información";
        $afp = "Sin información";
        $fechacontrato = "Sin información";
        $salario = "Sin información";
        if($contrato){
            $salario = $contrato->salary;
            $fechacontrato = $contrato->date;
            $health = DB::table("health_forecasts")->where("id",$contrato->health_forecast_id)->first();
            if($health){
                $salud = $health->name;
            }
            $prevision = DB::table("afps")->where("id",$contrato->afp_id)->first();
            if($prevision){
                $afp = $prevision->name;
            }
        }

        $redday = "";
        if($tripulante->red_day=="0"){     
            $redday = "NO";
        }
        if($tripulante->red_day=="1"){
            $redday = "SI";
        }
      

        $tripulante->contract = $fechacontrato;
        $tripulante->role = $rol;    
        $tripulante->marital = $estadocivil;
        $tripulante->ship = $barco;
        $tripulante->health = $salud;
        $tripulante->afp = $afp;
        $tripulante->redday = $redday;
        $tripulante->salary = $salario;
        $tripulante->id = $id;

        $tripulante->epps = EPPDelivery::byWorkerId($id)->with('group', 'size')->orderBy("id","desc")->get();

        $this->tripulante = $tripulante;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.new_worker');
    }
}
