<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Correo para notificar a Operaciones que debe validar o rechazar una cotización.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Mail
 */
class NewQuotation extends Mailable
{
    use Queueable, SerializesModels;

    /** @var int */
    public $quotationId;

    /** @var string */
    public $subject = 'Se ha generado una nueva cotización';

    /**
     * Create a new message instance.
     *
     * @param int $quotationId
     */
    public function __construct(int $quotationId)
    {
        $this->quotationId = $quotationId;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.new_quotation');
    }
}
