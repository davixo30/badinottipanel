<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Correo para notificar a bodega que la compra solicitada fue rechazada.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Mail
 */
class PurchaseRejected extends Mailable
{
    use Queueable, SerializesModels;

    /** @var string */
    public $reason;

    /** @var string */
    public $subject = 'Su compra ha sido rechazada';

    /**
     * Create a new message instance.
     *
     * @param string $reason
     */
    public function __construct(string $reason)
    {
        $this->reason = $reason;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // TODO: en caso de venir de una solicitud de abastecimiento, incluir link en correo
        return $this->view('mail.purchase_rejected');
    }
}
