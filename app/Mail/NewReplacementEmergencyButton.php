<?php

namespace App\Mail;

use App\Models\Client;
use App\Models\Crew;
use App\Models\Ship;
use App\Models\Worker;
use App\Models\EmergencyButton;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

/**
 * Correo para notificar al jefe de flota que algunos trabajadores fueron filtrados. Debe volver a armar la tripulación.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com
 * @package App\Mail
 */
class NewReplacementEmergencyButton extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var Client
     */
    public $client;

    /**
     * @var Ship
     */
    public $ship;

    /**
     * @var Crew
     */
    public $crew;

      /**
     * @var Worker
     */
    public $workers;


    public $type;

     /**
     * @var EmergencyButton
     */
    public $emergency;

    /** @var string */
    public $subject = 'Se ha reemplazado un tripulante mediante boton de emergencia';

    /**
     * Create a new message instance.
     *
     * @param Crew $crew
     * @param Client $client
     * @param Ship $ship
     * @param Worker $worker
     * @param EmergencyButton $emergency
     */
    public function __construct(Crew $crew, Client $client, Ship $ship, Worker $worker,$type,EmergencyButton $emergency)
    {
        $this->crew = $crew;
        $this->client = $client;
        $this->ship = $ship;
        $this->workers = $worker;
        $this->type = $type;
        $this->emergency = $emergency;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->type == 1){
            return $this->view('mail.new_replacement_emergency_button_with_link');
        }
        return $this->view('mail.new_replacement_emergency_button');
    }
}
