<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class Quotation
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Quotation extends Model
{
    /** @var string[] */
    protected $fillable = ['status', 'rejection_reason', 'crew_id', 'confirmed_at'];

    /** @var bool */
    public $timestamps = false;

    /**
     * Define la relación con la tripulación de la cotización
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function crew()
    {
        return $this->belongsTo(Crew::class)->with('client', 'ship', 'workers');
    }

    public function crewbackup()
    {
        return $this->belongsTo(Crew::class)->with('client', 'ship','workers');
    }


    public function crewWithoutEmergencyAndReplace()
    {
        return $this->belongsTo(Crew::class)->with('client', 'ship', 'workersWithoutEmergencyAndReplace');
    }

    /**
     * Define la relación con los pasajes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany(TicketGroup::class)
            ->with('airline', 'arrival_airport', 'departure_airport', 'flight_stretch', 'tickets');
    }

    /**
     * Define la relación con la factura de la cotización
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function invoice()
    {
        return $this->hasOne(InvoicedQuotation::class)->with('bank_account');
    }

    /**
     * Define el 'scope' para cotizaciones de la vista de operaciones.
     * Incluye todas las cotizaciones excepto las 'fantasma'
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public static function scopeOperations(Builder $query)
    {
        return $query->where('entered', 1);
    }

    /**
     * Define la creación de una cotización, pasa a poner 'entered' en true y deja de ser una cotización 'fantasma'
     * TODO faltan algunas validaciones de negocio, p.e: que todos los q solicitaron traslado alojamiento lo tengan
     * TODO borrar pasajes sueltos
     *
     * @param int $id
     * @param array $observations
     *
     * @return self
     * @throws \Exception
     */
    public static function enter(int $id, array $observations)
    {
        $quotation = null;

        DB::transaction(function () use ($id, $observations, &$quotation) {
            $quotation = self::findOrFail($id);

            // primero valida que la tripulación se encuentre en estado 'en calendario'
            $crew = Crew::select('id', 'status_id')->findOrFail($quotation->crew_id);

            if ($crew->status_id !== CrewStatus::getInCalendarStatusId()) {
                throw new \Exception("Solo se pueden crear cotizaciones en tripulaciones con estado 'En calendario'");
            }

            // TODO de alguna forma debe validar que no tenga otra cotización previa sin ser aprobada o rechazada

            // luego valida que todos los trabajadores de la tripulación tengan su pasaje,
            // además de añadir su observación de ser necesario
            $workers = CrewWorker::where('crew_id', $crew->id)->where('filtered', 0)->where('with_emergency', 0)
                ->where('is_replacement', 0)
                ->select('id')->get()->pluck('id');
            $collection = Collection::make($observations);

            foreach ($workers as $worker) {
                if (!Ticket::where('crew_worker_id', $worker)->exists()) {
                    throw new \Exception(
                        'Solo se pueden crear cotizaciones si a todos los trabajadores se les ha asignado un pasaje'
                    );
                }

                // guarda observación en caso de existir
                $i = $collection->firstWhere('crew_worker_id', $worker);

                if ($i) {
                    CrewWorker::find($worker)->update(['observations' => $i['observation']]);
                }
            }

            // finalmente guarda la cotización
            $quotation->entered = 1;
            $quotation->entered_at = Carbon::now();
            $quotation->save();
        });

        return $quotation;
    }

    /**
     * Rechaza una cotización
     *
     * @param int $id ID de cotización
     * @param string $reason Motivo de rechazo
     *
     * @return mixed
     * @throws \Exception
     */
    public static function rejectQuotation(int $id, string $reason)
    {
        $quotation = self::findOrFail($id);

        if ($quotation->confirmed_at !== null && $quotation->status !== null) {
            throw new \Exception('Solo se puede rechazar una cotización que no haya sido confirmada');
        }

        DB::transaction(function () use (&$quotation, $reason) {
            // cambia estado de cotización a rechazada
            $quotation->update(['status' => false, 'rejection_reason' => $reason, 'confirmed_at' => Carbon::now()]);

            // crea nueva cotización 'fantasma'
            $quotation = Quotation::create(['crew_id' => $quotation->crew_id]);
        });

        return $quotation;
    }

    /**
     * Valida una cotización
     *
     * @param int $id ID de cotización
     *
     * @throws \Exception
     * @return mixed
     */
    public static function validateQuotation(int $id)
    {
        $quotation = self::findOrFail($id);

        if ($quotation->confirmed_at !== null && $quotation->status !== null) {
            throw new \Exception('Solo se puede validar una cotización que no haya sido confirmada');
        }

        return $quotation->update(['status' => true, 'confirmed_at' => Carbon::now()]);
    }
}
