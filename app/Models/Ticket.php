<?php

namespace App\Models;

use App\Types\TicketStatusesTypes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Ticket
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Ticket extends Model
{
    /** @var string[] */
    protected $fillable = ['crew_worker_id', 'ticket_group_id'];

    /** @var bool */
    public $timestamps = false;

    /**
     * Al crear, añade por defecto el tipo 'No comprado'
     */
    protected static function boot()
    {
        parent::boot();

        // auto-sets values on creation
        static::creating(function ($query) {
            $query->ticket_status_id = TicketStatus::where('name', TicketStatusesTypes::$NO_COMPRADO)
                ->select('id')->first()->id;
        });
    }

    /**
     * Define la relación con el trabajador
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function crew_worker()
    {
        return $this->belongsTo(CrewWorker::class)->with('worker');
    }

    /**
     * Define la relación con el estado del pasaje
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket_status()
    {
        return $this->belongsTo(TicketStatus::class);
    }

    /**
     * Obtiene los pasajes que se encuentren en stock de cierta aerolínea
     *
     * @param int $airlineId
     *
     * @return mixed
     */
    public static function inStock(int $airlineId)
    {
        return DB::table('tickets')
            ->select('tickets.id', 'ticket_groups.created_at','workers.name','workers.last_name')
            ->join('ticket_groups', 'ticket_groups.id', '=', 'tickets.ticket_group_id')
            ->join('crew_worker', 'crew_worker.id', '=', 'tickets.crew_worker_id')
            ->join('workers', 'workers.id', '=', 'crew_worker.worker_id')
            ->where('ticket_groups.airline_id', $airlineId)
            ->where('tickets.ticket_status_id', function ($q) {
                return $q->select('id')->from('ticket_statuses')->where('name', TicketStatusesTypes::$STOCK);
            });
    }

    /**
     * Envía un pasaje a stock
     *
     * @param int $id
     *
     * @return mixed
     */
    public static function toStock(int $id)
    {
        $status = TicketStatus::where('name', TicketStatusesTypes::$STOCK)->first();

        $ticket = self::findOrFail($id);
        $ticket->ticket_status_id = $status->id;
        $ticket->save();

        return $ticket;
    }
}
