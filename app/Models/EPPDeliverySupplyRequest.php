<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EPPDeliverySupplyRequest
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class EPPDeliverySupplyRequest extends Model
{
    /** @var string[] */
    protected $fillable = ['warehouse_item_size_id', 'worker_id', 'supply_request_id','purchase_date','supplier_id'];

    /** @var string */
    protected $table = 'epp_delivery_supply_request';

    /** @var bool */
    public $timestamps = false;

    /**
     * Define la relación con el EPP
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function epp()
    {
        return $this->hasOne(WarehouseItemSize::class, 'id', 'warehouse_item_size_id')
            ->with('item');
    }

    /**
     * Define la relación con el trabajador
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function worker()
    {
        return $this->belongsTo(Worker::class)->with('roles');
    }
}
