<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TechnicalSpecifications
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class TechnicalSpecifications extends Model
{
    /** @var string[] */
    protected $fillable = [
        'shipowner', 'plate_port', 'trg', 'bollard_pull', 'light_displacement', 'year_of_construction', 'country',
        'shipyards', 'total_length', 'sleeve', 'strut_to_main_deck', 'fuel_tank_capacity', 'fresh_water_pond_capacity',
        'observations', 'main_engine', 'power', 'motor_series', 'transmission_box', 'transmission_box_series',
        'relation', 'cruising_speed', 'fuel_consumption', 'propeller_type', 'ship_id'
    ];

    /** @var string */
    protected $primaryKey = 'ship_id';
}
