<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Jobs\NewReplacementEmergencyButton;
use App\Jobs\NewWorkerEmergencyButton;
use App\Models\WorkerAttribute;
use App\Models\PendingAttribute;
use App\Http\Controllers\EskuadController;
/**
 * Class EmergencyButton
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class EmergencyButton extends UploaderIdModel
{
    /** @var string[] */
    protected $fillable = ['reason', 'crew_id', 'emergency_crew_worker_id', 'replacement_crew_worker_id','with_ticket'];

    /**
     * Define el 'scope' solo para botones con reemplazo
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeWithReplacement($query)
    {
        return $query->where('replacement_crew_worker_id', '<>', null);
    }

    public function scopeWithTicket($query)
    {
        return $query->where('with_ticket', '1');
    }

    /**
     * Crea un nuevo botón de emergencia
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public static function create(array $attributes = [])
    {
        $button = null;

        DB::transaction(function () use ($attributes, &$button) {
            $updateDatasource = false;
            // marca el crew_worker anterior como bajado por emergencia
            $crewWorker = CrewWorker::findOrFail($attributes['emergency_crew_worker_id']);
            $crewWorker->update(['with_emergency' => true]);
                        $hoy = date("Y-m-d");
                        if($attributes['emergency_crew_worker_id']){

                            DB::delete("DELETE from pending_attributes where worker_id=? and attribute_id=? and crew_id=?",array($crewWorker->worker_id,2,$crewWorker->crew_id));
                            if ($attributes['replacement_worker_id']) {
                                $np = new PendingAttribute;
                                $np->attribute_id = 2;
                                $np->worker_id = $crewWorker->worker_id;
                                $np->date = date('Y-m-d', strtotime($attributes['entry_date']. ' + 1 days'));
                                $np->crew_id = $crewWorker->crew_id;
                                $np->late_entry = 1;
                                $np->date_entry = $attributes['entry_date'];
                                $np->status = 0;
                                $np->save();
    
                                $crew = Crew::with('ship:id,code', 'workers')->findOrFail($crewWorker->crew_id);
                                if($hoy==$crew->upload_date){
                                    EskuadController::updateDatasource($crewWorker->crew_id, false);
                                }
                            }else{
                                if($hoy == date('Y-m-d', strtotime($attributes['entry_date']))){
                                    $theworker = Worker::find($crewWorker->worker_id);
                                    $theworker->attribute_id = 2;
                                    $theworker->save();
    
                                    $w = new WorkerAttribute;
                                    $w->worker_id = $crewWorker->worker_id;
                                    $w->attribute_id = 2;
                                    $w->crew_id = $crewWorker->crew_id;
                                    $inicio = date('Y-m-d', strtotime($attributes['entry_date']));
                                    $fin = date('Y-m-d', strtotime($attributes['entry_date'].' + 20 days'));
                                    $disponible = date('Y-m-d', strtotime($attributes['entry_date']. ' + 21 days'));
                                    $w->date_start = $inicio;
                                    $w->date_end = $fin;
                                    $w->save();

                                    $np = new PendingAttribute;
                                    $np->attribute_id = 10;
                                    $np->worker_id = $crewWorker->worker_id;
                                    $np->date = $disponible;
                                    $np->crew_id = $crewWorker->crew_id;
                                    $np->status = 0;
                                    $np->save();

                                    $crew = Crew::with('ship:id,code', 'workers')->findOrFail($crewWorker->crew_id);
                                    if($hoy==$crew->upload_date){
                                        EskuadController::updateDatasource($crewWorker->crew_id, false);
                                    }
                                }else{
                                    $np = new PendingAttribute;
                                    $np->attribute_id = 2;
                                    $np->worker_id = $crewWorker->worker_id;
                                    $np->date = date('Y-m-d', strtotime($attributes['entry_date']));
                                    $np->crew_id = $crewWorker->crew_id;
                                    $np->status = 0;
                                    $np->save();
                                }
                            }
                        }
            // debe crear un crew worker en caso de existir un id de reemplazo
            $replacementCrewWorker = (object) ['id' => null];
            if ($attributes['replacement_worker_id']) {
                // primero debe crear el crew_worker de este nuevo trabajador
                $services = $attributes['services'];
                $replacementCrewWorker = CrewWorker::create([
                    'lodging' => $services['lodging'],
                    'lodging_food' => $services['lodging_food'],
                    'relocation' => $services['relocation'],
                    'crew_id' => $crewWorker->crew_id,
                    'worker_id' => $attributes['replacement_worker_id'],
                    'is_replacement' => true,
                    'entry_date'=>$attributes['entry_date']
                ]);
            }
            // registra los datos del botón de emergencia
            $button = static::query()->create([
                'emergency_crew_worker_id' => $crewWorker->id,
                'replacement_crew_worker_id' => $replacementCrewWorker->id,
                'crew_id' => $crewWorker->crew_id,
                'reason' => $attributes['reason'],
                'with_ticket' => $attributes['with_ticket']
            ]);


            if ($attributes['replacement_worker_id']) {
                    $crew = Crew::find($crewWorker->crew_id);
                    $newworker = Worker::find($attributes['replacement_worker_id']);
                    NewReplacementEmergencyButton::dispatch($crew,$newworker,$button);


                    if($hoy==$attributes['entry_date']){
                        $newworker->attribute_id = 1;
                        $newworker->save();

                        $w = new WorkerAttribute;
                        $w->worker_id = $attributes['replacement_worker_id'];
                        $w->attribute_id = 1;
                        $w->date_start = $crew->upload_date;
                        $w->date_end = date('Y-m-d', strtotime($crew->real_download_date));
                        $w->crew_id = $crew->id;
                        $w->save();

                        $np = new PendingAttribute;
                        $np->attribute_id = 2;
                        $np->worker_id = $attributes['replacement_worker_id'];
                        $np->date = date('Y-m-d', strtotime($crew->real_download_date. ' + 1 days'));
                        $np->status = 0;
                        $np->crew_id = $crew->id;
                        $np->save();

                        $newpeople = 0;
                        $crew = Crew::with('ship:id,code', 'workers')->findOrFail($crewWorker->crew_id);
                        EskuadController::updateDatasource($crewWorker->crew_id, true);
                    }else{
                        $subida = date("Y-m-d",strtotime($attributes['entry_date']));
                        if(($subida>$crew->upload_date) && $subida>$hoy) {
                            $newatributo = 13;
                            if($newworker->attribute_id==1){
                                $newatributo = 17;
                            }
                            if($newworker->attribute_id==10){
                                $newatributo = 11;
                            }
                            if($newworker->attribute_id==2){
                                $newatributo = 11;
                            }
                            $newworker->attribute_id = $newatributo;
                            $newworker->save();

                            $w = new WorkerAttribute;
                            $w->worker_id = $attributes['replacement_worker_id'];
                            $w->attribute_id = $newatributo;
                            $w->date_start = date("Y-m-d");
                            $w->date_end = date('Y-m-d', strtotime($attributes['entry_date']. ' - 1 days'));
                            $w->crew_id = $crew->id;
                            $w->save();

                            $np = new PendingAttribute;
                            $np->attribute_id = 1;
                            $np->worker_id = $attributes['replacement_worker_id'];
                            $np->date = $attributes['entry_date'];
                            $np->crew_id = $crew->id;
                            $np->late_entry = 1;
                            $np->date_entry = $attributes['entry_date'];
                            $np->status = 0;
                            $np->save();
                        }else{
                            if(($subida>=$crew->upload_date) && $subida<$hoy) {
                                $newworker->attribute_id = 1;
                                $newworker->save();
        
                                $w = new WorkerAttribute;
                                $w->worker_id = $attributes['replacement_worker_id'];
                                $w->attribute_id = 1;
                                $w->date_start = $crew->upload_date;
                                $w->date_end = date('Y-m-d', strtotime($crew->real_download_date));
                                $w->crew_id = $crew->id;
                                $w->save();
        
                                $np = new PendingAttribute;
                                $np->attribute_id = 2;
                                $np->worker_id = $attributes['replacement_worker_id'];
                                $np->date = date('Y-m-d', strtotime($crew->real_download_date. ' + 1 days'));
                                $np->status = 0;
                                $np->crew_id = $crew->id;
                                $np->save();
        
                                $newpeople = 0;
                                $crew = Crew::with('ship:id,code', 'workers')->findOrFail($crewWorker->crew_id);
                                EskuadController::updateDatasource($crewWorker->crew_id, true);
                            }
                        }
                    }
            }
            // crea una cotización de emergencia 'fantasma'
            EmergencyQuotation::create(['id' => $button->id]);
        });

        return $button;
    }


    public static function createAddWorker(array $attributes = [])
    {
        $button = null;

        DB::transaction(function () use ($attributes, &$button) {
            // marca el crew_worker anterior como bajado por emergencia
            $crewWorker = Crew::findOrFail($attributes['crew_id']);
            //$crewWorker->update(['with_emergency' => true]);

            // debe crear un crew worker en caso de existir un id de reemplazo
            $replacementCrewWorker = (object) ['id' => null];


                // primero debe crear el crew_worker de este nuevo trabajador
                $services = $attributes['services'];
                $replacementCrewWorker = CrewWorker::create([
                    'lodging' => $services['lodging'],
                    'lodging_food' => $services['lodging_food'],
                    'relocation' => $services['relocation'],
                    'crew_id' => $crewWorker->id,
                    'worker_id' => $attributes['replacement_worker_id'],
                    'is_replacement' => true,
                    'with_emergency' => false,
                    'late_entry'=>true,
                    'entry_date'=>$attributes['entry_date']
                ]);



            // registra los datos del botón de emergencia
            $button = static::query()->create([
                'emergency_crew_worker_id' => $replacementCrewWorker->id,
                'replacement_crew_worker_id' => $replacementCrewWorker->id,
                'crew_id' => $crewWorker->id,
                'reason' => $attributes['reason'],
                'with_ticket' => $attributes['with_ticket']
            ]);

            if ($attributes['replacement_worker_id']) {
                    $crew = Crew::find($crewWorker->id);
                    $newworker = Worker::find($attributes['replacement_worker_id']);
                    NewWorkerEmergencyButton::dispatch($crew,$newworker,$button);

                    $hoy = date("Y-m-d");
                    if($hoy==$attributes['entry_date']){
                        $newworker->attribute_id = 1;
                        $newworker->save();

                        $w = new WorkerAttribute;
                        $w->worker_id = $attributes['replacement_worker_id'];
                        $w->attribute_id = 1;
                        $w->date_start = $crew->upload_date;
                        $w->date_end = $crew->real_download_date;
                        $w->crew_id = $crew->id;
                        $w->save();

                        $np = new PendingAttribute;
                        $np->attribute_id = 2;
                        $np->worker_id = $attributes['replacement_worker_id'];
                        $np->date = date('Y-m-d', strtotime($crew->real_download_date. ' + 1 days'));
                        $np->status = 0;
                        $np->crew_id = $crew->id;
                        $np->save();

                        EskuadController::updateDatasource($crew->id, true);
                    }else{
                        $subida = date("Y-m-d",strtotime($attributes['entry_date']));
                        if(($subida>$crew->upload_date) && $subida>$hoy) {
                            $newatributo = 13;
                            if($newworker->attribute_id==1){
                                $newatributo = 17;
                            }
                            if($newworker->attribute_id==10){
                                $newatributo = 11;
                            }
                            if($newworker->attribute_id==2){
                                $newatributo = 11;
                            }
                            $newworker->attribute_id = $newatributo;
                            $newworker->save();

                            $w = new WorkerAttribute;
                            $w->worker_id = $attributes['replacement_worker_id'];
                            $w->attribute_id = $newatributo;
                            $w->date_start = date("Y-m-d");
                            $w->date_end = date('Y-m-d', strtotime($attributes['entry_date']." - 1 days"));
                            $w->crew_id = $crew->id;
                            $w->save();

                            $np = new PendingAttribute;
                            $np->attribute_id = 1;
                            $np->worker_id = $attributes['replacement_worker_id'];
                            $np->date = $attributes['entry_date'];
                            $np->late_entry = 1;
                            $np->date_entry = $attributes['entry_date'];
                            $np->status = 0;
                            $np->crew_id = $crew->id;
                            $np->save();
                        }else{
                            if(($subida>=$crew->upload_date) && $subida<$hoy) {
                                

                                $newworker->attribute_id = 1;
                                $newworker->save();
        
                                $w = new WorkerAttribute;
                                $w->worker_id = $attributes['replacement_worker_id'];
                                $w->attribute_id = 1;
                                $w->date_start = $crew->upload_date;
                                $w->date_end = $crew->real_download_date;
                                $w->crew_id = $crew->id;
                                $w->save();
        
                                $np = new PendingAttribute;
                                $np->attribute_id = 2;
                                $np->worker_id = $attributes['replacement_worker_id'];
                                $np->date = date('Y-m-d', strtotime($crew->real_download_date. ' + 1 days'));
                                $np->status = 0;
                                $np->crew_id = $crew->id;
                                $np->save();
        
                                EskuadController::updateDatasource($crew->id, true);
                            }
                        }
                    }
            }


            // crea una cotización de emergencia 'fantasma'
            EmergencyQuotation::create(['id' => $button->id]);
        });

        return $button;
    }

    /**
     * Define la relación con la tripulación
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function crew()
    {
        return $this->belongsTo(Crew::class)->with(
            'client:id,name',
            'ship:id,name',
            'status'
        );
    }

    /**
     * Defime la relación con la cotización del botón de emergenci<
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function quotation()
    {
        return $this->hasOne(EmergencyQuotation::class, 'id', 'id')->with('tickets');
    }

    /**
     * Define la relación con el tripulante de reemplazo
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function replacement()
    {
        return $this->hasOne(CrewWorker::class, 'id', 'replacement_crew_worker_id')
            ->with('lodging_data', 'relocation_data', 'worker');
    }
}
