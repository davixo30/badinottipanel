<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class EmergencyQuotation
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class EmergencyQuotation extends Model
{
    /** @var string[] */
    protected $fillable = ['id'];

    /** @var bool */
    public $timestamps = false;

    /**
     * Define la relación con los pasajes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany(TicketGroup::class, 'emergency_quotation_id', 'id')
            ->with('airline', 'arrival_airport', 'departure_airport', 'flight_stretch', 'tickets');
    }

    /**
     * Ingresa una cotización de emergencia
     *
     * @param int $id
     * @param $observations
     *
     * @return null
     */
    public static function enter(int $id, $observations)
    {
        $quotation = null;

        DB::transaction(function () use ($id, $observations, &$quotation) {
            $quotation = self::findOrFail($id);
            $button = EmergencyButton::findOrFail($id);

            // añade observaciones de ser necesario
            $worker = CrewWorker::where('id', $button->replacement_crew_worker_id)->first();
            $worker->update(['observations' => $observations]);

            // finalmente guarda la cotización
            $quotation->entered = 1;
            $quotation->entered_at = Carbon::now();
            $quotation->save();
        });

        return $quotation;
    }
}
