<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = "persons";
    protected $fillable = ['name', 'last_name','rut','email','status','role','phone'];
    public $timestamps = false;



    public static function scopeList($value){
        $search = $value;
     return self::when($search, function ($query, $search) {
            return $query->where('name', 'like', "%$search%")
                ->orWhere('last_name', 'like', "%$search%");
        })->where("status",1)->orderBy('name','asc');
    }
}
