<?php

namespace App\Models;

use App\Types\CrewStatusesTypes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class Crew
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Crew extends Model
{
    /** @var string[] */
    protected $fillable = [
        'client_id', 'ship_id', 'status_id', 'upload_date',
        'download_date', 'real_download_date', 'observations','supervisor_id','supply_request','extra_days'
    ];

    /** @var string[] */
    protected $hidden = ['client_id', 'ship_id', 'status_id', 'updated_at'];

    /**
     * Define el 'scope' para tripulaciones con estado 'En calendario'
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeCalendar(Builder $query)
    {
        return $query->where('status_id', function ($q) {
            return $q->select('id')->from('crew_statuses')->where('name', CrewStatusesTypes::$EN_CALENDARIO);
        });
    }


    /**
     * Define el 'scope' para tripulaciones de la vista de logística.
     * Incluye las tripulaciones que se encuentren en estado 'En calendario' y no tienen cotización o ha sido rechazada.
     * También incluye las tripulaciones que tienen cotización aprobada y necesitan completar datos de la factura.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeLogistics(Builder $query)
    {
        return $query->where('status_id', function ($q) {
                return $q->select('id')
                    ->from('crew_statuses')
                    ->where('name', CrewStatusesTypes::$EN_CALENDARIO);
            })
            ->whereIn('id', function ($q) {
                // obtiene tripulaciones en estado 'En calendario' y con cotizaciones fantasmas
                return $q->select('crew_id')
                    ->from('quotations')
                    ->where('entered', 0);
            })
            ->orWhereIn('id', function ($q) {
                // obtiene tripulaciones con cotizaciones aprobadas sin facturar
                return $q->select('crew_id')
                    ->from('quotations')
                    ->where('status', 1)
                    ->whereNotIn('id', function ($_q) {
                        return $_q->select('quotation_id')
                            ->from('invoiced_quotations');
                    });
            })
            ->orderBy('crews.created_at',"desc");
    }

    /**
     * Define el 'scope' para tripulaciones con solicitud de abastecimiento pendiente de comprar
     *
     * @param Builder $query
     *
     * @return Builder
     */
    /*public function scopePendingSupplyRequestPurchase(Builder $query)
    {
        return $query->whereIn('id', function ($q) {
            // obtiene las tripulaciones con la compra de la solicitud de abastecimiento rechazada (para crear otra)
            return $q->select('id')
                ->from('supply_requests')
                ->whereIn('id', function ($_q) {
                    return $_q->select('supply_request_id')
                        ->from('purchases')
                        ->where('status', false);
                });
        })->orWhereNotIn('id', function ($q) {
            // obtiene tripulaciones sin compra (para generar una nueva)
            return $q->select('id')
                ->from('supply_requests')
                ->whereIn('id', function ($_q) {
                    return $_q->select('supply_request_id')
                        ->from('purchases')
                        ->where('supply_request_id', '<>', null)
                        ->where('status', true)
                        ->orWhere('status', null);
                });
        });
    }*/

    public function scopePendingSupplyRequestPurchase(Builder $query)
    {
        return $query->whereIn('id', function ($q) {
            // obtiene las tripulaciones con la compra de la solicitud de abastecimiento rechazada (para crear otra)
            return $q->select('id')
                ->from('supply_requests')
                ->whereIn('id', function ($_q) {
                    return $_q->select('supply_request_id')
                        ->from('purchases')
                        ->where('validated',0)
                        ->where('status', false);
                });
        })->orWhereNotIn('id', function ($q) {
            // obtiene tripulaciones sin compra (para generar una nueva)
            return $q->select('id')
                ->from('supply_requests')
                ->whereIn('id', function ($_q) {
                    return $_q->select('supply_request_id')
                        ->from('purchases')
                        ->where('supply_request_id', '<>', null)
                   
                        ->where('status', true)
                        ->orWhere('status', null);
                });
        });
    }

    /**
     * Define el 'scope' para tripulaciones de la vista de supervisor.
     * Incluye las tripulaciones que se encuentren en estado 'Filtro caso cliente' y 'En calendario' que no tengan
     * aún una solicitud de abastecimiento.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeSupervisor(Builder $query)
    {
        return $query->where('status_id', function ($query) {
            // incluye tripulaciones para filtro caso cliente
            return $query->select('id')
                ->from('crew_statuses')
                ->where('name', CrewStatusesTypes::$FILTRO_CASO_CLIENTE);
        })->orWhere('status_id', function ($query) {
            // incluye tripulaciones en calendario...
            return $query->select('id')
                ->from('crew_statuses')
                ->where('name', CrewStatusesTypes::$EN_CALENDARIO);
        })->whereNotIn('id', function ($query) {
            // ...omitiendo las que ya tengan una solicitud de abastecimiento
            return $query->select('id')
                ->from('supply_requests');
        });
    }

    /**
     * Define la relación con el cliente asociado a la tripulación
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * Define la relación con los botones de emergencia de la tripulación
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function emergency_buttons()
    {
        return $this->hasMany(EmergencyButton::class)->with('quotation', 'replacement');
    }

    /**
     * Define la relación con la cotización aprobada de la tripulación
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function emergency_quotation()
    {
        return $this->hasOne(EmergencyQuotation::class);
    }

    /**
     * Obtiene el listado trabajadores filtrados
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function filtered_workers()
    {
        return $this->hasMany(CrewWorker::class)->where('filtered', 1)->with('worker');
    }

    /**
     * Obtiene el listado completo de trabajadores, incluso los filtrados y con emergencia
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function full_workers()
    {
        return $this->hasMany(CrewWorker::class)->with('worker');
    }

    /**
     * Define la relación con la cotización 'fanstasma' de la tripulación
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ghost_quotation()
    {
        return $this->hasOne(Quotation::class)->where('entered', 0);
    }

    /**
     * Define la relación con la compra. Se utiliza en el listado de compras en Abastecimiento
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOneThrough
     */
    public function purchase()
    {
        return $this->hasOneThrough(
            Purchase::class,
            SupplyRequest::class,
            'id',
            'supply_request_id'
        );
    }

    /**
     * Define la relación con las compras
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function purchases()
    {
        return $this->hasManyThrough(
            Purchase::class,
            SupplyRequest::class,
            'id',
            'supply_request_id'
        )
            ->with('supplier', 'warehouse_item_sizes')
            ->orderBy('created_at', 'desc');
    }

    /**
     * Define la relación con la cotización aprobada de la tripulación
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function quotation()
    {
        return $this->hasOne(Quotation::class)->where('status', 1);
    }

    /**
     * Define la relación con las cotizaciones (no fantasmas) de la tripulación
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function quotations()
    {
        return $this->hasMany(Quotation::class)->where('entered', 1)
            ->with('crew', 'tickets')
            ->orderBy('entered_at', 'desc');
    }

    /**
     * Define la relación con la embarcación de la tripulación
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ship()
    {
        return $this->belongsTo(Ship::class);
    }

    /**
     * Define la relación con el estado de la tripulación
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(CrewStatus::class);
    }

    /**
     * Define la relación con la solicitud de abastecimiento
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function supply_request()
    {
        return $this->hasOne(SupplyRequest::class, 'id', 'id')
            ->with('entered_purchase', 'epp_deliveries', 'purchase', 'warehouse_items');
    }

    /**
     * Define la relación con los trabajadores de la tripulación solo los aprobados y sin emergencias
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workers()
    {   
        $workers = $this->hasMany(CrewWorker::class)->where('filtered', 0)
        ->with('lodging_data', 'relocation_data', 'worker');


        return $workers;
    }

    public function workersBackup()
    {
        return $this->hasMany(CrewWorker::class)->where('filtered', 0)
            ->where('with_emergency', 0)
            ->with('lodging_data', 'relocation_data', 'worker');
    }

    public function workersWithEmergency()
    {
        return $this->hasMany(CrewWorker::class)->where('filtered', 0)
            ->with('lodging_data', 'relocation_data', 'worker');
    }

    

    public function workersWithoutEmergencyAndReplace()
    {
        return $this->hasMany(CrewWorker::class)->where('filtered', 0)
            ->where('with_emergency', 0)
            ->where('is_replacement', 0)
            ->with('lodging_data', 'relocation_data', 'worker');
    }

    /**
     * Confirma la tripulación y modifica su estado
     *
     * @param int $id
     * @param array $lodgings
     * @param array $lodgingsFood
     * @param array $relocations
     *
     * @throws \Exception
     */
    public static function confirmCrew(int $id, array $lodgings, array $lodgingsFood, array $relocations)
    {
        // verifica que solo se pueda confirmar en estado borrador
        $statusId = CrewStatus::getDefaultStatusId();
        $crew = self::findOrFail($id);

        if ($crew->status_id !== $statusId) {
            throw new \Exception("Solo se puede confirmar una tripulación en estado borrador");
        }

        DB::transaction(function () use ($crew, $lodgings, $lodgingsFood, $relocations) {
            // guarda el nuevo estado de la tripulación
            $crew->status_id = CrewStatus::getCrewFilterStatusId();
            $crew->save();

            // añade las solicitudes de alojamiento, alimentación y traslado
            foreach ($lodgings as $lodging) {
                CrewWorker::where('id', $lodging)->update(['lodging' => true]);
            }

            foreach ($lodgingsFood as $lodgingFood) {
                CrewWorker::where('id', $lodgingFood)->update(['lodging_food' => true]);
            }

            foreach ($relocations as $relocation) {
                CrewWorker::where('id', $relocation)->update(['relocation' => true]);
            }
        });
    }

    /**
     * Obtiene la última tripulación donde el trabajador realizó una faena. Debe devolver principalmente la embarcación.
     * TODO: de momento utiliza el estado en calendario, ya que no cuenta con los demás estados. Debe ser con faena.
     * TODO: debería usar upload_date? o cambiar download_date?
     * @param $workerId
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public static function getWorkerLastCrew($workerId)
    {
        return DB::table('crews')
            ->leftJoin('crew_worker', 'crew_worker.crew_id', '=', 'crews.id')
            ->leftJoin('ships', 'crews.ship_id', '=', 'ships.id')
            ->where('crew_worker.worker_id', $workerId)
            ->where('crews.status_id', CrewStatus::getInCalendarStatusId())
            ->select('crews.id', 'ships.name','upload_date','real_download_date')
            ->orderBy('crews.upload_date')
            ->first();
    }

    public static function getWorkerLastCrewAddedStatus($workerId)
    {
        return DB::table('crews')
            ->leftJoin('crew_worker', 'crew_worker.crew_id', '=', 'crews.id')
            ->leftJoin('ships', 'crews.ship_id', '=', 'ships.id')
            ->where('crew_worker.worker_id', $workerId)
            ->select('crews.id', 'ships.name','upload_date','real_download_date','crew_worker.filtered','crew_worker.with_emergency')
            ->orderBy('crews.upload_date','desc')
            ->first();
    }

    /**
     * Guarda la información de filtros caso cliente
     *
     * @param $id
     * @param $workers
     *
     * @throws \Exception
     * @return bool
     */
    public static function setCrewFilter($id, $workers)
    {
        // verifica que solo se pueda confirmar en estado filtro caso cliente
        $statusId = CrewStatus::getCrewFilterStatusId();
        $crew = self::find($id);

        if ($crew->status_id !== $statusId) {
            throw new \Exception("Solo se puede filtrar una tripulación en estado 'filtro case cliente'");
        }

        $withFilters = count($workers) === 0;

        // verifica a qué estado debe modificarse la tripulación
        if ($withFilters) {
            DB::transaction(function () use ($crew) {
                // primero cambia el estado de la tripulación
                $crew->status_id = CrewStatus::getInCalendarStatusId();
                $crew->save();

                // crea cotización 'fantasma'
                Quotation::create(['crew_id' => $crew->id]);

                // además debe crear las fechas de descanso de los trabajadores, la cual debe ser confirmada al bajar
                $crewWorkers = DB::table('crew_worker')
                    ->select('id')
                    ->where('crew_id', $crew->id)
                    ->where('filtered', 0)
                    ->get();

                $download = Carbon::create($crew->download_date);
                $start = $download->addDays(1)->toDateTimeString();
                $end = $download->addDays(21)->toDateTimeString();

                foreach ($crewWorkers as $crewWorker) {
                    WorkerBreak::create([
                        'crew_worker_id' => $crewWorker->id,
                        'start' => $start,
                        'end' => $end
                    ]);
                }
            });
        } else {
            DB::transaction(function () use ($id, $crew, $workers) {
                // cambia nuevamente a borrador
                $crew->status_id = CrewStatus::getDefaultStatusId();
                $crew->save();

                foreach ($workers as $worker) {
                    DB::table('crew_worker')
                        ->where('crew_id', $id)
                        ->where('worker_id', $worker['id'])
                        ->update(['filtered' => true, 'filter_reason' => $worker['reason']]);
                }
            });
        }

        return $withFilters;
    }

    /**
     * Guarda los trabajadores asociados a esta tripulación
     *
     * @param int $crewId
     * @param array $workers
     *
     * @throws \Exception
     * @return void
     */
    public static function setWorkers(int $crewId, array $workers)
    {
        // primero debe verificar que la tripulación se encuentre en estado borrador
        $exists = DB::table('crews')
            ->where('id', $crewId)
            ->where('status_id', CrewStatus::getDefaultStatusId())
            ->exists();

        if (!$exists) {
            throw new \Exception('Solo se pueden agregar trabajadores en una tripulación en borrador');
        }

        // verifica que al menos se haya añadido 1 trabajador
        if (count($workers) === 0) {
            throw new \Exception('Debe guardar al menos un trabajador');
        }

        // obtiene la embarcación para obtener capacidad
        $ship = DB::table('crews')
            ->join('ships', 'crews.ship_id', '=', 'ships.id')
            ->where('crews.id', $crewId)
            ->select('ships.capacity')
            ->first();

        // cuenta los trabajadores ya asociados
        $totalWorkers = DB::table('crew_worker')
            ->where('crew_id', $crewId)
            ->where('filtered', false)
            ->count();

        // verifica que no se supere el número máximo de habitabilidad de la embarcación
        if ((count($workers) + $totalWorkers) > $ship->capacity) {
            throw new \Exception(
                'La cantidad de trabajadores no puede superar la habitabilidad máxima de la embarcación'
            );
        }

        // verifica también que el trabajador ya no se encuentre asociado a esta misma tripulación
        $exists = DB::table('crew_worker')
            ->where('crew_id', $crewId)
            ->whereIn('worker_id', $workers)
            ->exists();

        if ($exists) {
            throw new \Exception('No se puede asociar dos veces a un trabajador en la misma tripulación');
        }

        // guarda las asociaciones de la tripulación con trabajadores
        DB::transaction(function () use ($crewId, $workers) {
            foreach ($workers as $worker) {
                DB::table('crew_worker')->insert([
                    'crew_id' => $crewId,
                    'worker_id' => $worker
                ]);
            }
        });
    }
}
