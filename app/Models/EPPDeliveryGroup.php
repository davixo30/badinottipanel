<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Jobs\CriticalStock;
/**
 * Class EPPDeliveryGroup
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class EPPDeliveryGroup extends UploaderIdModel
{
    /** @var string[] */
    protected $fillable = ['delivery', 'worker_id', 'validated_at'];

    /** @var string */
    protected $table = 'epp_delivery_groups';

    /** @var bool */
    public $timestamps = false;

    /**
     * Define la relación con el trabajador
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function worker()
    {
        return $this->belongsTo(Worker::class)->select('id', 'name', 'last_name', 'rut');
    }

    /**
     * Crea una nueva entrega de EPP
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public static function create(array $attributes = [])
    {
        return DB::transaction(function () use ($attributes) {
            // primero crea el grupo
            $groupId = static::query()->create([
                'delivery' => $attributes['delivery'],
                'worker_id' => $attributes['worker_id']
            ]);

            // luego inserta cada entrega con el id del grupo

            $array =[];
            foreach ($attributes['deliveries'] as $delivery) {

                DB::UPDATE("update warehouse_item_sizes set quantity=quantity-1 where id=?",array($delivery['id']));


                EPPDelivery::create([
                    'observations' => $delivery['observations'],
                    'warehouse_item_size_id' => $delivery['id'],
                    'epp_delivery_group_id' => $groupId->id
                ]);

                $stock = DB::table("warehouse_item_sizes")->where("id",$delivery['id'])->first();
                if($stock){
                    $critical = DB::table("warehouse_items")->where("id",$stock->warehouse_item_id)->where("critical_stock","<>",null)->first();
                    if($critical){
                        if(intval($stock->quantity) <= intval($critical->critical_stock)){
                            array_push($array,$delivery['id']);
                        }
                    }
                }



            }

            if(count($array)>0){
                CriticalStock::dispatch($array);
            }

        });



    }
}
