<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

/**
 * Class ShipCareen
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class ShipCareen extends UploaderIdModel
{
    /** @var string[] */
    protected $fillable = ['observations', 'careen_date', 'ship_id', 'certificate'];

    /** @var bool */
    public $timestamps = false;

    /**
     * Crea una nueva carena, debe además cambiar la fecha de la misma en la tupla de embarcación
     *
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    public static function create(array $attributes = [])
    {
        $careen = null;

        DB::transaction(function () use ($attributes, &$review) {
            // registra la carena como tal
            $careen = static::query()->create($attributes);

            // cambia la fecha de última carena en la embarcación
            Ship::findOrFail($attributes['ship_id'])->update(['last_careen' => $careen->careen_date]);
        });

        return $careen;
    }
}
