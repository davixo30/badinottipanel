<?php

namespace App\Models;

use App\Scopes\EPPScope;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EPP
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class EPP extends Model
{
    /** @var string */
    protected $table = 'warehouse_items';

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new EPPScope());
    }

    /**
     * Define la relación con las tallas de EPP
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sizes()
    {
        return $this->hasMany(WarehouseItemSize::class, 'warehouse_item_id', 'id');
    }
}
