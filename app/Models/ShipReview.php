<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

/**
 * Class ShipReview
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class ShipReview extends UploaderIdModel
{
    /** @var string[] */
    protected $fillable = ['observations', 'review_date', 'ship_id', 'certificate'];

    /** @var bool */
    public $timestamps = false;

    /**
     * Crea una nueva revista, debe además cambiar la fecha de la misma en la tupla de embarcación
     *
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    public static function create(array $attributes = [])
    {
        $review = null;

        DB::transaction(function () use ($attributes, &$review) {
            // registra la revista como tal
            $review = static::query()->create($attributes);

            // cambia la fecha de última revista en la embarcación
            Ship::findOrFail($attributes['ship_id'])->update(['last_review' => $review->review_date]);
        });

        return $review;
    }
}
