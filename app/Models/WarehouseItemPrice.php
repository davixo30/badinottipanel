<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WarehouseItemPrice
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class WarehouseItemPrice extends Model
{
    /** @var string[] */
    protected $fillable = ['warehouse_item_id', 'price'];

    /** @var bool */
    public $timestamps = false;
}
