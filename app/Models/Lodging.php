<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Lodging
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Lodging extends Model
{
    /** @var string[] */
    protected $fillable = ['arrival_date', 'departure_date', 'lodging_provider_id', 'price', 'crew_worker_id'];

    /**
     * Define la relación con el proveedor de alojamiento
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lodging_provider()
    {
        return $this->belongsTo(Supplier::class);
    }
}
