<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class Certificate
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Certificate extends Model
{
    use SoftDeletes;

    /** @var string[] */
    protected $fillable = ['name', 'ship_type_id'];

    /**
     * Define la relación con el tipo de embarcación
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ship_type()
    {
        return $this->belongsTo(ShipType::class);
    }

    /**
     * Obtiene certificados disponibles para una embarcación
     *
     * @param int $shipId ID embarcación
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getByShipId(int $shipId)
    {
        return DB::table('certificates')
            ->join('ship_types', 'ship_types.id', '=', 'certificates.ship_type_id')
            ->join('ships', 'ships.type_id', '=', 'ship_types.id')
            ->select('certificates.id', 'certificates.name')
            ->where('ships.id', $shipId)
            ->get();
    }

    /**
     * Obtiene las fechas de emision y vencimiento de certificados de una embarcación
     *
     * @param int $shipId ID embarcación
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getShipCertificates(int $shipId)
    {

        //$certificates =
        /*
        return DB::table('certificates')
            ->leftJoin(
                'certificate_ship',
                'certificates.id',
                '=',
                'certificate_ship.certificate_id'
            )
            ->join(
                'ships',
                'ships.id',
                '=',
                'certificate_ship.ship_id'
            )
            ->select('certificates.name', 'certificate_ship.emission', 'certificate_ship.expiration')
            ->where('ships.id', $shipId)
            ->get();
        */
    }
}
