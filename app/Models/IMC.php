<?php

namespace App\Models;

/**
 * Class IMC
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class IMC extends UploaderIdModel
{
    /** @var string[] */
    protected $fillable = ['month', 'year', 'height', 'weight', 'imc', 'worker_id'];

    /** @var string */
    protected $table = 'imcs';

    /** @var bool */
    public $timestamps = false;
}
