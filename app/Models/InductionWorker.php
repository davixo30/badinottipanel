<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class InductionWorker
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class InductionWorker extends Model
{
    /** @var string[] */
    protected $fillable = ['certificate', 'induction_date', 'induction_id', 'worker_id'];

    /** @var bool */
    public $timestamps = false;

    /** @var string[] */
    protected $hidden = ['id', 'induction_id', 'worker_id'];

    /**
     * Añade la propiedad de quien guarda la inducción
     */
    protected static function boot()
    {
        parent::boot();

        // auto-sets values on creation
        static::creating(function ($query) {
            $query->uploader_id= Auth::id();
        });
    }

    /**
     * Define la relación con la inducción
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function induction()
    {
        return $this->belongsTo(Induction::class);
    }

    /**
     * Define la relación con el usuario que sube la inducción
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function uploader()
    {
        return $this->belongsTo(User::class, 'uploader_id', 'id');
    }
}
