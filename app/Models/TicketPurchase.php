<?php

namespace App\Models;

use App\Types\TicketStatusesTypes;
use Illuminate\Support\Facades\DB;

/**
 * Class TicketPurchase
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class TicketPurchase extends UploaderIdModel
{
    /** @var string[] */
    protected $fillable = ['id', 'flight_code', 'dearness', 'penalty'];

    /** @var bool */
    public $timestamps = false;

    /**
     * Crea una compra de un pasaje, además cambia su estado a por volar
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public static function create(array $attributes = [])
    {
        $purchase = null;

        DB::transaction(function () use ($attributes, &$purchase) {
            $status = TicketStatus::select('id')->where('name', TicketStatusesTypes::$POR_VOLAR)->first()->id;
            Ticket::where('id', $attributes['id'])->update(['ticket_status_id' => $status]);
            $purchase = static::query()->create($attributes);
        });

        return $purchase;
    }
}
