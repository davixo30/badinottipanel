<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ContractType
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class ContractType extends Model
{
    /** @var string */
    protected $table = 'contract_types';
}
