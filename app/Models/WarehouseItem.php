<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class WarehouseItem
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class WarehouseItem extends Model
{
    use SoftDeletes;

    /** @var string[] */
    protected $fillable = [
        'name', 'price', 'critical_stock',
        'response_time', 'classification_id', 'measurement_unit_id'
    ];

    /** @var string[] */
    protected $hidden = ['deleted_at', 'updated_at'];

    /**
     * Define la relación con la clasificación del artículo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function classification()
    {
        return $this->belongsTo(WarehouseItemClassification::class);
    }

    /**
     * Define la relación con la unidad de medida
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function measurement_unit()
    {
        return $this->belongsTo(MeasurementUnit::class);
    }

    /**
     * Define la relación con las tallas de los artículos de bodega
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sizes()
    {
        return$this->hasMany(WarehouseItemSize::class);
    }

    /**
     * Define la creación de un artículo de bodega
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public static function create(array $attributes = [])
    {
        return DB::transaction(function () use ($attributes) {
            $item = static::query()->create($attributes);
            WarehouseItemPrice::create(['warehouse_item_id' => $item->id, 'price' => $attributes['price']]);
            WarehouseItemSize::create(['name' => '', 'warehouse_item_id' => $item->id]);
        });
    }

    /**
     * Obtiene solo los elementos pertenecientes a la clasificación Otros
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeOthers($query)
    {
        return $query->where('classification_id', function ($subQuery) {
            return $subQuery->select('id')
                ->from('warehouse_item_classifications')
                ->where('name', 'Otros');
        });
    }

    /**
     * Modifica los datos de un artículo, además registra su nuevo precio
     *
     * @param int $itemId
     * @param array $attributes
     *
     * @return mixed
     */
    public static function updateItem(int $itemId, array $attributes)
    {
        $item = self::findOrFail($itemId);

        return DB::transaction(function () use ($item, $attributes) {
            // verifica si debe ingresar nuevo precio
            if ($item->price !== $attributes['price']) {
                WarehouseItemPrice::create(['warehouse_item_id' => $item->id, 'price' => $attributes['price']]);
            }

            $item->update($attributes);
        });
    }
}
