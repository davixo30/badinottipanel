<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TicketStatus
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class TicketStatus extends Model
{
    /** @var bool */
    public $timestamps = false;
}
