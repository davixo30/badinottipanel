<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class User extends Authenticatable
{
    use Notifiable, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'code', 'rut', 'email', 'password', 'type', 'address', 'avatar',
        'phone_number', 'position_id', 'commune_id', 'marital_status_id', 'birthdate'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email_verified_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the profile photo URL attribute.
     *
     * @return string
     */
    public function getPhotoAttribute()
    {
        return 'https://www.gravatar.com/avatar/' . md5(strtolower($this->email)) . '.jpg?s=200&d=mm';
    }

    /**
     * Define el scope para obtener solo los que tengan el cargo de 'Supervisor'
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeSupervisor($query)
    {
        $positionId = Position::select('id')
            ->where('name', 'Supervisor')
            ->first()
            ->id;

        return $query->where('position_id', $positionId);
    }



    /**
     * Define el rol de administrador
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->position()->where('name', 'Administrador')->exists();
    }

    /**
     * Define el rol de supervisor
     *
     * @return bool
     */
    public function isSupervisor()
    {
        return $this->position()->where('name', 'Supervisor')->exists();
    }


    public function isAbastecimiento()
    {
        return $this->position()->where('name', 'Abastecimiento')->exists();
    }

    public function isBodega()
    {
        return $this->position()->where('name', 'Bodega')->exists();
    }

    public function isGerente()
    {
        return $this->position()->where('name', 'Gerente de operaciones')->exists();
    }

    public function isPlanificacion()
    {
        return $this->position()->where('name', 'Ing. planificación y control de mantenimiento')->exists();
    }

    public function isServiciotaller()
    {
        return $this->position()->where('name', 'Ing. servicio y taller')->exists();
    }

    public function isJefeflota()
    {
        return $this->position()->where('name', 'Jefe de flota')->exists();
    }

    public function isJefeoperaciones()
    {
        return $this->position()->where('name', 'Jefe de operaciones')->exists();
    }

    public function isLogistica()
    {
        return $this->position()->where('name', 'Logística')->exists();
    }

    public function isPrevencionista()
    {
        return $this->position()->where('name', 'Prevencionista')->exists();
    }

    public function isRrhh()
    {
        return $this->position()->where('name', 'RRHH')->exists();
    }

    public function isSubcontratacion()
    {
        return $this->position()->where('name', 'Sub contratación')->exists();
    }


    /**
     * Define la relación con la comuna
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function commune()
    {
        return $this->belongsTo(Commune::class)->with('region');
    }

    /**
     * Define la relación con el contrato del trabajador
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contract()
    {
        return $this->hasOne(UserContract::class)->with('afp', 'contract_type', 'health_forecast');
    }

    /**
     * Define la relación con el cargo del usuario
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function position()
    {
        return $this->belongsTo(Position::class);
    }
}
