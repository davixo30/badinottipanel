<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WorkerBreak
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class WorkerBreak extends Model
{
    /** @var string[] */
    protected $fillable = ['crew_worker_id', 'start', 'end'];

    /** @var bool */
    public $timestamps = false;
}
