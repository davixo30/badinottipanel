<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Commune
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Commune extends Model
{
    /**
     * Define la relación con la región
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo(Region::class);
    }
}
