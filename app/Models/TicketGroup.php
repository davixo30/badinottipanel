<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class TicketGroup
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class TicketGroup extends UploaderIdModel
{
    /** @var string[] */
    protected $fillable = [
        'price', 'flight_date', 'departure_time', 'arrival_time', 'airline_id', 'flight_stretch_id',
        'departure_airport_id', 'arrival_airport_id', 'quotation_id', 'emergency_quotation_id', 'stock_ticket_id','oc','oc_date'
    ];

    /** @var bool */
    public $timestamps = false;

    /**
     * Define el 'scope' para los que pueden ser comprados, es decir, que tengan una cotización aprobada
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeAbleToBuy($query)
    {
        return $query->whereIn('quotation_id', function ($q) {
            return $q->select('id')->from('quotations')->where('status', 1);
        })->orWhereIn('emergency_quotation_id', function ($q) {
            return $q->select('id')->from('emergency_quotations')->where('entered', 1);
        });
    }

    /**
     * Define la relación con la aerolínea
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function airline()
    {
        return $this->belongsTo(Airline::class);
    }

    /**
     * Define la relación con el aeropuerto de destino
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function arrival_airport()
    {
        return $this->belongsTo(Airport::class, 'arrival_airport_id', 'id');
    }

    /**
     * Define la relación con el aeropuerto de partida
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function departure_airport()
    {
        return $this->belongsTo(Airport::class, 'departure_airport_id', 'id');
    }

    /**
     * Define la relación con el tramo del vuelo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function flight_stretch()
    {
        return $this->belongsTo(FlightStretch::class);
    }

    /**
     * Define la relación con los pasajes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany(Ticket::class)->with('crew_worker', 'ticket_status');
    }

     /**
      * Define la creación de un grupo de pasajes
      *
      * @param array $attributes
      *
      * @throws \Exception
      * @return \Illuminate\Database\Eloquent\Builder|Model
      */
    public static function create(array $attributes = [])
    {
        // valida que la cotización se encuentre en estado 'fantasma'
        $isGhost = Quotation::where('id', $attributes['quotation_id'])->where('entered', 0)->exists();

        if (!$isGhost) {
            throw new \Exception("No se puede crear pasajes en una cotización ya enviada");
        }

        return static::query()->create($attributes);
    }

    /**
     * Asigna un pasaje de stock a un trabajador
     *
     * @param int $crewWorkerId
     * @param int $ticketGroupId
     *
     * @return mixed
     */
    public static function assignStock(int $crewWorkerId, int $ticketGroupId)
    {
        $result = null;

        DB::transaction(function () use ($crewWorkerId, $ticketGroupId, &$result) {
            // elimina cualquier registro que contenga este pasaje anteriormente
            Ticket::where('ticket_group_id', $ticketGroupId)->delete();

            // crea el nuevo registro
            $result = Ticket::create([
                'crew_worker_id' => $crewWorkerId,
                'ticket_group_id' => $ticketGroupId
            ]);
        });

        return $result;
    }

    /**
     * Define la creación de un grupo de pasajes en cotización de emergencia
     *
     * @param array $attributes
     *
     * @throws \Exception
     * @return \Illuminate\Database\Eloquent\Builder|Model
     */
    public static function createEmergency(array $attributes = [])
    {
        $result = null;

        DB::transaction(function () use ($attributes, &$result) {
            // valida que la cotización se encuentre en estado 'fantasma'
            $isGhost = EmergencyQuotation::where('id', $attributes['emergency_quotation_id'])->where('entered', 0)->exists();

            if (!$isGhost) {
                throw new \Exception("No se puede crear pasajes en una cotización ya enviada");
            }

            // guarda el grupo de pasajes
            $result = static::query()->create($attributes);

            // obtiene el trabajador para guardar el item del pasaje
            $button = EmergencyButton::with('replacement')->findOrFail($attributes['emergency_quotation_id']);

            Ticket::create([
                'crew_worker_id' => $button->replacement->id,
                'ticket_group_id' => $result->id
            ]);
        });

        return $result;
    }

    /**
     * Asocia trabajadores con un pasaje
     *
     * @param int $id
     * @param array $workers
     */
    public static function associateWorkers(int $id, array $workers)
    {
        DB::transaction(function () use ($id, $workers) {
            foreach ($workers as $worker) {
                // TODO: valida que no tenga otro pasaje asociado en esta misma cotización
                Ticket::create([
                    'crew_worker_id' => $worker['crew_worker_id'],
                    'ticket_group_id' => $id
                ]);
            }
        });
    }

    /**
     * Obtiene los detalles de cada pasaje en el grupo
     *
     * @param int $id
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model|null
     */
    public static function getDetails(int $id)
    {
        // obtiene el grupo de pasajes
        $ticketGroup = TicketGroup::with('tickets')->findOrFail($id);

        // dd($ticketGroup);

        // obtiene cuenta de gastos

        // obtiene OC y factura

        // itera sobre cada pasaje y obtiene su información relevante
        foreach ($ticketGroup->tickets as $ticket) {

        }

        return $ticketGroup;
    }
}
