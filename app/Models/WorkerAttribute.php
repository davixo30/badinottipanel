<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AFP
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class WorkerAttribute extends Model
{
    /** @var string */
    protected $table = 'worker_attributes';
    public $timestamps = true;
}
