<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class Ship
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Ship extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code', 'plate', 'call_identifier', 'capacity', 'engines',
        'generators', 'supervisor_id', 'type_id', 'last_careen', 'last_review','external_code'
    ];

    /**
     * Define la relación con los titulares de la embarcación
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function headlines()
    {
        return $this->hasMany(Headline::class)->with('worker');
    }

    /**
     * Define la relación con el supervisor
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function supervisor()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Crea una nueva certificación para una embarcación
     *
     * @param array $data Datos de la certificación
     */
    public static function createCertificate(array $data)
    {
        $data['uploader_id'] = Auth::id();
        DB::table('certificate_ship')->insert($data);
    }

    public static function createPlans(array $data)
    {
        $data['uploader_id'] = Auth::id();
        DB::table('ship_plans')->insert($data);
    }

    /**
     * Obtiene el estado de certificados de la embarcación
     *
     * @param int $id
     *
     * @return Collection
     */
    public static function getCertificates(int $id): Collection
    {
        return DB::table('certificates')
            ->leftJoin(
                'certificate_ship',
                'certificate_ship.certificate_id',
                '=',
                'certificates.id'
            )
            ->leftJoin('ships', 'certificate_ship.ship_id', '=', 'ships.id')
            ->leftJoin('users', 'certificate_ship.uploader_id', '=', 'users.id')
            ->select(
                'certificates.id',
                'certificates.name',
                'certificate_ship.emission',
                'certificate_ship.expiration',
                'certificate_ship.certificate',
                'users.name as uploader_name',
                'users.last_name as uploader_last_name'
            )
            ->where('ships.id', $id)
            ->where('certificates.deleted_at',null)
            ->orderBy('certificate_ship.created_at')
            ->get();
    }

    public static function getPlans(int $id): Collection
    {
        return DB::table('ship_plans')
            ->join('ship_plans_types','ship_plans_types.id','=','ship_plans.ship_plans_type_id')
            ->join('users','users.id','=','ship_plans.uploader_id')
            ->select("ship_plans_types.name as type","ship_plans.date","users.name as uploader_name",'users.last_name as uploader_last_name','ship_plans.filename')
            ->get();  
    }

    /**
     * Guarda trabajadores como titulares
     *
     * @param int $id
     * @param array $headlines
     *
     * @throws \Exception
     */
    public static function setHeadlines(int $id, array $headlines)
    {
        // primero debe verificar que los headlines otorgados ya no se encuentran asociados
        if (Headline::where('ship_id', $id)->whereIn('worker_id', $headlines)->exists()) {
            throw new \Exception('No se puede colocar de titular el mismo trabajador dos veces');
        }

        // inserta los titulares
        foreach ($headlines as $headline) {
            Headline::create([
                'ship_id' => $id,
                'worker_id' => $headline
            ]);
        }
    }
}
