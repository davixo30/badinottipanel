<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class Qualification
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Qualification extends Model
{
    /**
     * Añade la propiedad de quien guarda el dato
     */
    protected static function boot()
    {
        parent::boot();

        // auto-sets values on creation
        static::creating(function ($query) {
            $query->qualifier_id= Auth::id();
        });
    }

    /**
     * Obtiene calificación general de un trabajador
     *
     * @param int $id
     *
     * @return array
     */
    public static function getGeneralQualification(int $id)
    {
        // obtiene año y semestre actual
        $now = Carbon::now();
        $year = $now->year;
        $half = $now->month < 6 ? 1 : 2;

        // busca los evaluadores
        $qualifiers = Qualifier::with('user:id,name,last_name')->get();
        $qualifications = [];
        $average = 0;
        $evaluatedBy = 0;

        foreach ($qualifiers as $qualifier) {
            // obtiene la evaluación correspondiente a este período
            $q = self::where('worker_id', $id)
                ->where('qualifier_id', $qualifier->id)
                ->where('year', $year)
                ->where('half', $half)
                ->first();

            if ($q) {
                $average += $q->average;
                $evaluatedBy += 1;

                array_push($qualifications, [
                    'qualification' => $q->average,
                    'qualifier' => $qualifier->user->name[0] . $qualifier->user->last_name[0]
                ]);
            }
        }

        // calcula el promedio
        if ($evaluatedBy > 0) {
            $average = $average / $evaluatedBy;
        }

        return [
            'average' => $average,
            'evaluated_by' => $evaluatedBy,
            'qualifications' => $qualifications
        ];
    }

    /**
     * Define la relación con el calificador
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function qualifier()
    {
        return $this->belongsTo(User::class, 'qualifier_id', 'id');
    }

    /**
     * Crea una calificación a un trabajador con sus respectivas notas en cada criterio
     *
     * @param int $workerId ID del trabajador
     * @param string $date
     * @param int $year
     * @param int $half
     * @param array $qualifications Calificaciones
     */
    public static function createWithCriteria(
        int $workerId,
        string $date,
        int $year,
        int $half,
        array $qualifications
    ) {
        DB::transaction(function () use ($workerId, $date, $qualifications, $year, $half) {
            $total = 0;

            $qualificationId = DB::table('qualifications')->insertGetId([
                'qualification_date' => $date,
                'year' => $year,
                'half' => $half,
                'qualifier_id' => auth()->id(),
                'worker_id' => $workerId,
                'created_at' => Carbon::now()
            ]);

            foreach ($qualifications as $qualification) {
                DB::table('criteria_qualification')->insert([
                    'grade' => (string) $qualification['grade'],
                    'criteria_id' => $qualification['criteria_id'],
                    'qualification_id' => $qualificationId
                ]);

                $total += $qualification['grade'];
            }

            DB::table('qualifications')->where('id', $qualificationId)->update([
                'average' => number_format($total / count($qualifications), 1)
            ]);
        });
    }
}
