<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class InvoicedPurchase
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class InvoicedPurchase extends Model
{
    /** @var string[] */
    protected $fillable = ['purchase_id', 'purchase_order','purchase_order_date', 'bill_number', 'bank_account_id'];

    /** @var string */
    protected $primaryKey = 'purchase_id';

    /** @var bool */
    public $timestamps = false;

    /**
     * Define la relación con la cuenta bancaria
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank_account()
    {
        return $this->belongsTo(BankAccount::class);
    }
}
