<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SupplyRequestWarehouseItem
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class SupplyRequestWarehouseItem extends Model
{
    /** @var array */
    protected $fillable = ['quantity', 'observations', 'supply_request_id', 'warehouse_item_size_id','purchase_date','supplier_id'];

    /** @var string[] */
    protected $hidden = ['supply_request_id'];

    /** @var string */
    protected $table = 'supply_request_item';

    /** @var bool */
    public $timestamps = false;

    /**
     * Define la relación con el artículo de bodega
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function size()
    {
        return $this->hasOne(WarehouseItemSize::class, 'id', 'warehouse_item_size_id')->with('item');
    }
}
