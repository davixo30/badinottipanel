<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Client
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Client extends Model
{
    /**
     * Obtiene el listado de clientes que tienen inducciones asociadas
     * TODO no funca
     *
     * @return \Illuminate\Support\Collection
     */
    public function getOnlyWithInductions()
    {
        return DB::table('clients')
            ->select('clients.id', 'clients.name')
            ->join(
                'inductions',
                'clients.id',
                '=',
                'inductions.client_id'
            )
            ->groupBy('clients.id')
            ->get();
    }
}
