<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BankAccount
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class BankAccount extends Model
{
    use SoftDeletes;

    /** @var string[] */
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
