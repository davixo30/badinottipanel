<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CrewWorker
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class CrewWorker extends Model
{
    /** @var string[] */
    protected $fillable = ['observations', 'lodging', 'lodging_food', 'relocation', 'crew_id', 'worker_id', 'with_emergency', 'is_replacement','late_entry','entry_date'];

    /** @var string */
    protected $table = 'crew_worker';

    /** @var bool */
    public $timestamps = false;

    /**
     * Define la relación con el alojamiento
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function lodging_data()
    {
        return $this->hasOne(Lodging::class)->with('lodging_provider:id,name');
    }

    /**
     * Define la relación con el traslado
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function relocation_data()
    {
        return $this->hasOne(Relocation::class)->with('relocation_provider:id,name');
    }

    /**
     * Define la relación con el trabajador
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function worker()
    {
        return $this->belongsTo(Worker::class)->select('id', 'name', 'last_name', 'rut','deleted_at')->withTrashed()
            ->with('roles');
    }
}
