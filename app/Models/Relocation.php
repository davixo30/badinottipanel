<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Relocate
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Relocation extends Model
{
    /** @var string[] */
    protected $fillable = ['arrival', 'pickup', 'departure', 'relocation_provider_id', 'price', 'crew_worker_id'];

    /**
     * Define la relación con el proveedor de traslado
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function relocation_provider()
    {
        return $this->belongsTo(Supplier::class);
    }
}
