<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PurchaseWarehouseItemSize
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class PurchaseWarehouseItemSize extends Model
{
    /** @var string[] */
    protected $fillable = ['purchase_id', 'warehouse_item_size_id', 'quantity', 'price','purchase_date','supplier_id'];

    /** @var string[] */
    protected $table = 'purchase_warehouse_item_size';

    /** @var bool */
    public $timestamps = false;

    /**
     * Define la relación con la talla del artículo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function size()
    {
        return $this->belongsTo(WarehouseItemSize::class, 'warehouse_item_size_id', 'id')
            ->with('item');
    }
}
