<?php

namespace App\Models;

use App\Types\PurchaseSupplyRequestActionTypes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Jobs\CriticalStock;
/**
 * Class Purchase
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Purchase extends Model
{
    /** @var string[] */
    protected $fillable = ['status', 'rejection_reason', 'supplier_id', 'supply_request_id', 'confirmed_at', 'purchase','only_warehouse','crew_id'];

    /** @var bool */
    public $timestamps = false;

    /**
     * Define el 'scope' para compras en no facturadas.
     *
     * @param $query
     *
     * @return mixed
     */
    public static function scopeInvoicePending($query)
    {
        return $query->where('status', 1)->whereNotIn('id', function ($q) {
            return $q->select('purchase_id')
                ->from('invoiced_purchases');
        });
    }

    /**
     * Crea una compra junto a sus artículos de bodega
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public static function create(array $attributes = [])
    {
        $purchase = null;

        DB::transaction(function () use ($attributes, &$purchase) {
            $purchase = static::query()->create([
                'supply_request_id' =>
                    array_key_exists('supply_request_id', $attributes) ? $attributes['supply_request_id']: null,
                'purchase' => $attributes['purchase'],
                'supplier_id' => $attributes['supplier_id']
            ]);

            foreach ($attributes['items'] as $item) {
                PurchaseWarehouseItemSize::create([
                    'price' => $item['price'],
                    'purchase_id' => $purchase->id,
                    'warehouse_item_size_id' => $item['size_id'],
                    'quantity' => $item['quantity'],
                    'purchase_date' => $item['purchase_date'],
                    'supplier_id' => $item['supplier_id']
                ]);
            }
        });

        return $purchase;
    }

    /**
     * Crea una compra a través de una solicitud de abastecimiento
     *
     * @param string $purchaseDate
     * @param int $supplierId
     * @param int $supplyRequestId
     * @param array $EPPDeliveries
     * @param array $warehouseItems
     *
     * @return mixed|null
     */
    public static function createThroughSupplyRequest(
        string $purchaseDate,
        int $supplierId,
        int $supplyRequestId,
        array $EPPDeliveries,
        array $warehouseItems
    ) {
        $purchase = null;

        DB::transaction(function () use (
            $purchaseDate,
            $supplierId,
            $supplyRequestId,
            $EPPDeliveries,
            $warehouseItems,
            &$purchase
        ) {
            $items = [];

            // obtiene los artículos de bodega
            foreach ($warehouseItems as $warehouseItem) {
                // guarda la acción realizada
                $item = SupplyRequestWarehouseItem::findOrFail($warehouseItem['id']);
                $item->action = $warehouseItem['action'];
                $item->price = $warehouseItem['price'];
                $item->purchase_date = $warehouseItem['date'];
                $item->supplier_id = $warehouseItem['supplier_id'];
                $item->save();

                // guarda el elemento en el array de items para ser guardado en la bdd
                if ($warehouseItem['action'] === PurchaseSupplyRequestActionTypes::$PURCHASE) {
                    array_push($items, [
                        'size_id' => $item->warehouse_item_size_id,
                        'quantity' => $item->quantity,
                        'price' => $item->price,
                        'supplier_id' => $item->supplier_id,
                        'purchase_date' => $item->purchase_date
                    ]);
                }
            }

            // obtiene los epp
            foreach ($EPPDeliveries as $EPPDelivery) {
                // guarda la acción realizada
                $delivery = EPPDeliverySupplyRequest::findOrFail($EPPDelivery['id']);
                $delivery->action = $EPPDelivery['action'];
                $delivery->price = $EPPDelivery['price'];
                $delivery->purchase_date = $EPPDelivery['date'];
                $delivery->supplier_id = $EPPDelivery['supplier_id'];
                $delivery->save();

                // guarda el elemento en el array de items para ser guardado en la bdd
                if ($EPPDelivery['action'] === PurchaseSupplyRequestActionTypes::$PURCHASE) {
                    array_push($items, [
                        'size_id' => $delivery->warehouse_item_size_id,
                        'quantity' => 1,
                        'price' => $delivery->price,
                        'supplier_id' => $delivery->supplier_id,
                        'purchase_date' => $delivery->purchase_date,
                        
                    ]);
                }
            }

            // crea la compra
            $purchase = self::create([
                'supply_request_id' => $supplyRequestId,
                'purchase' => $purchaseDate,
                'supplier_id' => $supplierId,
                'items' => $items
            ]);
        });

        return $purchase;
    }

    /**
     * Define la relación con la factura de la compra
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function invoice()
    {
        return $this->hasOne(InvoicedPurchase::class)->with('bank_account');
    }

    /**
     * Define la relación con el proveedor
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    /**
     * Define la relación con la solicitud de abastecimiento.
     * Se utiliza en el listado de compras en operaciones.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function supply_request()
    {
        return $this->belongsTo(SupplyRequest::class);
    }

    /**
     * Define la relación con los artículos de la compra
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function warehouse_item_sizes()
    {
        return $this->hasMany(PurchaseWarehouseItemSize::class, 'purchase_id', 'id')
            ->with('size');
    }

    /**
     * Rechaza una compra
     *
     * @param int $id
     * @param string $reason
     *
     * @throws \Exception
     * @return mixed
     */
    public static function rejectPurchase(int $id, string $reason)
    {
        $purchase = self::findOrFail($id);

        if ($purchase->confirmed_at !== null && $purchase->status !== null) {
            throw new \Exception('Solo se puede rechazar una compra que no haya sido confirmada');
        }

        $purchase->update(['status' => false, 'rejection_reason' => $reason, 'confirmed_at' => Carbon::now()]);

        return $purchase;
    }

    /**
     * Valida una compra
     *
     * @param int $id ID de compra
     *
     * @throws \Exception
     * @return mixed
     */
    public static function validatePurchase(int $id)
    {
        $purchase = null;

        DB::transaction(function () use ($id, &$purchase) {
            // valida si la compra no ha sido confirmada aún
            $purchase = self::findOrFail($id);
            $array =[];

            if ($purchase->confirmed_at !== null && $purchase->status !== null) {
                throw new \Exception('Solo se puede validar una compra que no haya sido confirmada');
            }

            // verifica acciones con artículos de bodega
            $warehouseItems = SupplyRequestWarehouseItem::where('supply_request_id', $purchase->supply_request_id)
                ->get();


            $isSupplyRequest = count($warehouseItems) > 0;

            if ($isSupplyRequest) {


                // verifica acciones con epp
                $EPPDeliveries = EPPDeliverySupplyRequest::where('supply_request_id', $purchase->supply_request_id)
                    ->get();

                // verifica las acciones en cada elemento de la compra
                foreach ($EPPDeliveries as $delivery) {
                    // obtiene el elemento original
                    $warehouseItemDb = WarehouseItemSize::findOrFail($delivery->warehouse_item_size_id);

                    switch ($delivery->action) {
                        // en caso de solicitar compra
                        case PurchaseSupplyRequestActionTypes::$PURCHASE:
                            // verifica si hay un nuevo precio y lo actualiza
                            $price = WarehouseItemPrice::select('price')
                                ->where('warehouse_item_id', $warehouseItemDb->warehouse_item_id)
                                ->latest()
                                ->first();

                            if ($price->price !== $delivery->price) {
                                WarehouseItemPrice::create([
                                    'price' => $delivery->price,
                                    'warehouse_item_id' => $warehouseItemDb['warehouse_item_id']
                                ]);
                            }

                            $warehouseItemDb->quantity = $warehouseItemDb->quantity + 1;
                            $warehouseItemDb->save();

                            break;
                        // en caso de solicitar en bodega
                        case PurchaseSupplyRequestActionTypes::$WAREHOUSE:
                            // descuenta la cantidad de elementos solicitados
                           // $warehouseItemDb->quantity -= 1;
                           // $warehouseItemDb->save();

                            break;
                        default:
                            throw new \Exception('Invalid action');
                    }
                }

                // verifica las acciones en cada elemento de la compra en caso de ser por solicitud de abastecimiento
                foreach ($warehouseItems as $item) {
                    // obtiene el elemento original
                    $warehouseItemDb = WarehouseItemSize::findOrFail($item->warehouse_item_size_id);

                    switch ($item->action) {
                        // en caso de solicitar compra
                        case PurchaseSupplyRequestActionTypes::$PURCHASE:
                           // $purchase->entro = 99;
                            // verifica si hay un nuevo precio y lo actualiza
                            $price = WarehouseItemPrice::select('price')
                                ->where('warehouse_item_id', $warehouseItemDb->warehouse_item_id)
                                ->latest()
                                ->first();

                            if ($price->price !== $item->price) {
                                WarehouseItemPrice::create([
                                    'price' => $item->price,
                                    'warehouse_item_id' => $warehouseItemDb['warehouse_item_id']
                                ]);
                            }

                            break;
                        // en caso de solicitar en bodega
                        case PurchaseSupplyRequestActionTypes::$WAREHOUSE:
                           // $purchase->entro = 3;
                            // descuenta la cantidad de elementos solicitados
                            $warehouseItemDb->quantity -= $item->quantity;
                            $warehouseItemDb->save();


                            $stock = DB::table("warehouse_item_sizes")->where("id",$item->warehouse_item_size_id)->first();
                            if($stock){
                                $critical = DB::table("warehouse_items")->where("id",$stock->warehouse_item_id)->where("critical_stock","<>",null)->first();
                                if($critical){
                                    if(intval($stock->quantity) <= intval($critical->critical_stock)){
                                        array_push($array,$item->warehouse_item_size_id);
                                    }
                                }
                            }

                            break;
                        default:
                        //$purchase->entro = 4;
                            throw new \Exception('Invalid action');
                    }
                }


            } else {
                //$purchase->entro = 2;
                // en caso de no venir por solicitud de abastecimiento
                $purchaseItems = PurchaseWarehouseItemSize::where('purchase_id', $id)->get();

                foreach ($purchaseItems as $item) {
                    $originalItemSize = WarehouseItemSize::findOrFail($item->warehouse_item_size_id);
                    $originalItem = WarehouseItem::findOrFail($originalItemSize->warehouse_item_id);

                    // actualiza las existencias
                    $originalItemSize->quantity += $item->quantity;
                    $originalItemSize->save();

                    // si existe un nuevo precio, lo actualiza
                    if ($originalItem->price !== $item->price) {
                        WarehouseItemPrice::create([
                            'price' => $item->price,
                            'warehouse_item_id' => $originalItem->id
                        ]);
                    }
                }
            }

            if(count($array)>0){
                CriticalStock::dispatch($array);
            }
            $purchase->update(['status' => true, 'confirmed_at' => Carbon::now()]);
            DB::table("purchases")->where("supply_request_id",$purchase->supply_request_id)->update(['validated'=>1]);
        });

        return $purchase;
    }



    public static function validatePurchaseOnlyWarehouse(int $id)
    {
        $purchase = null;
        DB::transaction(function () use ($id, &$purchase) {
            // valida si la compra no ha sido confirmada aún
            $purchase = self::findOrFail($id);
            $array =[];
            if ($purchase->confirmed_at !== null && $purchase->status !== null) {
                throw new \Exception('Solo se puede validar una compra que no haya sido confirmada');
            }
            // verifica acciones con artículos de bodega
            $warehouseItems = SupplyRequestWarehouseItem::where('supply_request_id', $purchase->supply_request_id)
                ->get();
            $isSupplyRequest = count($warehouseItems) > 0;
            if ($isSupplyRequest) {
                // verifica acciones con epp
                $EPPDeliveries = EPPDeliverySupplyRequest::where('supply_request_id', $purchase->supply_request_id)
                    ->get();
                // verifica las acciones en cada elemento de la compra
                foreach ($EPPDeliveries as $delivery) {
                    // obtiene el elemento original
                    $warehouseItemDb = WarehouseItemSize::findOrFail($delivery->warehouse_item_size_id);
                    switch ($delivery->action) {
                        // en caso de solicitar compra
                        case PurchaseSupplyRequestActionTypes::$PURCHASE:
                            break;
                        // en caso de solicitar en bodega
                        case PurchaseSupplyRequestActionTypes::$WAREHOUSE:
                            break;
                        default:
                            throw new \Exception('Invalid action');
                    }
                }
                // verifica las acciones en cada elemento de la compra en caso de ser por solicitud de abastecimiento
                foreach ($warehouseItems as $item) {
                    // obtiene el elemento original
                    $warehouseItemDb = WarehouseItemSize::findOrFail($item->warehouse_item_size_id);

                    switch ($item->action) {
                        // en caso de solicitar compra
                        case PurchaseSupplyRequestActionTypes::$PURCHASE:
                            break;
                        // en caso de solicitar en bodega
                        case PurchaseSupplyRequestActionTypes::$WAREHOUSE:
                           // $purchase->entro = 3;
                            // descuenta la cantidad de elementos solicitados
                            $warehouseItemDb->quantity -= $item->quantity;
                            $warehouseItemDb->save();


                            $stock = DB::table("warehouse_item_sizes")->where("id",$item->warehouse_item_size_id)->first();
                            if($stock){
                                $critical = DB::table("warehouse_items")->where("id",$stock->warehouse_item_id)->where("critical_stock","<>",null)->first();
                                if($critical){
                                    if(intval($stock->quantity) <= intval($critical->critical_stock)){
                                        array_push($array,$item->warehouse_item_size_id);
                                    }
                                }
                            }
                            break;
                        default:
                        //$purchase->entro = 4;
                            throw new \Exception('Invalid action');
                    }
                }
            }

            if(count($array)>0){
                CriticalStock::dispatch($array);
            }
            $purchase->update(['confirmed_at' => Carbon::now(),'only_warehouse'=>1]);
        });
    }
}
