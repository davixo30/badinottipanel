<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class Document
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Document extends Model
{
    /** @var string[] */
    protected $fillable = ['document_type_id', 'expiration_date', 'worker_id'];

    /** @var bool */
    public $timestamps = false;

    /**
     * Añade la propiedad de quien guarda el documento
     */
    protected static function boot()
    {
        parent::boot();

        // auto-sets values on creation
        static::creating(function ($query) {
            $query->uploader_id= Auth::id();
        });
    }

    /**
     * Define la relación con el tipo del documento
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(DocumentType::class, 'document_type_id', 'id');
    }

    /**
     * Define la relación con el usuario que subió la información
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function uploader()
    {
        return $this->belongsTo(User::class, 'uploader_id', 'id');
    }
}
