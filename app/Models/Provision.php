<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Provision extends Model
{
    protected $fillable = ['supplier_id', '	date','price','workers_quantity','crew_id','user_id','created_at','invoice_id'];
    public $timestamps = false;
}
