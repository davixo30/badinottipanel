<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class EPPWorker
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class EPPDelivery extends Model
{
    /** @var string[] */
    protected $fillable = [
        'status', 'warehouse_item_size_id', 'rejection_reason',
        'epp_delivery_group_id', 'confirmed_at', 'observations'
    ];

    /** @var string */
    protected $table = 'epp_deliveries';

    /** @var bool */
    public $timestamps = false;

    /**
     * Define la relación con epp
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function size()
    {
        return $this->belongsTo(WarehouseItemSize::class, 'warehouse_item_size_id', 'id')
            ->with('item');
    }

    /**
     * Define la relación con el grupo de entregas
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(EPPDeliveryGroup::class, 'epp_delivery_group_id', 'id')
            ->with('uploader');
    }

    /**
     * Define el 'scope' para entregas de un trabajador. Utiliza la relación con epp_delivery_groups.worker_id
     *
     * @param Builder $query
     * @param int $id
     *
     * @return Builder
     */
    public function scopeByWorkerId(Builder $query, int $id)
    {
        return $query->whereIn('epp_delivery_group_id', function ($q) use ($id) {
            return $q->select('id')->from('epp_delivery_groups')->where('worker_id', $id);
        });
    }

    /**
     * Obtiene la última entrega de un EPP a un trabajador
     *
     * @param int $sizeId
     * @param int $workerId
     * @param int|null $except
     *
     * @return Model|\Illuminate\Database\Query\Builder|object|null
     */
    public static function getLastDelivery(int $sizeId, int $workerId, int $except = null)
    {
        $query = DB::table('epp_deliveries')
            ->join(
                'epp_delivery_groups',
                'epp_deliveries.epp_delivery_group_id',
                '=',
                'epp_delivery_groups.id'
            )
            ->where('warehouse_item_size_id', $sizeId)
            ->where('worker_id', $workerId)
            ->select('epp_delivery_groups.delivery')
            ->orderBy('delivery', 'desc');

        if ($except !== null) $query->where('epp_deliveries.id', '<>', $except);

        return $query->first();
    }
}
