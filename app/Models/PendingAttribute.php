<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AFP
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class PendingAttribute extends Model
{
    /** @var string */
    protected $table = 'pending_attributes';
    public $timestamps = false;
}
