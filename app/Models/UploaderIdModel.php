<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Esta clase se diseñó para reutilizar la capacidad de guardar automáticamente el ID de quien sube la información.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class UploaderIdModel extends Model
{
    /**
     * Añade la propiedad de quien guarda el dato
     */
    protected static function boot()
    {
        parent::boot();

        // auto-sets values on creation
        static::creating(function ($query) {
            $query->uploader_id= Auth::id();
        });
    }

    /**
     * Define la relación con el usuario que subió la información
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function uploader()
    {
        return $this->belongsTo(User::class, 'uploader_id', 'id');
    }
}
