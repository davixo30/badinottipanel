<?php

namespace App\Models;

use App\Types\DocumentsTypes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use InvalidArgumentException;

/**
 * Class Worker
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Worker extends Model
{
    use SoftDeletes;

    /**
     * Cantidad de días para considerar en observación un documento del trabajador
     *
     * @var int
     */
    public static $DIFF = 45;
    public static $INDUCTION_DIFF = 30;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'avatar', 'code', 'rut', 'birthdate',
        'phone_number', 'address', 'attribute_id', 'commune_id', 'marital_status_id','red_day','send_email'
    ];

    /**
     * Define la relación con el atributo del trabajador
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }

    /**
     * Define la relación con la comuna
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function commune()
    {
        return $this->belongsTo(Commune::class)->with('region');
    }

    /**
     * Define la relación con el contrato del trabajador
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contract()
    {
        return $this->hasOne(Contract::class)->with('afp', 'contract_type', 'health_forecast');
    }

    /**
     * Define la relación con los epp entregados
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function epps()
    {
        return $this->hasManyThrough(
            EPPDelivery::class,
            EPPDeliveryGroup::class,
            'worker_id',
            'epp_delivery_group_id',
            'id',
            'id'
        )
            ->select('epp_deliveries.id', 'epp_delivery_groups.delivery', 'epp_id', 'size')
            ->with('epp')
            ->where('epp_deliveries.status', 1)
            ->orderBy('epp_deliveries.epp_id');
    }

    /**
     * Define la relación con los vencimientos de documentos del trabajador
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function expirations()
    {
        return $this->hasOne(Expiration::class);
    }

    /**
     * Define una relación con un titular. Sirve para verificar si el trabajador es uno o no
     * TODO: esto no está funcionando, se trae cualquier titular, no dependiente del barco
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function headline()
    {
        return $this->hasOne(Headline::class);
    }

    /**
     * Define la relación con las inducciones realizadas
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inductions()
    {
        return $this->hasMany(InductionWorker::class)
            ->with('induction');
    }

    /**
     * Define la relación con la última calificación del trabajador
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function qualifications()
    {
        return $this->hasMany(Qualification::class);
    }

    /**
     * Define la relación con los roles del trabajador
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Asocia roles a un trabajador
     *
     * @param int $id
     * @param array $roles
     */
    public static function setRoles(int $id, array $roles)
    {
        DB::transaction(function () use ($id, $roles) {
            DB::table('role_worker')
                ->where('worker_id', $id)
                ->delete();

            foreach ($roles as $role) {
                if (is_int($role)) {
                    DB::table('role_worker')
                        ->insert([
                            'role_id' => $role,
                            'worker_id' => $id
                        ]);
                } else {
                    throw new InvalidArgumentException();
                }
            }
        });
    }

    /**
     * Obtiene la evaluación de un trabajador considerando solo documentos
     * TODO: mezclar con crew evaluation, solo si el segundo atributo es indicado
     *
     * @param int $workerId
     *
     * @return array
     */
    public static function getWorkerEvaluation(int $workerId,int $client_id)
    {
        $now = Carbon::now();

        // solo documentos, coloca el array de inducciones pero no se utiliza, con el fin de reutilizar un web component
        $reasons = [
            'expirations' => [],
            'inductions' => []
        ];
        $status = 'Ok';

        // carpeta documentación
        $resolver = function (string $documentName, bool $useA = false) use ($now, $workerId, &$reasons, &$status) {
            // obtiene el documento en cuestión
            $document = Document::where('worker_id', $workerId)->where('document_type_id', function ($query) use ($documentName) {
                return $query->select('id')->from('document_types')->where('name', $documentName);
            })->orderBy('created_at', 'desc')->first();

            if ($document) {
                $diff = $now->diffInDays(Carbon::create($document->expiration_date), false);

                if ($diff <= 0) {
                    if ($status !== 'Error') $status = 'Error';
                    $reasons['expirations'][] = sprintf("%s vencid%s", $documentName, $useA ? 'a': 'o');
                } elseif ($diff < self::$DIFF) {
                    if ($status !== 'Error') $status = 'Observaciones';
                    $reasons['expirations'][] = "$documentName por vencer ($diff días)";
                }
            } else {
                if ($status !== 'Error') $status = 'Error';
                $reasons['expirations'][] = sprintf("%s no ingresad%s", $documentName, $useA ? 'a': 'o');
            }
        };

        $resolver(DocumentsTypes::$CARNET_IDENTIDAD);
        $resolver(DocumentsTypes::$MATRICULA_EMBARQUE, true);
        // $resolver(DocumentsTypes::$REGLAMENTO_INTERNO);
        $resolver(DocumentsTypes::$SEGURO_VIDA);


        $inductions = DB::table("client_inductions")
                     ->join("inductions","client_inductions.induction_id","=","inductions.id")
                     ->where("client_inductions.client_id",$client_id)
                     ->where("inductions.deleted_at",NULL)
                     ->select("inductions.id","inductions.name")
                     ->get();
        if($inductions){
            foreach($inductions as $in){
                $worker = DB::table("induction_workers")->where("worker_id",$workerId)->where("induction_id",$in->id)->first();
                if (!$worker) {
                    if ($status !== 'Error') $status = 'Error';
                    $reasons['expirations'][] = "Falta induccion ".$in->name;
                } else {

                        $aniomas = Carbon::create($worker->induction_date)->addYear(1);

                        $diff = $now->diffInDays(Carbon::create($worker->induction_date)->addYear(1), false);
                        if ($diff <= 0) {
                            if ($status !== 'Error') $status = 'Error';
                            $reasons['expirations'][] = "Induccion ".$in->name." vencida.";
                        } elseif ($diff < self::$INDUCTION_DIFF) {
                            if ($status !== 'Error') $status = 'Observaciones';
                            $reasons['expirations'][] = "Induccion ".$in->name." por vencer ($diff días)";
                        }

                }
            }

        }




       if($client_id === 7){
            // examen ocupacional
            $exam = OccupationalExam::where('worker_id', $workerId)->orderBy('created_at', 'desc')->first();

            if (!$exam) {
                if ($status !== 'Error') $status = 'Error';
                $reasons['expirations'][] = "Examen ocupacional no ingresado";
            } else {
                if ($exam->condition) {
                    $diff = $now->diffInDays(Carbon::create($exam->expiration_date), false);

                    if ($diff <= 0) {
                        if ($status !== 'Error') $status = 'Error';
                        $reasons['expirations'][] = "Examen ocupacional vencido";
                    } elseif ($diff < self::$DIFF) {
                        if ($status !== 'Error') $status = 'Observaciones';
                        $reasons['expirations'][] = "Examen ocupacional por vencer ($diff días)";
                    }
                } else {
                    if ($status !== 'Error') $status = 'Error';
                    $reasons['expirations'][] = "Examen ocupacional con contraindicaciones";
                }
            }
            return [
                'status' => $status,
                'reasons' => $reasons
            ];
        }else{



        $role = DB::table("role_worker")->where("worker_id",$workerId)->where("role_id","5")->first();

        if(!$role){

        }else{
            // examen ocupacional
            $exam = OccupationalExam::where('worker_id', $workerId)->orderBy('created_at', 'desc')->first();

            if (!$exam) {
                if ($status !== 'Error') $status = 'Error';
                $reasons['expirations'][] = "Examen ocupacional no ingresado";
            } else {
                if ($exam->condition) {
                    $diff = $now->diffInDays(Carbon::create($exam->expiration_date), false);

                    if ($diff <= 0) {
                        if ($status !== 'Error') $status = 'Error';
                        $reasons['expirations'][] = "Examen ocupacional vencido";
                    } elseif ($diff < self::$DIFF) {
                        if ($status !== 'Error') $status = 'Observaciones';
                        $reasons['expirations'][] = "Examen ocupacional por vencer ($diff días)";
                    }
                } else {
                    if ($status !== 'Error') $status = 'Error';
                    $reasons['expirations'][] = "Examen ocupacional con contraindicaciones";
                }
            }
        }





        return [
            'status' => $status,
            'reasons' => $reasons
        ];

    }

    }

    /**
     * Obtiene el listado de trabajadores disponibles para asignar como titulares en una tripulación
     *
     * @param int $shipId
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getAvailableForShipHeadline(int $shipId)
    {
        return self::select('id', 'name', 'last_name', 'rut')
            ->whereNotIn('id', function ($query) use ($shipId) {
                $query->select('worker_id')
                    ->from('headlines')
                    ->where("deleted_at",null)
                    ->where('ship_id', $shipId);
            });
    }

    /**
     * Obtiene el listado de trabajadores según una tripulación entregada.
     * Omite trabajadores ya asociados a la tripulación y prioriza a los titulares de la embarcación.
     * TODO: solo puede considerar descansos aprobados
     *
     * @param $query
     * @param int $crewId
     *
     * @return mixed
     */
    public static function scopeInCrew($query, int $crewId)
    {
        $crew = Crew::find($crewId);

        return $query->leftJoin('headlines', 'headlines.worker_id', '=', 'workers.id')
            // omite trabajadores ya agregados
            ->whereNotIn('workers.id', function ($q) use ($crewId) {
                $q->select('worker_id')
                    ->from('crew_worker')
                    ->where('crew_id', $crewId);
            })
            // omite trabajadores titulares en otros barcos
            ->whereNotIn('workers.id', function ($q) use ($crew) {
                $q->select('worker_id')
                    ->from('headlines')
                    ->where('deleted_at', null)
                    ->where('ship_id', '<>', $crew->ship_id);
            })
            // omite trabajadores que deberían estar en descanso
           /*->whereNotIn('workers.id', function ($q) use ($crew) {
                $q->select('crew_worker.worker_id')
                    ->from('worker_breaks')
                    ->join('crew_worker', 'worker_breaks.crew_worker_id', '=', 'crew_worker.id')
                    // TODO: solo puede considerar descansos aprobados
                    // ->where('worker_breaks.status', 1);
                    ->whereDate('worker_breaks.end', '>', $crew->upload_date);
            })*/
            ->groupBy('workers.id')
            ->orderBy('headlines.id', 'desc')
            ->orderBy('workers.last_name', 'asc');
    }

    /**
     * Obtiene los trabajadores para sala de espera, con errores y/o por vencer
     *
     * @param $query
     *
     * @return mixed
     */
    public static function scopeWaitingRoom($query)
    {
        $limit = Carbon::now()->toDateTimeString();

        // TODO: refactorizar esta query

        return $query->select('workers.id', 'name', 'last_name', 'rut', 'birthdate','attribute_id')
            // obtiene todos los que tengan carnet vencido
            ->whereNotIn('workers.id', function ($q) use ($limit) {
                return $q->select('worker_id')
                    ->from('documents')
                    ->where('documents.document_type_id', function ($_q) {
                        return $_q->select('id')
                            ->from('document_types')
                            ->where('name', DocumentsTypes::$CARNET_IDENTIDAD);
                    })
                    ->whereDate('documents.expiration_date', '>=', $limit);
            })
            // obtiene todos los que tengan matrícula de embarque vencida
            ->orWhereNotIn('workers.id', function ($q) use ($limit) {
                return $q->select('worker_id')
                    ->from('documents')
                    ->where('documents.document_type_id', function ($_q) {
                        return $_q->select('id')
                            ->from('document_types')
                            ->where('name', DocumentsTypes::$MATRICULA_EMBARQUE);
                    })
                    ->whereDate('documents.expiration_date', '>=', $limit);
            })
            // obtiene todos los que tengan seguro de vida vencido
            ->orWhereNotIn('workers.id', function ($q) use ($limit) {
                return $q->select('worker_id')
                    ->from('documents')
                    ->where('documents.document_type_id', function ($_q) {
                        return $_q->select('id')
                            ->from('document_types')
                            ->where('name', DocumentsTypes::$SEGURO_VIDA);
                    })
                    ->whereDate('documents.expiration_date', '>=', $limit);
            })
            // obtiene todos los que tengan examen ocupacional vencido o no apto
           /* ->orWhereNotIn('workers.id', function ($q) use ($limit) {
                return $q->select('worker_id')
                    ->from('occupational_exams')
                    ->where('condition', 1)
                    ->whereDate('occupational_exams.expiration_date', '>=', $limit);
            })*/
            ->groupBy('workers.id');
    }

    /**
     * Obtiene los trabajadores sin errores
     *
     * @param $query
     *
     * @return mixed
     */
    public static function scopeWithoutErrors($query,int $crewId)
    {
        $limit = Carbon::now()->toDateTimeString();

        return $query->select('workers.id', 'name', 'last_name', 'rut', 'birthdate', 'phone_number','attribute_id')
            // obtiene todos los que no tengan carnet vencido
            ->whereIn('workers.id', function ($q) use ($limit) {
                return $q->select('worker_id')
                    ->from('documents')
                    ->where('documents.document_type_id', function ($_q) {
                        return $_q->select('id')
                            ->from('document_types')
                            ->where('name', DocumentsTypes::$CARNET_IDENTIDAD);
                    })
                    ->whereDate('documents.expiration_date', '>=', $limit);
            })
            // obtiene todos los que no tengan matrícula de embarque vencida
            ->whereIn('workers.id', function ($q) use ($limit) {
                return $q->select('worker_id')
                    ->from('documents')
                    ->where('documents.document_type_id', function ($_q) {
                        return $_q->select('id')
                            ->from('document_types')
                            ->where('name', DocumentsTypes::$MATRICULA_EMBARQUE);
                    })
                    ->whereDate('documents.expiration_date', '>=', $limit);
            })
            // obtiene todos los que no tengan seguro de vida vencido
            ->whereIn('workers.id', function ($q) use ($limit) {
                return $q->select('worker_id')
                    ->from('documents')
                    ->where('documents.document_type_id', function ($_q) {
                        return $_q->select('id')
                            ->from('document_types')
                            ->where('name', DocumentsTypes::$SEGURO_VIDA);
                    })
                    ->whereDate('documents.expiration_date', '>=', $limit);
            });



    }
}
