<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Headline
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Headline extends Model
{
    use SoftDeletes;

    /** @var string[] */
    protected $fillable = ['ship_id', 'worker_id'];

    /** @var array */
    protected $hidden = ['ship_id', 'worker_id', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * Define la relación con el trabajador
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function worker()
    {
        return $this->belongsTo(Worker::class)->select('id', 'name', 'last_name', 'rut')->with('roles');
    }
}
