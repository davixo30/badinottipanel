<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Qualifier
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Qualifier extends Model
{
    /**
     * Define la relación con el usuario
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'id');
    }
}
