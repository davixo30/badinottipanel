<?php

namespace App\Models;

/**
 * Class OccupationalExam
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class OccupationalExam extends UploaderIdModel
{
    /** @var string[] */
    protected $fillable = [
        'exam_date', 'condition', 'expiration_date',
        'observations', 'certificate', 'worker_id', 'uploader_id'
    ];

    /** @var bool */
    public $timestamps = false;
}
