<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserContract
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class UserContract extends Model
{
    /** @var bool */
    public $timestamps = false;

    /** @var string */
    protected $primaryKey = 'user_id';

    /** @var string[] */
    protected $fillable = ['date', 'identity_card_copy', 'afp_id', 'health_forecast_id', 'contract_type_id', 'user_id'];

    /**
     * Define la relación con la AFP
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function afp()
    {
        return $this->belongsTo(AFP::class);
    }

    /**
     * Define la relación con el tipo de contrato
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contract_type()
    {
        return $this->belongsTo(ContractType::class);
    }

    /**
     * Define la relación con la previsión de salud
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function health_forecast()
    {
        return $this->belongsTo(HealthForecast::class);
    }
}
