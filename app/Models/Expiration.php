<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Expiration
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Expiration extends Model
{
    /** @var string */
    protected $primaryKey = 'worker_id';

    /** @var string[] */
    protected $fillable = ['worker_id', 'ci', 'enrollment', 'ri', 'exam', 'life_insurance'];
}
