<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientInduction
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class ClientInduction extends Model
{
    /** @var string[] */
    protected $fillable = ['client_id', 'induction_id'];

    /** @var string */
    protected $table = 'client_inductions';

    /** @var bool */
    public $timestamps = false;
}
