<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class SupplyRequest
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class SupplyRequest extends Model
{
    /** @var string[] */
    protected $fillable = ['id'];

    /** @var bool */
    public $timestamps = false;

    /**
     * Define la relación de la entrega de EPP en la solicitud de abastecimiento
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function epp_deliveries()
    {
        return $this->hasMany(EPPDeliverySupplyRequest::class, 'supply_request_id', 'id','warehouse_item_size_id')
            ->with('epp', 'worker:id,name,last_name,rut');
    }

    /**
     * Define la relación con la compra ingresada, sin confirmación aún
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function entered_purchase()
    {
        return $this->hasOne(Purchase::class)
            ->orderBy('created_at')
            ->where('status', null)
            ->with('supplier:id,name');
    }

    /**
     * Define la relación con la compra válida
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function purchase()
    {
        return $this->hasOne(Purchase::class)
            ->where('status', true)
            ->with('supplier:id,name');
    }

    /**
     * Define la relación con los artículos de bodega
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function warehouse_items()
    {
        return $this->hasMany(SupplyRequestWarehouseItem::class)->with('size');
    }

    /**
     * Crea una solicitud de abastecimiento
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public static function create(array $attributes = [])
    {
        $supplyRequest = null;

        DB::transaction(function () use ($attributes, &$supplyRequest) {
            $supplyRequest = static::query()->create(['id' => $attributes['crew_id']]);
            // si no se asigna manualmente, devuelve id = 0
            $supplyRequest->id = $attributes['crew_id'];

           /* if (count($attributes['items']) === 0 && count($attributes['epp']) === 0) {
                throw new \Exception("Debe asignar al menos un artículo o un EPP");
            }*/

            foreach ($attributes['items'] as $item) {
                SupplyRequestWarehouseItem::create([
                    'supply_request_id' => $attributes['crew_id'],
                    'warehouse_item_size_id' => $item['id'],
                    'quantity' => $item['quantity'],
                    'observations' => $item['observations']
                ]);
            }

            foreach ($attributes['epp'] as $epp) {
                EPPDeliverySupplyRequest::create([
                    'supply_request_id' => $attributes['crew_id'],
                    'warehouse_item_size_id' => $epp['id'],
                    'worker_id' => $epp['worker_id']
                ]);
            }
        });

        return $supplyRequest;
    }
}
