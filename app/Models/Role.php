<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Role
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Role extends Model
{
    /**
     * Obtiene listado de roles asociados a un trabajador
     *
     * @param int $id
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getByWorkerId(int $id)
    {
        return DB::table('roles')
            ->join('role_worker', 'roles.id', '=', 'role_worker.role_id')
            ->select('roles.id', 'roles.name')
            ->where('role_worker.worker_id', '=', $id)
            ->get();
    }
}
