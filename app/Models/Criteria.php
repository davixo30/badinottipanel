<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Criteria
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Criteria extends Model
{
    /** @var string */
    protected $table = 'criteria';
}
