<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class Contract
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Contract extends UploaderIdModel
{
    /** @var bool */
    public $timestamps = false;

    /** @var string */
    protected $primaryKey = 'worker_id';

    /** @var string[] */
    protected $fillable = [
        'date', 'salary', 'afp_id', 'health_forecast_id', 'contract_type_id',
        'worker_id', 'identity_card_copy', 'boarding_license'
    ];

    /**
     * Añade la propiedad de quien guarda el dato
     */
    protected static function boot()
    {
        parent::boot();

        // auto-sets values on edition
        static::updating(function ($query) {
            $query->uploader_id= Auth::id();
        });
    }

    /**
     * Define la relación con la AFP
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function afp()
    {
        return $this->belongsTo(AFP::class);
    }

    /**
     * Define la relación con el tipo de contrato
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contract_type()
    {
        return $this->belongsTo(ContractType::class);
    }

    /**
     * Define la relación con la previsión de salud
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function health_forecast()
    {
        return $this->belongsTo(HealthForecast::class);
    }
}
