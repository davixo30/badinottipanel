<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class WarehouseItemSize
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class WarehouseItemSize extends Model
{
    use SoftDeletes;

    /** @var string[] */
    protected $fillable = ['warehouse_item_id', 'name', 'quantity'];

    /** @var string[] */
    protected $hidden = ['warehouse_item_id', 'created_at', 'deleted_at', 'updated_at'];

    /**
     * Define la relación con el artículo de bodega
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item()
    {
        return $this->belongsTo(WarehouseItem::class, 'warehouse_item_id', 'id')
            ->select('id', 'name', 'price', 'classification_id', 'measurement_unit_id')
            ->with('classification', 'measurement_unit');
    }

    /**
     * Crea una nueva talla de un artículo de bodega
     *
     * @param array $attributes
     *
     * @throws \Exception
     * @return \Illuminate\Database\Eloquent\Builder|Model
     */
    public static function create(array $attributes = [])
    {
        // valida que no se repita la talla en este artículo
        $e = self::where('warehouse_item_id', $attributes['warehouse_item_id'])
            ->where('name', $attributes['name'])
            ->exists();

        if ($e) throw new \Exception("Ya se ha creado la talla '" . $attributes['name'] . "' para este artículo");

        return static::query()->create($attributes);
    }

    /**
     * Modifica una talla de un artículo de bodega
     *
     * @param array $attributes
     * @param array $options
     *
     * @throws \Exception
     * @return bool|\Illuminate\Database\Eloquent\Builder|Model
     */
    public function update(array $attributes = [], array $options = [])
    {
        // valida que no se repita la talla en este artículo
        $id = $attributes['id'];
        $e = self::where('warehouse_item_id', $attributes['warehouse_item_id'])
            ->where('name', $attributes['name'])
            ->where('id', '!=', $id)
            ->exists();

        if ($e) throw new \Exception("Ya se ha creado la talla '" . $attributes['name'] . "' para este artículo");

        unset($attributes['id']);

        return static::query()->where('id', $id)->update($attributes);
    }

    /**
     * Actualiza las cantidades de los artículos de bodega
     *
     * @param array $sizes
     */
    public static function updateQuantities(array $sizes)
    {
        DB::transaction(function () use ($sizes) {
            $now = Carbon::now();

            foreach ($sizes as $size) {
                DB::table('warehouse_item_sizes')
                    ->where('id', $size['id'])
                    ->update(['quantity' => $size['quantity'], 'updated_at' => $now]);
            }
        });
    }
}
