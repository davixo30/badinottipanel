<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class Induction
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class Induction extends Model
{
    use SoftDeletes;

    /** @var string[] */
    protected $fillable = ['name'];

    /**
     * Define la relación con los clientes asociados a las inducciones.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function clients()
    {
        return $this->hasManyThrough(
            Client::class,
            ClientInduction::class,
            'induction_id',
            'id',
            'id',
            'client_id'
        );
    }

    /**
     * Crea una nueva inducción y asocia clientes a la misma
     *
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function create(array $attributes = [])
    {
        $induction = null;

        DB::transaction(function () use ($attributes, &$induction) {
            $induction = static::query()->create(['name' => $attributes['name']]);

            foreach ($attributes['clients'] as $client) {
                ClientInduction::create([
                    'client_id' => $client,
                    'induction_id' => $induction->id
                ]);
            }
        });

        return $induction;
    }

    /**
     * Modifica una inducción y sus asociaciones
     *
     * @param $id
     * @param $name
     * @param $clients
     *
     * @return mixed
     */
    public static function edit($id, $name, $clients)
    {
        $induction = self::findOrFail($id);

        DB::transaction(function () use ($name, $clients, &$induction) {
            // modifica el nombre de la inducción
            $induction->name = $name;
            $induction->save();

            // elimina asociaciones anteriores
            ClientInduction::where('induction_id', $induction->id)->delete();

            // genera nuevas asociaciones con los clientes
            foreach ($clients as $client) {
                ClientInduction::create([
                    'client_id' => $client,
                    'induction_id' => $induction->id
                ]);
            }
        });

        return $induction;
    }
}
