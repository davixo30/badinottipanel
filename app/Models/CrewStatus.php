<?php

namespace App\Models;

use App\Types\CrewStatusesTypes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class CrewStatus
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Models
 */
class CrewStatus extends Model
{
    /**
     * Obtiene el id del estado por defecto al crear una tripulación
     *
     * @return int
     */
    public static function getDefaultStatusId(): int
    {
        return DB::table('crew_statuses')
            ->where('name', CrewStatusesTypes::$BORRADOR)
            ->select('id')
            ->first()
            ->id;
    }

    /**
     * Obtiene el id del estado 'Filtro caso cliente'
     *
     * @return int
     */
    public static function getCrewFilterStatusId(): int
    {
        return DB::table('crew_statuses')
            // REMOVIDO SOLICITUD ELIMINACION FILTRO SUPERVISOR
            //->where('name', CrewStatusesTypes::$FILTRO_CASO_CLIENTE)
            ->where('name', CrewStatusesTypes::$EN_CALENDARIO)
            ->select('id')
            ->first()
            ->id;
    }

    /**
     * Obtiene el id del estado 'En calendario'
     *
     * @return int
     */
    public static function getInCalendarStatusId(): int
    {
        return DB::table('crew_statuses')
            ->where('name', CrewStatusesTypes::$EN_CALENDARIO)
            ->select('id')
            ->first()
            ->id;
    }
}
