<?php

namespace App\Jobs;

use App\Mail\ScheduledCrewHumanResources;
use App\Mail\ScheduledCrewLogistics;
use App\Mail\ScheduledCrewSupervisor;
use App\Models\WarehouseItemSize;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

/**
 * Este job se dispara cuando una tripulación es filtrada en caso cliente. Debe notificar al jefe de flota.
 *
 * TODO pasar positions a Types
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Jobs
 */
class EmailDownloadDate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $crew_id;
    private $date;
    private $ship;
    private $client;
    /**
     * Create a new job instance.
     *
     * @param WarehouseItemSize $items
     */
    public function __construct(int $crew)
    {

        $this->crew_id = $crew;
        $datos = DB::table("crews")->where("id",$crew)->first();
        $barco = DB::table("ships")->where("id",$datos->ship_id)->first();
        $cliente = DB::table("clients")->where("id",$datos->client_id)->first();
        $this->date = $datos->download_date;
        $this->ship = $barco->name;
        $this->client = $cliente->name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $to = env('NOTIFICATIONS_RECEIVER');

        if (!$to) {
            $to = DB::table("users")->join("positions","positions.id","=","users.position_id")
            ->whereIn("positions.name",array("Abastecimiento","Administrador","Bodega","Gerente de operaciones","Ing. planificación y control de mantenimiento","Ing. servicio y taller","Jefe de flota","Jefe de operaciones","Logística","Prevencionista","RRHH","Sub contratación","Supervisor"))->get();

            foreach($to as $t){
                Mail::to($t->email)->send(new \App\Mail\EmailDownloadDate($this->crew_id,$this->ship,$this->date,$this->client));
            }
        } else {
            Mail::to($to)->send(new \App\Mail\EmailDownloadDate($this->crew_id,$this->ship,$this->date,$this->client));
        }

    }
}
