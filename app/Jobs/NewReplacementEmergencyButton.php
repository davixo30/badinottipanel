<?php

namespace App\Jobs;

use App\Mail\ScheduledCrewHumanResources;
use App\Mail\ScheduledCrewLogistics;
use App\Mail\ScheduledCrewSupervisor;
use App\Models\Client;
use App\Models\Crew;
use App\Models\Quotation;
use App\Models\Ship;
use App\Models\Worker;
use App\Models\EmergencyButton;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

/**
 * Este job se dispara cuando una tripulación es filtrada en caso cliente. Debe notificar al jefe de flota.
 *
 * TODO pasar positions a Types
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Jobs
 */
class NewReplacementEmergencyButton implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var Client */
    private $client;

    /** @var Ship */
    private $ship;

    /** @var Crew */
    private $crew;

     /** @var Worker */
     private $worker;

    /** @var EmergencyButton */
     private $emergency;

    /**
     * Create a new job instance.
     *
     * @param Crew $crew
     */
    public function __construct(Crew $crew,Worker $worker,EmergencyButton $emergency)
    {
        $this->crew = $crew;
        $this->client = Client::find($crew->client_id);
        $this->ship = Ship::find($crew->ship_id);
        $this->worker = $worker;
        $this->emergency = $emergency;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $to = env('NOTIFICATIONS_RECEIVER');

        if (!$to) {
                $to = DB::table("users")->join("positions","positions.id","=","users.position_id")
                ->whereIn("positions.name",array("Abastecimiento","Administrador","Bodega","Gerente de operaciones","Ing. planificación y control de mantenimiento","Ing. servicio y taller","Jefe de flota","Jefe de operaciones","Logística","Prevencionista","RRHH","Sub contratación","Supervisor"))->select("positions.name as position_name","users.email")->get();
                foreach($to as $t){
                    if($t->position_name == "Logística" || $t->position_name == "Administrador"){
                        Mail::to($t->email)->send(new \App\Mail\NewReplacementEmergencyButton($this->crew, $this->client, $this->ship,$this->worker,1,$this->emergency));
                    }else{
                        Mail::to($t->email)->send(new \App\Mail\NewReplacementEmergencyButton($this->crew, $this->client, $this->ship,$this->worker,0,$this->emergency));
                    }
                }
        } else {
            Mail::to($to)->send(new \App\Mail\NewReplacementEmergencyButton($this->crew, $this->client, $this->ship,$this->worker,1,$this->emergency));
            Mail::to($to)->send(new \App\Mail\NewReplacementEmergencyButton($this->crew, $this->client, $this->ship,$this->worker,0,$this->emergency));
        }

    }
}
