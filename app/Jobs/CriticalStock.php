<?php

namespace App\Jobs;

use App\Mail\ScheduledCrewHumanResources;
use App\Mail\ScheduledCrewLogistics;
use App\Mail\ScheduledCrewSupervisor;
use App\Models\WarehouseItemSize;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

/**
 * Este job se dispara cuando una tripulación es filtrada en caso cliente. Debe notificar al jefe de flota.
 *
 * TODO pasar positions a Types
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Jobs
 */
class CriticalStock implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $items;
    /**
     * Create a new job instance.
     *
     * @param WarehouseItemSize $items
     */
    public function __construct(Array $items)
    {
        $this->items = DB::table("warehouse_items")
        ->join("warehouse_item_sizes","warehouse_items.id","=","warehouse_item_sizes.warehouse_item_id")
                    ->whereIn("warehouse_item_sizes.id",$items)->select("warehouse_items.name","warehouse_item_sizes.name as size","warehouse_item_sizes.quantity","warehouse_items.critical_stock")->get();

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $to = env('NOTIFICATIONS_RECEIVER');

        if (!$to) {
            $to = DB::table("users")->join("positions","positions.id","=","users.position_id")
            ->whereIn("positions.name",array("Abastecimiento",'Bodega','Logística'))->get();

            foreach($to as $t){
                Mail::to($t->email)->send(new \App\Mail\CriticalStock($this->items));
            }
        } else {
                Mail::to($to)->send(new \App\Mail\CriticalStock($this->items));
        }
    }
}
