<?php

namespace App\Jobs;

use App\Mail\PurchaseRejected as PurchaseRejectedMail;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

/**
 * Este job se dispara cuando se rechaza una compra por parte de Operaciones.
 * Debe notificar a bodega para que genere una nueva compra en caso de ser solicitud de abastecimiento.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Jobs
 */
class PurchaseRejected implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var int */
    private $reason;

    /**
     * Create a new job instance.
     *
     * @param string $reason
     */
    public function __construct(string $reason)
    {
        $this->reason = $reason;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $to = env('NOTIFICATIONS_RECEIVER');

        if (!$to) {
            $to = DB::table("users")->join("positions","positions.id","=","users.position_id")
            ->whereIn("positions.name",array("Abastecimiento",'Logística'))->select("positions.name as position_name","users.email")->get();

            foreach($to as $t){
                Mail::to($t->email)->send(new PurchaseRejectedMail($this->reason));
            }
        } else {
            Mail::to($to)->send(new PurchaseRejectedMail($this->reason));
        }
    }
}
