<?php

namespace App\Jobs;

use App\Mail\ScheduledCrewHumanResources;
use App\Mail\ScheduledCrewLogistics;
use App\Mail\ScheduledCrewSupervisor;
use App\Models\Client;
use App\Models\Crew;
use App\Models\Quotation;
use App\Models\Ship;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

/**
 * Este job se dispara cuando una tripulación es filtrada en caso cliente. Debe notificar al jefe de flota.
 *
 * TODO pasar positions a Types
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Jobs
 */
class NewWorkerCreated implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $worker_id;
    /**
     * Create a new job instance.
     *
     * @param Crew $crew
     */
    public function __construct(int $id)
    {
        $this->worker_id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $to = env('NOTIFICATIONS_RECEIVER');

        if (!$to) {
            $to = DB::table("users")->join("positions","positions.id","=","users.position_id")
            ->whereIn("positions.name",array("Abastecimiento","Administrador","Bodega","Gerente de operaciones","Ing. planificación y control de mantenimiento","Ing. servicio y taller","Jefe de flota","Jefe de operaciones","Logística","Prevencionista","RRHH","Sub contratación","Supervisor"))->get();

                foreach($to as $t){
                    Mail::to($t->email)->send(new \App\Mail\NewWorkerMail($this->worker_id));
                }
               
        }else{
            Mail::to($to)->send(new \App\Mail\NewWorkerMail($this->worker_id));
        }
       
    }
}
