<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

/**
 * Este job se dispara cuando se confirma una tripulación por parte del jefe de flota, notifica a supervisor para
 * aplicar filtro caso cliente
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Jobs
 */
class NewClientCaseFilter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var int */
    private $crewId;

    /**
     * Create a new job instance.
     *
     * @param int $crewId
     */
    public function __construct(int $crewId)
    {
        $this->crewId = $crewId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
        $to = env('NOTIFICATIONS_RECEIVER');

        if (!$to) {
            $to = DB::table('crews')
                ->where('crews.id', $this->crewId)
                ->join('ships', 'crews.ship_id', '=', 'ships.id')
                ->join('users', 'ships.supervisor_id', '=', 'users.id')
                ->select('users.email')
                ->first()
                ->email;

            Mail::to($to)->send(new \App\Mail\NewClientCaseFilter($this->crewId));
        } else {
            Mail::to($to)->send(new \App\Mail\NewClientCaseFilter($this->crewId));
        }
    }
}
