<?php

namespace App\Jobs;

use App\Mail\PurchaseValidated as PurchaseValidatedMail;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

/**
 * Este job se dispara cuando se valida una compra por parte de Operaciones.
 * Debe notificar a bodega para que genere una factura con respecto a la compra validada.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Jobs
 */
class PurchaseValidated implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var int */
    private $purchaseId;

    /**
     * Create a new job instance.
     *
     * @param int $purchaseId
     */
    public function __construct(int $purchaseId)
    {
        $this->purchaseId = $purchaseId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $to = env('NOTIFICATIONS_RECEIVER');

        if (!$to) {
            $to = DB::table("users")->join("positions","positions.id","=","users.position_id")
            ->whereIn("positions.name",array("Abastecimiento",'Logística'))->select("positions.name as position_name","users.email")->get();

            foreach($to as $t){
                Mail::to($t->email)->send(new PurchaseValidatedMail($this->purchaseId));
            }
        } else {
            Mail::to($to)->send(new PurchaseValidatedMail($this->purchaseId));
        }
    }
}
