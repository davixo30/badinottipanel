<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

/**
 * Este job se dispara cuando se genera una nueva cotización.
 * Notifica a Operaciones que debe validar o rechazar dicha cotización.
 * TODO pasar positions a Types
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Jobs
 */
class NewQuotation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var int */
    private $quotationId;

    /**
     * Create a new job instance.
     *
     * @param int $quotationId
     */
    public function __construct(int $quotationId)
    {
        $this->quotationId = $quotationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $to = env('NOTIFICATIONS_RECEIVER');

        if (!$to) {
                $to = DB::table("users")->join("positions","positions.id","=","users.position_id")
                    ->whereIn("positions.name",array("Jefe de operaciones",'Administrador'))->get();

                    foreach($to as $t){
                        Mail::to($t->email)->send(new \App\Mail\NewQuotation($this->quotationId));
                    }
        } else {
            Mail::to($to)->send(new \App\Mail\NewQuotation($this->quotationId));   
        }
    }
}
