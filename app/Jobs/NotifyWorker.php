<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

/**
 * Este job se dispara cuando se aprueba una cotización.
 * Notifica a los trabajadores a través de SMS.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Jobs
 */
class NotifyWorker implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var int */
    private $ticketId;

    /**
     * Create a new job instance.
     *
     * @param int $ticketId
     */
    public function __construct(int $ticketId)
    {
        $this->ticketId = $ticketId;
    }

    /**
     * Execute the job.
     *
     * @throws \Twilio\Exceptions\ConfigurationException
     * @throws \Twilio\Exceptions\TwilioException
     * @return void
     */
    public function handle()
    {   
              Artisan::call('sms:send', ['ticketId' => $this->ticketId]);
    }
}
