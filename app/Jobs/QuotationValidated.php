<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

/**
 * Este job se dispara cuando se valida una cotización por parte de Operaciones.
 * Debe notificar a logística para que genere una factura con respecto a la cotización validada.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Jobs
 */
class QuotationValidated implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var int */
    private $quotationId;

    /**
     * Create a new job instance.
     *
     * @param int $quotationId
     */
    public function __construct(int $quotationId)
    {
        $this->quotationId = $quotationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $to = env('NOTIFICATIONS_RECEIVER');

        if (!$to) {
            $to = DB::table("users")->join("positions","positions.id","=","users.position_id")
            ->whereIn("positions.name",array('Administrador','Logística'))->get();

            foreach($to as $t){
                Mail::to($t->email)->send(new \App\Mail\QuotationValidated($this->quotationId));
            }
        } else {
            Mail::to($to)->send(new \App\Mail\QuotationValidated($this->quotationId));
        }
        
    }
}
