<?php

namespace App\Jobs;

use App\Mail\ScheduledCrewHumanResources;
use App\Mail\ScheduledCrewLogistics;
use App\Mail\ScheduledCrewRiskPreventionist;
use App\Mail\ScheduledCrewSupervisor;
use App\Models\Client;
use App\Models\Crew;
use App\Models\Quotation;
use App\Models\Ship;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
/**
 * Este job se dispara cuando una tripulación pasa a estar 'En calendario'. Debe notificar a las distintas áreas de
 * las tareas por completar
 * TODO: position pasar a Types
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Jobs
 */
class ScheduledCrew implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var Client */
    private $client;

    /** @var Ship */
    private $ship;

    /** @var Crew */
    private $crew;

    /**
     * Create a new job instance.
     *
     * @param Crew $crew
     */
    public function __construct(Crew $crew)
    {
        $this->crew = $crew;
        $this->client = Client::find($crew->client_id);
        $this->ship = Ship::find($crew->ship_id);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $to = env('NOTIFICATIONS_RECEIVER');

        if (!$to) {
            $toSupervisor = User::select('email')->where('id', $this->ship->supervisor_id)->first()->email;
            Mail::to($toSupervisor)->send(new ScheduledCrewSupervisor($this->crew, $this->client, $this->ship));

            $toLogistics = DB::table("users")->join("positions","positions.id","=","users.position_id")
            ->whereIn("positions.name",array('Administrador','Logística'))->get();

            $quotation = Quotation::where('crew_id', $this->crew->id)->where('entered', 0)->first();
            foreach($toLogistics as $t){      
                Mail::to($t->email)->send(new ScheduledCrewLogistics($this->crew, $this->client, $this->ship, $quotation));
            }
 

            $toHumanResources = User::select('email')->where('position_id', function ($query) {
                return $query->select('id')->from('positions')->whereIn('name', array("Abastecimiento","Administrador","Bodega","Gerente de operaciones","Ing. planificación y control de mantenimiento","Ing. servicio y taller","Jefe de flota","Jefe de operaciones","Logística","Prevencionista","RRHH","Sub contratación","Supervisor"));
            })->first()->email;

            Mail::to($toHumanResources)->send(new ScheduledCrewHumanResources($this->crew, $this->client, $this->ship));

            $toOutsourcing = User::select('email')->where('position_id', function ($query) {
                return $query->select('id')->from('positions')->where('name', 'Sub contratación');
            })->first()->email;
            Mail::to($toOutsourcing)->send(new ScheduledCrewHumanResources($this->crew, $this->client, $this->ship));


            $toRiskPreventionist = User::select('email')->where('position_id', function ($query) {
                return $query->select('id')->from('positions')->where('name', 'Prevencionista');
            })->first()->email;
            Mail::to($toRiskPreventionist)->send(new ScheduledCrewRiskPreventionist($this->crew, $this->client, $this->ship));
        } else {
            Mail::to($to)->send(new ScheduledCrewSupervisor($this->crew, $this->client, $this->ship));
            $quotation = Quotation::where('crew_id', $this->crew->id)->where('entered', 0)->first();   
            Mail::to($to)->send(new ScheduledCrewLogistics($this->crew, $this->client, $this->ship, $quotation));

            Mail::to($to)->send(new ScheduledCrewHumanResources($this->crew, $this->client, $this->ship));

            Mail::to($to)->send(new ScheduledCrewHumanResources($this->crew, $this->client, $this->ship));

            Mail::to($to)->send(new ScheduledCrewRiskPreventionist($this->crew, $this->client, $this->ship));
        }
    }
}
