<?php

namespace App\Jobs;

use App\Mail\ScheduledCrewHumanResources;
use App\Mail\ScheduledCrewLogistics;
use App\Mail\ScheduledCrewSupervisor;
use App\Models\WarehouseItemSize;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

/**
 * Este job se dispara cuando una tripulación es filtrada en caso cliente. Debe notificar al jefe de flota.
 *
 * TODO pasar positions a Types
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Jobs
 */
class EmailDocuments implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $crew_id;
    private $date;
    private $ship;
    private $client;
    /**
     * Create a new job instance.
     *
     * @param WarehouseItemSize $items
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $date = date('Y-m-d', strtotime('+30 days'));

        $inducciones = DB::table("induction_workers")->join("workers","workers.id","=","induction_workers.worker_id")
                        ->join("inductions","inductions.id","=","induction_workers.induction_id")
                         ->select("workers.rut","workers.id","induction_workers.id","workers.id as id_worker","workers.name","workers.last_name",'inductions.name as induction',DB::raw("DATE_ADD(induction_workers.induction_date,INTERVAL 1 YEAR) as expiration_date"))
                         ->whereRaw("DATE_ADD(induction_workers.induction_date,INTERVAL 1 YEAR) = '".$date."'")
                         ->get();

        $examenes = DB::table("occupational_exams")->join("workers","workers.id","=","occupational_exams.worker_id")
                    ->select("workers.rut","workers.id","workers.name","workers.last_name","occupational_exams.expiration_date")
                    ->where("expiration_date","=",$date)
                    ->get();

        $documentos = DB::table("documents")
                        ->join("workers","workers.id","=","documents.worker_id")
                        ->join("document_types","document_types.id","=","documents.document_type_id")
                        ->select("workers.id","workers.rut","workers.name","workers.last_name","document_types.name as document","documents.expiration_date")
                        ->where("expiration_date","=",$date)
                        ->get();

        $carenas = DB::table("ships")->join("ship_careens","ship_careens.ship_id","=","ships.id")
                                    ->select("ships.name","ships.plate")
                                    ->whereRaw("DATE_ADD(ship_careens.careen_date,INTERVAL 1 YEAR) = '".$date."'")
                                    ->get();
        
        $revistas = DB::table("ships")->join("ship_reviews","ship_reviews.ship_id","=","ships.id")
                                    ->select("ships.name","ships.plate")
                                    ->whereRaw("DATE_ADD(ship_reviews.review_date,INTERVAL 1 YEAR) = '".$date."'")
                                    ->get();
        
         $epp = DB::table("epp_deliveries")->join("epp_delivery_groups","epp_deliveries.epp_delivery_group_id","=","epp_delivery_groups.id")
                                          ->join("workers","workers.id","=","epp_delivery_groups.worker_id")
                                          ->join("warehouse_item_sizes","warehouse_item_sizes.id","=","epp_deliveries.warehouse_item_size_id")
                                          ->join("warehouse_items","warehouse_items.id","=","warehouse_item_sizes.warehouse_item_id")
                                            ->select("workers.name","workers.last_name","workers.rut","warehouse_items.name as item")
                                            ->whereRaw("DATE_ADD(epp_delivery_groups.delivery,INTERVAL 1 YEAR) = '".$date."'")
                                            ->get();
        
        $seguro = "";
        $segurodevida = DB::table("company_documents")->where("document_type_id",1)->orderBy("id","desc")->first()->expiration_date;
        if($segurodevida){
            if($segurodevida==$date){
                $seguro = $segurodevida;
            }
        }
    
        $to = env('NOTIFICATIONS_RECEIVER');

        if (!$to) {
            $to = DB::table("users")->join("positions","positions.id","=","users.position_id")->select("positions.name","users.email")
                        ->whereIn("positions.name",array("Prevencionista",'Administrador','Abastecimiento','Bodega','Logística','RRHH','Sub contratación','Ing. planificación y control de mantenimiento'))->get();

                foreach($to as $t){
                    if($t->name=="Prevencionista" && ( sizeOf($inducciones)>0 || sizeOf($examenes)>0 || sizeOf($epp)>0)){
                        Mail::to($t->email)->send(new \App\Mail\EmailDocuments($inducciones,$examenes,$documentos,$t->name,$date,$carenas,$revistas,$seguro,$epp));
                    }
                    if($t->name=="Administrador" && ($seguro!="" || sizeOf($carenas)>0 || sizeOf($revistas)>0 || sizeOf($epp)>0)){
                        Mail::to($t->email)->send(new \App\Mail\EmailDocuments($inducciones,$examenes,$documentos,$t->name,$date,$carenas,$revistas,$seguro,$epp));
                    }
                    if($t->name=="Ing. planificación y control de mantenimiento" && (sizeOf($carenas)>0 || sizeOf($revistas)>0 )){
                        Mail::to($t->email)->send(new \App\Mail\EmailDocuments($inducciones,$examenes,$documentos,$t->name,$date,$carenas,$revistas,$seguro,$epp));
                    }
                    if($t->name=="RRHH" && ($seguro!="" || sizeOf($documentos)>0)){
                        Mail::to($t->email)->send(new \App\Mail\EmailDocuments($inducciones,$examenes,$documentos,$t->name,$date,$carenas,$revistas,$seguro,$epp));
                    }
                    if($t->name=="Sub contratación" && sizeOf($documentos)>0){
                        Mail::to($t->email)->send(new \App\Mail\EmailDocuments($inducciones,$examenes,$documentos,$t->name,$date,$carenas,$revistas,$seguro,$epp));
                    }

                    if(($t->name=="Bodega" || $t->name=="Abastecimiento" || $t->name=="Logística" ) && (sizeOf($epp)>0)){
                        Mail::to($t->email)->send(new \App\Mail\EmailDocuments($inducciones,$examenes,$documentos,$t->name,$date,$carenas,$revistas,$seguro,$epp));
                    }         
                }
        }else{
            Mail::to($to)->send(new \App\Mail\EmailDocuments($inducciones,$examenes,$documentos,'Administrador',$date,$carenas,$revistas,$seguro,$epp));              
        }

    }
}
