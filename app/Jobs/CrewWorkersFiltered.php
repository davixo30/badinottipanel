<?php

namespace App\Jobs;

use App\Mail\ScheduledCrewHumanResources;
use App\Mail\ScheduledCrewLogistics;
use App\Mail\ScheduledCrewSupervisor;
use App\Models\Client;
use App\Models\Crew;
use App\Models\Quotation;
use App\Models\Ship;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

/**
 * Este job se dispara cuando una tripulación es filtrada en caso cliente. Debe notificar al jefe de flota.
 *
 * TODO pasar positions a Types
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Jobs
 */
class CrewWorkersFiltered implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var Client */
    private $client;

    /** @var Ship */
    private $ship;

    /** @var Crew */
    private $crew;

    /**
     * Create a new job instance.
     *
     * @param Crew $crew
     */
    public function __construct(Crew $crew)
    {
        $this->crew = $crew;
        $this->client = Client::find($crew->client_id);
        $this->ship = Ship::find($crew->ship_id);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
        $to = env('NOTIFICATIONS_RECEIVER');
        if (!$to) {
            $to = DB::table('users')
                ->where('position_id', function ($query) {
                    return $query->select('id')->from('positions')->where('name','Jefe de flota');
                })
                ->select('users.email')
                ->first()
                ->email;
        
               Mail::to($to)->send(new \App\Mail\CrewWorkersFiltered($this->crew, $this->client, $this->ship));
        } else {
               Mail::to($to)->send(new \App\Mail\CrewWorkersFiltered($this->crew, $this->client, $this->ship));
        }
    }
}
