<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;

/**
 * Este job se dispara cuando se valida un filtro caso cliente. Envía la información a Eskuad para generar la tarea.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Jobs
 */
class CrewToEskuad implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var int */
    private $crewId;

    /**
     * Create a new job instance.
     *
     * @param int $crewId
     */
    public function __construct(int $crewId)
    {
        $this->crewId = $crewId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       // Artisan::call('eskuad:new_crew', ['crewId' => $this->crewId]);
    }
}
