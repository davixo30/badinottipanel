<?php

namespace App\Jobs;

use App\Mail\NewSupplyRequest as NewSupplyRequestMail;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

/**
 * Este job se dispara cuando se confirma genera una solicitud de abastecimiento de una tripulación.
 * Notifica a bodega para que genera una compra.
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Jobs
 */
class NewSupplyRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var int */
    private $supplyRequestId;

    /**
     * Create a new job instance.
     *
     * @param int $supplyRequestId
     */
    public function __construct(int $supplyRequestId)
    {
        $this->supplyRequestId = $supplyRequestId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $to = env('NOTIFICATIONS_RECEIVER');

        if (!$to) {
            $to = DB::table("users")->join("positions","positions.id","=","users.position_id")
                ->whereIn("positions.name",array("Abastecimiento","Administrador","Bodega","Gerente de operaciones","Ing. planificación y control de mantenimiento","Ing. servicio y taller","Jefe de flota","Jefe de operaciones","Logística","Prevencionista","RRHH","Sub contratación","Supervisor"))->select("positions.name as position_name","users.email")->get();

            foreach($to as $t){
                Mail::to($t->email)->send(new NewSupplyRequestMail($this->supplyRequestId));
            }

        } else {
            Mail::to($to)->send(new NewSupplyRequestMail($this->supplyRequestId));
        }
    }
}
