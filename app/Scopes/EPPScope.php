<?php

namespace App\Scopes;

use App\Types\WarehouseItemClassificationsTypes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

/**
 * Class EPPScope
 *
 * @author Juan Gamonal H <juangamonalh@gmail.com>
 * @package App\Scopes
 */
class EPPScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereIn('classification_id', function ($query) {
            return $query->select('id')
                ->from('warehouse_item_classifications')
                ->where('name', '<>', WarehouseItemClassificationsTypes::$OTROS);
        });
    }
}
