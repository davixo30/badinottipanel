<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        /**
         * Defining the user Roles
         */
        Gate::define('isAdmin', function ($user) {
            return $user->isAdmin();
        });

        Gate::define('isSupervisor', function ($user) {
            return $user->isSupervisor();
        });

        Gate::define('isAbastecimiento', function ($user) {
            return $user->isAbastecimiento();
        });

        Gate::define('isBodega', function ($user) {
            return $user->isBodega();
        });

        Gate::define('isGerente', function ($user) {
            return $user->isGerente();
        });

        Gate::define('isPlanificacion', function ($user) {
            return $user->isPlanificacion();
        });

        Gate::define('isServiciotaller', function ($user) {
            return $user->isServiciotaller();
        });

        Gate::define('isJefeflota', function ($user) {
            return $user->isJefeflota();
        });

        Gate::define('isJefeoperaciones', function ($user) {
            return $user->isJefeoperaciones();
        });

        Gate::define('isLogistica', function ($user) {
            return $user->isLogistica();
        });

        Gate::define('isPrevencionista', function ($user) {
            return $user->isPrevencionista();
        });

        Gate::define('isRrhh', function ($user) {
            return $user->isRrhh();
        });

        Gate::define('isSubcontratacion', function ($user) {
            return $user->isSubcontratacion();
        });
    }
}
