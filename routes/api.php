<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('uploadXlsxTripulantes', 'EskuadController@uploadWorkers');

Route::get('updateDatasource/{crewId}/{updateEskuad}', 'EskuadController@updateDatasource');

Route::group(['middleware' => 'auth:api', 'namespace' => 'API'], function () {

    // clientes
    Route::get('clientes', 'ClientController');

      // documentos empresa
      Route::get('company/documents', 'CompanydocumentController@getDocuments');
      Route::post('company/documents', 'CompanydocumentController@createDocument');

    // ficha de tripulación
    Route::get('ficha-tripulacion/{id}/detalles', 'CrewDetailController');
    Route::get('ficha-tripulacion/{id}/tripulantes', 'CrewDetailController@getWorkers');
    Route::get('ficha-tripulacion/{id}/filtros', 'CrewDetailController@getFilteredWorkers');
    Route::get('ficha-tripulacion/{id}/cotizaciones', 'CrewDetailController@getQuotations');
    Route::get('ficha-tripulacion/{id}/solicitud-abastecimiento', 'CrewDetailController@getSupplyRequest');
    Route::get('ficha-tripulacion/{id}/compras', 'CrewDetailController@getPurchases');
    Route::get('ficha-tripulacion/{id}/botones-emergencia', 'CrewDetailController@getEmergencyButtons');

    // listados para administración de usuarios y trabajadores
    Route::get('cargos', 'PositionController');
    Route::get('afps', 'AFPController');
    Route::get('previsiones-salud', 'HealthForecastController');
    Route::get('tipos-contrato', 'ContractTypeController');
    Route::get('roles', 'RoleController');
    Route::get('tipos-documentos', 'DocumentTypeController');
    Route::get('tipos-documentos-extra', 'DocumentTypeController@extraDocuments');

    // administración de usuarios
    Route::get('usuarios/{id}/contrato', 'UserContractController@get');
    Route::post('usuarios/{id}/contrato', 'UserContractController@set');
    Route::post('usuarios/{id}/foto', 'UserController@updateAvatar');
    Route::post('usuarios/updatePass', 'UserController@updatePass');

    // administración de embarcaciones
    Route::get('embarcaciones/{id}/certificados', 'ShipController@getCertificates');
    Route::post('embarcaciones/{id}/certificados', 'ShipController@createCertificate');
    Route::get('embarcaciones/{id}/planos', 'ShipController@getPlans');
    Route::post('embarcaciones/{id}/planos', 'ShipController@createPlans');
    Route::get('embarcaciones/{id}/revista', 'ShipController@getReviews');
    Route::post('embarcaciones/{id}/revista', 'ShipController@createReview');
    Route::get('embarcaciones/{id}/carena', 'ShipController@getCareens');
    Route::post('embarcaciones/{id}/carena', 'ShipController@createCareen');
    Route::get('certificados/{shipId}', 'CertificateController');
    Route::get('planos/{shipId}', 'PlansController');
    Route::get('supervisores', 'SupervisorController');
    Route::get('tipos-embarcaciones', 'ShipTypeController');
    Route::get('embarcaciones/{id}/trabajadores-disponibles-titulares', 'ShipController@getAvailableWorkersForHeadline');
    Route::get('embarcaciones/{id}/titulares', 'ShipController@getHeadlines');
    Route::post('embarcaciones/{id}/titulares', 'ShipController@setHeadlines');
    Route::delete('embarcaciones/{id}/titulares/{workerId}', 'ShipController@deleteHeadline');
    Route::get('embarcaciones/{id}/especificaciones-tecnicas', 'ShipController@getTechnicalSpecifications');
    Route::post('embarcaciones/{id}/especificaciones-tecnicas', 'ShipController@setTechnicalSpecifications');

    // administración y ficha de trabajadores
    Route::post('trabajadores/sendmail', 'WorkerController@sendEmail');
    Route::get('trabajadores/{id}/contrato', 'ContractController@get');
    Route::post('trabajadores/{id}/contrato', 'ContractController@set');
    Route::get('trabajadores/{id}/roles', 'WorkerController@getRoles');
    Route::post('trabajadores/{id}/roles', 'WorkerController@setRoles');
    Route::get('trabajadores/{id}/documentos', 'WorkerController@getDocuments');
    Route::get('trabajadores/{id}/documentosExtra', 'WorkerController@getDocumentsExtra');
    Route::post('trabajadores/{id}/documentos', 'WorkerController@setDocument');
    Route::post('trabajadores/{id}/documentosExtra', 'WorkerController@setDocumentExtra');
    Route::get('trabajadores/{id}/calificaciones', 'WorkerController@getQualifications');
    Route::get('trabajadores/{id}/inducciones', 'WorkerController@getInductions');
    Route::post('trabajadores/{id}/inducciones', 'WorkerController@setInduction');
    Route::get('trabajadores/{id}/entregas-epp', 'WorkerController@getEPPDeliveries');
    Route::get('trabajadores/{id}/imc/{year}', 'WorkerController@getIMC');
    Route::get('trabajadores/{id}/examenes-ocupacionales', 'WorkerController@getOccupationalExams');
    // TODO: hacer esto? seria para reemplazar la url que está en Warehouse
    // Route::post('trabajadores/{id}/entregas-epp', 'WorkerController@getEPPDeliveries');
    Route::post('trabajadores/{id}/foto', 'WorkerController@updateAvatar');
    Route::get('atributos', 'AttributeController');
    Route::get('estados-civiles', 'MaritalStateController');
    Route::get('regiones', 'RegionController');
    Route::get('regiones/{id}/comunas', 'RegionController@getWithCommunes');

    // Jefe de flota
    Route::group(['namespace' => 'FleetChief', 'prefix' => 'jefe-flota'], function () {
        Route::get('tripulaciones', 'CrewController@index');
        Route::post('tripulaciones', 'CrewController@store');
        Route::post('tripulaciones/update-upload-date', 'CrewController@updateuploaddate');
        Route::get('tripulaciones/{id}/editar', 'CrewController@getForEdition');
        Route::get('tripulaciones/{id}/trabajadores-disponibles', 'CrewController@getAvailableWorkers');
        Route::get('tripulaciones/{id}/trabajadores-espera', 'CrewController@getWaitingWorkers');
        Route::get('tripulaciones/{id}/trabajadores-reemplazo', 'CrewController@getReplacementWorkers');
        //Route::get('tripulaciones/{id}/trabajadores-reemplazo-no-ingresados', 'CrewController@getReplacementWorkersNoAdded');
        Route::post('tripulaciones/{id}/trabajadores', 'CrewController@setWorkers');
        Route::delete('tripulaciones/{id}/trabajadores/{workerId}', 'CrewController@deleteWorker');
        Route::post('tripulaciones/{id}/confirmar', 'CrewController@confirm');
        Route::delete('tripulaciones/{id}/eliminar', 'CrewController@destroy');
        Route::get('tripulaciones/{id}/boton-emergencia', 'CrewController@getEmergencyButtonAddWorker');
        Route::post('tripulaciones/{id}/boton-emergencia', 'CrewController@setEmergencyButtonAddWorker');
        Route::get('tripulaciones/{id}/boton-emergencia/{crewWorkerId}', 'CrewController@getEmergencyButton');
        Route::post('tripulaciones/{id}/boton-emergencia/{crewWorkerId}', 'CrewController@setEmergencyButton');
        // TODO: eliminar controller?
        // Route::get('proveedores-alojamiento', 'LodgingController@getProviders');
    });

    // Supervisor
    Route::group(['namespace' => 'Supervisor', 'prefix' => 'supervisor'], function () {
        // filtro caso cliente
        Route::get('tripulaciones', 'CrewController@index');
        Route::get('tripulaciones/{id}', 'CrewController@show');
        Route::post('tripulaciones/{id}/filtro-tripulacion', 'CrewController@setCrewFilter');

        // solicitud abastecimiento
        Route::get('tripulaciones/{id}/solicitud-abastecimiento', 'SupplyRequestController@show');
        Route::post('tripulaciones/{id}/solicitud-abastecimiento', 'SupplyRequestController@store');
        Route::get('articulos', 'WarehouseItemController');
        Route::get('epp', 'EPPController');
        Route::get('{id}/trabajadores', 'WorkerController');

        // calificación de trabajadores
        Route::post('calificacion-trabajador/{id}', 'WorkerController@qualify');
        Route::get('criterios-calificacion', 'CriteriaController');
    });

    // Operaciones
    Route::group(['namespace' => 'Operations', 'prefix' => 'operaciones'], function () {
        Route::get('compras', 'PurchaseController@index');
        Route::get('compras-tripulacion', 'PurchaseController@indexCrew');
        Route::get('compras/{id}', 'PurchaseController@show');
        Route::get('compras-tripulacion/{id}', 'PurchaseController@showCrew');
        Route::post('compras/{id}/rechazar', 'PurchaseController@rejectPurchase');
        Route::post('compras/{id}/validar', 'PurchaseController@validatePurchase');
        Route::get('cotizaciones', 'QuotationController@index');
        Route::get('cotizaciones/{id}', 'QuotationController@show');
        Route::post('cotizaciones/{id}/rechazar', 'QuotationController@rejectQuotation');
        Route::post('cotizaciones/{id}/validar', 'QuotationController@validateQuotation');
    });

    // Logística
    Route::group(['namespace' => 'Logistics', 'prefix' => 'logistica'], function () {
        Route::get('tripulaciones', 'CrewController@crewWithoutEmpy');
        Route::get('cotizaciones/{id}', 'QuotationController@show');
        Route::post('cotizaciones/{id}/pasajes', 'TicketController@store');
        Route::post('cotizaciones/{id}/pasajes/asignar-stock', 'TicketController@assignStock');
        Route::post('cotizaciones/{id}/pasajes/asociar-trabajadores', 'TicketController@associateWorkers');
        Route::post('cotizaciones/{id}/pasajes/desasociar-trabajador', 'TicketController@disassociateWorker');
        Route::post('cotizaciones/{id}/pasajes/{ticketId}', 'TicketController@update');
        Route::delete('cotizaciones/{id}/pasajes/{ticketId}', 'TicketController@deleteTicket');
        Route::post('cotizaciones/{id}/confirmar-cotizacion', 'QuotationController@store');
        Route::post('cotizaciones/{id}/facturar-cotizacion', 'QuotationController@invoice');
        Route::get('aerolineas', 'AirlineController');
        Route::get('aerolineas/{id}/stock', 'AirlineController@getStock');
        Route::get('aeropuertos', 'AirportController');
        Route::get('cuentas-bancarias', 'BankAccountController');
        Route::get('tramos', 'FlightStretchController');

        // alojamiento y traslado
        Route::get('proveedores-alojamiento', 'LodgingController@getProviders');
        Route::get('proveedores-traslado', 'RelocationController@getProviders');
        Route::post('{crewWorkerId}/alojamiento', 'LodgingController@store');
        Route::post('{crewWorkerId}/traslado', 'RelocationController@store');

        // pasajes
        Route::get('pasajes', 'TicketController@index');
        Route::get('pasajes/{id}', 'TicketController@show');
        Route::post('pasajes/{id}/comprar', 'TicketController@buy');
        Route::get('pasajes/{id}/stock', 'TicketController@toStock');

        // emergencia
        Route::group(['prefix' => 'boton-emergencia'], function () {
            Route::get('/', 'EmergencyButtonController@index');
            Route::get('/{id}/cotizacion', 'EmergencyButtonController@getQuotation');
            Route::post('/{id}/cotizacion', 'EmergencyButtonController@setQuotation');

            Route::group(['prefix' => 'cotizaciones'], function () {
                Route::post('/{id}/pasajes', 'EmergencyTicketController@store');
                Route::post('/{id}/pasajes/asociar-trabajadores', 'EmergencyTicketController@associateWorkers');
                Route::post('/{id}/pasajes/desasociar-trabajador', 'EmergencyTicketController@disassociateWorker');
                Route::post('/{id}/pasajes/{ticketId}', 'EmergencyTicketController@update');
                Route::delete('/{id}/pasajes/{ticketId}', 'EmergencyTicketController@deleteTicket');
            });
        });
    });

    // Prevencionista
    Route::group(['namespace' => 'RiskPreventionist', 'prefix' => 'prevencionista'], function () {
        // inducciones
        Route::get('inducciones', 'InductionController');
        Route::post('inducciones', 'InductionController@store');
        Route::post('inducciones/{id}', 'InductionController@update');
        Route::delete('inducciones/{id}', 'InductionController@destroy');

        // salud
        Route::post('trabajadores/{id}/imc', 'WorkerController@setIMC');
        Route::post('trabajadores/{id}/examen-ocupacional', 'WorkerController@setOccupationalExam');
    });

    // Bodega
    Route::group(['namespace' => 'Warehouse', 'prefix' => 'bodega'], function () {
        // administrador de artículos
        Route::get('articulos', 'WarehouseItemController@index');
        Route::post('articulos', 'WarehouseItemController@store');
        Route::get('articulos/{id}', 'WarehouseItemController@show');
        Route::post('articulos/{id}', 'WarehouseItemController@update');
        Route::delete('articulos/{id}', 'WarehouseItemController@destroy');
        Route::get('articulos/{id}/tallas', 'WarehouseItemSizeController@index');
        Route::post('articulos/{id}/tallas', 'WarehouseItemSizeController@store');
        Route::post('articulos/{id}/tallas/confirmar-cantidades', 'WarehouseItemSizeController@confirmQuantities');
        Route::post('articulos/{itemId}/tallas/{id}', 'WarehouseItemSizeController@update');
        Route::delete('articulos/{itemId}/tallas/{id}', 'WarehouseItemSizeController@destroy');

        // solicitudes de abastecimiento y compras
        Route::get('articulos-tallas', 'WarehouseItemController@withSizes');
        Route::get('proveedores', 'SupplierController');
        Route::get('proveedores/{id}', 'SupplierController@getSuppliersByType');
        Route::get('compras', 'PurchaseController@index');
        Route::get('compras-tripulacion', 'PurchaseController@getPurchaseCrew');
        Route::post('compras', 'PurchaseController@store');
        Route::get('compras/{id}', 'PurchaseController@show');
        Route::get('compras-tripulacion/{id}', 'PurchaseController@showCrew');
        Route::post('compras/{id}/facturar', 'PurchaseController@invoice');
        Route::get('tripulaciones', 'CrewController@index');
        Route::get('tripulaciones/{id}', 'CrewController@show');
        Route::post('tripulaciones/{id}/comprar-solicitud-abastecimiento', 'CrewController@purchaseSupplyRequest');

        // entrega de epp
        Route::get('epp', 'EPPController');
        Route::get('trabajadores/{id}/entrega-epp', 'WorkerController@getForEPPDelivery');
        Route::post('trabajadores/{id}/entrega-epp', 'WorkerController@saveEPPDelivery');
        Route::get('trabajadores/{id}/entrega-epp/{eppId}', 'WorkerController@getLastEPPDelivery');

        // listados
        Route::get('clasificaciones-articulos', 'WarehouseItemClassificationController');
        Route::get('cuentas-bancarias', 'BankAccountController');
        Route::get('unidades-medida', 'MeasurementUnitController');
    });

    // mantenimiento
    Route::group(['namespace' => 'Maintenance', 'prefix' => 'mantencion'], function () {
        Route::get('certificaciones', 'CertificateController');
        Route::post('certificaciones', 'CertificateController@store');
        Route::post('certificaciones/{id}', 'CertificateController@update');
        Route::delete('certificaciones/{id}', 'CertificateController@destroy');
    });


     // mantenimiento
     Route::group(['namespace' => 'Administration', 'prefix' => 'administracion'], function () {
        Route::get('personas', 'PersonController@index');
        Route::post('personas', 'PersonController@store');
        Route::post('personas/{id}', 'PersonController@update');
        Route::delete('personas/{id}', 'PersonController@destroy');
        Route::get('personas/{id}', 'PersonController@getPerson');
        Route::get('personas/{id}/alojamientos', 'PersonController@getAccomodations');
        Route::get('personas/{id}/traslados', 'PersonController@getTransfers');
        Route::post('personas/{id}/alojamientos', 'PersonController@setAccomodations');
        Route::post('personas/{id}/traslados', 'PersonController@setTransfers');
        Route::post('personas/nuevo/traslado', 'PersonController@setTransfersMultiple');
        Route::post('personas/nuevo/trasladoCrew', 'PersonController@setTransfersCrew');
        Route::post('personas/nuevo/alojamiento', 'PersonController@setAccommodationsMultiple');
        Route::post('personas/nuevo/alojamientoCrew', 'PersonController@setAccommodationsCrew');
        Route::post('personas/nuevo/colacionCrew', 'PersonController@setSnackCrew');
        Route::get('traslados', 'PersonController@getAllTransfers');
        Route::get('traslados-facturables', 'PersonController@getAllTransfersQuotable');
        Route::get('trasladosCrew/{id}', 'PersonController@getAllTransfersCrew');
        Route::get('find-traslado/{id}', 'PersonController@getTransfer');
        Route::get('find-alojamiento/{id}', 'PersonController@getAccommodation');
        Route::get('find-pasaje/{id}', 'PersonController@getTicket');
        Route::get('find-pasaje-external/{id}', 'PersonController@getTicketExternal');
        Route::get('alojamientos', 'PersonController@getAllAccommodations');
        Route::get('alojamientos-facturables', 'PersonController@getAllAccommodationsQuotable');
        Route::get('alojamientosCrew/{id}', 'PersonController@getAllAccommodationsCrew');
        Route::get('pasajes', 'PersonController@getAllTickets');
        Route::get('pasajes-facturables', 'PersonController@getAllTicketsQuotable');
        Route::get('vouchers', 'PersonController@getAllVouchers');
        Route::get('vouchers/detalle/{id}', 'PersonController@getVoucherDetails');
        Route::get('vouchers/vouchersof/{id}', 'PersonController@getVouchersOf');
        Route::get('vouchers/data/{id}', 'PersonController@getVoucherData');
        Route::get('pasajes/vouchers/{id}', 'PersonController@getVouchers');
        Route::get('pasajes/vouchersWithUsed/{id}/{voucher_id}/{ticket_id}', 'PersonController@getVouchersWithUsed');
        Route::get('pasajes/stocks/{id}', 'PersonController@getStocks');
        Route::get('pasajes/stocksWithUsed/{id}/{ticket_id}', 'PersonController@getStocksWithUsed');
        Route::get('stock-de-pasajes', 'PersonController@getAllTicketsInStock');
        Route::get('pasajes-perdidos', 'PersonController@getAllTicketsLost');
        Route::get('workers', 'PersonController@getWorkers');
        Route::post('personas/{id}/pasajes', 'PersonController@setTickets');
        Route::post('personas/pasajes/a-stock', 'PersonController@setStockTicket');
        Route::post('personas/pasajes/nuevo-a-stock', 'PersonController@setNewStockTicket');
        Route::post('personas/pasajes/a-voucher', 'PersonController@setVoucherTicket');
        Route::post('personas/nuevo/pasaje', 'PersonController@setTicketsMultiple');
        Route::delete('cotizaciones/traslado/{trasladoid}', 'PersonController@deleteTransfer');
        Route::delete('cotizaciones/alojamiento/{alojamientoid}', 'PersonController@deleteAccommodation');
        Route::delete('cotizaciones/colacion/{colacionid}', 'PersonController@deleteSnack');
        Route::post('pasajes/quitar-pasaje', 'PersonController@deleteWorkerTicket');
        Route::post('traslados/quitar-traslado', 'PersonController@deleteWorkerTransfer');
        Route::post('colaciones/quitar-colacion', 'PersonController@deleteWorkerSnack');
        Route::post('alojamientos/quitar-alojamiento', 'PersonController@deleteWorkerAccommodation');

        Route::get('tripulantes/pasajes/{transferid}/{quotationid}', 'PersonController@getTicketWorkers');
        Route::get('tripulantes/traslados/{transferid}/{quotationid}', 'PersonController@getTransfersWorkers');
        Route::get('tripulantes/alojamientos/{accommodation}/{quotationid}', 'PersonController@getAccommodationWorkers');
        Route::get('tripulantes/colacion/{colacionid}/{quotationid}', 'PersonController@getSnackWorkers');
        Route::post('traslados/asociar-trabajadores', 'PersonController@setTransferCrewWorker');
        Route::post('alojamientos/asociar-trabajadores', 'PersonController@setAccommodationCrewWorker');
        Route::post('colacion/asociar-trabajadores', 'PersonController@setSnackCrewWorker');
        Route::post('pasajes/asociar-trabajadores', 'PersonController@setTicketWorkersCrew');
        Route::post('pasajes/asociar-trabajador', 'PersonController@setTicketWorkerCrew');

        Route::post('pasajes/guardar-pasaje', 'PersonController@setTicketsCrew');
        Route::post('pasajes/actualiza-tripulantes', 'PersonController@setWorkersTicketData');
        Route::post('pasajes/actualiza-tripulante', 'PersonController@setWorkerTicketData');
        Route::get('facturacion/proveedores/{id}', 'PersonController@getSuppliers');
        Route::get('facturacion/cuentas-banco', 'PersonController@getBankAccounts');
        Route::get('facturacion/facturas-compra/{id}', 'PersonController@getPurchaseInvoice');
        Route::post('facturacion/nueva-factura', 'PersonController@setInvoice');
        Route::post('facturacion/nueva-factura-externa', 'PersonController@setInvoiceExternal');
        Route::post('facturacion/nueva-factura-compra', 'PersonController@setInvoicePurchase');
        Route::delete('facturacion/elimina-factura/{invoice_id}/{crew_id}', 'PersonController@deleteInvoice');

        Route::post('facturacion/externa/asociar-factura', 'PersonController@associateExternalInvoice');
        Route::post('facturacion/alojamiento/asociar-factura', 'PersonController@associateAccommodationInvoice');
        Route::post('facturacion/traslado/asociar-factura', 'PersonController@associateTransferInvoice');
        Route::post('facturacion/pasaje/asociar-factura', 'PersonController@associateTicketInvoice');
        Route::post('facturacion/maleta/asociar-factura', 'PersonController@associateWorkerCarryInvoice');
        Route::post('facturacion/multa/asociar-factura', 'PersonController@associateWorkerPenaltyInvoice');
        Route::delete('facturacion/elimina-factura-alojamiento/{accommodation_id}', 'PersonController@deleteAccommodationInvoice');
        Route::delete('facturacion/elimina-factura-traslado/{transfer_id}', 'PersonController@deleteTransferInvoice');
        Route::delete('facturacion/elimina-factura-pasaje/{ticket_id}', 'PersonController@deleteTicketInvoice');
        Route::delete('facturacion/elimina-factura-maleta/{worker_carry}', 'PersonController@deleteWorkerCarryInvoice');

        Route::post('facturacion/desasocia-factura-externa', 'PersonController@desassociateInvoiceExternal');

        Route::delete('facturacion/elimina-factura-externa/{id}/{id2}', 'PersonController@deleteExternalInvoice');

        Route::delete('facturacion/elimina-factura-multa/{worker_carry}', 'PersonController@deleteWorkerPenaltyInvoice');

        Route::post('facturacion/facturar', 'PersonController@quotateAny');

        Route::post('facturacion/compra-tripulacion/asociar-factura', 'PersonController@associateInvoiceArticle');
        Route::post('facturacion/compra-tripulacion/quitar-factura', 'PersonController@desassociateInvoiceArticle');
        Route::post('facturacion/compra-tripulacion/elimina-factura', 'PersonController@deleteInvoicePurchase');
        Route::post('facturacion/compra-tripulacion/facturado', 'PersonController@purchaseQuoted');

       
        Route::get('facturacion/facturas', 'PersonController@getInvoicesHistory');

        Route::get('facturacion/facturas-externas/{id}', 'PersonController@getInvoiceExternal');

        Route::get('factura/detalle/{id}', 'PersonController@getInvoiceDetails');

        Route::post('cotizaciones/cerrar-cotizacion', 'PersonController@closeQuotation');
        Route::post('cotizaciones/abrir-cotizacion', 'PersonController@openQuotation');
        Route::post('send-sms', 'PersonController@sendsms');

    


    });

   
    // sala de espera
    Route::get('sala-espera', 'WaitingRoomController');

    Route::get('reports/generate/{id}/{d1}/{d2}', 'ReportController@exportReport');
    Route::get('bodega/descargar-solicitud-abastecimiento/{id}', 'ReportController@exportSupplyrequest');

    Route::get('pending-attributes', 'AttributeController@pendingList');

    // calendario trip
    Route::get('calendario-trip/tripulaciones', 'TripCalendarController');
    Route::get('calendario-trip/ultimas-tripulaciones', 'TripCalendarController@lastcrews');
});

Route::apiResources([
    'usuarios' => 'API\UserController',
    'embarcaciones' => 'API\ShipController',
    'trabajadores' => 'API\WorkerController',
    'product' => 'API\V1\ProductController',
    'category' => 'API\V1\CategoryController',
    'tag' => 'API\V1\TagController',
]);
