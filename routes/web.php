<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('ajusta/{id}', 'HomeController@ajustaEmbarcados');
Route::get('descargasolicitud/{id}', 'HomeController@exportSupplyrequest');
Auth::routes(['verify' => true]);

// TODO: sacar ruta
/*
Route::get('region', function () {

    $regiones = DB::table('regiones')
        ->select('region_id as id', 'region_nombre as name', 'region_ordinal as ordinal')
        ->get();

    foreach ($regiones as $region) {
        $region->communes = DB::table('comunas')
            ->join('provincias', 'provincias.provincia_id', '=', 'comunas.provincia_id')
            ->select('comunas.comuna_nombre as name')
            ->where('provincias.region_id', $region->id)
            ->get();
    }

    return response()->json($regiones);


});
*/

Route::get('/home', 'HomeController@index')->name('home');



/*Route::get('home', function () {
    return redirect('/dashboard');
});*/

Route::get('/{vue_capture?}', function () {
    return view('home');
})->where('vue_capture', '^(?!api\/).*$')->middleware('auth');
