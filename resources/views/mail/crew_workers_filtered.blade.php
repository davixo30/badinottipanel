<html>
    <p>
        Algunos tripulantes han sido filtrados por el Supervisor. Para volver a armar la tripulación diríjase a
        <a href="{{ env('APP_URL') }}/jefe-flota/tripulaciones/{{ $crew->id }}/editar">
            este enlace
        </a>.
    </p>

    <table>
        <thead>
            <tr>
                <td>Cliente</td>
                <td>Embarcación</td>
                <td>Fecha subida</td>
                <td>Fecha bajada</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $client->name }}</td>
                <td>{{ $ship->name }}</td>
                <td>{{ $crew->upload_date }}</td>
                <td>{{ $crew->download_date }}</td>
            </tr>
        </tbody>
    </table>

    <p>Los tripulantes filtrados son:</p>

    <table>
        <thead>
            <tr>
                <td>Nombre</td>
                <td>Rut</td>
                <td>Observación</td>
                <td>Acciones</td>
            </tr>
        </thead>
        <tbody>
            @foreach($workers as $worker)
                <tr>
                    <td>{{ $worker->name }} {{ $worker->last_name }}</td>
                    <td>{{ $worker->rut }}</td>
                    <td>{{ $worker->observation }}</td>
                    <td>
                        <a href="{{ env('APP_URL') }}/admin/trabajadores/{{ $worker->id }}">Ficha tripulante</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</html>
