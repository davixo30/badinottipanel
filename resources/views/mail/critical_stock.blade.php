<html>
    <p>
       Los siquientes productos alcanzaron su stock crítico.
    </p>

    <table border="1">
        <thead>
            <tr>
                <td align="center">Nombre</td>
                <td align="center">Talla</td>
                <td align="center">Stock Actual</td>
                <td align="center">Stock Crítico</td>
            </tr>
        </thead>
        <tbody>
            @foreach($items as $item)
                <tr>
                    <td align="center">{{ $item->name}}</td>
                    <td align="center">{{ $item->size }}</td>
                    <td align="center">{{ $item->quantity }}</td>
                    <td align="center">{{ $item->critical_stock }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <p>
        Para realizar una compra diríjase al siguiente enlace <a href="{{ env('APP_URL') }}/bodega/compras/nueva">
            este enlace
        </a>.
     </p>
</html>
