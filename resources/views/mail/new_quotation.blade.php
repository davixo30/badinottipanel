<html>
    <p>
        Se ha generado una nueva cotización. Para validar o rechazar la misma diríjase a
        <a href="{{ env('APP_URL') }}/operaciones/cotizaciones/{{ $quotationId }}">
            este enlace
        </a>.
    </p>
</html>
