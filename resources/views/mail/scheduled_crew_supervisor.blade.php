<html>
    <p>
        Se ha calendarizado una nueva tripulación. Para generar una solicitud de abastecimiento diríjase a
        <a href="{{ env('APP_URL') }}/supervisor/tripulaciones/{{ $crew->id }}/solicitud-abastecimiento">
            este enlace
        </a>.
    </p>

    <table>
        <thead>
            <tr>
                <td>Cliente</td>
                <td>Embarcación</td>
                <td>Fecha subida</td>
                <td>Fecha bajada</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $client->name }}</td>
                <td>{{ $ship->name }}</td>
                <td>{{ $crew->upload_date }}</td>
                <td>{{ $crew->download_date }}</td>
            </tr>
        </tbody>
    </table>

    @include('mail/layouts/workers')
</html>
