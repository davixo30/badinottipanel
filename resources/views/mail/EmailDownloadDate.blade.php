<html>
    <p>
        La tripulación de {{$ship}} que se encuentra embarcada para el cliente {{$client}} bajará en una semana el día {{$date}}, recordamos comprar los pasajes de vuelta, presionando el siguiente
        <a href="{{ env('APP_URL') }}/logistica/cotizacion/{{ $crewid }}">
             enlace
        </a>.
    </p>
</html>
