<html>
    <p>
        Se ha validado una cotización. Para facturar la misma diríjase a
        <a href="{{ env('APP_URL') }}/logistica/cotizacion/{{ $quotationId }}/facturar">
            este enlace
        </a>.
    </p>
</html>
