<html>
    <p>
        Se ha rechazado una compra solicitada.<br>
        Motivo: {{ $reason }}.
    </p>

    <p>
        Puede generar una nueva compra en <a href="{{ env('APP_URL') }}/bodega/compras/nueva">
            este enlace
        </a>.
    </p>
</html>
