<html>
    <p>
        Se ha validado una compra. Para facturar la misma diríjase a
        <a href="{{ env('APP_URL') }}/bodega/compras/{{ $purchaseId }}/facturar">
            este enlace
        </a>.
    </p>
</html>
