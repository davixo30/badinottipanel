<p>Los siguientes trabajadores han sido incluídos en la tripulación.</p>

<table>
    <thead>
        <tr>
            <td>Nombre</td>
            <td>Rut</td>
            <td>Acciones</td>
        </tr>
    </thead>
    <tbody>
        @foreach($workers as $worker)
            <tr>
                <td>{{ $worker->name }} {{ $worker->last_name }}</td>
                <td>{{ $worker->rut }}</td>
                <td>
                    <a href="{{ env('APP_URL') }}/admin/trabajadores/{{ $worker->id }}">Ficha tripulante</a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
