<html>
    <p>
        Se ha generado una nueva compra. Para validar o rechazar la misma diríjase a
        <a href="{{ env('APP_URL') }}/operaciones/compras/{{ $purchaseId }}">
            este enlace
        </a>.
    </p>
</html>
