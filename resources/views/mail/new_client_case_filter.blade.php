<html>
    <p>
        Se ha generado una nueva tripulación. Para aplicar filtros sobre la misma diríjase a
        <a href="{{ env('APP_URL') }}/supervisor/tripulaciones/{{ $crewId }}/filtro-tripulacion">
            este enlace
        </a>.
    </p>
</html>
