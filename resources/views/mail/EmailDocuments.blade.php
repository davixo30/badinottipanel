<html>
{{$perfil}}
    <p>
       Vencimientos dentro de 1 mes, el dia {{$fecha}}.
    </p>

    @if(($perfil=="Administrador" || $perfil=="RRHH") && $seguro!="")
    <p>Seguro de Vida vence el dia {{$seguro}}</p>
    @endif

    @if($perfil=="Prevencionista")
    @if(sizeof($inducciones)>0)
    <h3>Inducciónes</h3>
        <table>
            <thead>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Rut</th>
                <th>Inducción</th>
            </thead>
            <tbody>
                @foreach($inducciones as $i)
                <tr><td>{{$i->name}}</td><td>{{$i->last_name}}</td><td>{{$i->rut}}</td><td>{{$i->induction}}</td></tr>
                @endforeach
            </tbody>
        <table>
    @endif
    @endif

    @if($perfil=="Prevencionista")
    @if(sizeof($examenes)>0)
    <h3>Examenes Ocupacionales</h3>
        <table>
            <thead>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Rut</th>
              
            </thead>
            <tbody>
                @foreach($examenes as $i)
                <tr><td>{{$i->name}}</td><td>{{$i->last_name}}</td><td>{{$i->rut}}</td></tr>
                @endforeach
            </tbody>
        <table>
    @endif
    @endif

    @if($perfil=="Sub contratación" || $perfil=="RRHH")
    @if(sizeof($documentos)>0)
    <h3>Documentos</h3>
        <table>
            <thead>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Rut</th>
                <th>Documento</th>
            </thead>
            <tbody>
                @foreach($documentos as $i)
                @if(($perfil=="Sub contratación" || $perfil=="RRHH" ) && $i->document<>"Seguro de vida")
                <tr><td>{{$i->name}}</td><td>{{$i->last_name}}</td><td>{{$i->rut}}</td><td>{{$i->document}}</td></tr>
                @endif
                @endforeach
            </tbody>
        <table>
    @endif
    @endif



    @if($perfil=="Administrador" || $perfil=="Ing. planificación y control de mantenimiento")
    @if(sizeof($carenas)>0)
    <h3>Carenas</h3>
        <table>
            <thead>
                <th>Nombre</th>
                <th>Patente</th>
            </thead>
            <tbody>
                @foreach($carenas as $i)        
                <tr><td>{{$i->name}}</td><td>{{$i->plate}}</td></tr>
                @endforeach
            </tbody>
        <table>
    @endif
    @endif


    @if($perfil=="Administrador" || $perfil=="Ing. planificación y control de mantenimiento")
    @if(sizeof($revistas)>0)
    <h3>Revistas</h3>
        <table>
            <thead>
                <th>Nombre</th>
                <th>Patente</th>
            </thead>
            <tbody>
                @foreach($revistas as $i)        
                <tr><td>{{$i->name}}</td><td>{{$i->plate}}</td></tr>
                @endforeach
            </tbody>
        <table>
    @endif
    @endif

    @if($perfil=="Administrador" || $perfil=="Bodega" || $perfil=="Abastecimiento" || $perfil=="Prevencionista" || $perfil=="Logística")
    @if(sizeof($epp)>0)
    <h3>EPP</h3>
        <table>
            <thead>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Rut</th>
                <th>EPP</th>
            </thead>
            <tbody>
                @foreach($epp as $i)        
                <tr><td>{{$i->name}}</td><td>{{$i->last_name}}</td><td>{{$i->rut}}</td><td>{{$i->item}}</td></tr>
                @endforeach
            </tbody>
        <table>
    @endif
    @endif


</html>
