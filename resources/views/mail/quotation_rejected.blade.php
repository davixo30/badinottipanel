<html>
    <p>
        Se ha rechazado una cotización solicitada.<br>
        Motivo: {{ $reason }}.
    </p>

    <p>
        Puede generar una nueva cotización en <a href="{{ env('APP_URL') }}/logistica/cotizacion/{{ $quotationId }}">
            este enlace
        </a>.
    </p>
</html>
