<html>
Estimados,
<br>
A continuación se adjunta los datos del nuevo tripulante creado:<br><br>

Nombre: {{$tripulante->name}} {{$tripulante->last_name}}<br>
Fecha inicio contrato: {{$tripulante->contract}}<br>
Plaza/Cargo: {{$tripulante->role}}<br>
Rut: {{$tripulante->rut}}<br>
Fecha de Nacimiento: {{$tripulante->birthdate}}<br>
Estado Civil: {{$tripulante->marital}}<br>
Dirección: {{$tripulante->address}}<br>
AFP: {{$tripulante->afp}}<br>
Salud: {{$tripulante->health}}<br>
Sueldo: {{$tripulante->salary}}<br>
Incluye días rojos: {{$tripulante->redday}}<br>
Fono: {{$tripulante->phone_number}}<br>
Embarcación: {{$tripulante->ship}}<br>
<br>

@if(sizeof($tripulante->epps)>0)
@php
$i = 1
@endphp
EPP Entregado: <br>
@foreach($tripulante->epps as $epp)
EPP {{$i}}: {{$epp['size']['item']['name']}} {{$epp['size']['name']}}<br>
@php
$i = $i+1
@endphp
@endforeach
@endif

<br>

Para revisar documentación (Matrícula de embarque, carnet de identidad, contrato, inducciones, entre otros) favor dirigirse al perfil del tripulante, a través del siguiente <a href="{{ env('APP_URL') }}/admin/trabajadores/{{$tripulante->id}}">
            link
        </a>.
</html>
