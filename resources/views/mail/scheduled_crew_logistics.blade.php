<html>
    <p>
        Se ha calendarizado una nueva tripulación. Para generar una cotización diríjase a
        <a href="{{ env('APP_URL') }}/logistica/cotizacion/{{ $quotation->id }}">
            este enlace
        </a>.
    </p>

    <table>
        <thead>
            <tr>
                <td>Cliente</td>
                <td>Embarcación</td>
                <td>Fecha subida</td>
                <td>Fecha bajada</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $client->name }}</td>
                <td>{{ $ship->name }}</td>
                <td>{{ $crew->upload_date }}</td>
                <td>{{ $crew->download_date }}</td>
            </tr>
        </tbody>
    </table>

    @include('mail/layouts/workers')
</html>
