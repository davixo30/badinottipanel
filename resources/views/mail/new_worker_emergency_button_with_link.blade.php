<html>
    <p>
    Se ha agregado un nuevo tripulante mediante el boton de ingreso tardío. Para generar una cotización diríjase a
        <a href="{{ env('APP_URL') }}/logistica/boton-emergencia/{{ $emergency->id }}">
            este enlace
        </a>.
    </p>

    <p>
        El motivo del ingreso es : {{$emergency->reason}}.
    </p>

    <table>
        <thead>
            <tr>
                <td>Cliente</td>
                <td>Embarcación</td>
                <td>Fecha subida</td>
                <td>Fecha bajada</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $client->name }}</td>
                <td>{{ $ship->name }}</td>
                <td>{{ $crew->upload_date }}</td>
                <td>{{ $crew->download_date }}</td>
            </tr>
        </tbody>
    </table>

    <p>El tripulante agregado es:</p>

    <table>
        <thead>
            <tr>
                <td>Nombre</td>
                <td>Rut</td>
                <td>Acciones</td>
            </tr>
        </thead>
        <tbody>

                <tr>
                    <td>{{ $workers->name }} {{ $workers->last_name }}</td>
                    <td>{{ $workers->rut }}</td>
                    <td>
                        <a href="{{ env('APP_URL') }}/admin/trabajadores/{{ $workers->id }}">Ficha tripulante</a>
                    </td>
                </tr>

        </tbody>
    </table>
</html>
