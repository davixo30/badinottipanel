<html>
    <p>
        Se ha generado una nueva solicitud de abastecimiento. Para generar una compra diríjase a
        <a href="{{ env('APP_URL') }}/bodega/solicitudes-abastecimiento/{{ $supplyRequestId }}">
            este enlace
        </a>.
        <br>
        Para descargar archivo Excel con la solicitud presione
        <a href="{{ env('APP_URL') }}/descargasolicitud/{{ $supplyRequestId }}">
            este link.
        </a>.
    </p>
</html>
