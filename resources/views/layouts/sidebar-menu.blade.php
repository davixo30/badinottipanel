<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
            <i class="fas fa-tools nav-icon white"></i>
                <p>Flota y Logistica
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                        <li class="nav-item second-level">
                            <router-link to="/jefe-flota/tripulaciones" class="nav-link">
                                        <p>
                                        <p style="margin-left:10px !important;">
                                        <i class="nav-icon fas fa-users white"></i>
                                        Crear tripulación</p>
                                        </p>
                            </router-link>
                        </li>
                        <li class="nav-item second-level">
                            <router-link to="/admin/trabajadores" class="nav-link">
                                        <p style="margin-left:10px !important;">
                                        <i class="fas fa-users-cog nav-icon white"></i>
                                        Ingreso Personal</p>
                                        </p>
                            </router-link>
                        </li>
                        <li class="nav-item second-level">
                            <router-link to="/calendario-trip" class="nav-link">
                                        <p style="margin-left:10px !important;">
                                        <i class="fa fa-calendar-alt nav-icon white"></i>
                                        Calendario Trip</p>
                                        </p>
                            </router-link>
                        </li>
                        <li class="nav-item second-level">
                                <router-link to="/sala-espera" class="nav-link">
                                    <p style="margin-left:10px !important;">
                                        <i class="fa fa-male nav-icon white"></i>
                                        Sala de espera</p>
                                        </p>
                                </router-link>
                        </li>
                        <li class="nav-item second-level">
                            <router-link to="/reports" class="nav-link">
                                <p style="margin-left:10px !important;">
                                        <i class="fa fa-file-alt nav-icon white"></i>
                                        Informes</p>
                                </p>
                            </router-link>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <p>
                                <p style="margin-left:10px;"><i class="fas fa-briefcase nav-icon white"></i>Logistica</p>
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item has-treeview">
                                    <a href="#" class="nav-link">
                                        <p>
                                        <p style="margin-left:20px !important;"><i class="fas fa-ticket-alt nav-icon white"></i>Adm. Pasajes</p>
                                            <i class="right fas fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview" style="margin-left:50px !important;">
                                    <li class="nav-item">
                                            <router-link to="/vouchers" class="nav-link">
                                                <p>Vouchers</p>
                                            </router-link>
                                        </li>
                                        <li class="nav-item">
                                            <router-link to="/stock-de-pasajes" class="nav-link">
                                                <p>Stock de Pasajes</p>
                                            </router-link>
                                        </li>
                                        <li class="nav-item">
                                            <router-link to="/pasajes-perdidos" class="nav-link">
                                                <p>Pasajes Perdidos</p>
                                            </router-link>
                                        </li>
                                        <li class="nav-item">
                                            <router-link to="/pasajes" class="nav-link">
                                                <p>Pasajes</p>
                                            </router-link>
                                        </li>
                                        <li class="nav-item">
                                            <router-link to="/alojamientos" class="nav-link">
                                                <p>Alojamiento</p>
                                            </router-link>
                                        </li>
                                        <li class="nav-item">
                                            <router-link to="/traslados" class="nav-link">
                                                <p>Traslado</p>
                                            </router-link>
                                        </li>
                                        <li class="nav-item">
                                            <router-link to="/personas" class="nav-link">
                                                <p>Personal</p>
                                            </router-link>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                <a href="#" class="nav-link">
                                        <p>
                                        <p style="margin-left:20px !important;"><i class="fas fa-usd nav-icon white"></i>Fact. Pasajes</p>
                                            <i class="right fas fa-angle-left"></i>
                                        </p>
                                </a>
                                <ul class="nav nav-treeview" style="margin-left:50px !important;">
                                    <li class="nav-item">
                                            <router-link to="/facturacion/tripulaciones" class="nav-link">
                                                <p>Cotización</p>
                                            </router-link>
                                        </li>
                                        <li class="nav-item">
                                            <router-link to="/facturacion/pasajes" class="nav-link">
                                                <p>Pasajes</p>
                                            </router-link>
                                        </li>
                                        <li class="nav-item">
                                            <router-link to="/facturacion/alojamientos" class="nav-link">
                                                <p>Alojamiento</p>
                                            </router-link>
                                        </li>
                                        <li class="nav-item">
                                            <router-link to="/facturacion/traslados" class="nav-link">
                                                <p>Traslado</p>
                                            </router-link>
                                        </li>

                                        <li class="nav-item">
                                            <router-link to="/facturas" class="nav-link">
                                                <p>Historial de Facturas</p>
                                            </router-link>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <p>
                                        <p style="margin-left:20px !important;"><i class="fas fa-users nav-icon white"></i>Tripulacion</p>
                                            <i class="right fas fa-angle-left"></i>
                                        </p>
                                </a>
                                <ul class="nav nav-treeview" style="margin-left:50px !important;">
                                    <li class="nav-item">
                                        <router-link to="/logistica/tripulaciones" class="nav-link">
                                            <p>
                                                Crear Cotización
                                            </p>
                                        </router-link>
                                    </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
            </ul>
        </li>

        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-cogs white"></i>
                <p>
                    Operaciones
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview second-level">
                <li class="nav-item">
                    <router-link to="/admin/embarcaciones" class="nav-link">
                        <p style="margin-left:10px !important;">
                                        <i class="fa fa-ship nav-icon white"></i>
                                        Embarcaciones</p>
                        </p>
                    </router-link>
                </li>
                <li class="nav-item">
                    <router-link to="/mantenimiento/certificaciones" class="nav-link">
                        <p style="margin-left:10px !important;">
                        <i class="nav-icon fas fa-book white"></i>
                                        Cert. Embarcaciones</p>
                        </p>
                    </router-link>
                </li>
            </ul>
        </li>

        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-list white"></i>
                <p>
                    Salud y Seguridad
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview second-level">
                <li class="nav-item">
                    <router-link to="/prevencionista/inducciones" class="nav-link">
                        <p style="margin-left:10px !important;">
                        <i class="fa fa-graduation-cap nav-icon white"></i>
                        Inducciones</p>
                        </p>
                    </router-link>
                </li>
                <li class="nav-item">
                            <router-link to="/admin/salud/trabajadores" class="nav-link">
                                <p style="margin-left:10px !important;">
                                <i class="fas fa fa-medkit nav-icon white"></i>
                                    Salud Tripulantes</p>
                                    </p>
                            </router-link>
                </li>
            </ul>
        </li>


        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-cog white"></i>
                <p>
                    Configuración
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview second-level">
                <li class="nav-item">
                    <router-link to="/admin/usuarios" class="nav-link">
                        <p style="margin-left:10px !important;">
                        <i class="fas fa-user-tie nav-icon white"></i>
                                Usuarios</p>
                                    </p>
                    </router-link>
                    <router-link to="/documents" class="nav-link">
                        <p style="margin-left:10px !important;">
                        <i class="fas fa-file-import nav-icon white"></i>
                                Documentos Empresa</p>
                                </p>
                    </router-link>
                </li>

                <!-- <li class="nav-item">
                    <router-link to="/supervisor/tripulaciones" class="nav-link">
                        <i class="nav-icon fas fa-user-check white"></i>
                        <p>
                            Supervisor
                        </p>
                    </router-link>
                </li> -->

                <!-- <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user-lock white"></i>
                        <p>
                            Operaciones
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <router-link to="/operaciones/compras" class="nav-link">
                                <i class="fa fa-dollar-sign nav-icon white"></i>
                                <p>
                                    Compras Bodega
                                </p>
                            </router-link>
                            <router-link to="/operaciones/compras-tripulacion" class="nav-link">
                                <i class="fa fa-dollar-sign nav-icon white"></i>
                                <p>
                                    Compras Tripulación
                                </p>
                            </router-link>
                        </li>
                    </ul>
                </li> -->


                <!-- <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="fas fa-truck-loading nav-icon white"></i>
                        <p>
                            Abastecimiento
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <router-link to="/bodega/compras" class="nav-link">
                                <i class="fa fa-dollar-sign nav-icon white"></i>
                                <p>Compras Bodega</p>
                            </router-link>
                        </li>

                        <li class="nav-item">
                            <router-link to="/bodega/compras-tripulacion" class="nav-link">
                                <i class="fa fa-dollar-sign nav-icon white"></i>
                                <p>Compras Tripulacion</p>
                            </router-link>
                        </li>

                        <li class="nav-item">
                            <router-link to="/bodega/solicitudes-abastecimiento" class="nav-link">
                                <i class="fa fa-file-alt nav-icon white"></i>
                                <p>Sol. abastecimiento</p>
                            </router-link>
                        </li>
                        <li class="nav-item">
                            <router-link to="/bodega/articulos" class="nav-link">
                                <i class="fas fa-boxes nav-icon white"></i>
                                <p>Bodega</p>
                            </router-link>
                        </li>
                    </ul>
                </li> -->

                <!-- <li class="nav-item">
                    <router-link to="/documents" class="nav-link">
                        <i class="fas fa-file-import nav-icon white"></i>
                        <p>Documentos Empresa</p>
                    </router-link>
                </li> -->

                <!-- <li class="nav-item">
                    <router-link to="/pending-attributes" class="nav-link">
                        <i class="fas fa-user-clock nav-icon white"></i>
                        <p>Atributos Pendientes</p>
                    </router-link>
                </li> -->

            </ul>
        </li>


        <li class="nav-item">
            <a href="https://app.eskuad.com" target="_blank" class="nav-link">
            <i class="fa fa-window-restore nav-icon white"></i>
                <p>Web Eskuad</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="#" class="nav-link" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-power-off red"></i>
                <p>
                    Cerrar sesión
                </p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</nav>
